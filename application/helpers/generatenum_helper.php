<?php	
	function generateSONo($txtsalesorderdate)
	{		
		$rs = getByQuery("SELECT `salesorder_no` FROM `salesordertable` WHERE YEAR(CURDATE()) = YEAR(salesorder_date) ORDER BY `salesorder_date` DESC, `salesorder_no` DESC LIMIT 1");
		
		if($rs)
		{
			$rs_salesorder_no = (int)$rs[0]->salesorder_no + 1;
		}
		else
		{
			$rs_salesorder_no = 1;		
		}
		
		if(strlen($rs_salesorder_no) == 4)
		{
			$salesorder_no = "" . (string)$rs_salesorder_no;
		}
		if(strlen($rs_salesorder_no) == 3)
		{
			$salesorder_no = "0" . (string)$rs_salesorder_no;
		}
		if(strlen($rs_salesorder_no) == 2)
		{
			$salesorder_no = "00" . (string)$rs_salesorder_no;
		}
		if(strlen($rs_salesorder_no) == 1)
		{
			$salesorder_no = "000" . (string)$rs_salesorder_no;
		}
				
        $last_so_number = $salesorder_no;
		$year_so_number = date('Y', strtotime($txtsalesorderdate));
		$month_of_date = date('m', strtotime($txtsalesorderdate));
		switch($month_of_date) {
			case '01':
				$month_so_number = 'I';
				break;
			case '02':
				$month_so_number = 'II';
				break;
			case '03':
				$month_so_number = 'III';
				break;
			case '04':
				$month_so_number = 'IV';
				break;
			case '05':
				$month_so_number = 'V';
				break;
			case '06':
				$month_so_number = 'VI';
				break;
			case '07':
				$month_so_number = 'VII';
				break;
			case '08':
				$month_so_number = 'VIII';
				break;
			case '09':
				$month_so_number = 'IX';
				break;
			case '10':
				$month_so_number = 'X';
			case '11':
				$month_so_number = 'XI';
				break;
			case '12':
				$month_so_number = 'XII';
				break;
		}
        $so_number = $last_so_number.'/SO/'.$month_so_number.'/'.$year_so_number;  
        return $so_number;    
    }

    function generateSiteIdAPI($argTowerTypeId, $argAreaId, $argRegionId, $argCityId)
	{		
        $rs_towertypetable = getByQuery("select short_txt from towertypetable where row_id = " . $argTowerTypeId . "");
        $rs_areatable = getByQuery("select short_txt from areatable where row_id = " . $argAreaId . "");
        $rs_regiontable = getByQuery("select short_txt from regiontable where row_id = " . $argRegionId . "");
        $rs_citytable = getByQuery("select short_txt from citytable where row_id = " . $argCityId . "");
		
		
		$rs = getByQuery("
								SELECT
									CASE  
									   WHEN LENGTH(IFNULL(REPLACE(SUBSTRING(site_id_api,9,3),'0',''),'0')) = 1 THEN CONCAT('00', IFNULL(REPLACE(MAX(SUBSTRING(site_id_api,9,3)),'0',''),'0')+'1')
									   WHEN LENGTH(IFNULL(REPLACE(SUBSTRING(site_id_api,9,3),'0',''),'0')) = 2 THEN CONCAT('0',  IFNULL(REPLACE(MAX(SUBSTRING(site_id_api,9,3)),'0',''),'0')+'1')
									   WHEN LENGTH(IFNULL(REPLACE(SUBSTRING(site_id_api,9,3),'0',''),'0')) = 3 THEN CONCAT('',   IFNULL(REPLACE(MAX(SUBSTRING(site_id_api,9,3)),'0',''),'0')+'1')
									END AS ordering_number 
								
								FROM
									salesorderlinetable 
								WHERE 
									SUBSTRING(site_id_api,1,1) = '".$rs_towertypetable[0]->short_txt."' AND
									SUBSTRING(site_id_api,2,1) = '".$rs_areatable[0]->short_txt."' AND
									SUBSTRING(site_id_api,3,1) = '".$rs_regiontable[0]->short_txt."' AND
									SUBSTRING(site_id_api,5,3) = '".$rs_citytable[0]->short_txt."'
								");			
		
		$site_id_api = $rs_towertypetable[0]->short_txt . $rs_areatable[0]->short_txt . $rs_regiontable[0]->short_txt . '-' .  $rs_citytable[0]->short_txt . '-' .  $rs[0]->ordering_number;  
		return $site_id_api;    
		
   }

    function generateWONo($txtworkorderdate)
	{
		$rs = getByQuery("SELECT `workorder_no` FROM `workordertable` WHERE YEAR(CURDATE()) = YEAR(workorder_date) ORDER BY `workorder_date` DESC, `workorder_no` DESC LIMIT 1");
		
		if($rs)
		{
			$rs_workorder_no = (int)$rs[0]->workorder_no + 1;
		}
		else
		{
			$rs_workorder_no = 1;		
		}
		
		if(strlen($rs_workorder_no) == 4)
		{
			$workorder_no = "" . (string)$rs_workorder_no;
		}
		if(strlen($rs_workorder_no) == 3)
		{
			$workorder_no = "0" .(string)$rs_workorder_no;
		}
		if(strlen($rs_workorder_no) == 2)
		{
			$workorder_no = "00" . (string)$rs_workorder_no;
		}
		if(strlen($rs_workorder_no) == 1)
		{
			$workorder_no = "000" . (string)$rs_workorder_no;
		}
						
        $last_wo_number = $workorder_no;
		$year_wo_number = date('Y', strtotime($txtworkorderdate));
		$month_of_date = date('m', strtotime($txtworkorderdate));
		switch($month_of_date) {
			case '01':
				$month_wo_number = 'I';
				break;
			case '02':
				$month_wo_number = 'II';
				break;
			case '03':
				$month_wo_number = 'III';
				break;
			case '04':
				$month_wo_number = 'IV';
				break;
			case '05':
				$month_wo_number = 'V';
				break;
			case '06':
				$month_wo_number = 'VI';
				break;
			case '07':
				$month_wo_number = 'VII';
				break;
			case '08':
				$month_wo_number = 'VIII';
				break;
			case '09':
				$month_wo_number = 'IX';
				break;
			case '10':
				$month_wo_number = 'X';
			case '11':
				$month_wo_number = 'XI';
				break;
			case '12':
				$month_wo_number = 'XII';
				break;
		}
        $wo_number = $last_wo_number.'/WO/'.$month_wo_number.'/'.$year_wo_number; 		
		
        return $wo_number;    
    }

    function generatePRNo($year) {
        $ci =& get_instance();
        $rs = getByQuery("select ifnull(max(row_id),0) as last_id from salesordertable");
        $last_id = $rs[0]->last_id + 1;
        $id = "SO".right($year, 2).right('000000'.$last_id, 6);  
        return $id;    
    }

    function generatePONo($year) {
        $ci =& get_instance();
        $rs = getByQuery("select ifnull(max(row_id),0) as last_id from salesordertable");
        $last_id = $rs[0]->last_id + 1;
        $id = "SO".right($year, 2).right('000000'.$last_id, 6);  
        return $id;    
    }
?>