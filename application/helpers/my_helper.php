<?php	
    function my_menu($menu_id_list, $menu_id)
	{
		$result = FALSE;
		
		foreach($menu_id_list as $row)
		{
			if($row->menu_id == $menu_id)
			{
				$result = TRUE;
				break;
			}
			unset($row);
		}
		
        return $result;
		unset($result);		
		unset($menu_id_list);
    }
	
	/* SALES ORDER APPROVAL FLOW
	 * ----------------------------------------------------
	 */
    function my_is_sales_order_no_in_approval_state($salesorder_no)
	{
		$CI =& get_instance();	
		$CI->load->model("Model_table_sales_order_approval");		
		$where[] = array('key' => 'salesorder_no', 'value' => $salesorder_no);
		$where[] = array('key' => 'status_id', 'value' => 1);
		if(is_array($CI->Model_table_sales_order_approval->get_rows($where)))
		{
			return TRUE;
		}
		else 
		{
			return FALSE;		   
		}			 
	}
	
    function my_sales_order_approval_flow_get_begining_state()
	{
		$CI =& get_instance();	
		$CI->load->model("Model_table_sales_order_approval_flow");
		return $CI->Model_table_sales_order_approval_flow->get_begining_state();	 
	}

    function my_sales_order_approval_flow_get_ending_state()
	{
		$CI =& get_instance();	
		$CI->load->model("Model_table_sales_order_approval_flow");
		return $CI->Model_table_sales_order_approval_flow->get_ending_state();	 
	}
	
    function my_sales_order_approval_result($salesorder_no)
	{
		$CI =& get_instance();	
		$CI->load->model("Model_table_work_flow_milestone");
		$CI->load->model("Model_table_sales_order");
		$CI->load->model("Model_table_sales_order_approval");
		$CI->load->model("Model_table_sales_order_approval_flow");
				
		$sales_order_approval_flow_current_state_id = $CI->Model_table_sales_order_approval->get_current_state($salesorder_no);		
		$sales_order_approval_flow_ending_state_id = $CI->Model_table_sales_order_approval_flow->get_ending_state()[0]->ordering;
		
		$sales_order_work_flow_current_state_id = $CI->Model_table_sales_order->get_work_flow_milestone_id_basedon_salesorder_no($salesorder_no)[0]->work_flow_milestone_id;
		$sales_order_work_flow_ending_state_id = $CI->Model_table_work_flow_milestone->get_ending_state()[0]->id;
		
		if(($sales_order_work_flow_current_state_id == $sales_order_work_flow_ending_state_id) && ($sales_order_approval_flow_current_state_id == $sales_order_approval_flow_ending_state_id))
		{
			return 'Done';	 
		}
		elseif(($sales_order_work_flow_current_state_id == $sales_order_work_flow_ending_state_id) && ($sales_order_approval_flow_current_state_id != $sales_order_approval_flow_ending_state_id))
		{
			return 'Reject';	 
		}
		else 
		{
			return 'In-progress';	 
		}				
	}

    function my_sales_order_approval_current_role($salesorder_no)
	{
		$CI =& get_instance();
		$CI->load->model("Model_table_work_flow_milestone");
		$CI->load->model("Model_table_sales_order");
		$CI->load->model("Model_table_sales_order_approval");
		$CI->load->model("Model_table_sales_order_approval_flow");
		$CI->load->model("Model_table_role");
		
		$where[] = array('key' => 'salesorder_no', 'value' => $salesorder_no);
		$salesordertable_work_flow_milestone_id = $CI->Model_table_sales_order->get_rows($where)[0]->work_flow_milestone_id;
		unset($where);
		
		if($salesordertable_work_flow_milestone_id != $CI->Model_table_work_flow_milestone->get_ending_state()[0]->id)
		{
			unset($salesordertable_work_flow_milestone_id);
			$sales_order_approval_flow_next_state_id = $CI->Model_table_sales_order_approval->get_current_state($salesorder_no);
			$sales_order_approval_flow_next_role_id = $CI->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($sales_order_approval_flow_next_state_id)[0]->role_id;
			unset($sales_order_approval_flow_next_state_id);
			$sales_order_approval_flow_next_role_name = $CI->Model_table_role->get_txt_basedon_row_id($sales_order_approval_flow_next_role_id)[0]->txt;	
			unset($sales_order_approval_flow_next_role_id);

			return($sales_order_approval_flow_next_role_name);		
			unset($sales_order_approval_flow_next_role_name);
		}
		else
		{
			return '---No Pending Approval---';		
		
		}
	}
	
    function my_sales_order_approval_current_user($salesorder_no)
	{
		$return = '';
		
		$CI =& get_instance();
		$CI->load->model("Model_table_work_flow_milestone");
		$CI->load->model("Model_table_sales_order");
		$CI->load->model("Model_table_sales_order_line");
		$CI->load->model("Model_table_sales_order_approval");
		$CI->load->model("Model_table_sales_order_approval_flow");
		$CI->load->model("Model_table_role");
		$CI->load->model("Model_table_user");
		$CI->load->model("Model_table_user_territory");
					
		if($CI->Model_table_sales_order->get_work_flow_milestone_id_basedon_salesorder_no($salesorder_no)[0]->work_flow_milestone_id != $CI->Model_table_work_flow_milestone->get_ending_state()[0]->id)
		{
			/** SET area id as of sales order
			 * check salesorder_no not exist in $salesorderlinetable
			 * if true area id = 0
			 * if false area id = salesorder line area id
			 */
			 
			if(!$CI->Model_table_sales_order_line->isSalesOrderExist($salesorder_no))
			{
				$salesorderlinetable_area_id = 0;
			}
			else
			{
				$salesorderlinetable_area_id = $CI->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($salesorder_no)[0]->area_id;
			}			
			
			/**
			 * $usertable_row_id
			 * if only one row
			 * if more than one row
			 *    if row id cek to user territory
			 */
			$usertable = $CI->Model_table_user->get_row_id_basedon_role_id($CI->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($CI->Model_table_sales_order_approval->get_current_state($salesorder_no))[0]->role_id);			

			foreach($usertable as $usertable_row)
			{
				$userterritorytable = $CI->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
				
				if(is_array($userterritorytable))
				{
					foreach($userterritorytable as $userterritorytable_row)
					{
						if($userterritorytable_row->territory_id == $salesorderlinetable_area_id)
						{
							$return = $CI->Model_table_user->get_name_basedon_row_id($usertable_row->row_id)[0]->name;
							break 2;
						}
						unset($userterritorytable_row);
					}
				}
				unset($usertable_row);
			}
			
			if($return == '')
			{
				$return = $CI->Model_table_role->get_txt_basedon_row_id($CI->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($CI->Model_table_sales_order_approval->get_current_state($salesorder_no))[0]->role_id)[0]->txt;					
			}
		}
		else
		{
			$return = '---No Pending Approval---';			
		}
		return $return;
	}
	
	
	
	/* WORK ORDER APPROVAL FLOW
	 * ----------------------------------------------------
	 */
    function my_is_work_order_no_in_approval_state($workorder_no)
	{
		$CI =& get_instance();	
		$CI->load->model("Model_table_work_order_approval");		
		$where[] = array('key' => 'workorder_no', 'value' => $workorder_no);
		$where[] = array('key' => 'status_id', 'value' => 1);
		if(is_array($CI->Model_table_work_order_approval->get_rows($where)))
		{
			return TRUE;
		}
		else 
		{
			return FALSE;		   
		}			 
	}
	
    function my_work_order_approval_flow_get_begining_state()
	{
		$CI =& get_instance();	
		$CI->load->model("Model_table_work_order_approval_flow");
		return $CI->Model_table_work_order_approval_flow->get_begining_state();	 
	}

    function my_work_order_approval_flow_get_ending_state()
	{
		$CI =& get_instance();	
		$CI->load->model("Model_table_work_order_approval_flow");
		return $CI->Model_table_work_order_approval_flow->get_ending_state();	 
	}
	
    function my_work_order_approval_result($workorder_no)
	{
		$CI =& get_instance();	
		$CI->load->model("Model_table_work_flow_milestone");
		$CI->load->model("Model_table_work_order");
		$CI->load->model("Model_table_work_order_approval");
		$CI->load->model("Model_table_work_order_approval_flow");
				
		$work_order_approval_flow_current_state_id = $CI->Model_table_work_order_approval->get_current_state($workorder_no);		
		$work_order_approval_flow_ending_state_id = $CI->Model_table_work_order_approval_flow->get_ending_state()[0]->ordering;
		
		$work_order_work_flow_current_state_id = $CI->Model_table_work_order->get_work_flow_milestone_id_basedon_workorder_no($workorder_no)[0]->work_flow_milestone_id;
		$work_order_work_flow_ending_state_id = $CI->Model_table_work_flow_milestone->get_ending_state()[0]->id;
		
		if(($work_order_work_flow_current_state_id == $work_order_work_flow_ending_state_id) && ($work_order_approval_flow_current_state_id == $work_order_approval_flow_ending_state_id))
		{
			return 'Done';	 
		}
		elseif(($work_order_work_flow_current_state_id == $work_order_work_flow_ending_state_id) && ($work_order_approval_flow_current_state_id != $work_order_approval_flow_ending_state_id))
		{
			return 'Reject';	 
		}
		else 
		{
			return 'In-progress';	 
		}				
	}
	
    function my_work_order_approval_current_role($workorder_no)
	{
		$CI =& get_instance();
		$CI->load->model("Model_table_work_flow_milestone");
		$CI->load->model("Model_table_work_order");
		$CI->load->model("Model_table_work_order_approval");
		$CI->load->model("Model_table_work_order_approval_flow");
		$CI->load->model("Model_table_role");
		
		$where[] = array('key' => 'workorder_no', 'value' => $workorder_no);
		$workordertable_work_flow_milestone_id = $CI->Model_table_work_order->get_rows($where)[0]->work_flow_milestone_id;		
		
		if($workordertable_work_flow_milestone_id != $CI->Model_table_work_flow_milestone->get_ending_state()[0]->id)
		{
			unset($workordertable_work_flow_milestone_id);
			$work_order_approval_flow_next_state_id = $CI->Model_table_work_order_approval->get_current_state($workorder_no);
			$work_order_approval_flow_next_role_id = $CI->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($work_order_approval_flow_next_state_id)[0]->role_id;
			unset($work_order_approval_flow_next_state_id);
			$work_order_approval_flow_next_role_name = $CI->Model_table_role->get_txt_basedon_row_id($work_order_approval_flow_next_role_id)[0]->txt;	
			unset($work_order_approval_flow_next_role_id);
			
			return($work_order_approval_flow_next_role_name);		
			unset($work_order_approval_flow_next_role_name);
		}
		else
		{
			return '---No Pending Approval---';		
		
		}
	}
	
    function my_work_order_approval_current_user($workorder_no)
	{
		//$workorder_no = "0001/WO/XII/2017";
		$return = '';
		
		$CI =& get_instance();
		$CI->load->model("Model_table_site");
		$CI->load->model("Model_table_work_flow_milestone");
		$CI->load->model("Model_table_work_order");
		$CI->load->model("Model_table_work_order_line");
		$CI->load->model("Model_table_work_order_approval");
		$CI->load->model("Model_table_work_order_approval_flow");
		$CI->load->model("Model_table_role");
		$CI->load->model("Model_table_user");
		$CI->load->model("Model_table_user_territory");
					
		if($CI->Model_table_work_order->get_work_flow_milestone_id_basedon_workorder_no($workorder_no)[0]->work_flow_milestone_id != $CI->Model_table_work_flow_milestone->get_ending_state()[0]->id)
		{
			/** SET area id as of work order
			 * check workorder_no not exist in $workorderlinetable
			 * if true area id = 0
			 * if false area id = workorder line area id
			 */
			 
			if(!$CI->Model_table_work_order_line->isWorkOrderExist($workorder_no))
			{
				$workorderlinetable_area_id = 0;
			}
			else
			{
				$workorderlinetable_area_id = $CI->Model_table_site->get_area_id_basedon_row_id($CI->Model_table_work_order_line->get_site_id_basedon_workorder_no($workorder_no)[0]->site_id)[0]->area_id;
			}			
			
			/**
			 * $usertable_row_id
			 * if only one row
			 * if more than one row
			 *    if row id cek to user territory
			 */
			$usertable = $CI->Model_table_user->get_row_id_basedon_role_id($CI->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($CI->Model_table_work_order_approval->get_current_state($workorder_no))[0]->role_id);			

			foreach($usertable as $usertable_row)
			{
				$userterritorytable = $CI->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
				
				if(is_array($userterritorytable))
				{
					foreach($userterritorytable as $userterritorytable_row)
					{
						if($userterritorytable_row->territory_id == $workorderlinetable_area_id)
						{
							$return = $CI->Model_table_user->get_name_basedon_row_id($usertable_row->row_id)[0]->name;
							break 2;
						}
						unset($userterritorytable_row);
					}
				}
				unset($usertable_row);				
			}
			
			if($return == '')
			{
				$return = $CI->Model_table_role->get_txt_basedon_row_id($CI->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($CI->Model_table_work_order_approval->get_current_state($workorder_no))[0]->role_id)[0]->txt;					
			}		
		}
		else
		{
			$return = '---No Pending Approval---';			
		}
		return $return;
	}  
	
	
	
	/* PENDING APPROVAL
	 * ----------------------------------------------------
	 */
    //function my_get_pending_approval_list($user_row_id)
	function my_get_all_pending_approval_list()
	{
		$CI =& get_instance();
		$CI->load->model("Model_table_user");
		$CI->load->model("Model_table_user_territory");
		$CI->load->model("Model_table_site");
		$CI->load->model("Model_table_tenant");
		$CI->load->model("Model_table_vendor");
		
		$CI->load->model("Model_table_sales_order");
		$CI->load->model("Model_table_sales_order_line");
		$CI->load->model("Model_table_sales_order_approval");
		$CI->load->model("Model_table_sales_order_approval_flow");
		
		$CI->load->model("Model_table_work_order");
		$CI->load->model("Model_table_work_order_line");
		$CI->load->model("Model_table_work_order_approval");
		$CI->load->model("Model_table_work_order_approval_flow");
		
		/** Sales Order Pending Approval
		 *
		 */		
		$where[] = array('key' => 'status_id', 'value' => 1);
		$where[] = array('key' => 'work_flow_milestone_id', 'value' => 2);
		$salesordertable = $CI->Model_table_sales_order->get_rows($where);
		unset($where);
		
		$salesorderview = array();
		
		if(is_array($salesordertable))
		{
			foreach($salesordertable as $salesordertable_row)
			{
				$current_state = $CI->Model_table_sales_order_approval->get_current_state($salesordertable_row->salesorder_no);
				$current_role_id = $CI->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($current_state)[0]->role_id;
				$salesorder_area_id = $CI->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($salesordertable_row->salesorder_no)[0]->area_id;
							
				$where[] = array('key' => 'status_id', 'value' => 1);
				$where[] = array('key' => 'role_id', 'value' => $current_role_id);
				$usertable = $CI->Model_table_user->get_rows($where);
				unset($where);
				
				if(is_array($usertable))
				{
					if(sizeof($usertable) == 1)
					{
						$userterritorytable = $CI->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable[0]->row_id);
						
						if(is_array($userterritorytable)) 
						{
							foreach($userterritorytable as $userterritorytable_row)
							{
								if($salesorder_area_id == $userterritorytable_row->territory_id)
								{
									$salesorderview[] = (object) array(
																		'main_controller' => 'sales_order',
																		'document_row_id' => $salesordertable_row->row_id,
																		'document_date' => $salesordertable_row->salesorder_date != '0000-00-00' ? date("d-m-Y", strtotime($salesordertable_row->salesorder_date)) : '-',
																		'document_no' => $salesordertable_row->salesorder_no,
																		'customer' => $CI->Model_table_tenant->get_name_basedon_row_id($salesordertable_row->tenant_id)[0]->name,
																		'number_of_line' => sizeof($CI->Model_table_sales_order_line->get_row_id_basedon_salesorder_no($salesordertable_row->salesorder_no)),
																		'current_state' => $current_state,														
																		'area_id' => $salesorder_area_id,														
																		'current_user_row_id' => $usertable[0]->row_id,
																		'current_user_territory_id' => $userterritorytable_row->territory_id,
																		'current_role_id' => $usertable[0]->role_id,															
																	);
								}
							}
						}
						else
						{
							$salesorderview[] = (object) array(
																'main_controller' => 'sales_order',
																'document_row_id' => $salesordertable_row->row_id,
																'document_date' => $salesordertable_row->salesorder_date != '0000-00-00' ? date("d-m-Y", strtotime($salesordertable_row->salesorder_date)) : '-',
																'document_no' => $salesordertable_row->salesorder_no,
																'customer' => $CI->Model_table_tenant->get_name_basedon_row_id($salesordertable_row->tenant_id)[0]->name,
																'number_of_line' => sizeof($CI->Model_table_sales_order_line->get_row_id_basedon_salesorder_no($salesordertable_row->salesorder_no)),
																'current_state' => $current_state,														
																'area_id' => $salesorder_area_id,														
																'current_user_row_id' => $usertable[0]->row_id,
																'current_user_territory_id' => 0,
																'current_role_id' => $usertable[0]->role_id,															
															);
						}
						unset($userterritorytable);
					}
					
					if(sizeof($usertable) > 1)
					{
						foreach($usertable as $usertable_row)
						{
							$userterritorytable = $CI->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
							
							
							if(is_array($userterritorytable))
							{
								foreach($userterritorytable as $userterritorytable_row)
								{
									if($salesorder_area_id == $userterritorytable_row->territory_id)
									{
										$salesorderview[] = (object) array(
																			'main_controller' => 'sales_order',
																			'document_row_id' => $salesordertable_row->row_id,
																			'document_date' => $salesordertable_row->salesorder_date != '0000-00-00' ? date("d-m-Y", strtotime($salesordertable_row->salesorder_date)) : '-',
																			'document_no' => $salesordertable_row->salesorder_no,
																			'customer' => $CI->Model_table_tenant->get_name_basedon_row_id($salesordertable_row->tenant_id)[0]->name,
																			'number_of_line' => sizeof($CI->Model_table_sales_order_line->get_row_id_basedon_salesorder_no($salesordertable_row->salesorder_no)),
																			'current_state' => $current_state,														
																			'area_id' => $salesorder_area_id,														
																			'current_user_row_id' => $usertable_row->row_id,
																			'current_user_territory_id' => $userterritorytable_row->territory_id,
																			'current_role_id' => $usertable_row->role_id,															
																		);
									}
									unset($userterritorytable_row);
								}							
							}
							else
							{
								$salesorderview[] = (object) array(
																	'main_controller' => 'sales_order',
																	'document_row_id' => $salesordertable_row->row_id,
																	'document_date' => $salesordertable_row->salesorder_date != '0000-00-00' ? date("d-m-Y", strtotime($salesordertable_row->salesorder_date)) : '-',
																	'document_no' => $salesordertable_row->salesorder_no,
																	'customer' => $CI->Model_table_tenant->get_name_basedon_row_id($salesordertable_row->tenant_id)[0]->name,
																	'number_of_line' => sizeof($CI->Model_table_sales_order_line->get_row_id_basedon_salesorder_no($salesordertable_row->salesorder_no)),
																	'current_state' => $current_state,														
																	'area_id' => $salesorder_area_id,														
																	'current_user_row_id' => $usertable_row->row_id,
																	'current_user_territory_id' => 0,
																	'current_role_id' => $usertable_row->role_id,															
																);
							}
							unset($userterritorytable);
							unset($usertable_row);
							
						}
					}			
				}
				unset($salesordertable);			
			}
		}
				
		
		/** Work Order Pending Approval
		 *
		 */
		$where[] = array('key' => 'status_id', 'value' => 1);
		$where[] = array('key' => 'work_flow_milestone_id', 'value' => 2);
		$workordertable = $CI->Model_table_work_order->get_rows($where);
		unset($where);
		
		$workorderview = array();
		
		if(is_array($workordertable))
		{
			foreach($workordertable as $workordertable_row)
			{
				$current_state = $CI->Model_table_work_order_approval->get_current_state($workordertable_row->workorder_no);
				$current_role_id = $CI->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($current_state)[0]->role_id;
				
				$workorder_area_id = $CI->Model_table_site->get_area_id_basedon_row_id($CI->Model_table_work_order_line->get_site_id_basedon_workorder_no($workordertable_row->workorder_no)[0]->site_id)[0]->area_id;
							
				$where[] = array('key' => 'status_id', 'value' => 1);
				$where[] = array('key' => 'role_id', 'value' => $current_role_id);
				$usertable = $CI->Model_table_user->get_rows($where);
				unset($where);
				
				if(is_array($usertable))
				{
					if(sizeof($usertable) == 1)
					{
						$userterritorytable = $CI->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable[0]->row_id);
						
						if(is_array($userterritorytable)) 
						{
							foreach($userterritorytable as $userterritorytable_row)
							{
								if($workorder_area_id == $userterritorytable_row->territory_id)
								{
									$workorderview[] = (object) array(
																		'main_controller' => 'work_order',
																		'document_row_id' => $workordertable_row->row_id,
																		'document_date' => $workordertable_row->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable_row->workorder_date)) : '-',
																		'document_no' => $workordertable_row->workorder_no,															
																		'customer' => $CI->Model_table_vendor->get_name_basedon_row_id($workordertable_row->vendor_id)[0]->name,
																		'number_of_line' => sizeof($CI->Model_table_work_order_line->get_row_id_basedon_workorder_no($workordertable_row->workorder_no)),
																		'current_state' => $current_state,														
																		'area_id' => $workorder_area_id,														
																		'current_user_row_id' => $usertable[0]->row_id,
																		'current_user_territory_id' => $userterritorytable_row->territory_id,
																		'current_role_id' => $usertable[0]->role_id,															
																	);
								}
							}
						}
						else
						{
							$workorderview[] = (object) array(
																'main_controller' => 'work_order',
																'document_row_id' => $workordertable_row->row_id,
																'document_date' => $workordertable_row->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable_row->workorder_date)) : '-',
																'document_no' => $workordertable_row->workorder_no,
																'customer' => $CI->Model_table_vendor->get_name_basedon_row_id($workordertable_row->vendor_id)[0]->name,
																'number_of_line' => sizeof($CI->Model_table_work_order_line->get_row_id_basedon_workorder_no($workordertable_row->workorder_no)),
																'current_state' => $current_state,														
																'area_id' => $workorder_area_id,														
																'current_user_row_id' => $usertable[0]->row_id,
																'current_user_territory_id' => 0,
																'current_role_id' => $usertable[0]->role_id,															
															);
						}
						unset($userterritorytable);
					}
					
					if(sizeof($usertable) > 1)
					{
						foreach($usertable as $usertable_row)
						{
							$userterritorytable = $CI->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
							
							if(is_array($userterritorytable))
							{
								foreach($userterritorytable as $userterritorytable_row)
								{
									if($workorder_area_id == $userterritorytable_row->territory_id)
									{
										$workorderview[] = (object) array(
																			'main_controller' => 'work_order',
																			'document_row_id' => $workordertable_row->row_id,
																			'document_date' => $workordertable_row->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable_row->workorder_date)) : '-',
																			'document_no' => $workordertable_row->workorder_no,
																			'customer' => $CI->Model_table_vendor->get_name_basedon_row_id($workordertable_row->vendor_id)[0]->name,
																			'number_of_line' => sizeof($CI->Model_table_work_order_line->get_row_id_basedon_workorder_no($workordertable_row->workorder_no)),
																			'current_state' => $current_state,														
																			'area_id' => $workorder_area_id,														
																			'current_user_row_id' => $usertable_row->row_id,
																			'current_user_territory_id' => $userterritorytable_row->territory_id,
																			'current_role_id' => $usertable_row->role_id,															
																		);
									}
									unset($userterritorytable_row);
								}							
							}
							else
							{
								$workorderview[] = (object) array(
																	'main_controller' => 'work_order',
																	'document_row_id' => $workordertable_row->row_id,
																	'document_date' => $workordertable_row->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable_row->workorder_date)) : '-',
																	'document_no' => $workordertable_row->workorder_no,
																	'customer' => $CI->Model_table_vendor->get_name_basedon_row_id($workordertable_row->vendor_id)[0]->name,
																	'number_of_line' => sizeof($CI->Model_table_work_order_line->get_row_id_basedon_workorder_no($workordertable_row->workorder_no)),
																	'current_state' => $current_state,														
																	'area_id' => $workorder_area_id,														
																	'current_user_row_id' => $usertable_row->row_id,
																	'current_user_territory_id' => 0,
																	'current_role_id' => $usertable_row->role_id,															
																);
							}
							unset($userterritorytable);
							unset($usertable_row);
							
						}
					}			
				}
				unset($workordertable);			
			}
		}

		 
		/** Merge
		 */		
		$salesandworkorderview = array_merge($salesorderview, $workorderview);
		unset($salesorderview);
		unset($workorderview);
		
		return $salesandworkorderview;

		
		/** Merge than filter by user id 
		$pending_approval_list = array();
		foreach($salesandworkorderview as $row)
		{
			if($user_row_id == $row->current_user_row_id)
			{
				$pending_approval_list[] = $row;
			}
			unset($row);
		}
		unset($salesandworkorderview);
		
		return $pending_approval_list;
	 */		

	}
	
	function my_get_pending_approval_list($user_row_id)
	{
		$CI =& get_instance();
		$CI->load->helper("my_helper");
		
		$pending_approval_list = array();
		$all_pending_approval = my_get_all_pending_approval_list();
		
		foreach($all_pending_approval as $row)
		{
			if($user_row_id == $row->current_user_row_id)
			{
				$pending_approval_list[] = $row;
			}
			unset($row);		
		}
		unset($all_pending_approval);
		
		return $pending_approval_list;
	}

	
?>