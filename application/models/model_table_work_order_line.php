<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_table_work_order_line extends CI_Model
{
	private $table = 'workorderlinetable';
	private $db;
	private $result;

    public function __construct()
    {
        parent::__construct();
        $this->db  = $this->load->database('default', TRUE);
    }

 	/**
	 * Insert Data
	 * ----------------
	 */
    public function insert($data)
    {
		$this->db->insert($this->table, $data);

		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Insert failed';
		}
    }

 	/**
	 * Update Data
	 * ----------------
	 */
    function update($data, $where)
    {
		if($where)
		{
			foreach($where as $row)
			{
   				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->update($this->table, $data);
		}

		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Update failed';
		}
    }

 	/**
	 * Delete Data
	 * ----------------
	 */
    public function delete($where)
    {
		if($where)
		{
			foreach($where as $row)
			{
   				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->delete($this->table);
		}

		if($this->db->affected_rows() > 0)
		{
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Delete failed';
		}
    }

	/**
	 * Retrieving Data
	 * ----------------
	 */
    public function get_all()
	{
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

    public function get_all_active()
	{
		$this->db->where('status_id', 1);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
    public function get_rows($where = NULL)
	{
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
		}

		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_row_id_basedon_row_id($row_id)
	{
		$this->db->select('row_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_workorder_no_basedon_row_id($row_id)
	{
		$this->db->select('workorder_no');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_site_id_basedon_row_id($row_id)
	{
		$this->db->select('site_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_projectactivity_id_basedon_row_id($row_id)
	{
		$select = $this->db->select('projectactivity_id');
		$where = $this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_start_date_basedon_row_id($row_id)
	{
		$select = $this->db->select('start_date');
		$where = $this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_end_date_basedon_row_id($row_id)
	{
		$select = $this->db->select('end_date');
		$where = $this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
		
	public function get_remark_basedon_row_id($row_id)
	{
		$select = $this->db->select('remark');
		$where = $this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_status_id_basedon_row_id($row_id)
	{
		$select = $this->db->select('status_id');
		$where = $this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	/**
	 *
	 *
	 */
	public function get_row_id_basedon_workorder_no($workorder_no)
	{
		$this->db->select('row_id');
		$this->db->where('workorder_no', $workorder_no);
		$this->db->where('status_id', 1);		
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_site_id_basedon_workorder_no($workorder_no)
	{
		$this->db->select('site_id');
		$this->db->where('workorder_no', $workorder_no);
		$this->db->where('status_id', 1);		
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }		
	public function isWorkOrderExist($workorder_no)
	{
		$this->db->where('workorder_no', $workorder_no);
		$this->db->where('status_id', 1);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		unset($this->result);
    }
	
	
	
}
