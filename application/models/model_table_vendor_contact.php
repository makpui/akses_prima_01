<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_table_vendor_contact extends CI_Model
{
	private $table = 'vendorcontacttable';
	private $db;
	private $result;

    public function __construct()
    {
        parent::__construct();
        $this->db  = $this->load->database('default', TRUE);
    }

 	/**
	 * Insert Data
	 * ----------------
	 */
    public function insert($data)
    {
			$this->db->insert($this->table, $data);

			if($this->db->affected_rows() > 0)
			{
				return 'Successfully';
			}
			else
			{
				return 'Error Code :  - Insert failed';
			}
			unset($data);
    }

 	/**
	 * Update Data
	 * ----------------
	 */
    function update($data, $where)
    {
		if($where)
		{
			foreach($where as $row)
			{
   				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->update($this->table, $data);
		}
		
		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Update failed';
		}
    }

 	/**
	 * Delete Data
	 * ----------------
	 */
    function delete($where)
    {
		if($where)
		{
			foreach($where as $row)
			{
   				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->delete($this->table);
		}

		if($this->db->affected_rows() > 0)
		{
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Delete failed';
		}
    }

	/**
	 * Retrieving Data
	 * ----------------
	 */
    public function get_all()
	{
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

    public function get_all_active()
	{
		$this->db->where('status_id', 1);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

    public function get_rows($where)
	{
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
		}

		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_row_id_basedon_row_id($row_id)
	{
		$this->db->select('row_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_vendor_id_basedon_row_id($row_id)
	{
		$this->db->select('vendor_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_name_basedon_row_id($row_id)
	{
		$this->db->select('name');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_position_basedon_row_id($row_id)
	{
		$this->db->select('position');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }	
	
	public function get_organization_basedon_row_id($row_id)
	{
		$this->db->select('organization');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_email_basedon_row_id($row_id)
	{
		$this->db->select('email');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_mobile_no_basedon_row_id($row_id)
	{
		$this->db->select('mobile_no');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_status_id_basedon_row_id($row_id)
	{
		$this->db->select('status_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
}
