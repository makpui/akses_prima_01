<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_table_sales_order_approval_flow extends CI_Model
{
	private $table = 'salesorderapprovalflowtable';
	private $db;
	private $result;

    public function __construct()
    {
        parent::__construct();
        $this->db  = $this->load->database('default', TRUE);
    }

 	/**
	 * Insert Data
	 * ----------------
   * $data is associative array
   *
	 */
    public function insert($data)
    {
		$this->db->insert($this->table, $data);

		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Insert failed';
		}
    }

 	/**
	 * Update Data
	 * ----------------
   * $data is associative array
   * $where is multidimensional associative ArrayAccess
   *
	 */
    public function update($data, $where)
    {
 		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->update($this->table, $data);
		}
		
		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Update failed';
		}
    }

 	/**
	 * Delete Data
	 * ----------------
	 */
    public function delete($where)
    {
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->delete($this->table);
		}

		if($this->db->affected_rows() > 0)
		{
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Delete failed';
		}		
    }

	/**
	 * Retrieving Data
	 * ----------------
	 */
    public function get_all()
	{
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }


    public function get_rows($where)
	{
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
		}

		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_row_id_basedon_row_id($row_id)
	{
		$this->db->select('row_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_ordering_basedon_row_id($row_id)
	{
		$this->db->select('ordering');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_role_id_basedon_row_id($row_id)
	{
		$this->db->select('role_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_remark_basedon_row_id($row_id)
	{
		$this->db->select('remark');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_role_id_basedon_ordering($ordering)
	{
		$this->db->select('role_id');
		$this->db->where('ordering', $ordering);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_ordering_basedon_role_id($role_id)
	{
		$this->db->select('ordering');
		$this->db->where('role_id', $role_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	/** 
	 * Function for work/approval flow purposes
	 *
	 */
	 
	public function get_begining_state()
	{
		$this->db->order_by('ordering', 'ASC');
		$this->db->limit(1); 		
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	 
	public function get_ending_state()
	{
		$this->db->order_by('ordering', 'DESC');
		$this->db->limit(1); 		
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }	

}
