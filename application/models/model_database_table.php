<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Datatables_mdl extends CI_Model 
{        
    public function __construct() 
    {
        parent::__construct();
        $this->db  = $this->load->database('default', TRUE);
    }
    
    private function insert($table, $column)
    {

    }
 
    function update($table, $column, $where = NULL)
    {
    }
 
    function delete($table, $column, , $where = NULL)
    {
    }
 
	/**
	 * Retrieving Data
	 * -------------------
	 * All rows
	 * All columns
	 * 
	 */	 
    public function get_all($table)
    {	
		$result = $this->db->get($table)->result();		
        return $result;		
		unset($table);
		unset($result);
    }

	/**
	 * Retrieving Data
	 * ----------------
	 * A row / rows
	 * All columns
	 * $where is associative array
	 * $order_by is associative array ['column'] ['ordering'] 
	 * $limit is array
	 *
	 */	 
    public function get_row($table, $where = NULL, $or_where = NULL, $not_in = NULL, $in = NULL, $order_by = NULL, $limit = NULL)
    {
		if($where != NULL)
		{
			foreach($where as $key => $value)
			{
				$this->db->where($key, $value);
			}
		}
		
		if($order_by != NULL)
		{
			foreach($where as $key => $value)
			{
				$this->db->order_by($key['column'], $key['ordering']);
			}
		}
		
		if($limit != NULL)
		{
			if(sizeof($limit) == 1)
			{
				$this->db->limit($limit[0]);
			}
			else
			{
				$this->db->limit($limit[0], $limit[1]);			
			}
		}

		$result = $this->db->get($table)->result();		
        return $result;
		unset($table);
		unset($result);
    }

	/**
	 * Retrieving Data
	 * ---------------
	 * A row
	 * All columns
	 *
	 */	 
    public function get_row($table, $column, $where = NULL)
    {
        return $result;
    }
	
	/**
	 * Retrieving Data
	 * ---------------
	 * All rows or A view rows
	 * A view columns
	 *
	 */	 
    public function get_columns($table, $column, $where = NULL)
    {
        return $result;
    }
	
	/**
	 * Retrieving Data
	 * ---------------
	 * All rows or A view rows
	 * A column
	 *
	 */	 
    public function get_column($table, $column, $where = NULL)
    {
        return $result;
    }
	
	/**
	 * Retrieving Data
	 * ---------------
	 * A row
	 * A column
	 *
	 */	
    public function get_cell($table, $column, $where = NULL)
    {
        return $result;
    }

	/**
	 * Retrieving Data
	 * ---------------
	 * Count all row
	 * 
	 *
	 */	
    public function get_count_all_row($table, $column, $where = NULL)
    {
        return $result;
    }

	/**
	 * Retrieving Data
	 * ---------------
	 * Count rows
	 * 
	 *
	 */	
    public function get_count_rows($table, $column, $where = NULL)
    {
        return $result;
    }


    public function get_max($table, $column, $where = NULL)
    {
        return $result;
    }

    public function get_min($table, $column, $where = NULL)
    {
        return $result;
    }	
}