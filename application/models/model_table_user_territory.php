<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_table_user_territory extends CI_Model
{
	private $table = 'userterritorytable';
	private $db;
	private $result;

    public function __construct()
    {
        parent::__construct();
        $this->db  = $this->load->database('default', TRUE);
    }

 	/**
	 * Insert Data
	 * ----------------
	 */
    public function insert($data)
    {
		$this->db->insert($this->table, $data);

		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Insert failed';
		}
    }

 	/**
	 * Update Data
	 * ----------------
	 */
    public function update($data, $where)
    {
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->update($this->table, $data);
		}
		
		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Update failed';
		}
    }

 	/**
	 * Delete Data
	 * ----------------
	 */
    public function delete($where)
    {
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->delete($this->table);
		}

		if($this->db->affected_rows() > 0)
		{
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Delete failed';
		}
    }

	/**
	 * Retrieving Data
	 * ----------------
	 */
    public function get_all()
	{
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

    public function get_rows($where = NULL)
	{
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
		}

		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_territory_id_basedon_user_row_id($user_row_id)
	{
		$this->db->select('territory_id');
		$this->db->where('user_row_id', $user_row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_user_row_id_basedon_territory_id($territory_id)
	{
		$this->db->select('user_row_id');
		$this->db->where('territory_id', $territory_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_user_row_id_basedon_user_row_territory_id($user_row_id, $territory_id)
	{
		$this->db->select('user_row_id');
		$this->db->where('user_row_id', $user_row_id);
		$this->db->where('territory_id', $territory_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_territory_id_basedon_user_row_territory_id($user_row_id, $territory_id)
	{
		$this->db->select('territory_id');
		$this->db->where('user_row_id', $user_row_id);
		$this->db->where('territory_id', $territory_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function isUserRowIdExist($user_row_id)
	{
		$this->db->where('user_row_id', $user_row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		unset($this->result);
    }	
	
}
