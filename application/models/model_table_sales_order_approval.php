<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_table_sales_order_approval extends CI_Model
{
	private $table = 'salesorderapprovaltable';
	private $db;
	private $result;

    public function __construct()
    {
        parent::__construct();
		
        $this->db  = $this->load->database('default', TRUE);
    }

 	/**
	 * Insert Data
	 * ----------------
   * $data is associative array
   *
	 */
    public function insert($data)
    {
		$this->db->insert($this->table, $data);

		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Insert failed';
		}
    }

 	/**
	 * Update Data
	 * ----------------
   * $data is associative array
   * $where is multidimensional associative ArrayAccess
   *
	 */
    public function update($data, $where)
    {
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->update($this->table, $data);
		}

		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Update failed';
		}
    }

 	/**
	 * Delete Data
	 * ----------------
	 */
    public function delete($where)
    {
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
			$this->db->delete($this->table);
		}

		if($this->db->affected_rows() > 0)
		{
			unset($data);
			return 'Successfully';
		}
		else
		{
			return 'Error Code :  - Delete failed';
		}    		
    }

	/**
	 * Retrieving Data
	 * ----------------
	 */
    public function get_all()
	{
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

    public function get_all_active()
	{
		$this->db->where('status_id', 1);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

    public function get_rows($where = NULL)
	{
		if($where)
		{
			foreach($where as $row)
			{
				$this->db->where($row['key'], $row['value']);
				unset($row);
			}
			unset($where);
		}
		
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_row_id_basedon_row_id($row_id)
	{
		$this->db->select('row_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_salesorder_no_basedon_row_id($row_id)
	{
		$this->db->select('salesorder_no');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_approvalflow_id_basedon_row_id($row_id)
	{
		$this->db->select('approvalflow_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
		}
		else
		{
			return 'Error Code :  - No data available';
		}
		unset($this->result);
    }

	public function get_approved_date_basedon_row_id($row_id)
	{
		$this->db->select('approved_date');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_approved_status_basedon_row_id($row_id)
	{
		$this->db->select('approved_status');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	public function get_remark_basedon_row_id($row_id)
	{
		$this->db->select('remark');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }

	public function get_status_id_basedon_row_id($row_id)
	{
		$this->db->select('status_id');
		$this->db->where('row_id', $row_id);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return $this->result;
			unset($this->result);
		}
		else
		{
			return 'Error Code :  - No data available';
		}
    }
	
	/** 
	 * Function for work/approval flow purposes
	 * - Previous state is one state just been done
	 * 
	 */	
	
	
	public function get_previous_state($so_number)
	{
		$CI = get_instance();
		$CI->load->helper("my_helper");
		
		if(my_is_sales_order_no_in_approval_state($so_number))
		{ 			
			$this->db->where('salesorder_no', $so_number);			
			$this->db->where('status_id', 1);
			$this->db->order_by('approvalflow_id', 'DESC');
			$this->db->limit(1); 
			
			$this->result = $this->db->get($this->table)->result()[0]->approvalflow_id;

			if($this->result)
			{
				return $this->result;
				unset($this->result);
			}
			else
			{
				return 'Error Code :  - No data available';
			}
		}		
		else
		{
			return 0;
		}
		
    }
	
	public function get_current_state($so_number)
	{
		$CI = get_instance();
		$CI->load->helper("my_helper");	
		
		$ending_state = my_sales_order_approval_flow_get_ending_state()[0]->ordering;
		$previous_state = $this->get_previous_state($so_number);		
		
		if(!$this->isSalesOrderExist($so_number))
		{
			return $previous_state+1;
		}
		
		if($previous_state == $ending_state )
		{
			return $previous_state;
		}
		else
		{
			return $previous_state+1;
		}
		
		unset($ending_state);
		unset($previous_state);		
    }
	
	public function get_next_state($so_number)
	{		
		$CI = get_instance();
		$CI->load->helper("my_helper");
		$begining_state = my_sales_order_approval_flow_get_begining_state()[0]->ordering;
		$current_state = $this->get_current_state($so_number);	
		
		if($current_state != $begining_state )
		{
			return  $current_state+1;
		}
		else
		{
			return  $current_state;
		}
		
    }	
	
	public function isSalesOrderExist($salesorder_no)
	{
		$this->db->where('salesorder_no', $salesorder_no);
		$this->db->where('status_id', 1);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		unset($this->result);
    }

	public function get_number_of_row_basedon_salesorder_no($salesorder_no)
	{
		$this->db->where('salesorder_no', $salesorder_no);
		$this->db->where('status_id', 1);
		$this->result = $this->db->get($this->table)->result();

		if($this->result)
		{
			return sizeof($this->result);
		}
		else
		{
			return 0;
		}
		unset($this->result);
    }
}
