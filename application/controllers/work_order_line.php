<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class work_order_line extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
        $this->load->library('sendmail');
		$this->load->helper("my_helper");		
 		$this->load->model("Model_table_status");
		$this->load->model("Model_table_user");
		$this->load->model("Model_table_user_territory");
		$this->load->model("Model_table_role");
		$this->load->model("Model_table_site");
		$this->load->model("Model_table_city");
		$this->load->model("Model_table_province");
		$this->load->model("Model_table_project_activity");
		$this->load->model("Model_table_vendor");
		$this->load->model("Model_table_vendor_contact");
		$this->load->model("Model_table_work_flow_milestone");

		$this->load->model("Model_table_sales_order_line");
		
		$this->load->model("Model_table_work_order");
		$this->load->model("Model_table_work_order_approval");
		$this->load->model("Model_table_work_order_approval_flow");
		$this->load->model("Model_table_work_order_line");
    }
    
    public function index()
	{
        $workordertable_row_id = '';
        $workordertable_workorder_no = '';
        $workordertable_workorder_date = '';
        $workordertable_vendor_name = '';
        $workordertable_vendor_address = '';
        $workordertable_vendor_phone_no = '';		
        $workordertable_vendor_contact_name = '';
        $workordertable_vendor_contact_mobile_no = '';
        $workordertable_vendor_contact_email = '';		
        $workordertable_product_group = '';
		$workordertable_project_manager = '';
		$workordertable_work_flow_milestone = '';
		$workordertable_approval_progress = '';
		$workordertable_pending_approval_role = '';
		$workordertable_approval_result = '';
		$workordertable_status_name = '';
		
		$workordertable_work_flow_milestone_id = '';
		$workflowmilestonetable_creation_state = '';
		$workflowmilestonetable_ending_state = '';
		$workorderlinetable_work_order_isexist = '';
		$workorderapprovaltable_work_order_isexist = '';
		//$userid_is_approver = 0;
		$is_so_maker = 0;
		
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$row = $this->Model_table_work_order->get_rows($where);				
		unset($where);

        if($row != "")
		{
			/**
			 * Find approver name 
			 */
			$users = ''; 
			
			if(is_array($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($row[0]->workorder_no)))
			{
				$workorderlinetable_area_id = $this->Model_table_site->get_area_id_basedon_row_id($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($row[0]->workorder_no)[0]->site_id)[0]->area_id;			
			}
			else
			{
				$workorderlinetable_area_id = 0; 
			}
						
			$where[] = array('key' => 'role_id', 'value' => $this->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_work_order_approval->get_current_state($row[0]->workorder_no))[0]->role_id);
			$where[] = array('key' => 'status_id', 'value' => 1);
			$usertable = $this->Model_table_user->get_rows($where);
			unset($where);

			foreach($usertable as $usertable_row)
			{
				$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
				
				if(is_array($userterritorytable))
				{
					foreach($userterritorytable as $userterritorytable_row)
					{
						if($userterritorytable_row->territory_id == $workorderlinetable_area_id)
						{
							$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
							$where[] = array('key' => 'status_id', 'value' => 1);
							$users = $this->Model_table_user->get_rows($where);
							unset($where);
							break 2;
						}
						unset($userterritorytable_row);
					}
				}
				unset($usertable_row);
			}

			if($users == '')
			{
				$users = $usertable;
			}		
			
			/**
			 * Create data for header / master
			 */		
			$workordertable_row_id 						= $row[0]->row_id;
			$workordertable_workorder_no 				= $row[0]->workorder_no;
			$workordertable_workorder_date 				= $row[0]->workorder_date;
			$workordertable_vendor_name 				= $this->Model_table_vendor->get_name_basedon_row_id($row[0]->vendor_id)[0]->name;
	        $workordertable_vendor_address 				= $this->Model_table_vendor->get_address_basedon_row_id($row[0]->vendor_id)[0]->address;
			$workordertable_vendor_phone_no 			= $this->Model_table_vendor->get_phone_no_basedon_row_id($row[0]->vendor_id)[0]->phone_no;			
			$workordertable_vendor_contact_name 		= $this->Model_table_vendor_contact->get_name_basedon_row_id($row[0]->vendorcontact_id)[0]->name;
			$workordertable_vendor_contact_mobile_no 	= $this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($row[0]->vendorcontact_id)[0]->mobile_no;
			$workordertable_vendor_contact_email 		= $this->Model_table_vendor_contact->get_email_basedon_row_id($row[0]->vendorcontact_id)[0]->email;			
			$workordertable_product_group 				= $row[0]->product_group;
			$workordertable_project_manager 			= $row[0]->project_manager;
			$workordertable_work_flow_milestone			= $row[0]->work_flow_milestone_id .' - '. $this->Model_table_work_flow_milestone->get_name_basedon_id($row[0]->work_flow_milestone_id)[0]->name . ' State';				
			$workordertable_approval_progress 			= $row[0]->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id ? ' - ': $this->Model_table_work_order_approval->get_previous_state($row[0]->workorder_no).' / '.$this->Model_table_work_order_approval_flow->get_ending_state()[0]->ordering;
			$workordertable_pending_approval_role 		= my_work_order_approval_current_role($row[0]->workorder_no) .'  [ '. $users[0]->name .' ]';
			$workordertable_approval_result 			= my_work_order_approval_result($row[0]->workorder_no);		
			$workordertable_status_name 				= $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
			
			$workordertable_work_flow_milestone_id 		= $row[0]->work_flow_milestone_id;
			$workflowmilestonetable_creation_state 		= $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id;
			$workflowmilestonetable_ending_state 		= $this->Model_table_work_flow_milestone->get_ending_state()[0]->id;
			$workorderlinetable_work_order_isexist 		= $this->Model_table_work_order_line->isWorkOrderExist($row[0]->workorder_no);
			$workorderapprovaltable_work_order_isexist 	= $this->Model_table_work_order_approval->isWorkOrderExist($row[0]->workorder_no);
			//$userid_is_approver 						= is_array($this->Model_table_work_order_approval_flow->get_ordering_basedon_role_id(($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id))) 1 : 0 ;
			$is_so_maker 								= ($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 5) ?   1 : 0 ;
		}	
				
		$data = array(			
			'lbl_controller' 								=> str_replace("_", " ", $this->uri->segment(1)),
			
			'workordertable_row_id' 						=> $workordertable_row_id,
			'workordertable_workorder_no' 					=> $workordertable_workorder_no,
			'workordertable_workorder_date' 				=> $workordertable_workorder_date,
			'workordertable_vendor_name' 					=> $workordertable_vendor_name,
			'workordertable_vendor_address' 				=> $workordertable_vendor_address,
			'workordertable_vendor_phone_no' 				=> $workordertable_vendor_phone_no,			
			'workordertable_vendor_contact_name' 			=> $workordertable_vendor_contact_name,
			'workordertable_vendor_contact_mobile_no' 		=> $workordertable_vendor_contact_mobile_no,
			'workordertable_vendor_contact_email' 			=> $workordertable_vendor_contact_email,			
			'workordertable_product_group' 					=> $workordertable_product_group,
			'workordertable_project_manager' 				=> $workordertable_project_manager,
			'workordertable_work_flow_milestone' 			=> $workordertable_work_flow_milestone,			
			'workordertable_approval_progress' 				=> $workordertable_approval_progress,
			'workordertable_pending_approval_role' 			=> $workordertable_pending_approval_role,
			'workordertable_approval_result' 				=> $workordertable_approval_result,
			'workordertable_status_name' 					=> $workordertable_status_name,
			
			'workordertable_work_flow_milestone_id' 		=> $workordertable_work_flow_milestone_id,
			'workflowmilestonetable_creation_state' 		=> $workflowmilestonetable_creation_state,
			'workflowmilestonetable_ending_state' 			=> $workflowmilestonetable_ending_state,
			'workorderlinetable_work_order_isexist' 		=> $workorderlinetable_work_order_isexist,
			'workorderapprovaltable_work_order_isexist' 	=> $workorderapprovaltable_work_order_isexist,
			//'userid_is_approver' 							=> $userid_is_approver,
			'is_so_maker' 									=> $is_so_maker,
			
        );
		
        $this->template->display_app('page/work_order_line_vw', $data);
    }

    public function gridview()
	{
		$where = $this->input->post("where");
		
		$list = $this->Model_table_work_order_line->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		
        $data = array();
        $no = $_POST['start'];		
        foreach ($list as $line) 
        {			
			$no++;          
            $row = array();	
			
			$link = '
						<div class="btn-group">
							<a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';
			
			/**
			 * Update & Delete button show in creation stage only
			 *
			 */			
			
			if($this->Model_table_work_order->get_work_flow_milestone_id_basedon_workorder_no($line->workorder_no)[0]->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id)
			{
				if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 5)
				{
				$link .= '								
								<li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>			
					';
				}
				
				if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1)
				{				
				$link .= '								
								<li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					';
				}
			}		
			
			if($line->status_id == 1 && $this->Model_table_work_order->get_work_flow_milestone_id_basedon_workorder_no($line->workorder_no)[0]->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_ending_state()[0]->id && ($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 5 ))
			{
			$link .= '
								<li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'drop\', \''.$line->row_id.'\')">Drop</a></li>			
					';
			}
			
			$link .= '
							</ul>
						</div>
					';
					
			/**
			 * Handling empty date & date format
			 * because this process cannot done in array 
			 */
			$start_date = $line->start_date != '0000-00-00' ? date("d-m-Y", strtotime($line->start_date)) : '-';
			$end_date = $line->end_date != '0000-00-00' ? date("d-m-Y", strtotime($line->end_date)) : '-';
			$color_red = $line->status_id == 2 ? '; color:red' : ''; 
			
            $row[] = '<div style="text-align:center">'.$link.'</div>';
			$row[] = '<div style="text-align:center '.$color_red.'">'.$this->Model_table_sales_order_line->get_salesorder_no_basedon_site_id_api($this->Model_table_site->get_site_id_api_basedon_row_id($line->site_id)[0]->site_id_api)[0]->salesorder_no.'</div>'; 
			$row[] = '<div style="text-align:center '.$color_red.'">'.$this->Model_table_site->get_site_id_api_basedon_row_id($line->site_id)[0]->site_id_api.'</div>'; 
            $row[] = '<div style="text-align:center '.$color_red.'">'.$this->Model_table_site->get_site_name_api_basedon_row_id($line->site_id)[0]->site_name_api.'</div>'; 
            $row[] = '<div style="text-align:center '.$color_red.'">'.$this->Model_table_site->get_address_basedon_row_id($line->site_id)[0]->address.'</div>'; 
            $row[] = '<div style="text-align:center '.$color_red.'">'.$this->Model_table_site->get_longitude_ori_basedon_row_id($line->site_id)[0]->longitude_ori.'</div>'; 
            $row[] = '<div style="text-align:center '.$color_red.'">'.$this->Model_table_site->get_latitude_ori_basedon_row_id($line->site_id)[0]->latitude_ori.'</div>'; 			
            $row[] = '<div style="text-align:center '.$color_red.'">'.$this->Model_table_project_activity->get_txt_basedon_row_id($line->projectactivity_id)[0]->txt.'</div>'; 
			$row[] = '<div style="text-align:center '.$color_red.'">'.$start_date.'</div>'; 
			$row[] = '<div style="text-align:center '.$color_red.'">'.$end_date.'</div>';			
			$row[] = '<div style="text-align:center '.$color_red.'">'.$line->remark.'</div>';			
			$row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>';
			
            $data[] = $row;
			
        }
		
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_work_order_line->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
		
        echo json_encode($output);
		
    }

    public function form()
	{
        $txtstate = $this->input->post("txtstate");
		
        $txtrowid = $this->input->post("txtrowid");
        $txtworkorderno = $this->input->post("txtheaderworkorder");
        $txtsiteid = '';		
        $txtsiteidapi = '';
		$txttowertypeid = '';
		$txtprojectactivityid = ''; 
		$txtstartdate = ''; 
		$txtenddate = '';  
		$txtremark = '';  
		$txtstatusid = '';

		$readonly = '';
		
		$option_project_activity = '<option value="">--Select a Project Scope--</option>';
		$option_status = '<option value="1">Active</option>';
        
		if($txtstate == "edit")
		{
 			$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
			$row = $this->Model_table_work_order_line->get_rows($where);
			
            if($row != "") {
				$txtworkorderno = $row[0]->workorder_no;
                $txtsiteid = $row[0]->site_id;
				$txtsiteidapi = $this->Model_table_site->get_site_id_api_basedon_row_id($txtsiteid)[0]->site_id_api;
				$txttowertypeid = $this->Model_table_site->towertype_id_basedon_row_id($txtsiteid)[0]->towertype_id;
				$txtprojectactivityid = $row[0]->projectactivity_id; 
				$txtstartdate = $row[0]->start_date != '0000-00-00' ? date("d-m-Y", strtotime($row[0]->start_date)) : '';
				$txtenddate = $row[0]->end_date != '0000-00-00' ? date("d-m-Y", strtotime($row[0]->end_date)) : '';
				$txtremark = $row[0]->remark;
				$txtstatusid = $row[0]->status_id;
            }
			$option_project_activity = '<option value="'.$txtprojectactivityid.'" selected >'.$this->Model_table_project_activity->get_txt_basedon_row_id($txtprojectactivityid)[0]->txt.'</option>';	
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';
		}
		
		if($txtstate == "drop")
		{
			$readonly = 'readonly';

 			$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
			$row = $this->Model_table_work_order_line->get_rows($where);
			
            if($row != "") {
				$txtworkorderno = $row[0]->workorder_no;
                $txtsiteid = $row[0]->site_id;		
				$txtsiteidapi = $this->Model_table_site->get_site_id_api_basedon_row_id($txtsiteid)[0]->site_id_api;
				$txttowertypeid = $this->Model_table_site->get_towertype_id_basedon_row_id($txtsiteid)[0]->towertype_id;
				$txtprojectactivityid = $row[0]->projectactivity_id; 
				$txtstartdate = $row[0]->start_date != '0000-00-00' ? date("d-m-Y", strtotime($row[0]->start_date)) : '';
				$txtenddate = $row[0]->end_date != '0000-00-00' ? date("d-m-Y", strtotime($row[0]->end_date)) : '';
                $txtremark = $row[0]->remark;		
				$txtstatusid = 2;
            }

		}
								
        $table = '<div class="row">
						<form method="POST" class="formInput" enctype="multipart/form-data">
							<input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'"/>
							<input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

							<div class="col-md-12">
							  <div class="form-group">
								<label>Work Order No. : <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtworkorderno_old" name="txtworkorderno_old" class="form-control" value="'.$txtenddate.'" />
								<input type="text" id="txtworkorderno" name="txtworkorderno" class="form-control" value="'.$txtworkorderno.'" readonly />
							  </div>
							</div>
														
							<div class="col-md-12">
								<div class="form-group">
								  <label>Site: <span style="color: #ff0000">*</span></label>
								  <input type="hidden" id="txtsiteid_old" name="txtsiteid_old" class="form-control" value="'.$txtsiteid.'" />
								  <input type="hidden" id="txtsiteid" name="txtsiteid" class="form-control" value="'.$txtsiteid.'" />
								  <input type="hidden" id="txttowertypeid" name="txttowertypeid" class="form-control" value="'.$txttowertypeid.'" />
								  
								  <input type="text" id="txtsiteidapi" name="txtsiteidapi" class="form-control" value="'.$txtsiteidapi.'" '.$readonly.'/>								  
								  ';
		
		$table .=  '						  									
								</div>
							</div>
											
							<div class="col-md-12">
							  <div class="form-group">
								<label>Activity / Scope : <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtprojectactivityid_old" name="txtprojectactivityid_old" class="form-control" value="'.$txtprojectactivityid.'" />';
		if($txtstate == "drop") {
		$table .=  '						  
								   <input type="hidden" id="txtprojectactivityid" name="txtprojectactivityid" class="form-control" value="'.$txtprojectactivityid.'" />
								   <input type="text" id="txtprojectactivityname" name="txtprojectactivityname" class="form-control" value="'.$this->Model_table_project_activity->get_txt_basedon_row_id($txtprojectactivityid)[0]->txt.'" readonly />';
		}
		else
		{
		$table .=  '
									<select id="txtprojectactivityid" name="txtprojectactivityid" class="form-control select2" required>
										'.$option_project_activity.'
									</select>';
        }
		
		$table .=  '							
							  </div>
							</div>

							<div class="col-md-12">
							  <div class="form-group">
								<label>Target Started: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtstartdate_old" name="txtstartdate_old" class="form-control" value="'.$txtstartdate.'" />
								<input type="text" id="txtstartdate" name="txtstartdate"  class="form-control" value="'.$txtstartdate.'" '.$readonly.' />
							  </div>
							</div>
							
							<div class="col-md-12">
							  <div class="form-group">
								<label>Target Finished: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtenddate_old" name="txtenddate_old" class="form-control" value="'.$txtenddate.'" />
								<input type="text" id="txtenddate" name="txtenddate" class="form-control" value="'.$txtenddate.'" '.$readonly.' />
							  </div>
							</div>

							<div class="col-md-12">
							  <div class="form-group">
								<label>Remark: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtremark_old" name="txtremark_old" class="form-control" value="'.$txtremark.'" />
								<input type="text" id="txtremark" name="txtremark" class="form-control" value="'.$txtremark.'" '.$readonly.' />
							  </div>
							</div>							
							
							<div class="col-md-6">
							  <div class="form-group">
								<label>Status: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />';
		if($txtstate == "drop") {
		$table .=  '						  
								   <input type="hidden" id="txtstatusid" name="txtstatusid" class="form-control" value="'.$txtstatusid.'" />
								   <input type="text" id="txtstatusname" name="txtstatusname" class="form-control" value="'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'" readonly />';
		}
		else
		{
		$table .=  '
								
 								<select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
								  '.$option_status.'
								</select>
							   ';
        }
		
		$table .=  '													
							  </div>
							</div> 
											
						</form>
					</div>
							
				';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $txtrowid = $this->input->post("txtrowid");
		
		$txtworkorderno = '';	
        $txtsitetable_site_id_api = '';
        $txtsitetable_site_name_api = '';
        $txtsitetable_address = '';
        $txtsitetable_longitude_ori = '';
        $txtsitetable_latitude_ori = '';		
		$txtprojectactivitytable_txt = '';		
		$txtstartdate = ''; 
		$txtenddate = '';  
		$txtremark = '';  
		$txtstatustable_name = '';
     
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
		$row = $this->Model_table_work_order_line->get_rows($where);

        if($row != "")
		{
			$txtworkorderno = $row[0]->workorder_no;
			$txtsitetable_site_id_api = $this->Model_table_site->get_site_id_api_basedon_row_id($row[0]->site_id)[0]->site_id_api;
			$txtsitetable_site_name_api = $this->Model_table_site->get_site_name_api_basedon_row_id($row[0]->site_id)[0]->site_name_api;
			$txtsitetable_address = $this->Model_table_site->get_address_basedon_row_id($row[0]->site_id)[0]->address;
			$txtsitetable_longitude_ori = $this->Model_table_site->get_longitude_ori_basedon_row_id($row[0]->site_id)[0]->longitude_ori;
			$txtsitetable_latitude_ori = $this->Model_table_site->get_latitude_ori_basedon_row_id($row[0]->site_id)[0]->latitude_ori;		
			$txtprojectactivitytable_txt = $this->Model_table_project_activity->get_txt_basedon_row_id($row[0]->projectactivity_id)[0]->txt;		
			$txtstartdate =  $row[0]->start_date; 
			$txtenddate = $row[0]->end_date;  
			$txtremark = $row[0]->remark;  
			$txtstatustable_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;				
        } 
		
        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Work Order</td>
                        <td class="view-txt">: '.$txtworkorderno.'</td>
                    </tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Site Id API</td>
                        <td class="view-txt">: '.$txtsitetable_site_id_api.'</td>
                    </tr>
                  
					<tr>
						<td class="view-title" style="width: 30%">Site Name API</td>
						<td class="view-txt">: '.$txtsitetable_site_name_api.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Address</td>
						<td class="view-txt">: '.$txtsitetable_site_name_api.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Longitude</td>
						<td class="view-txt">: '.$txtsitetable_longitude_ori.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Latitude</td>
						<td class="view-txt">: '.$txtsitetable_latitude_ori.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Address</td>
						<td class="view-txt">: '.$txtsitetable_address.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Scope</td>
						<td class="view-txt">: '.$txtprojectactivitytable_txt. '</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Target Started</td>
						<td class="view-txt">: '.$txtstartdate.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Target Finished</td>
						<td class="view-txt">: '.$txtenddate.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Remark</td>
						<td class="view-txt">: '.$txtremark.'</td>
					</tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Status</td>
                        <td class="view-txt">: '.$txtstatustable_name.'</td>
                    </tr>
                  
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
		
        $txtrowid = (int)$this->input->post("txtrowid");
        $txtworkorderno = $this->input->post("txtworkorderno");
		$txtsiteid = (int)$this->input->post("txtsiteid");
		$txtprojectactivityid = (int)$this->input->post("txtprojectactivityid");
		$txtstartdate = $this->input->post("txtstartdate");
		$txtenddate = $this->input->post("txtenddate");
		$txtremark = $this->input->post("txtremark");
		$txtstatusid = (int)$this->input->post("txtstatusid");		
		
        if($txtstate == 'add')
        {
            $insert = array(
                'workorder_no' => $txtworkorderno,
				'site_id' => $txtsiteid,
				'projectactivity_id' => $txtprojectactivityid,
				'start_date' => $txtstartdate != '' ? date('Y-m-d', strtotime($txtstartdate)) : $txtstartdate,
				'end_date' => $txtenddate != '' ? date('Y-m-d', strtotime($txtenddate)) : $txtenddate,
				'remark' => $txtremark,
				'status_id' => $txtstatusid,
            );
			
			$this->Model_table_work_order_line->insert($insert);
			unset($insert);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'workorder_no' => $txtworkorderno,
				'site_id' => $txtsiteid,
				'projectactivity_id' => $txtprojectactivityid,
				'start_date' => $txtstartdate != '' ? date('Y-m-d', strtotime($txtstartdate)) : $txtstartdate,
				'end_date' => $txtenddate != '' ? date('Y-m-d', strtotime($txtenddate)) : $txtenddate,
				'remark' => $txtremark,
				'status_id' => $txtstatusid,
			);
			
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$this->Model_table_work_order_line->update($update, $where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
		
    }

    public function delete()
	{
        $txtrowid = $this->input->post("txtrowid");
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_work_order_line->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }
		
	
	/**
	 * Support drop down list
	 */
	public function getAvailableSiteList()
	{		
		$searched_site = $this->input->post("searched_site");
		$length_searched_site = strlen($searched_site);
		
		$list = array();
		$where = array();
		$where[] = array('key' => 'isscope', 'value' => 1);
		$projectactivitytable = $this->Model_table_project_activity->get_rows($where);
		unset($where);		
		
		foreach($this->Model_table_site->get_all_active() as $line)
		{	
			if(strtolower(substr($line->site_id_api,0,$length_searched_site)) == strtolower($searched_site))
			{		
				$where[] = array('key' => 'site_id', 'value' => $line->row_id);	
				$where[] = array('key' => 'status_id', 'value' => 1);					
				$workorderlinetable = $this->Model_table_work_order_line->get_rows($where);
				unset($where);			
				
				if(is_array($workorderlinetable))
				{				
					if(sizeof($workorderlinetable) < sizeof($projectactivitytable))
					{
						$list[] = $line;
					}
				}
				else
				{
					$list[] = $line;
				}
			}
			
		}
				
		echo json_encode($list);
		unset($list);
		unset($projectactivitytable);
		unset($workorderlinetable);

	}	
	  
	public function getAvailableProjectActivityList($site_id)
	{
		$txtstate = $this->input->post("txtstate");
		$txtprojectactivityid = $this->input->post("txtprojectactivityid");
				
		$where = array();
		
		$where[] = array('key' => 'site_id', 'value' => $site_id);		
		$where[] = array('key' => 'status_id', 'value' => 1);
		$row = $this->Model_table_work_order_line->get_rows($where);
		unset($where);
		if(is_array($row))
		{
			foreach($row as $line)
			{
				if($txtstate == 'edit')
				{					
					if($line->projectactivity_id != $txtprojectactivityid)
					{
						$where[] = array('key' => 'row_id !=', 'value' => $line->projectactivity_id);
					}					
				}
				else
				{
					$where[] = array('key' => 'row_id !=', 'value' => $line->projectactivity_id);
				}
				unset($line);		
			}
		}
		
		unset($row);		
		$where[] = array('key' => 'isscope', 'value' => 1);
		$row = $this->Model_table_project_activity->get_rows($where);
		echo json_encode($row);
		unset($row);		
		unset($where);
	}	
	
	public function getSiteList()
	{
		echo json_encode($this->Model_table_site->get_all_active());
	}	
	
	public function getSiteIdApiFromSiteTable()
	{
		$where = array(); 
		$where[] = array('key' => key($this->input->post()), 'value' => $this->input->post("site_id_api"));
		$where[] = array('key' => 'status_id', 'value' => 1);
		$query_result = $this->Model_table_site->get_rows($where);
		if(is_array($query_result))
		{
			echo json_encode(1);
		}
		else
		{
			echo json_encode(0);		
		}
	}
	
	
	public function getProjectActivityList()
	{
		$where[] = array('key' => 'isscope', 'value' => 1);
		echo json_encode($this->Model_table_project_activity->get_rows($where));
		unset($where);
	}

	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}
	
	/**
	 * Complete / Un-complete Creation Stage
	 */
    public function completeCreation($row_id)
    {
		/**
		 * 1. Update workordertable : set work_flow_milestoen_id = 2 : close creation state and start approval state  
		 *
		 */
		$update = array(
						'work_flow_milestone_id' => 2,
				  );
	
		$where[] = array('key' => 'row_id', 'value' => $row_id);
		$this->Model_table_work_order->update($update, $where);
		unset($where);

		/**
		 * 2. Send email notification to first approver / begining state  in approval state   
		 */
		
		// 2.a. get user list which is approver as email notification recipient
		$users = ''; 

		$workorderlinetable_area_id = $this->Model_table_site->get_area_id_basedon_row_id($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($this->Model_table_work_order->get_workorder_no_basedon_row_id($row_id)[0]->workorder_no)[0]->site_id)[0]->area_id;
		
		$where[] = array('key' => 'role_id', 'value' => $this->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_work_order_approval_flow->get_begining_state()[0]->ordering)[0]->role_id);
		$where[] = array('key' => 'status_id', 'value' => 1);
		$usertable = $this->Model_table_user->get_rows($where);
		unset($where);		


		foreach($usertable as $usertable_row)
		{
			$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);			
			
			if(is_array($userterritorytable))
			{
				foreach($userterritorytable as $userterritorytable_row)
				{
					if($userterritorytable_row->territory_id == $workorderlinetable_area_id)
					{
						$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
						$where[] = array('key' => 'status_id', 'value' => 1);
						$users = $this->Model_table_user->get_rows($where);
						unset($where);
						break 2;
					}
					unset($userterritorytable_row);
				}
			}
			unset($usertable_row);
		}

		if($users == '')
		{
			$users = $usertable;			
		}	
		
		// 2.b.get data from work order table for email body
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$workordertable = $this->Model_table_work_order->get_rows($where);	
		unset($where);
		
		//
				
		foreach($users as $user)
		{
			$this->sendmail->work_order_approval(
				$user->email,
				$user->name,											
				base_url("work_order_approval/index/".$this->uri->segment(3)),
				$workordertable[0]->workorder_no,
				$workordertable[0]->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable[0]->workorder_date)) : ' - ',
				$this->Model_table_vendor->get_name_basedon_row_id($workordertable[0]->vendor_id)[0]->name,
				$this->Model_table_vendor_contact->get_name_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->name,
				$this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->mobile_no,
				$this->Model_table_vendor_contact->get_email_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->email,
				$workordertable[0]->product_group,
				$workordertable[0]->project_manager,
				1,
				''															
			);
			unset($user);
		}
		unset($users);
		unset($workordertable);	
											
		header("Location: " . base_url("work_order_line/index/".$row_id)); 
	}

    public function unCompleteCreation($row_id)
    {
		$update = array(
						'work_flow_milestone_id' => 1,
				  );
	
		$where[] = array('key' => 'row_id', 'value' => $row_id);
		$this->Model_table_work_order->update($update, $where);	
	
		header("Location: " . base_url("work_order_line/index/".$row_id)); 
	}

	/**
	 *
	 */
    public function printwo()
	{
	    /**
		 * Generate Work Order
		 *
		 */
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$workordertable = $this->Model_table_work_order->get_rows($where);	
		unset($where);
		
		// query result for $row
		$where[] = array('key' => 'workorder_no', 'value' => $workordertable[0]->workorder_no);			
		$workorderlinetable = $this->Model_table_work_order_line->get_rows($where);
		$workorderlinetable[0]->salesordertable_salesorder_no = $this->Model_table_sales_order_line->get_salesorder_no_basedon_site_id_api($this->Model_table_site->get_site_id_api_basedon_row_id($workorderlinetable[0]->site_id)[0]->site_id_api)[0]->salesorder_no;
				
		unset($where);
				
		// to set a number of approval column
		$workorderapprovalflowtable = $this->Model_table_work_order_approval_flow->get_all();
		
		$where[] = array('key' => 'workorder_no', 'value' => $workordertable[0]->workorder_no);
		$where[] = array('key' => 'status_id', 'value' => 1);		
		$workorderapprovaltable = $this->Model_table_work_order_approval->get_rows($where);
		unset($where);
		
		$prnorien = '-L';
        $data = array(
            'primary_label' => "WORK ORDER",
            'primary_no' => $workordertable[0]->workorder_no,
            'workorder_date' => $workordertable[0]->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable[0]->workorder_date)) : ' - ',
			'product_group' => $workordertable[0]->product_group,
			'project_manager' => $workordertable[0]->project_manager,			
			'vendor_name' => $this->Model_table_vendor->get_name_basedon_row_id($workordertable[0]->vendor_id)[0]->name,
			'vendor_address' => $this->Model_table_vendor->get_address_basedon_row_id($workordertable[0]->vendor_id)[0]->address,
			'vendor_phone_no' => $this->Model_table_vendor->get_phone_no_basedon_row_id($workordertable[0]->vendor_id)[0]->phone_no,
			'vendor_contact_name' => $this->Model_table_vendor_contact->get_name_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->name,
			'vendor_contact_mobile_no' => $this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->mobile_no,
			'vendor_contact_email' => $this->Model_table_vendor_contact->get_email_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->email,						
            'approvers' => $workorderapprovaltable,
            'columns' => sizeof($workorderapprovalflowtable),
			'data' => $workorderlinetable
        );
		unset($time_list_approve);
		unset($role_list_approve);
		unset($workorderlinetable);
		unset($workorderapprovaltable);
		unset($workorderapprovalflowtable);
		unset($row);

        generatePDF("workorder_rpt", "work_order_no_".$workordertable[0]->workorder_no, $data, $prnorien);    
		unset($workordertable);			
    }	
}
