<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class work_order_approval extends CI_Controller {
    function __construct() {
        parent::__construct();				
        $this->load->library("template");
        $this->load->library('sendmail');
		$this->load->helper("my_helper");		
 		$this->load->model("Model_table_status");
		$this->load->model("Model_table_user");
		$this->load->model("Model_table_user_territory");
		$this->load->model("Model_table_role");
		$this->load->model("Model_table_site");
		$this->load->model("Model_table_city");
		$this->load->model("Model_table_province");
		$this->load->model("Model_table_project_activity");
		$this->load->model("Model_table_vendor");
		$this->load->model("Model_table_vendor_contact");
		$this->load->model("Model_table_work_flow_milestone");
		
		$this->load->model("Model_table_work_order");
		$this->load->model("Model_table_work_order_approval");
		$this->load->model("Model_table_work_order_approval_flow");
		$this->load->model("Model_table_work_order_line");
	}
	
    public function index()
	{
		$show_button_add =  FALSE;		

        $workordertable_row_id = '';
        $workordertable_workorder_no = '';
        $workordertable_workorder_date = '';
        $workordertable_vendor_name = '';
        $workordertable_vendor_address = '';
        $workordertable_vendor_phone_no = '';		
        $workordertable_vendor_contact_name = '';
        $workordertable_vendor_contact_mobile_no = '';
        $workordertable_vendor_contact_email = '';		
        $workordertable_product_group = '';
		$workordertable_project_manager = '';
		$workordertable_work_flow_milestone = '';
		$workordertable_approval_progress = '';
		$workordertable_pending_approval_role = '';
		$workordertable_approval_result = '';
		$workordertable_status_name = '';
		
		$workordertable_work_flow_milestone_id = '';
		$workflowmilestonetable_creation_state = '';
		$workflowmilestonetable_ending_state = '';
		$workorderlinetable_work_order_isexist = '';
		$workorderapprovaltable_work_order_isexist = '';

		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$row = $this->Model_table_work_order->get_rows($where);				
		unset($where);

        if($row != "")
		{		
			/**
			 * setting show add button 
			 */
			$users = ''; 

			if(is_array($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($row[0]->workorder_no)))
			{
				$workorderlinetable_area_id = $this->Model_table_site->get_area_id_basedon_row_id($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($row[0]->workorder_no)[0]->site_id)[0]->area_id;			
			}
			else
			{
				$workorderlinetable_area_id = 0; 
			}
					
			$where[] = array('key' => 'role_id', 'value' => $this->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_work_order_approval->get_current_state($row[0]->workorder_no))[0]->role_id);
			$where[] = array('key' => 'status_id', 'value' => 1);
			$usertable = $this->Model_table_user->get_rows($where);
			unset($where);

			foreach($usertable as $usertable_row)
			{
				$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
				
				if(is_array($userterritorytable))
				{
					foreach($userterritorytable as $userterritorytable_row)
					{
						if($userterritorytable_row->territory_id == $workorderlinetable_area_id)
						{
							$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
							$where[] = array('key' => 'status_id', 'value' => 1);
							$users = $this->Model_table_user->get_rows($where);
							unset($where);
							break 2;
						}
						unset($userterritorytable_row);
					}
				}
				unset($usertable_row);
			}

			if($users == '')
			{
				$users = $usertable;
			}
						
			if(($this->session->userdata['SessionLogin']['SesLoginId'] == $users[0]->row_id) && ($this->Model_table_work_order_approval->get_number_of_row_basedon_workorder_no($workordertable_workorder_no) < $this->Model_table_work_order_approval->get_current_state($workordertable_workorder_no)))
			{
				$show_button_add 							=  TRUE;
			}
			
			
			/**
			 * Create data for header / master
			 */
			$workordertable_row_id 						= $row[0]->row_id;
			$workordertable_workorder_no 				= $row[0]->workorder_no;
			$workordertable_workorder_date 				= $row[0]->workorder_date;
			$workordertable_vendor_name 				= $this->Model_table_vendor->get_name_basedon_row_id($row[0]->vendor_id)[0]->name;
	        $workordertable_vendor_address 				= $this->Model_table_vendor->get_address_basedon_row_id($row[0]->vendor_id)[0]->address;
			$workordertable_vendor_phone_no 			= $this->Model_table_vendor->get_phone_no_basedon_row_id($row[0]->vendor_id)[0]->phone_no;			
			$workordertable_vendor_contact_name 		= $this->Model_table_vendor_contact->get_name_basedon_row_id($row[0]->vendorcontact_id)[0]->name;
			$workordertable_vendor_contact_mobile_no 	= $this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($row[0]->vendorcontact_id)[0]->mobile_no;
			$workordertable_vendor_contact_email 		= $this->Model_table_vendor_contact->get_email_basedon_row_id($row[0]->vendorcontact_id)[0]->email;			
			$workordertable_product_group 				= $row[0]->product_group;
			$workordertable_project_manager 			= $row[0]->project_manager;
			$workordertable_work_flow_milestone			= $row[0]->work_flow_milestone_id .' - '. $this->Model_table_work_flow_milestone->get_name_basedon_id($row[0]->work_flow_milestone_id)[0]->name . ' State';				
			$workordertable_approval_progress 			= $row[0]->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id ? ' - ': $this->Model_table_work_order_approval->get_previous_state($row[0]->workorder_no).' / '.$this->Model_table_work_order_approval_flow->get_ending_state()[0]->ordering;
			$workordertable_pending_approval_role 		= my_work_order_approval_current_role($row[0]->workorder_no) .'  [ '. $users[0]->name .' ]';
			$workordertable_approval_result 			= my_work_order_approval_result($row[0]->workorder_no);		
			$workordertable_status_name 				= $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
			
			$workordertable_work_flow_milestone_id 		= $row[0]->work_flow_milestone_id;
			$workflowmilestonetable_creation_state 		= $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id;
			$workflowmilestonetable_ending_state 		= $this->Model_table_work_flow_milestone->get_ending_state()[0]->id;
			$workorderlinetable_work_order_isexist 		= $this->Model_table_work_order_line->isWorkOrderExist($row[0]->workorder_no);
			$workorderapprovaltable_work_order_isexist 	= $this->Model_table_work_order_approval->isWorkOrderExist($row[0]->workorder_no);
			
		}	
				
		$data = array(			
			'lbl_controller' 								=> str_replace("_", " ", $this->uri->segment(1)),

			'show_button_add'								=> $show_button_add,
			
			'workordertable_row_id' 						=> $workordertable_row_id,
			'workordertable_workorder_no' 					=> $workordertable_workorder_no,
			'workordertable_workorder_date' 				=> $workordertable_workorder_date,
			'workordertable_vendor_name' 					=> $workordertable_vendor_name,
			'workordertable_vendor_address' 				=> $workordertable_vendor_address,
			'workordertable_vendor_phone_no' 				=> $workordertable_vendor_phone_no,			
			'workordertable_vendor_contact_name' 			=> $workordertable_vendor_contact_name,
			'workordertable_vendor_contact_mobile_no' 		=> $workordertable_vendor_contact_mobile_no,
			'workordertable_vendor_contact_email' 			=> $workordertable_vendor_contact_email,			
			'workordertable_product_group' 					=> $workordertable_product_group,
			'workordertable_project_manager' 				=> $workordertable_project_manager,
			'workordertable_work_flow_milestone' 			=> $workordertable_work_flow_milestone,			
			'workordertable_approval_progress' 				=> $workordertable_approval_progress,
			'workordertable_pending_approval_role' 			=> $workordertable_pending_approval_role,
			'workordertable_approval_result' 				=> $workordertable_approval_result,
			'workordertable_status_name' 					=> $workordertable_status_name,
			
			'workordertable_work_flow_milestone_id' 		=> $workordertable_work_flow_milestone_id,
			'workflowmilestonetable_creation_state' 		=> $workflowmilestonetable_creation_state,
			'workflowmilestonetable_ending_state' 			=> $workflowmilestonetable_ending_state,
			'workorderlinetable_work_order_isexist' 		=> $workorderlinetable_work_order_isexist,
			'workorderapprovaltable_work_order_isexist' 	=> $workorderapprovaltable_work_order_isexist,
						
        );
		
        $this->template->display_app('page/work_order_approval_vw', $data);
    }

    public function gridview()
	{
        $where = $this->input->post("where"); 
		
		$list = $this->Model_table_work_order_approval->get_rows($where);			
		if(!is_array($list)) { $list = array(); }		
		unset($where);
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {			
            $no++;          
            $row = array();

			$link = '
						<div class="btn-group">
							<a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';

			$users = ''; 

			$workorderlinetable_area_id = $this->Model_table_site->get_area_id_basedon_row_id($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($line->workorder_no)[0]->site_id)[0]->area_id;
			
			$where[] = array('key' => 'role_id', 'value' => $this->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_work_order_approval->get_current_state($line->workorder_no))[0]->role_id);
			$where[] = array('key' => 'status_id', 'value' => 1);
			$usertable = $this->Model_table_user->get_rows($where);
			unset($where);

			foreach($usertable as $usertable_row)
			{
				$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
				
				if(is_array($userterritorytable))
				{
					foreach($userterritorytable as $userterritorytable_row)
					{
						if($userterritorytable_row->territory_id == $workorderlinetable_area_id)
						{
							$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
							$where[] = array('key' => 'status_id', 'value' => 1);
							$users = $this->Model_table_user->get_rows($where);
							unset($where);
							break 2;
						}
						unset($userterritorytable_row);
					}
				}
				unset($usertable_row);
			}

			if($users == '')
			{
				$users = $usertable;
			}					 
			
			if(($this->session->userdata['SessionLogin']['SesLoginId'] == $users[0]->row_id) && ($line->approvalflow_id == $this->Model_table_work_order_approval->get_previous_state($line->workorder_no)) && ($this->Model_table_work_order->get_work_flow_milestone_id_basedon_workorder_no($line->workorder_no)[0]->work_flow_milestone_id !=  $this->Model_table_work_order_approval_flow->get_ending_state()[0]->ordering) )
			{			
				$link .= '
								<li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\', \''.$line->approved_status.'\')">Update</a></li>
					';
			}
					
			$link .= '
							</ul>
						</div>
					';
			/**
			 * Handling empty date & date format
			 * because this process cannot done in array 
			 */
			$approved_date = $line->approved_date != '0000-00-00' ? date("d-m-Y", strtotime($line->approved_date)) : '-';				
			$approved_status = $line->approved_status == 0 ? "Reject" : "Approved";

            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->approvalflow_id.'</div>'; 
            $row[] = '<div style="text-align:center">'.$approved_date.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_role->get_txt_basedon_row_id($this->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($line->approvalflow_id)[0]->role_id)[0]->txt.'</div>';
			$row[] = '<div style="text-align:center">'.$approved_status.'</div>';
            $row[] = '<div style="text-align:center">'.$line->remark.'</div>'; 			
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_work_order_approval->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form()
	{
		$txtstate          = $this->input->post("txtstate");
		
        $txtrowid          = $this->input->post("txtrowid");
        $txtworkorderno   = $this->input->post("txtfilter_workordertable_workorder_no");
		$txtapprovalflowid = $this->Model_table_work_order_approval->get_current_state($txtworkorderno);		
		$txtapproveddate = date("d-m-Y h:i:s");		
        $txtapprovedstatus = $this->input->post("txtapprovedstatus");		
		$txtremark = '';
		$txtstatusid = '';
		
		$approved_status_name = $txtapprovedstatus == 0 ? 'Reject' : 'Approved';
		
        if($txtstate == "edit")
		{		
 			$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
			$row = $this->Model_table_work_order_approval->get_rows($where);
			unset($where);
			
            if($row != "") {
                $txtworkorderno = $row[0]->workorder_no;				
				$txtapprovalflowid = $row[0]->approvalflow_id;
				$txtapproveddate = $row[0]->approved_date != '0000-00-00' ? date("d-m-Y h:i:s", strtotime($row[0]->approved_date)) : '';		
				$txtapprovedstatus = $row[0]->approved_status;		
				$txtremark = $row[0]->remark;
				$txtstatusid = $row[0]->status_id;				
            }
        }
								
        $table = '
					<div class="row">
						<form method="POST" class="formInput" enctype="multipart/form-data">
							<input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />
							<input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
							<input type="hidden" id="txtworkorderno" name="txtworkorderno" class="form-control" value="'.$txtworkorderno.'" />
							<input type="hidden" id="txtapprovalflowid" name="txtapprovalflowid" class="form-control" value="'.$txtapprovalflowid.'" />
							<input type="hidden" id="txtstatusid" name="txtstatusid" class="form-control" value="'.$txtstatusid.'" />
														
							<div class="col-md-12">
								<div class="form-group">
								  <label>Date: <span style="color: #ff0000">*</span></label>
								  <input type="hidden" id="txtapproveddate_old" name="txtapproveddate_old" class="form-control" value="'.$txtapproveddate.'" />
								  <input type="text" id="txtapproveddate" name="txtapproveddate" class="form-control" value="'.$txtapproveddate.'" readonly/>
								</div>
							</div>
											
							<div class="col-md-6">
							  <div class="form-group">
								<label>Action: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtapprovedstatus_old" name="txtapprovedstatus_old" class="form-control" value="'.$txtapprovedstatus.'" />
								<input type="hidden" id="txtapprovedstatus" name="txtapprovedstatus" class="form-control" value="'.$txtapprovedstatus.'" />
								<input type="text" id="txtapprovedstatusname" name="txtapprovedstatusname" class="form-control" value="'.$approved_status_name.'" readonly/>
							  </div>
							</div> 

							<div class="col-md-12">
								<div class="form-group">
								  <label>Remark: <span style="color: #ff0000">*</span></label>
								  <input type="hidden" id="txtremark_old" name="txtremark_old" class="form-control" value="'.$txtremark.'" />
								  <input type="text" id="txtremark" name="txtremark" class="form-control" value="'.$txtremark.'" />
								</div>
							</div>
							
						</form>
					</div>
							
				';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $txtrowid = $this->input->post("txtrowid");

        $txtordering = '';		
		$txtapproveddate = ''; 
		$txtrolename = ''; 
		$txtapprovedstatus = '';
		$txtremark = '';		
		$txtstatusid = '';
        $txtstatustablename = '';
		$txtapprovalaction = '';			
		
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
		$row = $this->Model_table_work_order_approval->get_rows($where);
		unset($where);
		
        if($row != "")
		{
			$txtordering = $row[0]->workorderapprovalflowtable_ordering;		
			$txtapproveddate = $row[0]->approved_date != '0000-00-00' ? date("d-m-Y h:i:s", strtotime($row[0]->approved_date)) : '';
			$txtrolename = $rs[0]->roletable_txt; 
			$txtapprovalaction = $row[0]->approved_status == 0 ? 'Reject' : 'Approved';
			$txtremark = $rs[0]->remark;		
			$txtstatusid = $rs[0]->status_id;
			$txtstatustable_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        


        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Approvel No.:</td>
                        <td class="view-txt">'.$txtordering.'</td>
                    </tr>
                  
					<tr>
						<td class="view-title" style="width: 30%">Date:</td>
						<td class="view-txt">'.$txtapproveddate.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Role Name:</td>
						<td class="view-txt">'.$txtrolename.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Action:</td>
						<td class="view-txt">'.$txtapprovalaction.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Remark:</td>
						<td class="view-txt">'.$txtremark.'</td>
					</tr>
					
                    <tr>
                        <td class="view-title" style="width: 30%">Status:</td>
                        <td class="view-txt">'.$txtstatustablename.'</td>
                    </tr>
                  
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
		
        $txtrowid = (int)$this->input->post("txtrowid");
        $txtworkorderno = $this->input->post("txtworkorderno");
		$txtapprovalflowid = (int)$this->input->post("txtapprovalflowid");		
		$txtapproveddate = $this->input->post("txtapproveddate");		
		$txtapprovedstatus = $this->input->post("txtapprovedstatus");		
		$txtremark = $this->input->post("txtremark");		
		$txtstatusid = (int)$this->input->post("txtstatusid");	
		
		if($txtstate == 'add')
        {
			/**
			 * Add process
			 * 1. Add data to workorderapprovaltable
			 * 2. Cek approved status 
			 *    --if Approved Cek approval state (workorderapproval approvalflow id / previous state != workorderapprovalflow ending state)
			 *    ---------if not ending state
			 *    --------------send mail (loop process)
			 *    ---------else (ending state)
			 *    --------------update work flow miles stone = work flow ending state at workordel table
			 *    --------------add work orderline data to site table WHEN not in sitetable (loop processs) 
			 *    --if Not Approved 
			 *    --------------update all approved status =  0 where work order
			 *    --------------send mail notification to first approver
			 *
			 */
			$insert = array(
				'workorder_no' => $txtworkorderno,				
				'approvalflow_id' => $txtapprovalflowid,
				'approved_date' => $txtapproveddate != '0000-00-00 00:00:00' ? date("Y-m-d h:i:s", strtotime($txtapproveddate)) : '',
				'approved_status' => $txtapprovedstatus,		
				'remark' => $txtremark,	
				'status_id' => 1,
			);						
			$this->Model_table_work_order_approval->insert($insert);
			unset($insert);
						
			// 2.a. approved_status = 1 - approved
			if($txtapprovedstatus == 1)
			{
				// 2.a.1. approvalflow_id (current state cause add alreadydone) not ending state
				if($txtapprovalflowid < $this->Model_table_work_order_approval_flow->get_ending_state()[0]->ordering)
				{
					// 2.a.1.1.send mail
					// --------create recipient list -- its current approver through current state

					$users = ''; 
					
					$workorderlinetable_area_id = $this->Model_table_site->get_area_id_basedon_row_id($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($txtworkorderno)[0]->site_id)[0]->area_id;
					
					$where[] = array('key' => 'role_id', 'value' => $this->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_work_order_approval->get_current_state($txtworkorderno))[0]->role_id);
					$where[] = array('key' => 'status_id', 'value' => 1);
					$usertable = $this->Model_table_user->get_rows($where);
					unset($where);

					foreach($usertable as $usertable_row)
					{
						$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
						
						if(is_array($userterritorytable))
						{
							foreach($userterritorytable as $userterritorytable_row)
							{
								if($userterritorytable_row->territory_id == $workorderlinetable_area_id)
								{
									$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
									$where[] = array('key' => 'status_id', 'value' => 1);
									$users = $this->Model_table_user->get_rows($where);
									unset($where);
									break 2;
								}
								unset($userterritorytable_row);
							}
						}
						unset($usertable_row);
					}

					if($users == '')
					{
						$users = $usertable;
					}

					// --------create data for content										
					$where[] = array('key' => 'workorder_no', 'value' => $txtworkorderno);			
					$where[] = array('key' => 'status_id', 'value' => 1);			
					$workordertable = $this->Model_table_work_order->get_rows($where);
					unset($where);
					
					
															
					foreach($users as $user)
					{
						$this->sendmail->work_order_approval
						(
							$user->email,
							$user->name,
							base_url("work_order_approval/index/".$this->uri->segment(3)),
							$workordertable[0]->workorder_no,
							$workordertable[0]->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable[0]->workorder_date)) : ' - ',
							$this->Model_table_vendor->get_name_basedon_row_id($workordertable[0]->vendor_id)[0]->name,
							$this->Model_table_vendor_contact->get_name_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->name,
							$this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->mobile_no,
							$this->Model_table_vendor_contact->get_email_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->email,
							$workordertable[0]->product_group,
							$workordertable[0]->project_manager,
							$txtapprovedstatus,
							$txtremark							
							
						);
						unset($user);
					}
					unset($users);
					unset($workordertable);
				}
				
				// 2.a.2. approvalflow_id == ending state
				if($txtapprovalflowid == $this->Model_table_work_order_approval_flow->get_ending_state()[0]->ordering)
				{
					// 2.a.2.1 set work_flow_milestone_id = 9 at workordertable
					$update = array(
						'work_flow_milestone_id' => $this->Model_table_work_flow_milestone->get_ending_state()[0]->id,				
					);					
					$where[] = array('key' => 'workorder_no', 'value' => $txtworkorderno);
					$this->Model_table_work_order->update($update, $where);					
					unset($update);
					unset($where);
				}
			}
		
			// 2.a.2. reject
			if($txtapprovedstatus == 0)
			{				
				// 2.a.2.1.  set status id = 0 
				$update = array(
					'status_id' => 2,				
				);					
				$where[] = array('key' => 'workorder_no', 'value' => $txtworkorderno);
				$where[] = array('key' => 'status_id', 'value' => 1);				
				$this->Model_table_work_order_approval->update($update, $where);					
				unset($update);
				unset($where);
				
				// 2.a.2.1 set work_flow_milestone_id = 1/ creational state at workordertable
				$update = array(
					'work_flow_milestone_id' => 1,				
				);					
				$where[] = array('key' => 'workorder_no', 'value' => $txtworkorderno);
				$this->Model_table_work_order->update($update, $where);					
				unset($update);
				unset($where);
				
				
				// 2.a.2.2.  send mail to first approver
				// --------create recipient list -- its begining state

				$users = ''; 

				$workorderlinetable_area_id = $this->Model_table_site->get_area_id_basedon_row_id($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($txtworkorderno)[0]->site_id)[0]->area_id;

				//$where[] = array('key' => 'role_id', 'value' => $this->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_work_order_approval_flow->get_begining_state()[0]->ordering)[0]->role_id);
				$where[] = array('key' => 'role_id', 'value' => 5);
				$where[] = array('key' => 'status_id', 'value' => 1);
				$usertable = $this->Model_table_user->get_rows($where);
				unset($where);

				foreach($usertable as $usertable_row)
				{
					$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
					
					if(is_array($userterritorytable))
					{
						foreach($userterritorytable as $userterritorytable_row)
						{
							if($userterritorytable_row->territory_id == $workorderlinetable_area_id)
							{
								$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
								$where[] = array('key' => 'status_id', 'value' => 1);
								$users = $this->Model_table_user->get_rows($where);
								unset($where);
								break 2;
							}
							unset($userterritorytable_row);
						}
					}
					unset($usertable_row);
				}

				if($users == '')
				{
					$users = $usertable;
				}
												
				// --------create data for content										
				$where[] = array('key' => 'workorder_no', 'value' => $txtworkorderno);			
				$where[] = array('key' => 'status_id', 'value' => 1);
				$workordertable = $this->Model_table_work_order->get_rows($where);
				unset($where);
														
				foreach($users as $user)
				{
					$this->sendmail->work_order_approval
					(
						$user->email,
						$user->name,
						base_url("work_order_approval/index/".$this->uri->segment(3)),
						$workordertable[0]->workorder_no,
						$workordertable[0]->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable[0]->workorder_date)) : ' - ',
						$this->Model_table_vendor->get_name_basedon_row_id($workordertable[0]->vendor_id)[0]->name,
						$this->Model_table_vendor_contact->get_name_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->name,
						$this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->mobile_no,
						$this->Model_table_vendor_contact->get_email_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->email,
						$workordertable[0]->product_group,
						$workordertable[0]->project_manager,
						$txtapprovedstatus,
						$txtremark						
					);
					unset($user);
				}
				unset($users);
				unset($workordertable);				
			}
	
        }		
        else
        {
			/**
			 * Update process
			 * 1. update data to workorderapprovaltable
			 * 2. Cek approved status 
			 *    --if Approved status is 0 / Not approved (only this becouce status can be update when approve = 1
			 *    --------------update all approved status =  0 where work order
			 *    --------------send mail to first approver -- find current state --- user id and email
			 */

			 // 2.a. set status id = 2 not active data
			$update = array(				
				'workorder_no' => $txtworkorderno,				
				'approvalflow_id' => $txtapprovalflowid,
				'approved_date' => $txtapproveddate != '0000-00-00 00:00:00' ? date("Y-m-d h:i:s", strtotime($txtapproveddate)) : '',
				'approved_status' => $txtapprovedstatus,		
				'remark' => $txtremark,	
				'status_id' => $txtstatusid,
            );

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$this->Model_table_work_order_approval->update($update, $where);					
			unset($update);
			unset($where);

			// 2.b. when approved status  is reject
			if($txtapprovedstatus == 0)
			{				
				// 2.a.2.1.  set status id = 2 rejectted
				$update = array(
					'status_id' => 2,				
				);					
				$where[] = array('key' => 'workorder_no', 'value' => $txtworkorderno);
				$where[] = array('key' => 'status_id', 'value' => 1);
				$this->Model_table_work_order_approval->update($update, $where);					
				unset($update);
				unset($where);
				
				// 2.a.2.1 set work_flow_milestone_id = 1/ approval state at workordertable
				$update = array(
					'work_flow_milestone_id' => 1,				
				);					
				$where[] = array('key' => 'workorder_no', 'value' => $txtworkorderno);
				$this->Model_table_work_order->update($update, $where);					
				unset($update);
				unset($where);				
				
				// 2.a.2.2.  send mail to first approver
				// --------create recipient list -- its begining state 

				$users = ''; 

				$workorderlinetable_area_id = $this->Model_table_site->get_area_id_basedon_row_id($this->Model_table_work_order_line->get_site_id_basedon_workorder_no($txtworkorderno)[0]->site_id)[0]->area_id;

				//$where[] = array('key' => 'role_id', 'value' => $this->Model_table_work_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_work_order_approval_flow->get_begining_state()[0]->ordering)[0]->role_id);
				$where[] = array('key' => 'role_id', 'value' => 5);
				$where[] = array('key' => 'status_id', 'value' => 1);
				$usertable = $this->Model_table_user->get_rows($where);
				unset($where);

				foreach($usertable as $usertable_row)
				{
					$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
					
					if(is_array($userterritorytable))
					{
						foreach($userterritorytable as $userterritorytable_row)
						{
							if($userterritorytable_row->territory_id == $workorderlinetable_area_id)
							{
								$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
								$where[] = array('key' => 'status_id', 'value' => 1);
								$users = $this->Model_table_user->get_rows($where);
								unset($where);
								break 2;
							}
							unset($userterritorytable_row);
						}
					}
					unset($usertable_row);
				}

				if($users == '')
				{
					$users = $usertable;
				}				
				
				// --------create data for content										
				$where[] = array('key' => 'workorder_no', 'value' => $txtworkorderno);			
				$where[] = array('key' => 'status_id', 'value' => 1);			
				$workordertable = $this->Model_table_work_order->get_rows($where);
				unset($where);

				foreach($users as $user)
				{
					$this->sendmail->work_order_approval
					(
						$user->email,
						$user->name,
						base_url("work_order_approval/index/").$this->uri->segment(3),
						$workordertable[0]->workorder_no,
						$workordertable[0]->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable[0]->workorder_date)) : ' - ',
						$this->Model_table_vendor->get_name_basedon_row_id($workordertable[0]->vendor_id)[0]->name,
						$this->Model_table_vendor_contact->get_name_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->name,
						$this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->mobile_no,
						$this->Model_table_vendor_contact->get_email_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->email,
						$workordertable[0]->product_group,
						$workordertable[0]->project_manager,
						$txtapprovedstatus,
						$txtremark						
					);
					unset($user);
				}
				unset($users);
				unset($salesordertable);				
			}
        }
		
		$data['msg'] = "Process successful";
		$data['type'] = 1;
		echo json_encode($data);	
	
    }
	
    public function printwo()
	{
	    /**
		 * Generate Work Order
		 *
		 */
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$workordertable = $this->Model_table_work_order->get_rows($where);	
		unset($where);
		
		// query result for $row
		$where[] = array('key' => 'workorder_no', 'value' => $workordertable[0]->workorder_no);			
		$workorderlinetable = $this->Model_table_work_order_line->get_rows($where);
		unset($where);
				
		// to set a number of approval column
		$workorderapprovalflowtable = $this->Model_table_work_order_approval_flow->get_all();
		
		$where[] = array('key' => 'workorder_no', 'value' => $workordertable[0]->workorder_no);
		$where[] = array('key' => 'status_id', 'value' => 1);		
		$workorderapprovaltable = $this->Model_table_work_order_approval->get_rows($where);
		unset($where);
					
		$prnorien = '-L';
        $data = array(
            'primary_label' => "WORK ORDER",
            'primary_no' => $workordertable[0]->workorder_no,
            'workorder_date' => $workordertable[0]->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($workordertable[0]->workorder_date)) : ' - ',
			'product_group' => $workordertable[0]->product_group,
			'project_manager' => $workordertable[0]->project_manager,			
			'vendor_name' => $this->Model_table_vendor->get_name_basedon_row_id($workordertable[0]->vendor_id)[0]->name,
			'vendor_address' => $this->Model_table_vendor->get_address_basedon_row_id($workordertable[0]->vendor_id)[0]->address,
			'vendor_phone_no' => $this->Model_table_vendor->get_phone_no_basedon_row_id($workordertable[0]->vendor_id)[0]->phone_no,
			'vendor_contact_name' => $this->Model_table_vendor_contact->get_name_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->name,
			'vendor_contact_mobile_no' => $this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->mobile_no,
			'vendor_contact_email' => $this->Model_table_vendor_contact->get_email_basedon_row_id($workordertable[0]->vendorcontact_id)[0]->email,						
            'approvers' => $workorderapprovaltable,
            'columns' => sizeof($workorderapprovalflowtable),
			'data' => $workorderlinetable
        );
		unset($time_list_approve);
		unset($role_list_approve);
		unset($workorderlinetable);
		unset($workorderapprovaltable);
		unset($workorderapprovalflowtable);
		unset($row);

        generatePDF("workorder_rpt", "work_order_no_".$workordertable[0]->workorder_no, $data, $prnorien);    
		unset($workordertable);			
    }	
		

	
}	

