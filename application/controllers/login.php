<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    public function index()
    {
        $i = rand(1, 9);
        $j = rand(1, 9);
        $data = array(
                'question' => $i." + ".$j,
                'answer' => $i + $j
            );
        $this->load->view('page/login_vw', $data);
    }

    public function doLogin()
    {
        $txtusername = md5($this->input->post("txtusername"));
        $txtpassword = md5($this->input->post("txtpassword"));
        $txtquestion = $this->input->post("txtprovequestion");
        $txtanswer = $this->input->post("txtproveanswer");

        if($txtquestion == $txtanswer) {
            $qry = "select * 
                    from vw_usertablelist 
                    where md5(user_id) = '".$txtusername."' 
                        and pswrd = '".$txtpassword."'";

            $rs = $this->dataaccess_mdl->getByQuery($qry);
            if($rs != "") {            
                $datalogin = array(
                    'SesIsLogin' => TRUE,
                    'SesLoginId' => $rs[0]->row_id,
                    'SesUserId' => $rs[0]->user_id,
                    'SesUserName' => $rs[0]->name,
                    'SesUserRoleTxt' => $rs[0]->roletable_txt,
                    'SesUserRoleId' => $rs[0]->role_id,
                    'SesUserPhoto' => $rs[0]->photo,
                    'SesUserEmail' => $rs[0]->email,
                );   
                $this->session->set_userdata("SessionLogin", $datalogin); 
                redirect(base_url("dashboard"));
            }
            else {
                redirect(base_url()."?msg=Invalid email or password");
            }
        }
        else {
            redirect(base_url()."?msg=Prove us you are not a robot!");            
        }
    }

    public function logout(){        
        $this->session->sess_destroy();
        redirect(base_url("login"));
    }
}	