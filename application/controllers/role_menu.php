<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class role_menu extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_role");
		$this->load->model("Model_table_menu");
		$this->load->model("Model_table_role_menu");		
    }
    
    public function index()
	{
        $roletable_row_id = '';
        $roletable_txt = '';
		
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));
		$row = $this->Model_table_role->get_rows($where);		
		
        if($row != "") {
            $roletable_row_id = $row[0]->row_id;
            $roletable_txt = $row[0]->txt;
        }
		
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),             
						
            'roletable_row_id' 		=> $roletable_row_id,			
            'roletable_txt' 		=> $roletable_txt,			
        );
        $this->template->display_app('page/role_menu_vw', $data);
    }

    public function gridview() {
	
        $where = $this->input->post("where");
				
		$list = $this->Model_table_role_menu->get_rows($where);				
		if(!is_array($list)) { $list = array(); }
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->role_id.'\', \''.$line->menu_id.'\')">View</a></li>
                    
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->role_id.'\', \''.$line->menu_id.'\')">Delete</a></li>
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_menu->get_code_basedon_row_id($line->menu_id)[0]->code.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_menu->get_name_basedon_row_id($line->menu_id)[0]->name.'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_role_menu->get_all()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form()
	{
        $txtstate = $this->input->post("txtstate");
        $txtroleid = $this->input->post("txtroleid");
        $txtmenuid = '';
		
		$option_menu = '<option value="">--Select a Menu--</option>';

        if($txtstate == "edit")
		{
			$where[] = array('key' => 'role_id', 'value' => $txtroleid);
			$where[] = array('key' => 'menu_id', 'value' => $this->input->post("txtmenuid"));			
			$row = $this->Model_table_role_menu->get_rows($where);
		
            if($row != "") {
                $txtroleid = $row[0]->role_id; 
                $txtmenuid = $row[0]->menu_id; 
            }
			$option_menu = '<option value="'.$txtmenuid.'" selected >'.$this->Model_table_menu->get_code_basedon_row_id($txtmenuid)[0]->code.' - '.$this->Model_table_menu->get_name_basedon_row_id($txtmenuid)[0]->name.'</option>';
        }


        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtroleid" name="txtroleid" class="form-control" value="'.$txtroleid.'"/>
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />
                    				
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Menu: <span style="color: #ff0000">*</span></label>
                    <input type="hidden" id="txtmenuid_old" name="txtmenuid_old" class="form-control" value="'.$txtmenuid.'" />
                    <select id="txtmenuid" name="txtmenuid" class="form-control select2"  required>
                      '.$option_menu.'
                    </select>
                  </div>
                </div>
                  
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $menutable_code = $this->Model_table_menu->get_code_basedon_row_id($this->input->post("txtmenuid"))[0]->code;
        $menutable_name = $this->Model_table_menu->get_name_basedon_row_id($this->input->post("txtmenuid"))[0]->name;        
		
        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Role:</td>
                        <td class="view-txt">'.$menutable_code.'</td>
                    </tr>
                  
                  <tr>
                      <td class="view-title" style="width: 30%">Name:</td>
                      <td class="view-txt">'.$menutable_name.'</td>
                  </tr>
                  
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
        $txtroleid = $this->input->post("txtroleid");
        $txtmenuid = $this->input->post("txtmenuid");

        if($txtstate == 'add')
        {
            $insert = array(
                'role_id' => $txtroleid,
				'menu_id' => $txtmenuid,
            );

			$this->Model_table_role_menu->insert($insert);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'role_id' => $txtroleid,
				'menu_id' => $txtmenuid,
            );

			$where[] = array('key' => 'role_id', 'value' => $txtroleid);
			$where[] = array('key' => 'menu_id', 'value' => $txtmenuid);			
            $this->Model_table_role_menu->update($update, $where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtroleid = $this->input->post("txtroleid");
        $txtmenuid = $this->input->post("txtmenuid");
        
		$where[] = array('key' => 'role_id', 'value' => $txtroleid);
		$where[] = array('key' => 'menu_id', 'value' => $txtmenuid);
		$this->Model_table_role_menu->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }

	public function getMenuList()
	{
		$txtroleid = $this->input->post("txtroleid");
		$where[] = array('key' => 'role_id', 'value' => $this->input->post("txtroleid"));
		$rows =	$this->Model_table_role_menu->get_rows($where);				
		$where = array() ;
		if(is_array($rows))
		{
			foreach($rows as $row)
			{
				$where[] = array('key' => 'row_id !=', 'value' => $row->menu_id);
				unset($row);
			}
		}
		unset($rows);		
		echo json_encode($this->Model_table_menu->get_rows($where));
		unset($where);
	}

}	