<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sales_order_line extends CI_Controller {
    function __construct()
	{
        parent::__construct();
        $this->load->library("template");
        $this->load->library('sendmail');
		$this->load->helper("my_helper");		
 		$this->load->model("Model_table_status");
		$this->load->model("Model_table_user");
		$this->load->model("Model_table_user_territory");
 		$this->load->model("Model_table_work_flow_milestone");	
 		$this->load->model("Model_table_tenant");
 		$this->load->model("Model_table_role");		
 		$this->load->model("Model_table_product");
 		$this->load->model("Model_table_tower_type");		
 		$this->load->model("Model_table_transmission_type");	
 		$this->load->model("Model_table_tower_height");
 		$this->load->model("Model_table_site_type");
 		$this->load->model("Model_table_city");
 		$this->load->model("Model_table_province");		
 		$this->load->model("Model_table_region");
 		$this->load->model("Model_table_area");
 		$this->load->model("Model_table_site");				
 		$this->load->model("Model_table_sales_order_approval_flow");		
 		$this->load->model("Model_table_sales_order_approval");	
 		$this->load->model("Model_table_sales_order");	
 		$this->load->model("Model_table_sales_order_line");	
	}
	
    public function index()
	{
		$salesordertable_row_id = '';
        $salesordertable_salesorder_no = '';
        $salesordertable_salesorder_date = '';
        $salesordertable_drm_date = '';
        $salesordertable_batch = '';
        $salesordertable_po_tenant_no = '';
        $salesordertable_po_tenant_date = '';
        $salesordertable_tenant_name = '';
		$salesordertable_work_flow_milestone = '';
		$salesordertable_approval_progress = '';
		$salesordertable_pending_approval_role = '';
		$salesordertable_approval_result = '';

		$salesordertable_work_flow_milestone_id = '';
		$workflowmilestonetable_creation_state = '';
		$workflowmilestonetable_ending_state = '';
		$salesorderlinetable_sales_order_isexist = '';
		$salesorderapprovaltable_sales_order_isexist = '';
		//$userid_is_approver = 0;
		$is_so_maker = 0;
		$roletable_row_id = 0;
		
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$row = $this->Model_table_sales_order->get_rows($where);				
		unset($where);

        if($row != "")
		{
			/**
			 * setting show add button 
			 */	
			 
			$users = ''; 

			$salesorderlinetable_area_id = is_array($this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($row[0]->salesorder_no)) ? $this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($row[0]->salesorder_no)[0]->area_id : 0;
			
			$where[] = array('key' => 'role_id', 'value' => $this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_sales_order_approval->get_current_state($row[0]->salesorder_no))[0]->role_id);
			$where[] = array('key' => 'status_id', 'value' => 1);
			$usertable = $this->Model_table_user->get_rows($where);
			unset($where);
			
			foreach($usertable as $usertable_row)
			{
				$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
				
				if(is_array($userterritorytable))
				{
					foreach($userterritorytable as $userterritorytable_row)
					{
						if($userterritorytable_row->territory_id == $salesorderlinetable_area_id)
						{
							$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
							$where[] = array('key' => 'status_id', 'value' => 1);
							$users = $this->Model_table_user->get_rows($where);
							unset($where);
							break 2;
						}
						unset($userterritorytable_row);
					}
				}
				unset($usertable_row);
			}

			if($users == '')
			{
				$users = $usertable;
			}					 
			
			if(($this->session->userdata['SessionLogin']['SesLoginId'] == $users[0]->row_id) && ($row[0]->work_flow_milestone_id != $this->Model_table_sales_order_approval_flow->get_begining_state()[0]->ordering) && ($row[0]->work_flow_milestone_id != $this->Model_table_sales_order_approval_flow->get_ending_state()[0]->ordering))
			{
				$show_button_add 							=  TRUE;
			}

			/** 
			 * Prepare data
			 */		
		
			$salesordertable_row_id 						= $row[0]->row_id;	
			$salesordertable_salesorder_no 					= $row[0]->salesorder_no;
			$salesordertable_salesorder_date 				= ($row[0]->salesorder_date == '0000-00-00' || $row[0]->salesorder_date == NULL) ? ' - ' : date("d-m-Y", strtotime($row[0]->salesorder_date));
			$salesordertable_drm_date 						= ($row[0]->drm_date == '0000-00-00' || $row[0]->drm_date == NULL)? ' - ' : date("d-m-Y", strtotime($row[0]->drm_date));
			$salesordertable_batch 							= $row[0]->batch;
			$salesordertable_po_tenant_no 					= $row[0]->po_tenant_no;
			$salesordertable_po_tenant_date 				= ($row[0]->po_tenant_date == '0000-00-00' || $row[0]->po_tenant_date == NULL) ? ' - ' : date("d-m-Y", strtotime($row[0]->po_tenant_date));
			$salesordertable_tenant_name 					= $this->Model_table_tenant->get_name_basedon_row_id($row[0]->tenant_id)[0]->name;
			$salesordertable_work_flow_milestone			= $row[0]->work_flow_milestone_id .' - '. $this->Model_table_work_flow_milestone->get_name_basedon_id($row[0]->work_flow_milestone_id)[0]->name . ' State';
			$salesordertable_approval_progress 				= $row[0]->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id ? ' - ': $this->Model_table_sales_order_approval->get_previous_state($row[0]->salesorder_no).' / '.$this->Model_table_sales_order_approval_flow->get_ending_state()[0]->ordering;
			$salesordertable_pending_approval_role 			= my_sales_order_approval_current_role($row[0]->salesorder_no) .'  [ '. $users[0]->name .' ]';
			$salesordertable_approval_result 				= my_sales_order_approval_result($row[0]->salesorder_no);
			$salesordertable_status_name	 				= $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
			
			$salesordertable_work_flow_milestone_id			= $row[0]->work_flow_milestone_id;			
			$workflowmilestonetable_creation_state 			= $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id;
			$workflowmilestonetable_ending_state 			= $this->Model_table_work_flow_milestone->get_ending_state()[0]->id;
			$salesorderlinetable_sales_order_isexist		= $this->Model_table_sales_order_line->isSalesOrderExist($row[0]->salesorder_no);
			$salesorderapprovaltable_sales_order_isexist 	= $this->Model_table_sales_order_approval->isSalesOrderExist($row[0]->salesorder_no);
			
			//$userid_is_approver 							= is_array($this->Model_table_sales_order_approval_flow->get_ordering_basedon_role_id(($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id))) 1 : 0 ;
			$is_so_maker 									= ($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 2 )?   1 : 0 ;
			$roletable_row_id 								= $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id;
        }		
				
		$data = array(
			'lbl_controller' 								=> str_replace("_", " ", $this->uri->segment(1)),
			
			'salesordertable_row_id' 						=> $salesordertable_row_id,
			'salesordertable_salesorder_no' 				=> $salesordertable_salesorder_no,
			'salesordertable_salesorder_date' 				=> $salesordertable_salesorder_date,
			'salesordertable_drm_date' 						=> $salesordertable_drm_date,
			'salesordertable_batch' 						=> $salesordertable_batch,
			'salesordertable_po_tenant_no' 					=> $salesordertable_po_tenant_no,
			'salesordertable_po_tenant_date' 				=> $salesordertable_po_tenant_date,
			'salesordertable_tenant_name' 					=> $salesordertable_tenant_name,
			'salesordertable_work_flow_milestone' 			=> $salesordertable_work_flow_milestone,
			'salesordertable_approval_progress' 			=> $salesordertable_approval_progress,
			'salesordertable_pending_approval_role' 		=> $salesordertable_pending_approval_role,
			'salesordertable_approval_result' 				=> $salesordertable_approval_result,
			'salesordertable_status_name' 					=> $salesordertable_status_name,
			
			'salesordertable_work_flow_milestone_id' 		=> $salesordertable_work_flow_milestone_id,
			'workflowmilestonetable_creation_state' 		=> $workflowmilestonetable_creation_state,
			'workflowmilestonetable_ending_state' 			=> $workflowmilestonetable_ending_state, 
			'salesorderlinetable_sales_order_isexist'		=> $salesorderlinetable_sales_order_isexist,
			'salesorderapprovaltable_sales_order_isexist'	=> $salesorderapprovaltable_sales_order_isexist,
			//'userid_is_approver' 							=> $userid_is_approver,
			'is_so_maker' 									=> $is_so_maker,
			'roletable_row_id'								=> $roletable_row_id,
			
        );
		
        $this->template->display_app('page/sales_order_line_vw', $data);
    }

    public function gridview()
	{
        $where = $this->input->post("where");
		
		$list = $this->Model_table_sales_order_line->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		

        $data = array();
        $no = $_POST['start'];
		foreach ($list as $line) 
        {
			$no++;          
            $row = array();
			
			$link = '
						<div class="btn-group">
							<a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';
			
			/**
			 * Update & Delete button show in creation stage only
			 *
			 */			
			
			if($this->Model_table_sales_order->get_work_flow_milestone_id_basedon_salesorder_no($line->salesorder_no)[0]->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id)
			{
				if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 2)
				{
				$link .= '							
								<li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>			
					';
				}
				
				if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1)
				{			
				$link .= '							
								<li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					';
				}
			}		
					
			$link .= '
							</ul>
						</div>
					';
			
			/**
			 * Handling empty date & date format
			 * because this process cannot done in array 
			 */
			$rfi_date = $line->rfi == '0000-00-00' || $line->rfi == NULL ? '-' : date("d-m-Y", strtotime($line->rfi));
			$baks_date = $line->baks == '0000-00-00' || $line->baks == NULL ? '-' : date("d-m-Y", strtotime($line->baks));
			$baps_date = $line->baps == '0000-00-00' || $line->baps ==  NULL ? '-' : date("d-m-Y", strtotime($line->baps));
			
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_product->get_txt_basedon_row_id($line->product_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_tower_type->get_txt_basedon_row_id($line->towertype_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->site_id_api.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->site_name_api.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->site_id_tenant.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->site_name_tenant.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_site_type->get_txt_basedon_row_id($line->sitetype_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_tower_height->get_txt_basedon_row_id($line->towerheight_id)[0]->txt.'</div>';
            $row[] = '<div style="text-align:center">'.$line->latitude.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->longitude.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_area->get_txt_basedon_row_id($line->area_id)[0]->txt.'</div>'; 
			$row[] = '<div style="text-align:center">'.$this->Model_table_region->get_txt_basedon_row_id($line->region_id)[0]->txt.'</div>';
			$row[] = '<div style="text-align:center">'.$this->Model_table_province->get_txt_basedon_row_id($line->province_id)[0]->txt.'</div>';			
			$row[] = '<div style="text-align:center">'.$this->Model_table_city->get_txt_basedon_row_id($line->city_id)[0]->txt.'</div>';			
            $row[] = '<div style="text-align:center">'.$rfi_date.'</div>'; 
            $row[] = '<div style="text-align:center">'.$baks_date.'</div>'; 
            $row[] = '<div style="text-align:center">'.$baps_date.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->remarks.'</div>'; 
            $row[] = '<div style="text-align:right">'.number_format($line->rent_period).'</div>';
            $row[] = '<div style="text-align:right">'.number_format($line->rent_price).'</div>';			
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_sales_order_line->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form()
	{
        $txtstate = $this->input->post("txtstate");
		
        $txtrowid = $this->input->post("txtrowid");
		$txtsalesorderno = $this->input->post("txtsalesorderno");		
        $txtproductid = '';		
        $txttowertypeid = '';		
		$txtsiteidtenant = ''; 
		$txtsitenametenant = ''; 
		$txtsiteidapi = 'Generated by System'; 
		$txtsitenameapi = '';
		$txtsitetypeid = '';
		$txttowerheightid = '';
		$txtlatitude = '';
		$txtlongitude = '';
		$txtareaid = '';
		$txtregionid = '';
		$txtprovinceid = '';
		$txtcityid = '';
		$txtbaks = '';
		$txtrfi = '';
		$txtbaps = '';
		$txtremarks = '';
        $txtrentperiod = ''; 
        $txtrentprice = NULL; 
		$txtstatusid = '';

		$option_product = '<option value="">--Select a Product--</option>';
		$option_tower_type = '<option value="">--Select a Tower Type--</option>';
		$option_area = '<option value="">--Select an Area--</option>';
		$option_region = '<option value="">--Select a Region--</option>';
		$option_province = '<option value="">--Select a Province--</option>';
		$option_city = '<option value="">--Select a City--</option>';
		$option_site_type = '<option value="">--Select a Site Type--</option>';
		$option_tower_height = '<option value="">--Select a Tower Height--</option>';
		$option_status = '<option value="1">Active</option>';
		
        if($txtstate == "edit")
		{		
 			$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
			$row = $this->Model_table_sales_order_line->get_rows($where);
						
            if($row != "") {
                $txtsalesorderno = $row[0]->salesorder_no; 
                $txtproductid = $row[0]->product_id;		
                $txttowertypeid = $row[0]->towertype_id;		
				$txtsiteidtenant = $row[0]->site_id_tenant; 
				$txtsitenametenant = $row[0]->site_name_tenant; 
				$txtsiteidapi = $row[0]->site_id_api; 
				$txtsitenameapi = $row[0]->site_name_api;
				$txtsitetypeid = $row[0]->sitetype_id;				
				$txttowerheightid = $row[0]->towerheight_id;
				$txtlatitude = $row[0]->latitude;
				$txtlongitude = $row[0]->longitude;
				$txtareaid = $row[0]->area_id;
				$txtregionid = $row[0]->region_id;
				$txtprovinceid = $row[0]->province_id;
				$txtcityid = $row[0]->city_id;
				$txtbaks = $row[0]->baks == '0000-00-00' || $row[0]->baks == NULL ? '' : date("d-m-Y", strtotime($row[0]->baks));
				$txtrfi = $row[0]->rfi == '0000-00-00' || $row[0]->rfi == NULL ? '' :  date("d-m-Y", strtotime($row[0]->rfi));
				$txtbaps = $row[0]->baps == '0000-00-00' || $row[0]->baps == NULL ? '' :  date("d-m-Y", strtotime($row[0]->baps));
				$txtremarks = $row[0]->remarks;
				$txtrentperiod = $row[0]->rent_period; 
				$txtrentprice = $row[0]->rent_price; 
				$txtstatusid = $row[0]->status_id;
            }
			
			$option_product = '<option value="'.$txtproductid.'" selected >'.$this->Model_table_product->get_txt_basedon_row_id($txtproductid)[0]->txt.'</option>';	
			$option_tower_type = '<option value="'.$txttowertypeid.'" selected >'.$this->Model_table_tower_type->get_txt_basedon_row_id($txttowertypeid)[0]->txt.'</option>';
			
			$option_area = '<option value="'.$txtareaid.'" selected >'.$this->Model_table_area->get_txt_basedon_row_id($txtareaid)[0]->txt.'</option>';	
			$option_region = '<option value="'.$txtregionid.'" selected >'.$this->Model_table_region->get_txt_basedon_row_id($txtregionid)[0]->txt.'</option>';	
			$option_province = '<option value="'.$txtprovinceid.'" selected >'.$this->Model_table_province->get_txt_basedon_row_id($txtprovinceid)[0]->txt.'</option>';	
			$option_city = '<option value="'.$txtcityid.'" selected >'.$this->Model_table_city->get_txt_basedon_row_id($txtcityid)[0]->txt.'</option>';	
			$option_site_type = '<option value="'.$txtsitetypeid.'" selected >'.$this->Model_table_site_type->get_txt_basedon_row_id($txtsitetypeid)[0]->txt.'</option>';	
			$option_tower_height = '<option value="'.$txttowerheightid.'" selected >'.$this->Model_table_tower_height->get_txt_basedon_row_id($txttowerheightid)[0]->txt.'</option>';	
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';
			
        }
								
        $table = '
					<div class="row">
						<form method="POST" class="formInput" enctype="multipart/form-data">
							<input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
							<input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />
														
							<div class="col-md-12">
							  <div class="form-group">
								<label>Sales Order No. : <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtsalesorderno_old" name="txtsalesorderno_old" class="form-control" value="'.$txtsalesorderno.'" />
								<input type="text" id="txtsalesorderno" name="txtsalesorderno" class="form-control" value="'.$txtsalesorderno.'" readonly/>
							  </div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
								  <label>Product: <span style="color: #ff0000">*</span></label>
								  <input type="hidden" id="txtproductid_old" name="txtproductid_old" class="form-control" value="'.$txtproductid.'" />
								  <select id="txtproductid" name="txtproductid" class="form-control select2" required>
									'.$option_product.'
								  </select>								  
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
								  <label>Tower Type: <span style="color: #ff0000">*</span></label>
								  <input type="hidden" id="txttowertypeid_old" name="txttowertypeid_old" class="form-control" value="'.$txttowertypeid.'" />
								  <select id="txttowertypeid" name="txttowertypeid" class="form-control select2" required>
									'.$option_tower_type.'
								  </select>								  
								</div>
							</div>
							
							<div class="col-md-12">
							  <div class="form-group">
								<label>Site Id Tenant: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtsiteidtenant_old" name="txtsiteidtenant_old" class="form-control" value="'.$txtsiteidtenant.'" />							
								<input type="text" id="txtsiteidtenant" name="txtsiteidtenant"  onkeyup="genSiteNameApi()" class="form-control" value="'.$txtsiteidtenant.'" />	
							  </div>
							</div>
							
							<div class="col-md-12">
							  <div class="form-group">
								<label>Site Name Tenant: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtsitenametenant_old" name="txtsitenametenant_old" class="form-control" value="'.$txtsitenametenant.'" />
								<input type="text" id="txtsitenametenant" name="txtsitenametenant"  onkeyup="genSiteNameApi()" class="form-control" value="'.$txtsitenametenant.'" />
							  </div>
							</div>
							
							<div class="col-md-12">
							  <div class="form-group">
								<label>Site Id API: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtsiteidapi_old" name="txtsiteidapi_old" class="form-control" value="'.$txtsiteidapi.'" />
								<input type="text" id="txtsiteidapi" name="txtsiteidapi" class="form-control" value="'.$txtsiteidapi.'" />
							  </div>
							</div>

							<div class="col-md-12">
							  <div class="form-group">
								<label>Site Name API: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtsitenameapi_old" name="txtsitenameapi_old" class="form-control" value="'.$txtsitenameapi.'" />
								<input type="text" id="txtsitenameapi" name="txtsitenameapi" class="form-control" value="'.$txtsitenameapi.'" readonly />
							  </div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
								  <label>Site Type: <span style="color: #ff0000">*</span></label>
								  <input type="hidden" id="txtsitetypeid_old" name="txtsitetypeid_old" class="form-control" value="'.$txtsitetypeid.'" />
								  <input type="text" id="txtsitetypeid" name="txtsitetypeid" class="form-control txtsitetypeid_input" value="'.$txtsitetypeid.'" />	
								  <select id="txtsitetypeid" name="txtsitetypeid" class="form-control select2 txtsitetypeid_select"  required>
									'.$option_site_type.'
								  </select>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
								  <label>Tower Height: <span style="color: #ff0000">*</span></label>								  
								  <input type="hidden" id="txttowerheightid_old" name="txttowerheightid_old" class="form-control" value="'.$txttowerheightid.'" />
								  <input type="text" id="txttowerheightid" name="txttowerheightid" class="form-control txttowerheightid_input" value="'.$txttowerheightid.'" />	
								  <select id="txttowerheightid" name="txttowerheightid" class="form-control select2 txttowerheightid_select"  required>
									'.$option_tower_height.'
								  </select>
								</div>
							</div>

							<div class="col-md-12">
							  <div class="form-group">
								<label>Latitude: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtlatitude_old" name="txtlatitude_old" class="form-control" value="'.$txtlatitude.'" />
								<input type="text" id="txtlatitude" name="txtlatitude" class="form-control" value="'.$txtlatitude.'" />
							  </div>
							</div>

							<div class="col-md-12">
							  <div class="form-group">
								<label>Longitude: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtlongitude_old" name="txtlongitude_old" class="form-control" value="'.$txtlongitude.'" />
								<input type="text" id="txtlongitude" name="txtlongitude" class="form-control" value="'.$txtlongitude.'" />
							  </div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
								  <label>Area: <span style="color: #ff0000">*</span></label>								  
								  <input type="hidden" id="txtareaid_old" name="txtareaid_old" class="form-control" value="'.$txtareaid.'" />
								  <input type="text" id="txtareaid" name="txtareaid" class="form-control txtareaid_input" value="'.$txtareaid.'" />	
								  <select id="txtareaid" name="txtareaid" class="form-control select2 txtareaid_select"  required>
									'.$option_area.'
								  </select>									
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
								  <label>Region: <span style="color: #ff0000">*</span></label>								  
								  <input type="hidden" id="txtregionid_old" name="txtregionid_old" class="form-control" value="'.$txtregionid.'" />
								  <input type="text" id="txtregionid" name="txtregionid" class="form-control txtregionid_input" value="'.$txtregionid.'" />	
								  <select id="txtregionid" name="txtregionid" class="form-control select2 txtregionid_select"  required>
									'.$option_region.'									
								  </select>
								</div>
							</div>		

							<div class="col-md-12">
								<div class="form-group">
								  <label>Province: <span style="color: #ff0000">*</span></label>								  
								  <input type="hidden" id="txtprovinceid_old" name="txtprovinceid_old" class="form-control" value="'.$txtprovinceid.'" />
								  <input type="text" id="txtprovinceid" name="txtprovinceid" class="form-control txtprovinceid_input" value="'.$txtprovinceid.'" />	
								  <select id="txtprovinceid" name="txtprovinceid" class="form-control select2 txtprovinceid_select"  required>
									'.$option_province.'									
								  </select>								  
								</div>
							</div>		

							<div class="col-md-12">
								<div class="form-group">
								  <label>City: <span style="color: #ff0000">*</span></label>								  
								  <input type="hidden" id="txtcityid_old" name="txtcityid_old" class="form-control" value="'.$txtcityid.'" />
								  <input type="text" id="txtcityid" name="txtcityid" class="form-control txtcityid_input" value="'.$txtcityid.'" />	
								  <select id="txtcityid" name="txtcityid" class="form-control select2 txtcityid_select"  required>
									'.$option_city.'									
								  </select>
								</div>
							</div>		

							<div class="col-md-12">
							  <div class="form-group">
								<label>Target RFI:</label>
								<input type="hidden" id="txtrfi_old" name="txtrfi_old" class="form-control" value="'.$txtrfi.'" />
								<input type="text" id="txtrfi" name="txtrfi" class="form-control" value="'.$txtrfi.'" />
							  </div>
							</div>

							<div class="col-md-12">
							  <div class="form-group">
								<label>Target BAKS:</label>
								<input type="hidden" id="txtbaks_old" name="txtbaks_old" class="form-control" value="'.$txtbaks.'" />
								<input type="text" id="txtbaks" name="txtbaks" class="form-control" value="'.$txtbaks.'" />
							  </div>
							</div>							
							
							<div class="col-md-12">
							  <div class="form-group">
								<label>Target BAPS:</label>
								<input type="hidden" id="txtbaps_old" name="txtbaps_old" class="form-control" value="'.$txtbaps.'" />
								<input type="text" id="txtbaps" name="txtbaps" class="form-control" value="'.$txtbaps.'" />
							  </div>
							</div>

							<div class="col-md-12">
							  <div class="form-group">
								<label>Remarks:</label>
								<input type="hidden" id="txtremarks_old" name="txtremarks_old" class="form-control" value="'.$txtremarks.'" />
								<input type="text" id="txtremarks" name="txtremarks" class="form-control" value="'.$txtremarks.'" />
							  </div>
							</div>

							<div class="col-md-12">
							  <div class="form-group">
								<label>Rent Period:</label>
								<input type="hidden" id="txtrentperiod_old" name="txtrentperiod_old" class="form-control" value="'.$txtrentperiod.'" />
								<input type="number" step="1" id="txtrentperiod" name="txtrentperiod" class="form-control" value="'.$txtrentperiod.'" />
							  </div>
							</div>			

							<div class="col-md-12">
							  <div class="form-group">
								<label>Rent Price (Yearly) :</label>
								<input type="hidden" id="txtrentprice_old" name="txtrentprice_old" class="form-control" value="'.$txtrentprice.'" />
								<input type="text" step="1" id="txtrentprice" name="txtrentprice" class="form-control" placeholder="0.000.000.000,00" value="'.$txtrentprice.'" />
							  </div>
							</div>			

							<div class="col-md-6">
							  <div class="form-group">
								<label>Status: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />								
								<select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
								  '.$option_status.'
								</select>
								
							  </div>
							</div> 
											
						</form>
					</div>
							
				';	
		
        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $txtrowid = $this->input->post("txtrowid");
		$txtsalesorderno = '';		
        $txtproducttable_txt = '';
        $txttowertypetable_txt = '';		
		$txtsiteidtenant = ''; 
		$txtsitenametenant = ''; 
		$txtsiteidapi = ''; 
		$txtsitenameapi = '';
		$txtsitetypetable_txt = '';
		$txttowerheighttable_txt = '';
		$txtlatitude = '';
		$txtlongitude = '';
		$txtareatable_txt = '';
		$txtregiontable_txt = '';
		$txtprovincetable_txt = '';
		$txtcitytable_txt = '';
		$txtbaks = '';
		$txtrfi = '';
		$txtbaps = '';
		$txtremarks = '';
        $txtrentperiod = ''; 
        $txtrentprice = 0; 
		$txtstatustable_name = '';

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
		$row = $this->Model_table_sales_order_line->get_rows($where);
		
        if($row != "")
		{
			$txtsalesorderno = $row[0]->salesorder_no;
			$txtproducttable_txt = $this->Model_table_product->get_txt_basedon_row_id($row[0]->product_id)[0]->txt;
			
			$txttowertypetable_txt = $this->Model_table_tower_type->get_txt_basedon_row_id($row[0]->towertype_id)[0]->txt;
			$txtsiteidtenant = $row[0]->site_id_tenant; 
			$txtsitenametenant = $row[0]->site_name_tenant; 
			$txtsiteidapi = $row[0]->site_id_api; 
			$txtsitenameapi = $row[0]->site_name_api;
			$txtsitetypetable_txt = $this->Model_table_site_type->get_txt_basedon_row_id($row[0]->sitetype_id)[0]->txt;				
			$txttowerheighttable_txt = $this->Model_table_tower_height->get_txt_basedon_row_id($row[0]->towerheight_id)[0]->txt;
			$txtlatitude = $row[0]->latitude;
			$txtlongitude = $row[0]->longitude;
			$txtareatable_txt = $this->Model_table_area->get_txt_basedon_row_id($row[0]->area_id)[0]->txt;
			$txtregiontable_txt = $this->Model_table_region->get_txt_basedon_row_id($row[0]->region_id)[0]->txt;
			$txtprovincetable_txt = $this->Model_table_province->get_txt_basedon_row_id($row[0]->province_id)[0]->txt;
			$txtcitytable_txt = $this->Model_table_city->get_txt_basedon_row_id($row[0]->city_id)[0]->txt;
			$txtbaks = $row[0]->baks == '0000-00-00' || $row[0]->baks == NULL ? ' - ' : date("d-m-Y", strtotime($row[0]->baks));
			$txtrfi = $row[0]->rfi == '0000-00-00' || $row[0]->rfi == NULL ? ' - ' : date("d-m-Y", strtotime($row[0]->rfi));
			$txtbaps = $row[0]->baps == '0000-00-00' || $row[0]->baps == NULL ? ' - ' : date("d-m-Y", strtotime($row[0]->baps));
			$txtremarks = $row[0]->remarks;
			$txtrentperiod = $row[0]->rent_period; 
			$txtrentprice = $row[0]->rent_price;
			$txtstatustable_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Sales Order No.:</td>
                        <td class="view-txt">'.$txtsalesorderno.'</td>
                    </tr>
                  
					<tr>
						<td class="view-title" style="width: 30%">Product:</td>
						<td class="view-txt">'.$txtproducttable_txt.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Tower Type:</td>
						<td class="view-txt">'.$txttowertypetable_txt.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Site Id Tenant:</td>
						<td class="view-txt">'.$txtsiteidtenant.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Site Name Tenant:</td>
						<td class="view-txt">'.$txtsitenametenant.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Site Id API:</td>
						<td class="view-txt">'.$txtsiteidapi.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Site Name API:</td>
						<td class="view-txt">'.$txtsitenameapi.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Site Id Tenant:</td>
						<td class="view-txt">'.$txtsiteidtenant.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Site Name Tenant:</td>
						<td class="view-txt">'.$txtsitenametenant.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Site Type:</td>
						<td class="view-txt">'.$txtsitetypetable_txt.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Tower Height:</td>
						<td class="view-txt">'.$txttowerheighttable_txt.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Latitude:</td>
						<td class="view-txt">'.$txtlatitude.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Longitude:</td>
						<td class="view-txt">'.$txtlongitude.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Area:</td>
						<td class="view-txt">'.$txtareatable_txt.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Region:</td>
						<td class="view-txt">'.$txtregiontable_txt.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Province:</td>
						<td class="view-txt">'.$txtprovincetable_txt.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">City:</td>
						<td class="view-txt">'.$txtcitytable_txt.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">RFI:</td>
						<td class="view-txt">'.$txtrfi.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">BAKS:</td>
						<td class="view-txt">'.$txtbaks.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">BAPS:</td>
						<td class="view-txt">'.$txtbaps.'</td>
					</tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Rent Period:</td>
                        <td class="view-txt">'.number_format($txtrentperiod).'</td>
                    </tr>


                    <tr>
                        <td class="view-title" style="width: 30%">Rent Price (Yearly):</td>
                        <td class="view-txt">'.number_format($txtrentprice).'</td>
                    </tr>

					<tr>
						<td class="view-title" style="width: 30%">Remarks:</td>
						<td class="view-txt">'.$txtremarks.'</td>
					</tr>
					

                    <tr>
                        <td class="view-title" style="width: 30%">Status:</td>
                        <td class="view-txt">'.$txtstatustable_name.'</td>
                    </tr>
                  
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {		
        $txtstate = $this->input->post("txtstate");

        $txtrowid = (int)$this->input->post("txtrowid");
        $txtsalesorderno = $this->input->post("txtsalesorderno");
		$txtproductid = (int)$this->input->post("txtproductid");
		$txttowertypeid = (int)$this->input->post("txttowertypeid");
		$txtsiteidtenant = $this->input->post("txtsiteidtenant");
		
		$txtsitenametenant = $this->input->post("txtsitenametenant");
		$txtsitenameapi = $this->input->post("txtsitenameapi");		
		$txtsitetypeid = $this->input->post("txtsitetypeid");		
		$txttowerheightid = $this->input->post("txttowerheightid");				
		$txtlatitude = (double)$this->input->post("txtlatitude");
		$txtlongitude = (double)$this->input->post("txtlongitude");	
		$txtareaid = $this->input->post("txtareaid");		
		$txtregionid = $this->input->post("txtregionid");			
		$txtprovinceid = $this->input->post("txtprovinceid");		
		$txtcityid = $this->input->post("txtcityid");		
		$txtbaks = $this->input->post("txtbaks") == '' || $this->input->post("txtbaks") == NULL ? NULL : date("Y-m-d", strtotime($this->input->post("txtbaks")));
		$txtrfi = $this->input->post("txtrfi") == '' || $this->input->post("txtrfi") == NULL ? NULL : date("Y-m-d", strtotime($this->input->post("txtrfi")));
		$txtbaps = $this->input->post("txtbaps") == '' || $this->input->post("txtbaps") ==  NULL ? NULL : date("Y-m-d", strtotime($this->input->post("txtbaps")));
		$txtremarks = $this->input->post("txtremarks");
		$txtrentperiod = (double)$this->input->post("txtrentperiod");
		$txtrentprice = (double)$this->input->post("txtrentprice");
		$txtstatusid = (int)$this->input->post("txtstatusid");
        		
		if($txtproductid != 2)
		{
			$txtsiteidapi = generateSiteIdAPI($txttowertypeid, $txtareaid, $txtregionid, $txtcityid);
		}
		else
		{
			$txtsiteidapi = $this->input->post("txtsiteidapi");
		}
				
        if($txtstate == 'add')
        {
            $insert = array(
                'salesorder_no' => $txtsalesorderno,
				'product_id' => $txtproductid,
				'towertype_id' => $txttowertypeid,
				'site_id_tenant' => $txtsiteidtenant,
				'site_name_tenant' => $txtsitenametenant,
				'site_id_api' => $txtsiteidapi,
				'site_name_api' => $txtsitenameapi,
				'sitetype_id' => $txtsitetypeid,
				'towerheight_id' => $txttowerheightid,
				'latitude' => $txtlatitude,
				'longitude' => $txtlongitude,
				'area_id' => $txtareaid,
				'region_id' => $txtregionid,
				'province_id' => $txtprovinceid,
				'city_id' => $txtcityid,
				'baks' => $txtbaks,
				'rfi' => $txtrfi,
				'baps' => $txtbaps,				
				'remarks' => $txtremarks,				
				'rent_period' => $txtrentperiod,
				'rent_price' => $txtrentprice,
				'status_id' => $txtstatusid,
            );

			$this->Model_table_sales_order_line->insert($insert);
			unset($insert);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'salesorder_no' => $txtsalesorderno,
				'product_id' => $txtproductid,
				'towertype_id' => $txttowertypeid,
				'site_id_tenant' => $txtsiteidtenant,
				'site_name_tenant' => $txtsitenametenant,
				'site_id_api' => $txtsiteidapi,
				'site_name_api' => $txtsitenameapi,
				'sitetype_id' => $txtsitetypeid,
				'towerheight_id' => $txttowerheightid,
				'latitude' => $txtlatitude,
				'longitude' => $txtlongitude,
				'area_id' => $txtareaid,
				'region_id' => $txtregionid,
				'province_id' => $txtprovinceid,
				'city_id' => $txtcityid,
				'baks' => $txtbaks,
				'rfi' => $txtrfi,
				'baps' => $txtbaps,				
				'remarks' => $txtremarks,
				'rent_period' => $txtrentperiod,
				'rent_price' => $txtrentprice,
				'status_id' => $txtstatusid,              
            );
			
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$this->Model_table_sales_order_line->update($update, $where);
			unset($update);
			unset($where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
		
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_sales_order_line->delete($where);
        
        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }

	
	/**
	 */
	public function getProductList()
	{
		echo json_encode($this->Model_table_product->get_all());
	}

	public function getTowerTypeList()
	{
		echo json_encode($this->Model_table_tower_type->get_all());
	}
	
	public function getSiteTypeList()
	{
		echo json_encode($this->Model_table_site_type->get_all());
	}

	public function getTowerHeightList()
	{
        $towertype_id = $this->input->post("towertype_id");
        $sitetype_id = $this->input->post("sitetype_id");

		if($towertype_id)
		{
			$where[] = array('key' => 'towertype_id', 'value' => $towertype_id);
		}

		if($sitetype_id)
		{
			$where[] = array('key' => 'sitetype_id', 'value' => $sitetype_id);
		}

		echo json_encode($this->Model_table_tower_height->get_rows($where));
	}	
	 
	public function getAreaList()
	{
		echo json_encode($this->Model_table_area->get_all());
	}
	
	public function getRegionList($area_id)
	{
		if($area_id)
		{
			$where[] = array('key' => 'area_id', 'value' => $area_id);
		}	
		echo json_encode($this->Model_table_region->get_rows($where));
	}

	public function getProvinceList($region_id)
	{
		if($region_id)
		{
			$where[] = array('key' => 'region_id', 'value' => $region_id);
		}	
		echo json_encode($this->Model_table_province->get_rows($where));
	}	
	
	public function getCityList($province_id)
	{
		if($province_id)
		{
			$where[] = array('key' => 'province_id', 'value' => $province_id);
		}	
		echo json_encode($this->Model_table_city->get_rows($where));
	}	
	
	public function getSiteList()
	{
		$where = array();

        $tower_type_id = $this->input->post("tower_type_id");
		if($tower_type_id)
		{
			$where[] = array('key' => 'towertype_id', 'value' => $tower_type_id);
		}
				
		$rs_sitetable = $this->Model_table_site->get_rows($where);
		$sitelist = array();
		$siteidapi_anchor = '';

        $searched_siteidapi = $this->input->post("searched_siteidapi");
		$length_searched_siteidapi = strlen($searched_siteidapi);
		
		foreach($rs_sitetable as $row)
		{
			if(strtolower(substr($row->site_id_api,0,$length_searched_siteidapi)) == strtolower($searched_siteidapi))
			{		
				if($siteidapi_anchor != $row->site_id_api)
				{
					$sitelist[] = array('site_id_api' => $row->site_id_api, 'site_name_api' => $row->site_name_api, 'site_id_tenant' => $row->site_id_tenant, 'site_name_tenant' => $row->site_name_tenant);
					$siteidapi_anchor = $row->site_id_api;
				}
				unset($row);
			}
		}
		unset($rs_sitetable);
		unset($siteidapi_anchor);
		
		echo json_encode($sitelist);
		unset($sitelist);
	}	

	
	public function getSiteIdApiFromSiteTable()
	{
		$where = array(); 
		$where[] = array('key' => key($this->input->post()), 'value' => $this->input->post("site_id_api"));
		$where[] = array('key' => 'status_id', 'value' => 1);
		$query_result = $this->Model_table_site->get_rows($where);
		if(is_array($query_result))
		{
			echo json_encode(1);
		}
		else
		{
			echo json_encode(0);		
		}
	}
	
	
	public function getSiteData()
	{	
        $siteidapi = $this->input->post("txt_site_id_api");

		if($siteidapi)
		{
			$where = array();
			$where[] = array('key' => 'site_id_api', 'value' => $siteidapi);
			$where[] = array('key' => 'status_id', 'value' => 1);
			$rs_sitetable = $this->Model_table_site->get_rows($where);
			
			$sitelist = array();
			$siteidapi_anchor = '';
			foreach($rs_sitetable as $row)
			{
				if($siteidapi_anchor != $row->site_id_api)
				{
					$sitelist[] = array('site_id_api' => $row->site_id_api,
										'site_name_api' => $row->site_name_api,
										'sitetype_id' => $row->sitetype_id,
										'towerheight_id' => $row->towerheight_id,
										'latitude_ori' => $row->latitude_ori,
										'longitude_ori' => $row->longitude_ori,
										'area_id' => $row->area_id,
										'region_id' => $row->region_id,
										'province_id' => $row->province_id,
										'city_id' => $row->city_id,
										
										'sitetypetable_txt' => $this->Model_table_site_type->get_txt_basedon_row_id($row->sitetype_id)[0]->txt,
										'towerheighttable_txt' => $this->Model_table_tower_height->get_txt_basedon_row_id($row->towerheight_id)[0]->txt,
										'areatable_txt' => $this->Model_table_area->get_txt_basedon_row_id($row->area_id)[0]->txt,
										'regiontable_txt' => $this->Model_table_region->get_txt_basedon_row_id($row->region_id)[0]->txt,
										'provincetable_txt' => $this->Model_table_province->get_txt_basedon_row_id($row->province_id)[0]->txt,
										'citytable_txt' => $this->Model_table_city->get_txt_basedon_row_id($row->city_id)[0]->txt
										
										);
										
					$siteidapi_anchor = $row->site_id_api;
				}
				unset($row);
			}
			unset($rs_sitetable);
			unset($siteidapi_anchor);
			
			/*
			foreach($rs_sitetable as $rs)
			{
				$rs->sitetypetable_txt = $this->Model_table_site_type->get_txt_basedon_row_id($rs->sitetype_id)[0]->txt;
				$rs->towerheighttable_txt = $this->Model_table_tower_height->get_txt_basedon_row_id($rs->towerheight_id)[0]->txt;
				$rs->areatable_txt = $this->Model_table_area->get_txt_basedon_row_id($rs->area_id)[0]->txt;
				$rs->regiontable_txt = $this->Model_table_region->get_txt_basedon_row_id($rs->region_id)[0]->txt;
				$rs->provincetable_txt = $this->Model_table_province->get_txt_basedon_row_id($rs->province_id)[0]->txt;
				$rs->citytable_txt = $this->Model_table_city->get_txt_basedon_row_id($rs->city_id)[0]->txt;
			}
			*/
			
			echo json_encode($sitelist);
		}
	}	
	
	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}	
	
	/** 
	 * Prohibit site it tenant choose more than 1 times except product is collo
	 * row_id = 5  short_txt = C txt = collo
	 *
	 */
	public function getSalesOrderLineList()
	{
		$where[] = array('key' => 'status_id =', 'value' => 1);
		echo json_encode($this->Model_table_sales_order_line->get_rows($where));
	}	
	
	
	/**
	 */			
    public function completeCreation($row_id)
    {
		/**
		 * 1. Update salesordertable : set work_flow_milestoen_id = 2 : close creation state and start approval state  
		 *
		 */
		$update = array(
						'work_flow_milestone_id' => 2,
				  );
	
		$where[] = array('key' => 'row_id', 'value' => $row_id);
		$this->Model_table_sales_order->update($update, $where);
		unset($where);		

		/**
		 * 2. Send email notification to first approver / begining state  in approval state   
		 */
		//print_r($this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_sales_order_approval_flow->get_begining_state())[0]->role_id);
		
		// 2.a. get user list which is approver as email notification recepient

		$users = ''; 

		$salesorderlinetable_area_id = $this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($this->Model_table_sales_order->get_salesorder_no_basedon_row_id($row_id)[0]->salesorder_no)[0]->area_id;

		$where[] = array('key' => 'role_id', 'value' => $this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_sales_order_approval_flow->get_begining_state()[0]->ordering)[0]->role_id);
		$where[] = array('key' => 'status_id', 'value' => 1);
		$usertable = $this->Model_table_user->get_rows($where);
		unset($where);

		foreach($usertable as $usertable_row)
		{
			$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
			
			if(is_array($userterritorytable))
			{
				foreach($userterritorytable as $userterritorytable_row)
				{
					if($userterritorytable_row->territory_id == $salesorderlinetable_area_id)
					{
						$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
						$where[] = array('key' => 'status_id', 'value' => 1);
						$users = $this->Model_table_user->get_rows($where);
						unset($where);
						break 2;
					}
					unset($userterritorytable_row);
				}
			}
			unset($usertable_row);
		}

		if($users == '')
		{
			$users = $usertable;
		}		
		
		// 2.b.get data from sales order table for email body
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$salesordertable = $this->Model_table_sales_order->get_rows($where);	
		unset($where);
				
		foreach($users as $user)
		{
			$this->sendmail->sales_order_approval(
				$user->email,
				$user->name,											
				base_url("sales_order_approval/index/".$this->uri->segment(3)),
				$salesordertable[0]->salesorder_no,
				$salesordertable[0]->salesorder_date == '0000-00-00' || $salesordertable[0]->salesorder_date ==  NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->salesorder_date)),
				$salesordertable[0]->drm_date == '0000-00-00' || $salesordertable[0]->drm_date == NULL  ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->drm_date)),
				$salesordertable[0]->batch,
				$salesordertable[0]->po_tenant_no,
				$salesordertable[0]->po_tenant_date == '0000-00-00' || $salesordertable[0]->po_tenant_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->po_tenant_date)),
				$this->Model_table_tenant->get_name_basedon_row_id($salesordertable[0]->tenant_id)[0]->name,
				1,
				''											
			);
			unset($user);
		}
		unset($users);
		unset($salesordertable);
		
		header("Location: " . base_url("sales_order_line/index/".$row_id)); 
	}

    public function unCompleteCreation($row_id)
    {		
		$update = array(
						'work_flow_milestone_id' => 1,
				  );
	
		$where[] = array('key' => 'row_id', 'value' => $row_id);
		$this->Model_table_sales_order->update($update, $where);	
	
		header("Location: " . base_url("sales_order_line/index/".$row_id)); 
	}
	
	/*
	 * 1. Generate SO
	 */
    public function printso()
	{
	    /**
		 * Generate Sales Order
		 *
		 */
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$salesordertable = $this->Model_table_sales_order->get_rows($where);	
		unset($where);
		
		// query result for $row
		$where[] = array('key' => 'salesorder_no', 'value' => $salesordertable[0]->salesorder_no);			
		$salesorderlinetable = $this->Model_table_sales_order_line->get_rows($where);
		unset($where);
				
		// to set a number of approval column
		$salesorderapprovalflowtable = $this->Model_table_sales_order_approval_flow->get_all();
		
		$where[] = array('key' => 'salesorder_no', 'value' => $salesordertable[0]->salesorder_no);
		$where[] = array('key' => 'status_id', 'value' => 1);		
		$salesorderapprovaltable = $this->Model_table_sales_order_approval->get_rows($where);
		unset($where);
			
		$prnorien = '-L';
        $data = array(
            'primary_label' => "SALES ORDER",
            'primary_no' => $salesordertable[0]->salesorder_no,
            'salesorder_date' => $salesordertable[0]->salesorder_date == '0000-00-00' || $salesordertable[0]->salesorder_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->salesorder_date)),
            'drm_date' => $salesordertable[0]->drm_date == '0000-00-00' || $salesordertable[0]->drm_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->drm_date)),
            'tenanttable_name' => $this->Model_table_tenant->get_name_basedon_row_id($salesordertable[0]->tenant_id)[0]->name,
            'tenant_po_no' => $salesordertable[0]->po_tenant_no,
            'tenant_po_date' => $salesordertable[0]->po_tenant_date == '0000-00-00' || $salesordertable[0]->po_tenant_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->po_tenant_date)),
            'batch' => $salesordertable[0]->batch,
			
            'approvers' => $salesorderapprovaltable,
            'columns' => sizeof($salesorderapprovalflowtable),
			'data' => $salesorderlinetable
        );
		unset($time_list_approve);
		unset($role_list_approve);
		unset($salesorderlinetable);
		unset($salesorderapprovaltable);
		unset($salesorderapprovalflowtable);
		unset($row);
		
        generatePDF("salesorder_rpt", "sales_order_no_".$salesordertable[0]->salesorder_no, $data, $prnorien);    
		unset($salesordertable);		
    }
	
	

	
	
}	

