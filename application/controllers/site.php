<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class site extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
 		$this->load->model("Model_table_work_flow_milestone");	
 		$this->load->model("Model_table_sales_order");	
 		$this->load->model("Model_table_sales_order_line");	
 		$this->load->model("Model_table_status");	
 		$this->load->model("Model_table_transmission_type");	
 		$this->load->model("Model_table_tower_height");
 		$this->load->model("Model_table_site_type");
 		$this->load->model("Model_table_city");
 		$this->load->model("Model_table_province");		
 		$this->load->model("Model_table_region");
 		$this->load->model("Model_table_area");
 		$this->load->model("Model_table_tenant");
 		$this->load->model("Model_table_product");		
 		$this->load->model("Model_table_tower_type");		
 		$this->load->model("Model_table_site");
 		$this->load->model("Model_table_role");
 		$this->load->model("Model_table_user");		
    }

    public function index() {
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1))                
        );
        $this->template->display_app('page/site_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");
				
		$list = $this->Model_table_site->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';
					
			if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1)
			{					
            $link .= '
                    <li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					';
			}

			$link .= '					
                  </ul>
                </div>
            ';
			
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_product->get_txt_basedon_row_id($line->product_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_tower_type->get_txt_basedon_row_id($line->towertype_id)[0]->txt.'</div>'; 
			
			$row[] = '<div style="text-align:center">'.$this->Model_table_tenant->get_name_basedon_row_id($line->tenant_id)[0]->name.'</div>';
            $row[] = '<div style="text-align:center">'.$line->site_id_api.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->site_name_api.'</div>';
            $row[] = '<div style="text-align:center">'.$line->site_id_tenant.'</div>';
            $row[] = '<div style="text-align:center">'.$line->site_name_tenant.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_area->get_txt_basedon_row_id($line->area_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_region->get_txt_basedon_row_id($line->region_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_province->get_txt_basedon_row_id($line->province_id)[0]->txt.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_city->get_txt_basedon_row_id($line->city_id)[0]->txt.'</div>';
            $row[] = '<div style="text-align:center">'.$line->address.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->longitude_ori.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->latitude_ori.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_site_type->get_txt_basedon_row_id($line->sitetype_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_tower_height->get_txt_basedon_row_id($line->towerheight_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->transmissiontype_id > 0 ? $this->Model_table_transmission_type->get_txt_basedon_row_id($line->transmissiontype_id)[0]->txt : ' - '.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->remark.'</div>'; 			
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 			
            $data[] = $row;
		}
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_site->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form()
	{	
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
				
		$txtproductid = '';
		$txttenantid = '';		
        $txtsiteidapi = ''; 
		$txtsitenameapi = '';
		$txtsiteidtenant = ''; 
		$txtsitenametenant = '';		
		$txtareaid = ''; 		
		$txtregionid = '';		
		$txtprovinceid = '';		
		$txtcityid = '';		
		$txtlongitudeori = ''; 
		$txtlatitudeori = ''; 
		$txtaddress = ''; 		
		$txtsitetypeid = '';		
		$txttowerheightid = ''; 		
		$txttransmissiontypeid = '';		
		$txtremark = '';
		$txtstatusid = '';
		
		$salesorderlinetable = '';		
		
		$start_hide = '';
		$end_hide = '';

		$txtsiteidtenant_readonly = 'readonly';		
		$txtsitenametenant_readonly = 'readonly';		
		$txtsiteidapi_readonly = 'readonly';		
		$txtsitenameapi_readonly = 'readonly';		
		$txtlongitudeori_readonly = 'readonly';
		$txtlatitudeori_readonly = 'readonly';

		$option_product = '<option value="">--Select a Product--</option>';
		$option_tenant = '<option value="">--Select a Tenant--</option>';
		$option_area = '<option value="">--Select a Area--</option>';
		$option_region = '<option value="">--Select a Region--</option>';
		$option_province = '<option value="">--Select a Province--</option>';
		$option_city = '<option value="">--Select a City--</option>';
		$option_site_type = '<option value="">--Select a Site Type--</option>';
		$option_tower_height = '<option value="">--Select a Tower Height--</option>';
		$option_transmission_type = '<option value="">--Select a Transmission Type--</option>';
		$option_status = '<option value="1">Active</option>';
		
        if($txtstate == "edit")
		{						
 			$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
			$row = $this->Model_table_site->get_rows($where);

            if($row != "") {
			
				$txtproductid 					= $row[0]->tenant_id; 
				$txttenantid 					= $row[0]->tenant_id; 
                $txtsiteidapi 					= $row[0]->site_id_api; 
                $txtsitenameapi 				= $row[0]->site_name_api; 
                $txtsiteidtenant 				= $row[0]->site_id_tenant; 
                $txtsitenametenant 				= $row[0]->site_name_tenant;
				$txtareaid						= $row[0]->area_id; 
                $txtregionid 					= $row[0]->region_id; 
				$txtprovinceid 					= $row[0]->province_id;
				$txtcityid 						= $row[0]->city_id;
                $txtlongitudeori 				= $row[0]->longitude_ori; 
                $txtlatitudeori 				= $row[0]->latitude_ori; 
                $txtaddress 					= $row[0]->address; 
				$txtsitetypeid 					= $row[0]->sitetype_id; 
				$txttowerheightid 				= $row[0]->towerheight_id;
                $txttransmissiontypeid 			= $row[0]->transmissiontype_id; 
				$txtremark 						= $row[0]->remark; 
                $txtstatusid 					= $row[0]->status_id;
            }

			$start_hide = '<!--';
			$end_hide = '-->';
			
			$txtsiteidtenant_readonly = '';		
			$txtsitenametenant_readonly = '';		
			$txtsiteidapi_readonly = 'readonly';		
			$txtsitenameapi_readonly = 'readonly';			
			$txtlongitudeori_readonly = '';
			$txtlatitudeori_readonly = '';
			
 			$option_product = '<option value="'.$txtproductid.'" selected >'.$this->Model_table_product->get_txt_basedon_row_id($txtproductid)[0]->txt.'</option>';	
			$option_tenant = '<option value="'.$txttenantid.'" selected >'.$this->Model_table_tenant->get_name_basedon_row_id($txttenantid)[0]->name.'</option>';	
			$option_area = '<option value="'.$txtareaid.'" selected >'.$this->Model_table_area->get_txt_basedon_row_id($txtareaid)[0]->txt.'</option>';	
			$option_region = '<option value="'.$txtregionid.'" selected >'.$this->Model_table_region->get_txt_basedon_row_id($txtregionid)[0]->txt.'</option>';	
			$option_province = '<option value="'.$txtprovinceid.'" selected >'.$this->Model_table_province->get_txt_basedon_row_id($txtprovinceid)[0]->txt.'</option>';	
			$option_city = '<option value="'.$txtcityid.'" selected >'.$this->Model_table_city->get_txt_basedon_row_id($txtcityid)[0]->txt.'</option>';	
			$option_site_type = '<option value="'.$txtsitetypeid.'" selected >'.$this->Model_table_site_type->get_txt_basedon_row_id($txtsitetypeid)[0]->txt.'</option>';	
			$option_tower_height = '<option value="'.$txttowerheightid.'" selected >'.$this->Model_table_tower_height->get_txt_basedon_row_id($txttowerheightid)[0]->txt.'</option>';	
			$option_transmission_type = $txttransmissiontypeid ? '<option value="'.$txttransmissiontypeid.'">'.$this->Model_table_transmission_type->get_txt_basedon_row_id($txttransmissiontypeid)[0]->txt.'</option>' : '<option value="">--Select a Transmission Type--</option>' ;
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';			
       }
		
        $table = '
	           <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

					'.$start_hide.'
					<div class="col-md-12" >
						<div class="form-group"  >
							<label>Site Data From Sales Order Line : <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtsalesorderlinetable_old" name="txtsalesorderlinetable_old" class="form-control" value="'.$salesorderlinetable.'" />
							<select id="txtsalesorderlinetable" name="txtsalesorderlinetable" class="form-control select2" >
								<option value="">--Select a Data From Sales Order Line (in-case copying process was failed)--</option>
							</select>							
						</div>
					</div>
					'.$end_hide.'					
					
					<div class="col-md-12">
						<div class="form-group">
							<label>Product: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtproductid_old" name="txtproductid_old" class="form-control" value="'.$txtproductid.'" />
							<select id="txtproductid" name="txtproductid" class="form-control select2">
								'.$option_product.'
							</select>							
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Tenant: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txttenantid_old" name="txttenantid_old" class="form-control" value="'.$txttenantid.'" />
							<select id="txttenantid" name="txttenantid" class="form-control select2">
								'.$option_tenant.'
							</select>											
						</div>
					</div>

                    <div class="col-md-12">
						<div class="form-group">
							<label>Site Id Tenant: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtsiteidtenant_old" name="txtsiteidtenant_old" class="form-control" value="'.$txtsiteidtenant.'" />
							<input type="text" id="txtsiteidtenant" name="txtsiteidtenant" class="form-control" value="'.$txtsiteidtenant.'" '.$txtsiteidtenant_readonly.'/>
						</div>
                    </div>
                  
                    <div class="col-md-12">
						<div class="form-group">
							<label>Site Name Tenant: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtsitenametenant_old" name="txtsitenametenant_old" class="form-control" value="'.$txtsitenametenant.'" />
							<input type="text" id="txtsitenametenant" name="txtsitenametenant" class="form-control" onkeyup="genSiteNameApi()" value="'.$txtsitenametenant.'" '.$txtsitenametenant_readonly.'/>
						</div>
                    </div>

                    <div class="col-md-12">
						<div class="form-group">
							<label>Site Id API: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtsiteidapi_old" name="txtsiteidapi_old" class="form-control" value="'.$txtsiteidapi.'" />
							<input type="text" id="txtsiteidapi" name="txtsiteidapi" class="form-control" value="'.$txtsiteidapi.'" '.$txtsiteidapi_readonly.'/>
						</div>
                    </div>
                  
                    <div class="col-md-12">
						<div class="form-group">
							<label>Site Name API: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtsitenameapi_old" name="txtsitenameapi_old" class="form-control" value="'.$txtsitenameapi.'" />
							<input type="text" id="txtsitenameapi" name="txtsitenameapi" class="form-control" value="'.$txtsitenameapi.'" '.$txtsitenameapi_readonly.'/>
						</div>
                    </div>
                  
					<div class="col-md-12">
						<div class="form-group">
							<label>Area: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtareaid_old" name="txtareaid_old" class="form-control" value="'.$txtareaid.'" />
							<select id="txtareaid" name="txtareaid" class="form-control select2">
								'.$option_area.'
							</select>																	  
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label>Region: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtregionid_old" name="txtregionid_old" class="form-control" value="'.$txtregionid.'" />
							<select id="txtregionid" name="txtregionid" class="form-control select2">
								'.$option_region.'
							</select>																	  						  
						</div>
					</div>		

                    <div class="col-md-12">
						<div class="form-group">
							<label>Province: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtprovinceid_old" name="txtprovinceid_old" class="form-control" value="'.$txtprovinceid.'" />
							<select id="txtprovinceid" name="txtprovinceid" class="form-control select2">
								'.$option_province.'
							</select>																	  						  
						</div>
                    </div>
		  
                    <div class="col-md-12">
						<div class="form-group">
							<label>City: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtcityid_old" name="txtcityid_old" class="form-control" value="'.$txtcityid.'" />
							<select id="txtcityid" name="txtcityid" class="form-control select2">
								'.$option_city.'
							</select>																	  						  
						</div>
                    </div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Address:</label>
							<input type="hidden" id="txtaddress_old" name="txtaddress_old" class="form-control" value="'.$txtaddress.'" />
							<input type="text" id="txtaddress" name="txtaddress" class="form-control" value="'.$txtaddress.'" />
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Longitude:</label>
							<input type="hidden" id="txtlongitudeori_old" name="txtlongitudeori_old" class="form-control" value="'.$txtlongitudeori.'" />
							<input type="text" id="txtlongitudeori" name="txtlongitudeori" class="form-control" value="'.$txtlongitudeori.'"  '.$txtlongitudeori_readonly.' />
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Latitude:</label>
							<input type="hidden" id="txtlatitudeori_old" name="txtlatitudeori_old" class="form-control" value="'.$txtlatitudeori.'" />
							<input type="text" id="txtlatitudeori" name="txtlatitudeori" class="form-control" value="'.$txtlatitudeori.'"  '.$txtlatitudeori_readonly.'/>
						</div>
					</div>
									  
					<div class="col-md-12">
						<div class="form-group">
							<label>Site Type: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtsitetypeid_old" name="txtsitetypeid_old" class="form-control" value="'.$txtsitetypeid.'" />
							<select id="txtsitetypeid" name="txtsitetypeid" class="form-control select2">
								'.$option_site_type.'
							</select>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label>Tower Height: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txttowerheightid_old" name="txttowerheightid_old" class="form-control" value="'.$txttowerheightid.'" />
							<select id="txttowerheightid" name="txttowerheightid" class="form-control select2">
								'.$option_tower_height.'
							</select>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Transmission Type:</label>
							<input type="hidden" id="txttransmissiontypeid_old" name="txttransmissiontypeid_old" class="form-control" value="'.$txttransmissiontypeid.'" />
							<select id="txttransmissiontypeid" name="txttransmissiontypeid" class="form-control select2" >
								'.$option_transmission_type.'
							</select>
						</div>
					</div>
										
                    <div class="col-md-12">
						<div class="form-group">
							<label>Remark: </label>
							<input type="hidden" id="txtremark_old" name="txtremark_old" class="form-control" value="'.$txtremark.'" />
							<textarea id="txtremark" name="txtremark" rows="4" class="form-control">'.$txtremark.'</textarea>
						</div>
                    </div>
								                  
					<div class="col-md-12">
						<div class="form-group">
							<label>Status: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
							<select id="txtstatusid" name="txtstatusid" class="form-control select2">
								'.$option_status .'
							</select>
						</div>
					</div>									
					
				</form>
			   </div>

				';
				
        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
		
		$txtproducttable_txt = ''; 
		$txttenanttable_name = ''; 
        $txtsiteidapi = ''; 
		$txtsitenameapi = '';
		$txtsiteidtenant = ''; 
		$txtsitenametenant = '';	
		$txtareatable_txt = ''; 
		$txtregiontable_txt = ''; 
		$txtprovincetable_txt = '';
		$txtcitytable_txt = '';		
		$txtlongitudeori = ''; 
		$txtlatitudeori = ''; 
		$txtaddress = '';
		$txtsitetypetable_txt = ''; 
		$txttowerheighttable_txt = '';
		$txttransmissiontypetable_txt = ''; 
		$txtremark = '';
		$txtstatustable_name = '';
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
		$row = $this->Model_table_site->get_rows($where);
		
        if($row != "") {
			$txtproducttable_txt			= $this->Model_table_product->get_txt_basedon_row_id($row[0]->product_id)[0]->txt; 
			$txttenanttable_name			= $this->Model_table_tenant->get_name_basedon_row_id($row[0]->tenant_id)[0]->name; 
			$txtsiteidapi 					= $row[0]->site_id_api; 
			$txtsitenameapi 				= $row[0]->site_name_api; 
			$txtsiteidtenant 				= $row[0]->site_id_tenant; 
			$txtsitenametenant 				= $row[0]->site_name_tenant;
			$txtareatable_txt				= $this->Model_table_area->get_txt_basedon_row_id($row[0]->area_id)[0]->txt; 
			$txtregiontable_txt				= $this->Model_table_region->get_txt_basedon_row_id($row[0]->region_id)[0]->txt; 
			$txtprovincetable_txt			= $this->Model_table_province->get_txt_basedon_row_id($row[0]->province_id)[0]->txt;
			$txtcitytable_txt				= $this->Model_table_city->get_txt_basedon_row_id($row[0]->city_id)[0]->txt;
			$txtlongitudeori 				= $row[0]->longitude_ori; 
			$txtlatitudeori 				= $row[0]->latitude_ori; 
			$txtaddress 					= $row[0]->address; 
			$txtsitetypetable_txt			= $this->Model_table_site_type->get_txt_basedon_row_id($row[0]->sitetype_id)[0]->txt; 
			$txttowerheighttable_txt		= $this->Model_table_tower_height->get_txt_basedon_row_id($row[0]->towerheight_id)[0]->txt ;
			$txttransmissiontypetable_txt	= $row[0]->transmissiontype_id ? $this->Model_table_transmission_type->get_txt_basedon_row_id($row[0]->transmissiontype_id)[0]->txt : ' - ' ; 
			$txtremark 						= $row[0]->remark; 
			$txtstatustable_name			= $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                    <tr>
                        <td class="view-title" style="width: 30%">Product:</td>
                        <td class="view-txt">'.$txtproducttable_txt.'</td>
                    </tr>
										
                    <tr>
                        <td class="view-title" style="width: 30%">Tenant:</td>
                        <td class="view-txt">'.$txttenanttable_name.'</td>
                    </tr>				  
					
                    <tr>
                        <td class="view-title" style="width: 30%">Site Id API:</td>
                        <td class="view-txt">'.$txtsiteidapi.'</td>
                    </tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Site Name API:</td>
                        <td class="view-txt">'.$txtsitenameapi.'</td>
                    </tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Site Id Tenant:</td>
                        <td class="view-txt">'.$txtsiteidtenant.'</td>
                    </tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Site Name Tenant:</td>
                        <td class="view-txt">'.$txtsitenametenant.'</td>
                    </tr>
					
					<tr>
					  <td class="view-title" style="width: 30%">Area:</td>
					  <td class="view-txt">'.$txtareatable_txt.'</td>
					</tr>

					<tr>
					  <td class="view-title" style="width: 30%">Region:</td>
					  <td class="view-txt">'.$txtregiontable_txt.'</td>
					</tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Province:</td>
                        <td class="view-txt">'.$txtprovincetable_txt.'</td>
                    </tr>

                    <tr>
                        <td class="view-title" style="width: 30%">City:</td>
                        <td class="view-txt">'.$txtcitytable_txt.'</td>
                    </tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Address:</td>
                        <td class="view-txt">'.$txtaddress.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Longitude:</td>
                        <td class="view-txt">'.$txtlongitudeori.'</td>
                    </tr>

                    <tr>
                        <td class="view-title" style="width: 30%">Latitude:</td>
                        <td class="view-txt">'.$txtlatitudeori.'</td>
                    </tr>
                  
					<tr>
					  <td class="view-title" style="width: 30%">Site Type:</td>
					  <td class="view-txt">'.$txtsitetypetable_txt.'</td>
					</tr>

					<tr>
					  <td class="view-title" style="width: 30%">Tower Height:</td>
					  <td class="view-txt">'.$txttowerheighttable_txt.'</td>
					</tr>

					<tr>
					  <td class="view-title" style="width: 30%">Transmission Type:</td>
					  <td class="view-txt">'.$txttransmissiontypetable_txt.'</td>
					</tr>
                  
					<tr>
					  <td class="view-title" style="width: 30%">Remark:</td>
					  <td class="view-txt">'.$txtremark.'</td>
					</tr>

					<tr>
					  <td class="view-title" style="width: 30%">Status:</td>
					  <td class="view-txt">'.$txtstatustable_name.'</td>
					</tr>
                
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
		$txtstate 				= $this->input->post("txtstate");
		
		$txtrowid 				= (int)$this->input->post("txtrowid");		
		$txtproductid 			= (int)$this->input->post("txtproductid");
		$txttenantid 			= (int)$this->input->post("txttenantid");		
		$txtsitenameapi 		= $this->input->post("txtsitenameapi");
		$txtsiteidtenant 		= $this->input->post("txtsiteidtenant");
		$txtsitenametenant 		= $this->input->post("txtsitenametenant");		
		$txtareaid 				= (int)$this->input->post("txtareaid");
		$txtregionid 			= (int)$this->input->post("txtregionid");
		$txtprovinceid 			= (int)$this->input->post("txtprovinceid");
		$txtcityid 				= (int)$this->input->post("txtcityid");		
		$txtsiteidapi 			= generateSiteIdAPI($txtproductid, $txtareaid, $txtregionid, $txtprovinceid);
		$txtlongitudeori 		= (double)$this->input->post("txtlongitudeori"); 
		$txtlatitudeori 		= (double)$this->input->post("txtlatitudeori");
		$txtaddress 			= $this->input->post("txtaddress");		
		$txtsitetypeid 			= (int)$this->input->post("txtsitetypeid");
		$txttowerheightid 		= (int)$this->input->post("txttowerheightid");		
		$txttransmissiontypeid 	= (int)$this->input->post("txttransmissiontypeid"); 
		$txtremark 				= $this->input->post("txtremark"); 		
		$txtstatusid 			= (int)$this->input->post("txtstatusid");
		
        if($txtstate == 'add')
        {
            $insert = array(
                'product_id' => $txtproductid,
                'tenant_id' => $txttenantid,
				'site_id_api' => $txtsiteidapi,
				'site_name_api' => $txtsitenameapi,
                'site_id_tenant' => $txtsiteidtenant,
				'site_name_tenant' => $txtsitenametenant,
				'area_id' => $txtareaid,
				'region_id' => $txtregionid,
				'province_id' => $txtprovinceid,
				'city_id' => $txtcityid,
				'longitude_ori' => $txtlongitudeori,
				'latitude_ori' => $txtlatitudeori,
				'address' => $txtaddress,				
				'sitetype_id' => $txtsitetypeid,
				'towerheight_id' => $txttowerheightid,
				'transmissiontype_id' => $txttransmissiontypeid,
				'remark' => $txtremark,
				'status_id' => $txtstatusid,         
            );
			
			$this->Model_table_site->insert($insert);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'product_id' => $txtproductid,
                'tenant_id' => $txttenantid,
				'site_id_api' => $txtsiteidapi,
				'site_name_api' => $txtsitenameapi,
                'site_id_tenant' => $txtsiteidtenant,
				'site_name_tenant' => $txtsitenametenant,
				'area_id' => $txtareaid,
				'region_id' => $txtregionid,
				'province_id' => $txtprovinceid,
				'city_id' => $txtcityid,				
				'longitude_ori' => $txtlongitudeori,
				'latitude_ori' => $txtlatitudeori,
				'address' => $txtaddress,				
				'sitetype_id' => $txtsitetypeid,
				'towerheight_id' => $txttowerheightid,
				'transmissiontype_id' => $txttransmissiontypeid,
				'remark' => $txtremark,
				'status_id' => $txtstatusid,                  
            );

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$this->Model_table_site->update($update, $where);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_site->delete($where);
        
        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }
	
	/**
	 *
	 */
	
	public function getSalesOrderLineTableNotInSiteTable()
	{		
		// to get so data already approve
		$where[] = array('key' => 'work_flow_milestone_id !=', 'value' => $this->Model_table_work_flow_milestone->get_ending_state()[0]->id);
		$rows = $this->Model_table_sales_order->get_rows($where);
		unset($where);		
		foreach($rows as $row)
		{
			$where[] = array('key' => 'salesorder_no !=', 'value' => $row->salesorder_no);
			unset($row);
		}
		unset($rows);
		
		// to get data not in site 
		$rows =	$this->Model_table_site->get_all_active();
		foreach($rows as $row)
		{
			$where[] = array('key' => 'site_id_tenant !=', 'value' => $row->site_id_tenant);
			unset($row);
		}
		unset($rows);	
		
		echo json_encode($this->Model_table_sales_order_line->get_rows($where));
		unset($where);
	}
	
	public function getSelectedSalesOrderLineTable($row_id)
	{		
		$where[] = array('key' => 'row_id', 'value' => $row_id);
		$row = $this->Model_table_sales_order_line->get_rows($where);
		unset($where);
		
		
		
		$row[0]->producttable_txt = $this->Model_table_product->get_txt_basedon_row_id($row[0]->product_id)[0]->txt;
		$row[0]->salesordertable_tenant_id = $this->Model_table_sales_order->get_tenant_id_basedon_salesorder_no($row[0]->salesorder_no)[0]->tenant_id;
		
		$row[0]->tenanttable_name = $this->Model_table_tenant->get_name_basedon_row_id($this->Model_table_sales_order->get_tenant_id_basedon_salesorder_no($row[0]->salesorder_no)[0]->tenant_id)[0]->name;
		
		$row[0]->sitetypetable_txt = $this->Model_table_site_type->get_txt_basedon_row_id($row[0]->sitetype_id)[0]->txt;
		$row[0]->towerheighttable_txt = $this->Model_table_tower_height->get_txt_basedon_row_id($row[0]->towerheight_id)[0]->txt;

		$row[0]->transmissiontypetable = $this->Model_table_transmission_type->get_all();
		
		$row[0]->areatable_txt = $this->Model_table_area->get_txt_basedon_row_id($row[0]->area_id)[0]->txt;
		$row[0]->regiontable_txt = $this->Model_table_region->get_txt_basedon_row_id($row[0]->region_id)[0]->txt;
		$row[0]->provincetable_txt = $this->Model_table_province->get_txt_basedon_row_id($row[0]->province_id)[0]->txt;
		$row[0]->citytable_txt = $this->Model_table_province->get_txt_basedon_row_id($row[0]->province_id)[0]->txt;
		$row[0]->statustable_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;		

		
		echo json_encode($row);
		unset($row);
		
	}	

	public function getProductList()
	{
		echo json_encode($this->Model_table_product->get_all_active());
	}

	public function getTenantList()
	{
		echo json_encode($this->Model_table_tenant->get_all_active());
	}	
	
	public function getAreaList()
	{
		echo json_encode($this->Model_table_area->get_all());
	}
	
	public function getRegionList($area_id)
	{
		if($area_id)
		{
			$where[] = array('key' => 'area_id', 'value' => $area_id);
		}	
		echo json_encode($this->Model_table_region->get_rows($where));
	}

	public function getProvinceList($region_id)
	{
		if($region_id)
		{
			$where[] = array('key' => 'region_id', 'value' => $region_id);
		}	
		echo json_encode($this->Model_table_province->get_rows($where));
	}		
	
	public function getCityList($province_id)
	{
		if($province_id)
		{
			$where[] = array('key' => 'province_id', 'value' => $province_id);
		}	
		echo json_encode($this->Model_table_city->get_rows($where));
	}
	
	public function getSiteTypeList()
	{
		echo json_encode($this->Model_table_site_type->get_all());
	}
	
	public function getTowerHeightList($sitetypeid)
	{
		// if GF - Green Field
		if($sitetypeid == 1)
		{
			$where[] = array('key' => 'row_id != ', 'value' => 1);
			$where[] = array('key' => 'row_id != ', 'value' => 2);
			$where[] = array('key' => 'row_id != ', 'value' => 3);
			$where[] = array('key' => 'row_id != ', 'value' => 5);
		}
		
		// if RT - Roof Top
		if($sitetypeid == 2)
		{
			$where[] = array('key' => 'row_id != ', 'value' => 4);
			$where[] = array('key' => 'row_id != ', 'value' => 6);
			$where[] = array('key' => 'row_id != ', 'value' => 7);
			$where[] = array('key' => 'row_id != ', 'value' => 8);
			$where[] = array('key' => 'row_id != ', 'value' => 9);
			$where[] = array('key' => 'row_id != ', 'value' => 10);
			$where[] = array('key' => 'row_id != ', 'value' => 11);
			$where[] = array('key' => 'row_id != ', 'value' => 12);
			$where[] = array('key' => 'row_id != ', 'value' => 13);
		}
		
		echo json_encode($this->Model_table_tower_height->get_rows($where));
	}
	
	public function getTransmissionTypeList()
	{
		echo json_encode($this->Model_table_transmission_type->get_all());
	}
	
	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}
		
}	