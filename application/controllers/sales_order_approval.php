<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sales_order_approval extends CI_Controller {
    function __construct()
	{
        parent::__construct();
        $this->load->library("template");
        $this->load->library('sendmail');
		$this->load->helper("my_helper");		
 		$this->load->model("Model_table_status");
		$this->load->model("Model_table_user");
		$this->load->model("Model_table_user_territory");
 		$this->load->model("Model_table_work_flow_milestone");	
 		$this->load->model("Model_table_product");
 		$this->load->model("Model_table_site_type");
 		$this->load->model("Model_table_tower_height");
 		$this->load->model("Model_table_area");
 		$this->load->model("Model_table_region");
 		$this->load->model("Model_table_province");
 		$this->load->model("Model_table_city");
 		$this->load->model("Model_table_tenant");
 		$this->load->model("Model_table_role");		
 		$this->load->model("Model_table_site");				
 		$this->load->model("Model_table_sales_order_approval_flow");		
 		$this->load->model("Model_table_sales_order_approval");	
 		$this->load->model("Model_table_sales_order");	
 		$this->load->model("Model_table_sales_order_line");			
	}

    public function index()
	{	
		$show_button_add =  FALSE;		
		
		$salesordertable_row_id = '';
        $salesordertable_salesorder_no = '';
        $salesordertable_salesorder_date = '';
        $salesordertable_drm_date = '';
        $salesordertable_batch = '';
        $salesordertable_po_tenant_no = '';
        $salesordertable_po_tenant_date = '';
        $salesordertable_tenant_name = '';
		$salesordertable_work_flow_milestone = '';
		$salesordertable_approval_progress = '';
		$salesordertable_pending_approval_role = '';
		$salesordertable_approval_result = '';
		$salesordertable_status_name = '';

		$row = '';
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$row = $this->Model_table_sales_order->get_rows($where);				
		unset($where);
		
        if($row != "")
		{
			/**
			 * setting show add button 
			 */	
			 
			$users = ''; 
			$salesorderlinetable_area_id = is_array($this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($row[0]->salesorder_no)) ? $this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($row[0]->salesorder_no)[0]->area_id : 0;			
			
			$where[] = array('key' => 'role_id', 'value' => $this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_sales_order_approval->get_current_state($row[0]->salesorder_no))[0]->role_id);
			$where[] = array('key' => 'status_id', 'value' => 1);
			$usertable = $this->Model_table_user->get_rows($where);
			unset($where);

			foreach($usertable as $usertable_row)
			{
				$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
				
				if(is_array($userterritorytable))
				{
					foreach($userterritorytable as $userterritorytable_row)
					{
						if($userterritorytable_row->territory_id == $salesorderlinetable_area_id)
						{
							$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
							$where[] = array('key' => 'status_id', 'value' => 1);
							$users = $this->Model_table_user->get_rows($where);
							unset($where);
							break 2;
						}
						unset($userterritorytable_row);
					}
				}
				unset($usertable_row);
			}

			if($users == '')
			{
				$users = $usertable;
			}					 
			
			if(($this->session->userdata['SessionLogin']['SesLoginId'] == $users[0]->row_id) && ($row[0]->work_flow_milestone_id != $this->Model_table_sales_order_approval_flow->get_begining_state()[0]->ordering) && ($row[0]->work_flow_milestone_id != $this->Model_table_sales_order_approval_flow->get_ending_state()[0]->ordering))
			{
				$show_button_add 							=  TRUE;
			}

			/** 
			 * Prepare data
			 */
			$salesordertable_row_id 						= $row[0]->row_id;	
			$salesordertable_salesorder_no 					= $row[0]->salesorder_no;
			$salesordertable_salesorder_date 				= $row[0]->salesorder_date == '0000-00-00' || $row[0]->salesorder_date == NULL ? ' - ' : date("d-m-Y", strtotime($row[0]->salesorder_date));
			$salesordertable_drm_date 						= $row[0]->drm_date == '0000-00-00' || $row[0]->drm_date == NULL ? ' - ' : date("d-m-Y", strtotime($row[0]->drm_date));
			$salesordertable_batch 							= $row[0]->batch;
			$salesordertable_po_tenant_no 					= $row[0]->po_tenant_no;
			$salesordertable_po_tenant_date 				= $row[0]->po_tenant_date == '0000-00-00' || $row[0]->po_tenant_date == NULL ? ' - ' : date("d-m-Y", strtotime($row[0]->po_tenant_date));
			$salesordertable_tenant_name 					= $this->Model_table_tenant->get_name_basedon_row_id($row[0]->tenant_id)[0]->name;
			$salesordertable_work_flow_milestone			= $row[0]->work_flow_milestone_id .' - '. $this->Model_table_work_flow_milestone->get_name_basedon_id($row[0]->work_flow_milestone_id)[0]->name . ' State';
			$salesordertable_approval_progress 				= $row[0]->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id ? ' - ': $this->Model_table_sales_order_approval->get_previous_state($row[0]->salesorder_no).' / '.$this->Model_table_sales_order_approval_flow->get_ending_state()[0]->ordering;
			$salesordertable_pending_approval_role 			= my_sales_order_approval_current_role($row[0]->salesorder_no) .'  [ '. $users[0]->name .' ]' ;
			$salesordertable_approval_result 				= my_sales_order_approval_result($row[0]->salesorder_no);			
			$salesordertable_status_name	 				= $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
			
		}
						
		$data = array(
			'lbl_controller' 								=> str_replace("_", " ", $this->uri->segment(1)),
			'show_button_add'								=> $show_button_add,
						
			'salesordertable_row_id' 						=> $salesordertable_row_id,
			'salesordertable_salesorder_no' 				=> $salesordertable_salesorder_no,
			'salesordertable_salesorder_date' 				=> $salesordertable_salesorder_date,
			'salesordertable_drm_date' 						=> $salesordertable_drm_date,
			'salesordertable_batch' 						=> $salesordertable_batch,
			'salesordertable_po_tenant_no' 					=> $salesordertable_po_tenant_no,
			'salesordertable_po_tenant_date' 				=> $salesordertable_po_tenant_date,
			'salesordertable_tenant_name' 					=> $salesordertable_tenant_name,
			'salesordertable_work_flow_milestone' 			=> $salesordertable_work_flow_milestone,
			'salesordertable_approval_progress' 			=> $salesordertable_approval_progress,
			'salesordertable_pending_approval_role' 		=> $salesordertable_pending_approval_role,
			'salesordertable_approval_result' 				=> $salesordertable_approval_result,
			'salesordertable_status_name' 					=> $salesordertable_status_name,
						
        );
		
        $this->template->display_app('page/sales_order_approval_vw', $data);
		
    }
	
    public function gridview()
	{
        $where = $this->input->post("where");
		
		$list = $this->Model_table_sales_order_approval->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		unset($where);
				
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {								
            $no++;          
            $row = array();

			$link = '
						<div class="btn-group">
							<a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';
					
			$users = ''; 

			$salesorderlinetable_area_id = $this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($line->salesorder_no)[0]->area_id;
			
			$where[] = array('key' => 'role_id', 'value' => $this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_sales_order_approval->get_current_state($line->salesorder_no))[0]->role_id);
			$where[] = array('key' => 'status_id', 'value' => 1);
			$usertable = $this->Model_table_user->get_rows($where);
			unset($where);

			foreach($usertable as $usertable_row)
			{
				$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
				
				if(is_array($userterritorytable))
				{
					foreach($userterritorytable as $userterritorytable_row)
					{
						if($userterritorytable_row->territory_id == $salesorderlinetable_area_id)
						{
							$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
							$where[] = array('key' => 'status_id', 'value' => 1);
							$users = $this->Model_table_user->get_rows($where);
							unset($where);
							break 2;
						}
						unset($userterritorytable_row);
					}
				}
				unset($usertable_row);
			}

			if($users == '')
			{
				$users = $usertable;
			}					 
			
			
			if(($this->session->userdata['SessionLogin']['SesLoginId'] == $users[0]->row_id) && ($this->Model_table_sales_order_approval->get_number_of_row_basedon_salesorder_no($line->salesorder_no) < $this->Model_table_sales_order_approval->get_current_state($line->salesorder_no)))
			{			
				$link .= '
								<li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\', \''.$line->approved_status.'\')">Update</a></li>
					';
			}
					
			$link .= '
							</ul>
						</div>
					';
						
			$approved_status = $line->approved_status == 0 ? "Reject" : "Approved";
			
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->approvalflow_id.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->approved_date == '0000-00-00' || $line->approved_date == NULL ? ' - ' : date("d-m-Y h:i:s", strtotime($line->approved_date)).'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_role->get_txt_basedon_row_id($this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($line->approvalflow_id)[0]->role_id)[0]->txt.'</div>';
			$row[] = '<div style="text-align:center">'.$approved_status.'</div>';
            $row[] = '<div style="text-align:center">'.$line->remark.'</div>'; 			
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_sales_order_approval->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form()
	{
		$txtstate          	= $this->input->post("txtstate");
        $txtrowid          	= $this->input->post("txtrowid");
        $txtsalesorderno   	= $this->input->post("txtfilter_salesordertable_salesorder_no");
		$txtapprovalflowid 	= $this->Model_table_sales_order_approval->get_current_state($txtsalesorderno);		
		$txtapproveddate 	= date('d-m-Y H:s:i');		
        $txtapprovedstatus = $this->input->post("txtapprovedstatus");	
		$txtremark = '';
		$txtstatusid = '';
		
		$approved_status_name = $txtapprovedstatus == 0 ? 'Reject' : 'Approved';
		
        if($txtstate == "edit") {
		
 			$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
			$row = $this->Model_table_sales_order_approval->get_rows($where);
			unset($where);
						
            if($row != "") {
                $txtsalesorderno = $row[0]->salesorder_no;				
				$txtapprovalflowid = $row[0]->approvalflow_id;
				$txtapproveddate = $row[0]->approved_date;		
				$txtapprovedstatus = $row[0]->approved_status;		
				$txtremark = $row[0]->remark;
				$txtstatusid = $row[0]->status_id;				
            }
		}
								
        $table = '
					<div class="row">
						<form method="POST" class="formInput" enctype="multipart/form-data">
							<input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />
							<input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" />
							<input type="hidden" id="txtsalesorderno" name="txtsalesorderno" class="form-control" value="'.$txtsalesorderno.'" />
							<input type="hidden" id="txtapprovalflowid" name="txtapprovalflowid" class="form-control" value="'.$txtapprovalflowid.'" />
							<input type="hidden" id="txtstatusid" name="txtstatusid" class="form-control" value="'.$txtstatusid.'" />
							
							<div class="col-md-12">
								<div class="form-group">
								  <label>Date: <span style="color: #ff0000">*</span></label>
								  <input type="hidden" id="txtapproveddate_old" name="txtapproveddate_old" class="form-control" value="'.$txtapproveddate.'" />
								  <input type="text" id="txtapproveddate" name="txtapproveddate" class="form-control" value="'.$txtapproveddate.'" readonly />
								</div>
							</div>
											
							<div class="col-md-6">
							  <div class="form-group">
								<label>Action: <span style="color: #ff0000">*</span></label>
								<input type="hidden" id="txtapprovedstatus_old" name="txtapprovedstatus_old" class="form-control" value="'.$txtapprovedstatus.'" />
								<input type="hidden" id="txtapprovedstatus" name="txtapprovedstatus" class="form-control" value="'.$txtapprovedstatus.'" />
								<input type="text" id="txtapprovedstatusname" name="txtapprovedstatusname" class="form-control" value="'.$approved_status_name.'" readonly/>

							  </div>
							</div> 

							<div class="col-md-12">
								<div class="form-group">
								  <label>Remark: <span style="color: #ff0000">*</span></label>
								  <input type="hidden" id="txtremark_old" name="txtremark_old" class="form-control" value="'.$txtremark.'" />
								  <input type="text" id="txtremark" name="txtremark" class="form-control" value="'.$txtremark.'" />
								</div>
							</div>
							
						</form>
					</div>
							
				';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $txtrowid = $this->input->post("txtrowid");
        $txtapprovalflowid = '';
		$txtapproveddate = '';
        $txtrolename = '';		
		$txtremark = '';				
		$txtstatustablename = '';
		
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
		$row = $this->Model_table_sales_order_approval->get_rows($where);
				
        if($row != "")
		{
			$txtapprovalflowid = $row[0]->approvalflow_id;		
			$txtapproveddate = $row[0]->approved_date == '0000-00-00' || $row[0]->approved_date == NULL ? ' - ' : date("d-m-Y h:i:s", strtotime($row[0]->approved_date)); 
			$txtroletable_name = $this->Model_table_role->get_txt_basedon_row_id($this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($row[0]->approvalflow_id)[0]->role_id)[0]->txt; 
			$txtapprovalaction = $row[0]->approved_status == 0 ? 'Reject' : 'Approved';			
			$txtremark = $row[0]->remark;
			$txtstatustable_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        


        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Approval Ordering</td>
                        <td class="view-txt">: '.$txtapprovalflowid.'</td>
                    </tr>
                  
					<tr>
						<td class="view-title" style="width: 30%">Approver Role Name</td>
						<td class="view-txt">: '.$txtroletable_name.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Date</td>
						<td class="view-txt">: '.$txtapproveddate.'</td>
					</tr>

					
					<tr>
						<td class="view-title" style="width: 30%">Action</td>
						<td class="view-txt">: '.$txtapprovalaction.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Remark</td>
						<td class="view-txt">: '.$txtremark.'</td>
					</tr>
					
                    <tr>
                        <td class="view-title" style="width: 30%">Status:</td>
                        <td class="view-txt">: '.$txtstatustable_name.'</td>
                    </tr>
                  
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {		
        $txtstate = $this->input->post("txtstate");
        $txtrowid = (int)$this->input->post("txtrowid");
        $txtsalesorderno = $this->input->post("txtsalesorderno");
		$txtapprovalflowid = (int)$this->input->post("txtapprovalflowid");		
		$txtapproveddate = $this->input->post("txtapproveddate") == '' || $this->input->post("txtapproveddate") == NULL ? NULL : date("Y-m-d h:i:s", strtotime($this->input->post("txtapproveddate"))) ;
		$txtapprovedstatus = $this->input->post("txtapprovedstatus");		
		$txtremark = $this->input->post("txtremark");		
		$txtstatusid = (int)$this->input->post("txtstatusid");
		
		$salesordertable_row_id = $this->Model_table_sales_order->get_row_id_basedon_salesorder_no($txtsalesorderno)[0]->row_id;
		
		if($txtstate == 'add')
        {
			/**
			 * Add process
			 * 1. Add data to salesorderapprovaltable
			 * 2. Cek approved status 
			 *    --if Approved Cek approval state (salesorderapproval approvalflow id / previous state != salesorderapprovalflow ending state)
			 *    ---------if not ending state
			 *    --------------send mail (loop process)
			 *    ---------else (ending state)
			 *    --------------update work flow miles stone = work flow ending state at salesordel table
			 *    --------------add sales orderline data to site table WHEN not in sitetable (loop processs) 
			 *    --if Not Approved 
			 *    --------------update all approved status =  0 where sales order
			 *    --------------send mail notification to first approver
			 *
			 */
			$insert = array(
				'salesorder_no' => $txtsalesorderno,				
				'approvalflow_id' => $txtapprovalflowid,
				'approved_date' => $txtapproveddate,
				'approved_status' => $txtapprovedstatus,		
				'remark' => $txtremark,	
				'status_id' => 1,
			);						
			$this->Model_table_sales_order_approval->insert($insert);
			unset($insert);
						
			// 2.a. approved_status = 1 - approved
			if($txtapprovedstatus == 1)
			{
				// 2.a.1. approvalflow_id (current state cause add alreadydone) not ending state
				if($txtapprovalflowid < $this->Model_table_sales_order_approval_flow->get_ending_state()[0]->ordering)
				{
					// 2.a.1.1.send mail
					// --------create recipient list -- its current approver through current state
					$users = ''; 

					$salesorderlinetable_area_id = $this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($txtsalesorderno)[0]->area_id;

					$where[] = array('key' => 'role_id', 'value' => $this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_sales_order_approval->get_current_state($txtsalesorderno))[0]->role_id);
					$where[] = array('key' => 'status_id', 'value' => 1);
					$usertable = $this->Model_table_user->get_rows($where);
					unset($where);

					foreach($usertable as $usertable_row)
					{
						$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
						
						if(is_array($userterritorytable))
						{
							foreach($userterritorytable as $userterritorytable_row)
							{
								if($userterritorytable_row->territory_id == $salesorderlinetable_area_id)
								{
									$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
									$where[] = array('key' => 'status_id', 'value' => 1);
									$users = $this->Model_table_user->get_rows($where);
									unset($where);
									break 2;
								}
								unset($userterritorytable_row);
							}
						}
						unset($usertable_row);
					}

					if($users == '')
					{
						$users = $usertable;
					}		
					
					// --------create data for content										
					$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);			
					$where[] = array('key' => 'status_id', 'value' => 1);			
					$salesordertable = $this->Model_table_sales_order->get_rows($where);
					unset($where);
										
					foreach($users as $user)
					{
						$this->sendmail->sales_order_approval
						(
							$user->email,
							$user->name,
							base_url("sales_order_approval/index/".$salesordertable_row_id),
							$salesordertable[0]->salesorder_no,
							$salesordertable[0]->salesorder_date == '0000-00-00' || $salesordertable[0]->salesorder_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->salesorder_date)),
							$salesordertable[0]->drm_date == '0000-00-00' || $salesordertable[0]->drm_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->drm_date)),
							$salesordertable[0]->batch,
							$salesordertable[0]->po_tenant_no,
							$salesordertable[0]->po_tenant_date == '0000-00-00' || $salesordertable[0]->po_tenant_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->po_tenant_date)),							
							$this->Model_table_tenant->get_name_basedon_row_id($salesordertable[0]->tenant_id)[0]->name,
							$txtapprovedstatus,
							$txtremark							
						);
						unset($user);
					}
					unset($users);
					unset($salesordertable);
				}
				
				// 2.a.2. approvalflow_id == ending state
				if($txtapprovalflowid == $this->Model_table_sales_order_approval_flow->get_ending_state()[0]->ordering)
				{
					// 2.a.2.1 set work_flow_milestone_id = 9 at salesordertable
					$update = array(
						'work_flow_milestone_id' => $this->Model_table_work_flow_milestone->get_ending_state()[0]->id,				
					);					
					$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);
					$this->Model_table_sales_order->update($update, $where);					
					unset($update);
					unset($where);
					
					// 2.a.2.2 copy sales order line table to site table 					
					//  ----------extract tenant id from salesordertable					
					$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);
					$where[] = array('key' => 'status_id', 'value' => 1);			
					$salesordertable = $this->Model_table_sales_order->get_rows($where);
					unset($where);
					
					//  ----------extract sales order line data
					$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);
					$where[] = array('key' => 'status_id', 'value' => 1);			
					$salesorderlinetable = $this->Model_table_sales_order_line->get_rows($where);
					unset($where);
					
					//  ----------save to site table					
					foreach($salesorderlinetable as $row)
					{
						$insert = array(
							'product_id' => $row->product_id,
							'towertype_id' => $row->towertype_id,
							'tenant_id' => $salesordertable[0]->tenant_id,
							'site_id_api' => $row->site_id_api,
							'site_name_api' => $row->site_name_api,
							'site_id_tenant' => $row->site_id_tenant,
							'site_name_tenant' => $row->site_name_tenant,
							'area_id' => $row->area_id,	
							'region_id' => $row->region_id,	
							'province_id' => $row->province_id,	
							'city_id' => $row->city_id,	
							'longitude_ori' => $row->longitude,	
							'latitude_ori' => $row->latitude,	
							'address' => '',	
							'sitetype_id' => $row->sitetype_id,	
							'towerheight_id' => $row->towerheight_id,	
							'transmissiontype_id' => 0,							
							'remark' => $row->remarks,	
							'status_id' => 1,
						);						
						$this->Model_table_site->insert($insert);
						unset($insert);					
						unset($row);					
					}
					unset($salesordertable);
					unset($salesorderlinetable);
				}
			}
		
			// 2.a.2. reject
			if($txtapprovedstatus == 0)
			{				
				// 2.a.2.1.  set status id = 0 
				$update = array(
					'status_id' => 2,				
				);					
				$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);
				$where[] = array('key' => 'status_id', 'value' => 1);				
				$this->Model_table_sales_order_approval->update($update, $where);					
				unset($update);
				unset($where);

				// 2.a.2.1 set sales_flow_milestone_id = 1/ creational state at salesordertable
				$update = array(
					'work_flow_milestone_id' => 1,				
				);					
				$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);
				$this->Model_table_sales_order->update($update, $where);					
				unset($update);
				unset($where);				
				
				// 2.a.2.2.  send mail to first approver
				// --------create recipient list -- its begining state 
				$users = ''; 

				$salesorderlinetable_area_id = $this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($txtsalesorderno)[0]->area_id;

				//$where[] = array('key' => 'role_id', 'value' => $this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_sales_order_approval_flow->get_begining_state()[0]->ordering)[0]->role_id);
				$where[] = array('key' => 'role_id', 'value' => 2);	
				$where[] = array('key' => 'status_id', 'value' => 1);
				$usertable = $this->Model_table_user->get_rows($where);
				unset($where);

				foreach($usertable as $usertable_row)
				{
					$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
					
					if(is_array($userterritorytable))
					{
						foreach($userterritorytable as $userterritorytable_row)
						{
							if($userterritorytable_row->territory_id == $salesorderlinetable_area_id)
							{
								$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
								$where[] = array('key' => 'status_id', 'value' => 1);
								$users = $this->Model_table_user->get_rows($where);
								unset($where);
								break 2;
							}
							unset($userterritorytable_row);
						}
					}
					unset($usertable_row);
				}

				if($users == '')
				{
					$users = $usertable;
				}
								
				// --------create data for content										
				$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);			
				$where[] = array('key' => 'status_id', 'value' => 1);			
				$salesordertable = $this->Model_table_sales_order->get_rows($where);
				unset($where);
														
				foreach($users as $user)
				{
					$this->sendmail->sales_order_approval
					(
						$user->email,
						$user->name,
						base_url("sales_order_approval/index/".$salesordertable_row_id),
						$salesordertable[0]->salesorder_no,
						$salesordertable[0]->salesorder_date == '0000-00-00' || $salesordertable[0]->salesorder_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->salesorder_date)),
						$salesordertable[0]->drm_date == '0000-00-00' || $salesordertable[0]->drm_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->drm_date)),

						$salesordertable[0]->batch,
						$salesordertable[0]->po_tenant_no,
						$salesordertable[0]->po_tenant_date == '0000-00-00' || $salesordertable[0]->po_tenant_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->po_tenant_date)),							
						$this->Model_table_tenant->get_name_basedon_row_id($salesordertable[0]->tenant_id)[0]->name,
						$txtapprovedstatus,
						$txtremark							
						
					);
					unset($user);
				}
				unset($users);
				unset($salesordertable);				
			}
	
        }		
        else
        {
			/**
			 * Update process
			 * 1. update data to salesorderapprovaltable
			 * 2. Cek approved status 
			 *    --if Approved status is 0 / Not approved (only this becouce status can be update when approve = 1
			 *    --------------update all approved status =  0 where sales order
			 *    --------------send mail to first approver -- find current state --- user id and email
			 */

			 // 2.a. set status id = 2 not active data
			$update = array(				
				'salesorder_no' => $txtsalesorderno,				
				'approvalflow_id' => $txtapprovalflowid,
				'approved_date' => $txtapproveddate,
				'approved_status' => $txtapprovedstatus,		
				'remark' => $txtremark,	
				'status_id' => $txtstatusid,
            );

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$this->Model_table_sales_order_approval->update($update, $where);					
			unset($update);
			unset($where);

			// 2.b. when approved status  is reject
			if($txtapprovedstatus == 0)
			{				
				// 2.a.2.1.  set status id = 2 rejectted
				$update = array(
					'status_id' => 2,				
				);					
				$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);
				$where[] = array('key' => 'status_id', 'value' => 1);
				$this->Model_table_sales_order_approval->update($update, $where);					
				unset($update);
				unset($where);

				// 2.a.2.1 set sales_flow_milestone_id = 1/ creation state at salesordertable
				$update = array(
					'work_flow_milestone_id' => 1,				
				);					
				$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);
				$this->Model_table_sales_order->update($update, $where);					
				unset($update);
				unset($where);
				
				// 2.a.2.2.  send mail to first approver
				// --------create recipient list -- its begining state
				$users = ''; 

				$salesorderlinetable_area_id = $this->Model_table_sales_order_line->get_area_id_basedon_salesorder_no($txtsalesorderno)[0]->area_id;

				//$where[] = array('key' => 'role_id', 'value' => $this->Model_table_sales_order_approval_flow->get_role_id_basedon_ordering($this->Model_table_sales_order_approval_flow->get_begining_state()[0]->ordering)[0]->role_id);	
				$where[] = array('key' => 'role_id', 'value' => 2);					
				$where[] = array('key' => 'status_id', 'value' => 1);
				$usertable = $this->Model_table_user->get_rows($where);
				unset($where);

				foreach($usertable as $usertable_row)
				{
					$userterritorytable = $this->Model_table_user_territory->get_territory_id_basedon_user_row_id($usertable_row->row_id);
					
					if(is_array($userterritorytable))
					{
						foreach($userterritorytable as $userterritorytable_row)
						{
							if($userterritorytable_row->territory_id == $salesorderlinetable_area_id)
							{
								$where[] = array('key' => 'row_id', 'value' => $usertable_row->row_id);
								$where[] = array('key' => 'status_id', 'value' => 1);
								$users = $this->Model_table_user->get_rows($where);
								unset($where);
								break 2;
							}
							unset($userterritorytable_row);
						}
					}
					unset($usertable_row);
				}

				if($users == '')
				{
					$users = $usertable;
				}
								
				// --------create data for content										
				$where[] = array('key' => 'salesorder_no', 'value' => $txtsalesorderno);			
				$where[] = array('key' => 'status_id', 'value' => 1);			
				$salesordertable = $this->Model_table_sales_order->get_rows($where);
				unset($where);
														
				foreach($users as $user)
				{
					$this->sendmail->sales_order_approval
					(
						$user->email,
						$user->name,
						base_url("sales_order_approval/index/".$salesordertable_row_id),
						$salesordertable[0]->salesorder_no,
						$salesordertable[0]->salesorder_date == '0000-00-00' || $salesordertable[0]->salesorder_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->salesorder_date)),
						$salesordertable[0]->drm_date == '0000-00-00' || $salesordertable[0]->drm_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->drm_date)),

						$salesordertable[0]->batch,
						$salesordertable[0]->po_tenant_no,
						$salesordertable[0]->po_tenant_date == '0000-00-00' || $salesordertable[0]->po_tenant_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->po_tenant_date)),							
						
						$this->Model_table_tenant->get_name_basedon_row_id($salesordertable[0]->tenant_id)[0]->name,
						$txtapprovedstatus,
						$txtremark							
						
						
					);
					unset($user);
				}
				unset($users);
				unset($salesordertable);				
			}
        }
		
		$data['msg'] = "Process successful";
		$data['type'] = 1;
		echo json_encode($data);		
    }
	
	
	/*
	 * 1. Generate SO
	 */
    public function printso()
	{
	    /**
		 * Generate Sales Order
		 *
		 */
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));			
		$salesordertable = $this->Model_table_sales_order->get_rows($where);	
		unset($where);
		
		// query result for $row
		$where[] = array('key' => 'salesorder_no', 'value' => $salesordertable[0]->salesorder_no);			
		$salesorderlinetable = $this->Model_table_sales_order_line->get_rows($where);
		unset($where);
				
		// to set a number of approval column
		$salesorderapprovalflowtable = $this->Model_table_sales_order_approval_flow->get_all();
		
		$where[] = array('key' => 'salesorder_no', 'value' => $salesordertable[0]->salesorder_no);
		$where[] = array('key' => 'status_id', 'value' => 1);		
		$salesorderapprovaltable = $this->Model_table_sales_order_approval->get_rows($where);
		unset($where);
			
		$prnorien = '-L';
        $data = array(
            'primary_label' => "SALES ORDER",
            'primary_no' => $salesordertable[0]->salesorder_no,
            'salesorder_date' => $salesordertable[0]->salesorder_date == '0000-00-00' || $salesordertable[0]->salesorder_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->salesorder_date)),
            'drm_date' => $salesordertable[0]->drm_date == '0000-00-00' ||  $salesordertable[0]->drm_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->drm_date)),
            'tenanttable_name' => $this->Model_table_tenant->get_name_basedon_row_id($salesordertable[0]->tenant_id)[0]->name,
            'tenant_po_no' => $salesordertable[0]->po_tenant_no,
            'tenant_po_date' => $salesordertable[0]->po_tenant_date == '0000-00-00' || $salesordertable[0]->po_tenant_date == NULL ? ' - ' : date("d-m-Y", strtotime($salesordertable[0]->po_tenant_date)),
            'batch' => $salesordertable[0]->batch,
			
            'approvers' => $salesorderapprovaltable,
            'columns' => sizeof($salesorderapprovalflowtable),
			'data' => $salesorderlinetable
        );
		unset($time_list_approve);
		unset($role_list_approve);
		unset($salesorderlinetable);
		unset($salesorderapprovaltable);
		unset($salesorderapprovalflowtable);
		unset($row);

        generatePDF("salesorder_rpt", "sales_order_no_".$salesordertable[0]->salesorder_no, $data, $prnorien);    
		unset($salesordertable);		
    }
		
	
}	

