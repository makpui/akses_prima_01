<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class systemlog extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
    }

    public $table_db = "systemlogtable";
    
    public function index() {
        $controller = $this->uri->segment(1);
        $lbl_controller = str_replace("_", " ", $controller);
        $page_url = base_url($controller);
		$data = array(
            'controller' => $this->uri->segment(1),
            'lbl_controller' => $lbl_controller,
            'page_url' => $page_url,
            'rs_usertable' => getByQuery('select * from usertable'),
                
        );
        $this->template->display_app('page/systemlog_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");        
        $database = "default";
        $column = array('row_id', 'created_by', 'created_date', 'module', 'key_id', 'action', 'data');
        $table = "vw_systemlogtablelist";
        $list = $this->datatables_mdl->get_datatables($database, $table, $column, $where);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="#" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->usertable_name.'</div>'; 
            $row[] = '<div style="text-align:center">'.date("m/d/Y H:i:s", strtotime($line->created_date)).'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->module.'</div>'; 
            $row[] = '<div style="text-align:left">'.$line->action.'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatables_mdl->count_all($database, $table, $column),
            "recordsFiltered" => $this->datatables_mdl->count_filtered($database, $table, $column, $where),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
        $txtcreatedby = ''; 
$usertable_list = "";$txtusertablename = ''; 
$txtcreateddate = ''; 
$txtmodule = ''; 
$txtkeyid = ''; 
$txtaction = ''; 
$txtdata = ''; 


        if($txtstate == "edit") {
            $qry = "select * from vw_systemlogtablelist where row_id = '".$txtrowid."'";
            $rs = getByQuery($qry);
            if($rs != "") {
                $txtcreatedby = $rs[0]->created_by; 
$txtusertablename = $rs[0]->usertable_name; 
$txtcreateddate = date('Y-m-d', strtotime($rs[0]->created_date)); 
$txtmodule = $rs[0]->module; 
$txtkeyid = $rs[0]->key_id; 
$txtaction = $rs[0]->action; 
$txtdata = $rs[0]->data; 

            }
        }

        
                  $rs_usertable = getByQuery("select * from usertable");
                  if($rs_usertable != "") {
                      foreach($rs_usertable as $row_usertable) {
                          $selected = ($row_usertable->row_id == $txtcreatedby) ? "selected" : "";
                          $usertable_list .= "<option value='".$row_usertable->row_id."' ".$selected." >".$row_usertable->name."</option>";
                      }
                  }
                

        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Created By: <span style="color: #ff0000">*</span></label>
                      <input type="hidden" id="txtcreatedby_old" name="txtcreatedby_old" class="form-control" value="'.$txtcreatedby.'" />
                      <select id="txtcreatedby" name="txtcreatedby" class="form-control select2"  required>
                        <option value=""></option>
                        '.$usertable_list.'
                      </select>
                    </div>
                  </div>
                
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Created Date:</label>
                        <input type="hidden" id="txtcreateddate_old" name="txtcreateddate_old" class="form-control" value="'.$txtcreateddate.'" />
                        <input type="date" id="txtcreateddate" name="txtcreateddate" class="form-control" value="'.$txtcreateddate.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Module:</label>
                        <input type="hidden" id="txtmodule_old" name="txtmodule_old" class="form-control" value="'.$txtmodule.'" />
                        <input type="text" id="txtmodule" name="txtmodule" class="form-control" value="'.$txtmodule.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Key Id:</label>
                        <input type="hidden" id="txtkeyid_old" name="txtkeyid_old" class="form-control" value="'.$txtkeyid.'" />
                        <input type="number" step="1" " id="txtkeyid" name="txtkeyid" class="form-control" value="'.$txtkeyid.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Action:</label>
                        <input type="hidden" id="txtaction_old" name="txtaction_old" class="form-control" value="'.$txtaction.'" />
                        <textarea id="txtaction" name="txtaction" rows="4" class="form-control">'.$txtaction.'</textarea>
                      </div>
                    </div>
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Data:</label>
                        <input type="hidden" id="txtdata_old" name="txtdata_old" class="form-control" value="'.$txtdata.'" />
                        <textarea id="txtdata" name="txtdata" rows="4" class="form-control">'.$txtdata.'</textarea>
                      </div>
                    </div>
                  
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
        $txtcreatedby = ''; 
$usertable_list = "";$txtusertablename = ''; 
$txtcreateddate = ''; 
$txtmodule = ''; 
$txtkeyid = ''; 
$txtaction = ''; 
$txtdata = ''; 

        
        $qry = "select * from vw_systemlogtablelist where row_id = '".$txtrowid."'";
        $rs = getByQuery($qry);
        if($rs != "") {
                $txtcreatedby = $rs[0]->created_by; 
$txtusertablename = $rs[0]->usertable_name; 
$txtcreateddate = date('Y-m-d', strtotime($rs[0]->created_date)); 
$txtmodule = $rs[0]->module; 
$txtkeyid = $rs[0]->key_id; 
$txtaction = $rs[0]->action; 
$txtdata = $rs[0]->data; 

        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                  <tr>
                      <td class="view-title" style="width: 30%">Created By:</td>
                      <td class="view-txt">'.$txtusertablename.'</td>
                  </tr>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Created Date:</td>
                        <td class="view-txt">'.$txtcreateddate.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Module:</td>
                        <td class="view-txt">'.$txtmodule.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Key Id:</td>
                        <td class="view-txt">'.$txtkeyid.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Action:</td>
                        <td class="view-txt">'.$txtaction.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Data:</td>
                        <td class="view-txt">'.$txtdata.'</td>
                    </tr>
                  
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $controller = $this->uri->segment(1);
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
        $txtcreatedby = $this->input->post("txtcreatedby"); $txtcreateddate = $this->input->post("txtcreateddate"); $txtmodule = $this->input->post("txtmodule"); $txtkeyid = $this->input->post("txtkeyid"); $txtaction = $this->input->post("txtaction"); $txtdata = $this->input->post("txtdata"); 

        

        if($txtstate == 'add')
        {
            $insert = array(
                'created_by' => $txtcreatedby, 'created_date' => date("Y-m-d H:i:s", strtotime($txtcreateddate)), 'module' => $txtmodule, 'key_id' => $txtkeyid, 'action' => $txtaction, 'data' => $txtdata,         
            );
            insertData("Insert ".$this->table_db, $controller, $this->table_db, $insert);
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'created_by' => $txtcreatedby, 'created_date' => date("Y-m-d H:i:s", strtotime($txtcreateddate)), 'module' => $txtmodule, 'key_id' => $txtkeyid, 'action' => $txtaction, 'data' => $txtdata,              
            );
            updateData("Update ".$this->table_db, $controller, $this->table_db, $update, "row_id = '".$txtrowid."'", $txtrowid);
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $controller = $this->uri->segment(1);
        $txtrowid = $this->input->post("txtrowid");
        
        deleteData("Delete ".$this->table_db, $controller, $this->table_db, "row_id = '".$txtrowid."'", $txtrowid);
        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }
}	