<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class role extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_role");
    }
    
    public function index() {
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1))                
        );
        $this->template->display_app('page/role_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");
		
		$list = $this->Model_table_role->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
					<li><a href="'.base_url("role_menu/index")."/".$line->row_id.'" style="color:#111" >Role Menu</a></li>					
					<li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
            ';

			if($line->row_id != 1 &&  $line->row_id != 2 &&  $line->row_id != 5)
			{
            $link .= '					
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
            ';
			}		
			
            $link .= '					
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
			$row[] = '<div style="text-align:center">'.$line->txt.'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_role->get_all()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
		$txttxt = ''; 

        if($txtstate == "edit")
		{
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$row = $this->Model_table_role->get_rows($where);
		
            if($row != "") {
                $txttxt = $row[0]->txt; 
            }
        }
	  
        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

					<div class="col-md-12">
						<div class="form-group">
							<label>Role Name: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txttxt_old" name="txttxt_old" class="form-control" value="'.$txttxt.'" />
							<input type="text" id="txttxt" name="txttxt" class="form-control" value="'.$txttxt.'" />
						</div>
					</div>
              
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
		$txttxt = ''; 
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_role->get_rows($where);

        if($row != "") {
			$txttxt = $row[0]->txt; 
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
			  
					<tr>
						<td class="view-title" style="width: 30%">Role Name:</td>
						<td class="view-txt">'.$txttxt.'</td>
					</tr>
              
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = (int)$this->input->post("txtrowid");
		$txttxt = $this->input->post("txttxt");

        if($txtstate == 'add')
        {
            $insert = array(
				'txt' => $txttxt,
            );
			
			$this->Model_table_role->insert($insert);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
				'txt' => $txttxt,
            );
			
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
            $this->Model_table_role->update($update, $where);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_role->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }
}	