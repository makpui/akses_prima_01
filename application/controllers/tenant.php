<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tenant extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_status");
		$this->load->model("Model_table_tenant");
 		$this->load->model("Model_table_role");
 		$this->load->model("Model_table_user");		
    }
    
    public function index() {
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),
            'user_role_id' => $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id,
			
        );
        $this->template->display_app('page/tenant_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");
		
		$list = $this->Model_table_tenant->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';
					
			if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 2)
			{					
            $link .= '
                    <li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					';
			}
			
            $link .= '
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->name.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->email.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->phone_no.'</div>'; 
            $row[] = '<div style="text-align:left">'.$line->address.'</div>'; 
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_tenant->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
        $txtname = ''; 
		$txtemail = ''; 
		$txtphoneno = ''; 
		$txtaddress = ''; 
		$txtstatusid = '';
		
		$option_status = '<option value="1">Active</option>';

        if($txtstate == "edit") {
		
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$row = $this->Model_table_tenant->get_rows($where);

            if($row != "") {
                $txtname = $row[0]->name; 
				$txtemail = $row[0]->email; 
				$txtphoneno = $row[0]->phone_no; 
				$txtaddress = $row[0]->address; 
				$txtstatusid = $row[0]->status_id; 
            }
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';
        }
		unset($where);
		unset($row);

        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

                    
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Company: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtname_old" name="txtname_old" class="form-control" value="'.$txtname.'" />
                        <input type="text" id="txtname" name="txtname" class="form-control" value="'.$txtname.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Contact Email: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtemail_old" name="txtemail_old" class="form-control" value="'.$txtemail.'" />
                        <input type="text" id="txtemail" name="txtemail" class="form-control" value="'.$txtemail.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Contact Phone No: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtphoneno_old" name="txtphoneno_old" class="form-control" value="'.$txtphoneno.'" />
                        <input type="text" id="txtphoneno" name="txtphoneno" class="form-control" placeholder="(555) 555-5555" value="'.$txtphoneno.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Address:</label>
                        <input type="hidden" id="txtaddress_old" name="txtaddress_old" class="form-control" value="'.$txtaddress.'" />
                        <textarea id="txtaddress" name="txtaddress" rows="4" class="form-control">'.$txtaddress.'</textarea>
                      </div>
                    </div>
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Status: <span style="color: #ff0000">*</span></label>
                      <input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
                      <select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
							'.$option_status.'                    
                      </select>
                    </div>
                  </div>
                
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
        $txtname = ''; 
		$txtemail = ''; 
		$txtphoneno = ''; 
		$txtaddress = ''; 
		$txtstatustablename = ''; 

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_tenant->get_rows($where);
        
        if($row != "") {
            $txtname = $row[0]->name; 
			$txtemail = $row[0]->email; 
			$txtphoneno = $row[0]->phone_no; 
			$txtaddress = $row[0]->address; 
			$txtstatustablename = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Company:</td>
                        <td class="view-txt">'.$txtname.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Contact Email:</td>
                        <td class="view-txt">'.$txtemail.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Contact Phone No:</td>
                        <td class="view-txt">'.$txtphoneno.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Address:</td>
                        <td class="view-txt">'.$txtaddress.'</td>
                    </tr>
                  
                  <tr>
                      <td class="view-title" style="width: 30%">Status:</td>
                      <td class="view-txt">'.$txtstatustablename.'</td>
                  </tr>
                
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $controller = $this->uri->segment(1);
        $txtstate = $this->input->post("txtstate");
        $txtrowid = (int)$this->input->post("txtrowid");
        $txtname = $this->input->post("txtname"); 
		$txtemail = $this->input->post("txtemail"); 
		$txtphoneno = $this->input->post("txtphoneno");
		$txtaddress = $this->input->post("txtaddress");
		$txtstatusid = (int)$this->input->post("txtstatusid");         

        if($txtstate == 'add')
        {
            $insert = array(
                'name' => $txtname,
				'email' => $txtemail,
				'phone_no' => $txtphoneno,
				'address' => $txtaddress,
				'status_id' => $txtstatusid,         
            );
			
			$this->Model_table_tenant->insert($insert);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'name' => $txtname,
				'email' => $txtemail,
				'phone_no' => $txtphoneno,
				'address' => $txtaddress,
				'status_id' => $txtstatusid,              
            );
			
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
            $this->Model_table_tenant->update($update, $where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $controller = $this->uri->segment(1);
        $txtrowid = $this->input->post("txtrowid");
		
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_tenant->delete($where);
        
        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }

	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}

}	