<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class vendor extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_status");
		$this->load->model("Model_table_vendor_type");
		$this->load->model("Model_table_vendor");
 		$this->load->model("Model_table_role");
 		$this->load->model("Model_table_user");			
    }
    
    public function index() {
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),
            'user_role_id' => $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id,
        );
        $this->template->display_app('page/vendor_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");
		
		$list = $this->Model_table_vendor->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="'.base_url("vendor_contact/index")."/".$line->row_id.'" style="color:#111" >Vendor Contact</a></li>
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';
					
			if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 5)
			{					
            $link .= '
                    <li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					';
			}
			
            $link .= '
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_vendor_type->get_txt_basedon_row_id($line->vendortype_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->id.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->name.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->address.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->phone_no.'</div>';
            $row[] = '<div style="text-align:center">'.$line->email.'</div>';			
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_vendor->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
		
        $txtvendortypeid = ''; 
        $txtid = ''; 
        $txtname = ''; 
        $txtaddress = ''; 
        $txtphoneno = ''; 
        $txtemail = '';
		$txtstatusid = ''; 
		
		$option_status = '<option value="1">Active</option>';
		$option_vendor_type = '<option value="">View All Vendor Type</option>';

        if($txtstate == "edit") {
		
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$row = $this->Model_table_vendor->get_rows($where);
			
            if($row != "") {
                $txtvendortypeid = $row[0]->vendortype_id; 
                $txtid = $row[0]->id; 
                $txtname = $row[0]->name; 
                $txtaddress = $row[0]->address; 
                $txtphoneno = $row[0]->phone_no; 
                $txtemail = $row[0]->email; 
                $txtstatusid = $row[0]->status_id;
            }
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';
			$option_vendor_type = '<option value="'.$txtvendortypeid.'" selected >'.$this->Model_table_vendor_type->get_txt_basedon_row_id($txtvendortypeid)[0]->txt.'</option>';
        }
		unset($where);
		unset($row);

        
        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Type: <span style="color: #ff0000">*</span></label>
                      <input type="hidden" id="txtvendortypeid_old" name="txtvendortypeid_old" class="form-control" value="'.$txtvendortypeid.'" />
                      <select id="txtvendortypeid" name="txtvendortypeid" class="form-control select2"  required>
							'.$option_vendor_type.'                    
                      </select>
                    </div>
                  </div>
                    
					
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Id: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtid_old" name="txtid_old" class="form-control" value="'.$txtid.'" />
                        <input type="text" id="txtid" name="txtid" class="form-control" value="'.$txtid.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Name: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtname_old" name="txtname_old" class="form-control" value="'.$txtname.'" />
                        <input type="text" id="txtname" name="txtname" class="form-control" value="'.$txtname.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Address: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtaddress_old" name="txtaddress_old" class="form-control" value="'.$txtaddress.'" />
                        <input type="text" id="txtaddress" name="txtaddress" class="form-control" value="'.$txtaddress.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Phone: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtphoneno_old" name="txtphoneno_old" class="form-control" value="'.$txtphoneno.'" />
                        <input type="text" id="txtphoneno" name="txtphoneno" class="form-control" value="'.$txtphoneno.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Email: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtemail_old" name="txtemail_old" class="form-control" value="'.$txtemail.'" />
                        <input type="text" id="txtemail" name="txtemail" class="form-control" value="'.$txtemail.'" />
                      </div>
                    </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label>Status: <span style="color: #ff0000">*</span></label>
                    <input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
                    <select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
							'.$option_status.'                    
                    </select>
                  </div>
                </div>
				  
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");

        $txtid = ''; 
        $txtname = ''; 
        $txtaddress = ''; 
        $txtphoneno = ''; 
        $txtemail = '';
		
		$txtvendortypetabletxt = '';		
		$txtstatustablename = ''; 
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_vendor->get_rows($where);

        if($row != "") {
				$txtvendortypetabletxt = $this->Model_table_vendor_type->get_txt_basedon_row_id($row[0]->vendortype_id)[0]->txt;
                $txtid = $row[0]->id; 
                $txtname = $row[0]->name; 
                $txtaddress = $row[0]->address; 
                $txtphoneno = $row[0]->phone_no; 
                $txtemail = $row[0]->email;
				$txtstatustablename = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Vendor Type:</td>
                        <td class="view-txt">'.$txtvendortypetabletxt.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Id:</td>
                        <td class="view-txt">'.$txtid.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Name:</td>
                        <td class="view-txt">'.$txtname.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Address:</td>
                        <td class="view-txt">'.$txtaddress.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Phone:</td>
                        <td class="view-txt">'.$txtphoneno.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">email:</td>
                        <td class="view-txt">'.$txtemail.'</td>
                    </tr>
                  				  
                <tr>
                    <td class="view-title" style="width: 30%">Status:</td>
                    <td class="view-txt">'.$txtstatustablename.'</td>
                </tr>
                
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = (int)$this->input->post("txtrowid");
		
        $txtvendortypeid = (int)$this->input->post("txtvendortypeid"); 
        $txtid = $this->input->post("txtid"); 
        $txtname = $this->input->post("txtname"); 
        $txtaddress = $this->input->post("txtaddress"); 
        $txtphoneno = $this->input->post("txtphoneno"); 
        $txtemail = $this->input->post("txtemail");
		$txtstatusid = (int)$this->input->post("txtstatusid");     
		

        if($txtstate == 'add')
        {
            $insert = array(
                'vendortype_id' => $txtvendortypeid,
				'id' => $txtid,
				'name' => $txtname,
				'email' => $txtemail,
				'phone_no' => $txtphoneno,
				'address' => $txtaddress,
				'status_id' => $txtstatusid,  	
            );
			
			$this->Model_table_vendor->insert($insert);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            $data['id'] = $txtid;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'vendortype_id' => $txtvendortypeid,
				'id' => $txtid,
				'name' => $txtname,
				'email' => $txtemail,
				'phone_no' => $txtphoneno,
				'address' => $txtaddress,
				'status_id' => $txtstatusid,         

            );
			
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
            $this->Model_table_vendor->update($update, $where);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            $data['id'] = $txtid;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_vendor->delete($where);
        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }

	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}
	
	public function getVendorTypeList()
	{
		echo json_encode($this->Model_table_vendor_type->get_all_active());
	}
	
}	