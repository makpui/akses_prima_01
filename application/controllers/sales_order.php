<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sales_order extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->library("template");
		$this->load->helper("my_helper");
 		$this->load->model("Model_table_status");
 		$this->load->model("Model_table_user");
 		$this->load->model("Model_table_work_flow_milestone");
 		$this->load->model("Model_table_tenant");
 		$this->load->model("Model_table_role");
 		$this->load->model("Model_table_sales_order_approval_flow");
 		$this->load->model("Model_table_sales_order_approval");
 		$this->load->model("Model_table_sales_order_line");
 		$this->load->model("Model_table_sales_order");
    }

    public function index()
	{
		$is_so_maker = ($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 2 ) ?   1 : 0 ;

		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),
			'is_so_maker'    =>  $is_so_maker
        );
        $this->template->display_app('page/sales_order_vw', $data);
    }

    public function gridview()
	{
        $where = $this->input->post("where");

		$list = $this->Model_table_sales_order->get_rows($where);
		if(!is_array($list)) { $list = array(); }

		$data = array();
        $no = $_POST['start'];
        foreach ($list as $line)
        {
			$no++;
            $row = array();

            $link = '
						<div class="btn-group">
							<a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="'.base_url("sales_order_line/index")."/".$line->row_id.'" style="color:#111" >Sales Order Line</a></li>
								<li><a href="'.base_url("sales_order_approval/index")."/".$line->row_id.'" style="color:#111" >Sales Order Approval</a></li>
								<li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';

			/**
			 * Update & Delete button show in creation stage only
			 *
			 */
			if($line->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id)
			{
				if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 2)
				{
					$link .= '
								<li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
					';
				}
				
				if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1)
				{
					
					$link .= '
								<li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					';
				}					
			}

            $link .= '
							</ul>
						</div>
					';

			/**
			 * Handling empty date & date format
			 * because this process cannot done in array
			 */
			$sodate = ($line->salesorder_date == '0000-00-00' || $line->salesorder_date == NULL) ? '-' : date("d-m-Y", strtotime($line->salesorder_date));
			$drmdate = ($line->drm_date == '0000-00-00' ||  $line->drm_date == NULL) ? '-' : date("d-m-Y", strtotime($line->drm_date));
			$potenantdate = ($line->po_tenant_date == '0000-00-00' || $line->po_tenant_date == NULL) ? '-' : date("d-m-Y", strtotime($line->po_tenant_date));

			// create progress of approval
			$approval_progress = $line->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id ? '0 / 0': $this->Model_table_sales_order_approval->get_previous_state($line->salesorder_no).' / '.$this->Model_table_sales_order_approval_flow->get_ending_state()[0]->ordering;

			$salesorderlinetable = $this->Model_table_sales_order_line->get_row_id_basedon_salesorder_no($line->salesorder_no);
			$salesorderlinetable_numberofline = is_array($salesorderlinetable) ? sizeof($salesorderlinetable) : 0;


			$row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->salesorder_no.'</div>';
            $row[] = '<div style="text-align:center">'.$sodate.'</div>';
			$row[] = '<div style="text-align:center">'.$drmdate.'</div>';
            $row[] = '<div style="text-align:center">'.$line->batch.'</div>';
            $row[] = '<div style="text-align:center">'.$line->po_tenant_no.'</div>';
            $row[] = '<div style="text-align:center">'.$potenantdate.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_tenant->get_name_basedon_row_id($line->tenant_id)[0]->name.'</div>';

			$row[] = '<div style="text-align:center">'.$salesorderlinetable_numberofline.'</div>';

			$row[] = '<div style="text-align:center">'.$this->Model_table_work_flow_milestone->get_name_basedon_id($line->work_flow_milestone_id)[0]->name.' State</div>';
            $row[] = '<div style="text-align:center">'.$approval_progress.'</div>';
			$row[] = '<div style="text-align:center">'.my_sales_order_approval_current_user($line->salesorder_no).'</div>';
            $row[] = '<div style="text-align:center">'.my_sales_order_approval_result($line->salesorder_no).'</div>';
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_sales_order->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");

        $txtrowid = $this->input->post("txtrowid");
        $txtsalesorderno = 'Generated by system';
        $txtsalesorderdate = '';
        $txtdrmdate = '';
        $txtbatch = '';
        $txtpotenantno = '';
        $txtpotenantdate = '';
        $txttenantid = '';
		$txtworkflowmilestoneid = 1;
		$txtstatusid = '';

		$option_tenant = '<option value="">--Select a Tenant--</option>';
		$option_status = '<option value="1">Active</option>';

        if($txtstate == "edit") {

 			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$row = $this->Model_table_sales_order->get_rows($where);

            if($row != "") {
                $txtsalesorderno = $row[0]->salesorder_no;
				$txtsalesorderdate = ($row[0]->salesorder_date == "0000-00-00" || $row[0]->salesorder_date == NULL)? '' : date("d-m-Y", strtotime($row[0]->salesorder_date));
				$txtdrmdate = ($row[0]->drm_date == "0000-00-00" || $row[0]->drm_date == NULL )? '' : date("d-m-Y", strtotime($row[0]->drm_date));
                $txtbatch = $row[0]->batch;
                $txtpotenantno = $row[0]->po_tenant_no;
				$txtpotenantdate = ($row[0]->po_tenant_date == "0000-00-00" || $row[0]->po_tenant_date == NULL) ? '' : date("d-m-Y", strtotime($row[0]->po_tenant_date));
                $txttenantid = $row[0]->tenant_id;
				$txtworkflowmilestoneid = $row[0]->work_flow_milestone_id;
                $txtstatusid = $row[0]->status_id;
            }
			$option_tenant = '<option value="'.$txttenantid.'" selected >'.$this->Model_table_tenant->get_name_basedon_row_id($txttenantid)[0]->name.'</option>';
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';
        }

        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />
                    <input type="hidden" id="txtworkflowmilestoneid" name="txtworkflowmilestoneid" class="form-control" value="'.$txtworkflowmilestoneid.'" />

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Sales Order No.: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtsalesorderno_old" name="txtsalesorderno_old" class="form-control" value="'.$txtsalesorderno.'" />
                        <input type="text" id="txtsalesorderno" name="txtsalesorderno" class="form-control" value="'.$txtsalesorderno.'" readonly />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Sales Order Date: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtsalesorderdate_old" name="txtsalesorderdate_old" class="form-control" value="'.$txtsalesorderdate.'" />
                        <input type="txt" id="txtsalesorderdate" name="txtsalesorderdate" class="form-control" value="'.$txtsalesorderdate.'" />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>DRM Date: </label>
                        <input type="hidden" id="txtdrmdate_old" name="txtdrmdate_old" class="form-control" value="'.$txtdrmdate.'" />
                        <input type="txt" id="txtdrmdate" name="txtdrmdate" class="form-control" value="'.$txtdrmdate.'" />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Batch:</label>
                        <input type="hidden" id="txtbatch_old" name="txtbatch_old" class="form-control" value="'.$txtbatch.'" />
                        <input type="text" id="txtbatch" name="txtbatch" class="form-control" value="'.$txtbatch.'" />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>PO No. Tenant: </label>
                        <input type="hidden" id="txtpotenantno_old" name="txtpotenantno_old" class="form-control" value="'.$txtpotenantno.'" />
                        <input type="text" id="txtpotenantno" name="txtpotenantno" class="form-control" value="'.$txtpotenantno.'" />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>PO Date Tenant: </label>
                        <input type="hidden" id="txtpotenantdate_old" name="txtpotenantdate_old" class="form-control" value="'.$txtpotenantdate.'" />
                        <input type="txt" id="txtpotenantdate" name="txtpotenantdate" class="form-control" value="'.$txtpotenantdate.'" />
                      </div>
                    </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tenant: <span style="color: #ff0000">*</span></label>
                      <input type="hidden" id="txttenantid_old" name="txttenantid_old" class="form-control" value="'.$txttenantid.'" />
                      <select id="txttenantid" name="txttenantid" class="form-control select2"  required>
                        '.$option_tenant.'
                      </select>
                    </div>
                  </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Status: <span style="color: #ff0000">*</span></label>
                    <input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
                    <select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
						'.$option_status .'
                    </select>
                  </div>
                </div>

                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $txtrowid = $this->input->post("txtrowid");
        $txtsalesorderno = '';
        $txtsalesorderdate = '';
        $txtdrmdate = '';
        $txtbatch = '';
        $txtpotenantno = '';
        $txtpotenantdate = '';
        $txttenanttable_id = '';
		$txtworkflowmilestonetable_name = '';
		$txtstatustable_name = '';

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_sales_order->get_rows($where);

        if($row != "") {
			$txtsalesorderno = $row[0]->salesorder_no;
			$txtsalesorderdate = $row[0]->salesorder_date == "0000-00-00" || $row[0]->salesorder_date == NULL ? ' - ' : date("d-m-Y", strtotime($row[0]->salesorder_date));
			$txtdrmdate = $row[0]->drm_date == "0000-00-00" || $row[0]->drm_date ==  NULL ? ' - ' : date("d-m-Y", strtotime($row[0]->drm_date));
			$txtbatch = $row[0]->batch;
			$txtpotenantno = $row[0]->po_tenant_no;
			$txtpotenantdate = $row[0]->po_tenant_date == "0000-00-00" || $row[0]->po_tenant_date == NULL? ' - ' : date("d-m-Y", strtotime($row[0]->po_tenant_date));
			$txttenanttable_name = $this->Model_table_tenant->get_name_basedon_row_id($row[0]->tenant_id)[0]->name;
			$txtworkflowmilestonetable_name =  $this->Model_table_work_flow_milestone->get_name_basedon_id($row[0]->work_flow_milestone_id)[0]->name;
			$txtstatustable_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }


        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>

				<tr>
					<td class="view-title" style="width: 30%">Sales Order No.:</td>
					<td class="view-txt">'.$txtsalesorderno.'</td>
				</tr>

				<tr>
					<td class="view-title" style="width: 30%">Sales Order Date:</td>
					<td class="view-txt">'.$txtsalesorderdate.'</td>
				</tr>

				<tr>
					<td class="view-title" style="width: 30%">DRM Date:</td>
					<td class="view-txt">'.$txtdrmdate.'</td>
				</tr>

				<tr>
					<td class="view-title" style="width: 30%">Batch:</td>
					<td class="view-txt">'.$txtbatch.'</td>
				</tr>

				<tr>
					<td class="view-title" style="width: 30%">PO No. Tenant:</td>
					<td class="view-txt">'.$txtpotenantno.'</td>
				</tr>

				<tr>
					<td class="view-title" style="width: 30%">PO Date Tenant:</td>
					<td class="view-txt">'.$txtpotenantdate.'</td>
				</tr>

				<tr>
				  <td class="view-title" style="width: 30%">Tenant:</td>
				  <td class="view-txt">'.$txttenanttable_name.'</td>
				</tr>

				<tr>
				  <td class="view-title" style="width: 30%">Approval Milestone:</td>
				  <td class="view-txt">'.$txtworkflowmilestonetable_name.' State</td>
				</tr>

                <tr>
                    <td class="view-title" style="width: 30%">Status:</td>
                    <td class="view-txt">'.$txtstatustable_name.'</td>
                </tr>

              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");

        $txtrowid = (int)$this->input->post("txtrowid");
		$txtsalesorderdate = $this->input->post("txtsalesorderdate") == '' || $this->input->post("txtsalesorderdate") == NULL ? NULL : date("Y-m-d", strtotime($this->input->post("txtsalesorderdate")));
		$txtsalesorderno = $this->input->post("txtsalesorderno");
		$txtdrmdate = $this->input->post("txtdrmdate") == '' || $this->input->post("txtdrmdate") == NULL ? NULL : date("Y-m-d", strtotime($this->input->post("txtdrmdate")));
        $txtbatch = $this->input->post("txtbatch");
        $txtpotenantno = $this->input->post("txtpotenantno");
		$txtpotenantdate = $this->input->post("txtpotenantdate") == '' || $this->input->post("txtpotenantdate") == NULL ? NULL : date("Y-m-d", strtotime($this->input->post("txtpotenantdate")));
		$txttenantid = (int)$this->input->post("txttenantid");
		$txtworkflowmilestoneid = (int)$this->input->post("txtworkflowmilestoneid");
		$txtstatusid = (int)$this->input->post("txtstatusid");

        if($txtstate == 'add')
        {
            $insert = array(
                'salesorder_no' => generateSONo($txtsalesorderdate),
				'salesorder_date' => $txtsalesorderdate,
				'drm_date' => $txtdrmdate,
				'batch' => $txtbatch,
				'po_tenant_no' => $txtpotenantno,
				'po_tenant_date' => $txtpotenantdate,
				'tenant_id' => $txttenantid,
				'work_flow_milestone_id' => $txtworkflowmilestoneid,
				'status_id' => $txtstatusid,

            );

			$this->Model_table_sales_order->insert($insert);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            $data['salesorder_no'] = $txtsalesorderno;
            echo json_encode($data);
        }
        else
        {
            $update = array(
                'salesorder_no' => $txtsalesorderno,
				'salesorder_date' => $txtsalesorderdate,
				'drm_date' => $txtdrmdate,
				'batch' => $txtbatch,
				'po_tenant_no' => $txtpotenantno,
				'po_tenant_date' => $txtpotenantdate,
				'tenant_id' => $txttenantid,
				'work_flow_milestone_id' => $txtworkflowmilestoneid,
				'status_id' => $txtstatusid,
            );

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$this->Model_table_sales_order->update($update, $where);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            $data['salesorder_no'] = $txtsalesorderno;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_sales_order->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }

	public function getTenantList()
	{
		echo json_encode($this->Model_table_tenant->get_all_active());
	}

	public function getWorkFlowMilestoneList()
	{
		echo json_encode($this->Model_table_work_flow_milestone->get_all());
	}

	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}


}
