<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pending_approval extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");		
        $this->load->library('sendmail');
		$this->load->helper("my_helper");
		$this->load->model("Model_table_user");		
    }
    
    public function index()
	{
		$data = array(
			'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),
			'lbl_session_role_id' => $this->session->userdata['SessionLogin']['SesUserRoleId'],
			
        );		
		
        $this->template->display_app('page/pending_approval_vw', $data);
    }	
	
    public function gridview()
	{
		$list = array();
		$sessionroleid = $this->session->userdata['SessionLogin']['SesUserRoleId']; 
		if($sessionroleid == 1)
		{
			$list = my_get_all_pending_approval_list();
		}
		elseif($sessionroleid == 2)
		{
			foreach(my_get_all_pending_approval_list() as $row)
			{
				if($row->main_controller == 'sales_order')
				{
					$list[] = $row;
				}
				unset($row);
			}
		}
		elseif($sessionroleid == 5)
		{
			foreach(my_get_all_pending_approval_list() as $row)
			{
				if($row->main_controller == 'work_order')
				{
					$list[] = $row;
				}
				unset($row);
			}
		}
		else
		{
			$list = my_get_pending_approval_list($this->session->userdata['SessionLogin']['SesLoginId']);
		}
		
        $data = array();
        $no = $_POST['start'];
		
		foreach ($list as $line) 
		{
			$no++;          
			$row = array();

			
			$link = '';
			if($line->current_user_row_id == $this->session->userdata['SessionLogin']['SesLoginId'])
			{
			$link .= '
						<button style="color: green;" onclick="window.location.href=\''.base_url($line->main_controller."_approval/index").'/'.$line->document_row_id.'#1\'"><strong>Approve</strong></button>					
						<button style="color: red;" onclick="window.location.href=\''.base_url($line->main_controller."_approval/index").'/'.$line->document_row_id.'#2\'"><strong>Reject</strong></button>
					';
			}
			
			$link .= '
						<button onclick="window.location.href=\''.base_url($line->main_controller."_line/index").'/'.$line->document_row_id.'\'"><strong>View</strong></button>					
					';
			
			$row[] = '<div style="text-align:center">'.$link.'</div>';
			if($sessionroleid == 1 || $sessionroleid == 2 || $sessionroleid == 5)
			{
			$row[] = '<div style="text-align:center">'.$this->Model_table_user->get_name_basedon_row_id($line->current_user_row_id)[0]->name.'</div>';
			}
			$row[] = '<div style="text-align:center">'.ucfirst(str_replace('_', ' ', $line->main_controller)).'</div>'; 
			$row[] = '<div style="text-align:center">'.$line->document_date.'</div>'; 
			$row[] = '<div style="text-align:center">'.$line->document_no.'</div>'; 
			$row[] = '<div style="text-align:center">'.$line->number_of_line.'</div>'; 
			$row[] = '<div style="text-align:center">'.$line->customer.'</div>'; 

			$data[] = $row;
		}
   
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($list),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }	
}	