<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tower_type extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_status");
		$this->load->model("Model_table_tower_type");
    }
    
    public function index() {
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1))                
        );
        $this->template->display_app('page/tower_type_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");
		$list = $this->Model_table_tower_type->get_rows($where);				
		if(!is_array($list)) { $list = array(); }        

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
			$row[] = '<div style="text-align:center">'.$line->short_txt.'</div>';
            $row[] = '<div style="text-align:center">'.$line->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_tower_type->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
		
		unset($where);
		unset($list);
		unset($row);
		unset($data);
		unset($output);		
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");

        $txtshorttxt = ''; 
        $txttxt = ''; 
		$txtstatusid = ''; 
		
		$option_status = '<option value="1">Active</option>';
				
        if($txtstate == "edit") {

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$row = $this->Model_table_tower_type->get_rows($where);
		
            if($row != "") {
                $txtshorttxt = $row[0]->short_txt; 
                $txttxt = $row[0]->txt; 
				$txtstatusid = $row[0]->status_id; 
			}
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';
        }
		unset($where);
		unset($row);
        
        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Tower Type Code: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtshorttxt_old" name="txtshorttxt_old" class="form-control" value="'.$txtshorttxt.'" />
                        <input type="text" id="txtshorttxt" name="txtshorttxt" class="form-control" value="'.$txtshorttxt.'" />
                      </div>
                    </div>
                    
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Tower Type: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txttxt_old" name="txttxt_old" class="form-control" value="'.$txttxt.'" />
                        <input type="text" id="txttxt" name="txttxt" class="form-control" value="'.$txttxt.'" />
                      </div>
                    </div>
                  
					<div class="col-md-12">
						<div class="form-group">
							<label>Status: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
							<select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
								'.$option_status.'                    
							</select>
						</div>
					</div>
                
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
		
		unset($table);
		unset($data);
		
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
        $txtshorttxt = '';
		$txttxt = '';
		$txtstatustablename = '';
		
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_tower_type->get_rows($where);
		
        if($row != "") {
            $txtshorttxt = $row[0]->short_txt;
			$txttxt = $row[0]->txt;
			$txtstatustablename =  $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Tower Type Code:</td>
                        <td class="view-txt">'.$txtshorttxt.'</td>
                    </tr>
                    <tr>
                        <td class="view-title" style="width: 30%">Tower Type Name:</td>
                        <td class="view-txt">'.$txttxt.'</td>
                    </tr>                 
					<tr>
					  <td class="view-title" style="width: 30%">Status:</td>
					  <td class="view-txt">'.$txtstatustablename.'</td>
					</tr>
                
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
		
		unset($where);
		unset($row);
		unset($table);
		unset($data);		
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
		
        $txtrowid = (int)$this->input->post("txtrowid");
		$txtshorttxt = $this->input->post("txtshorttxt");
        $txttxt = $this->input->post("txttxt");
		$txtstatusid = (int)$this->input->post("txtstatusid");         

        if($txtstate == 'add')
        {
            $insert = array(
                'short_txt' => strtoupper($txtshorttxt),
                'txt' => $txttxt,
				'status_id' => $txtstatusid,         
            );
			
			$this->Model_table_tower_type->insert($insert);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'short_txt' => strtoupper($txtshorttxt),
                'txt' => $txttxt,
				'status_id' => $txtstatusid,         
            );
			
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);

            $this->Model_table_tower_type->update($update, $where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_tower_type->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }

	public function getStatusList()
	{
			echo json_encode($this->Model_table_status->get_all());
	}
	
}	