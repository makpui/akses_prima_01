<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class work_order extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->library("template");
		$this->load->helper("my_helper");		
 		$this->load->model("Model_table_user");	
 		$this->load->model("Model_table_status");	
 		$this->load->model("Model_table_work_flow_milestone");
 		$this->load->model("Model_table_vendor");		
 		$this->load->model("Model_table_vendor_contact");
 		$this->load->model("Model_table_role");		
 		$this->load->model("Model_table_work_order_approval_flow");		
 		$this->load->model("Model_table_work_order_approval");		
 		$this->load->model("Model_table_work_order_line");		
 		$this->load->model("Model_table_work_order");
		
	}
    
    public function index()
	{
		$is_so_maker = ($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 5) ?   1 : 0 ;

		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),
			'is_so_maker'    =>  $is_so_maker			
        );		
        $this->template->display_app('page/work_order_vw', $data);
    }

    public function gridview()
	{	
        $where = $this->input->post("where");		
		
		$list = $this->Model_table_work_order->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
			$no++;
            $row = array();

            $link = '
						<div class="btn-group">
							<a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="'.base_url("work_order_line/index")."/".$line->row_id.'" style="color:#111" >Work Order Line</a></li>
								<li><a href="'.base_url("work_order_approval/index")."/".$line->row_id.'" style="color:#111" >Work Order Approval</a></li>
								<li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';
			
			/**
			 * Update & Delete button show in creation stage only
			 *
			 */
			if($line->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id)
			{
				if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 5)
				{
					$link .= '
								<li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
					';
				}
				
				if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1)
				{
					$link .= '
								<li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					';
				}
			}

            $link .= '					
							</ul>
						</div>
					';
			
			/**
			 * Handling empty date & date format
			 * because this process cannot done in array 
			 */
			$wodate = $line->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($line->workorder_date)) : '-';
			
			// create progress of approval
			$approval_progress = $line->work_flow_milestone_id == $this->Model_table_work_flow_milestone->get_beginning_state()[0]->id ? '0 / 0': $this->Model_table_work_order_approval->get_previous_state($line->workorder_no).' / '.$this->Model_table_work_order_approval_flow->get_ending_state()[0]->ordering;

			$workorderlinetable = $this->Model_table_work_order_line->get_row_id_basedon_workorder_no($line->workorder_no);
			$workorderlinetable_numberofline = is_array($workorderlinetable) ? sizeof($workorderlinetable) : 0;
			
			/**
			 * Create grid content
			 */
			$row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->workorder_no.'</div>'; 
            $row[] = '<div style="text-align:center">'.$wodate.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_vendor->get_name_basedon_row_id($line->vendor_id)[0]->name.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_vendor_contact->get_name_basedon_row_id($line->vendorcontact_id)[0]->name.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($line->vendorcontact_id)[0]->mobile_no.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_vendor_contact->get_email_basedon_row_id($line->vendorcontact_id)[0]->email.'</div>';
            $row[] = '<div style="text-align:center">'.$line->product_group.'</div>';
            $row[] = '<div style="text-align:center">'.$line->project_manager.'</div>';
            $row[] = '<div style="text-align:center">'.$line->remark.'</div>';
			
			$row[] = '<div style="text-align:center">'.$workorderlinetable_numberofline.'</div>';

            $row[] = '<div style="text-align:center">'.$this->Model_table_work_flow_milestone->get_name_basedon_id($line->work_flow_milestone_id)[0]->name.' State</div>';			
            $row[] = '<div style="text-align:center">'.$approval_progress.'</div>';
			$row[] = '<div style="text-align:center">'.my_work_order_approval_current_user($line->workorder_no).'</div>';			
            $row[] = '<div style="text-align:center">'.my_work_order_approval_result($line->workorder_no).'</div>';
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_work_order->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form()
	{
        $txtstate = $this->input->post("txtstate");
		
        $txtrowid = $this->input->post("txtrowid");
        $txtworkorderno = 'Generated by system'; 
        $txtworkorderdate = ''; 
        $txtvendorid = ''; 
        $txtvendorcontactid = ''; 
        $txtproductgroup = ''; 
        $txtprojectmanager = ''; 
        $txtremark = ''; 
		$txtworkflowmilestoneid = 1;
        $txtstatusid = ''; 
		
		$option_vendor = '<option value="">--Select a Vendor--</option>';
		$option_vendor_contact = '<option value="">--Select a Vendor Contact--</option>';
		$option_status = '<option value="1">Active</option>';		

        if($txtstate == "edit")
		{		
 			$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
			$row = $this->Model_table_work_order->get_rows($where);
						
            if($row != "")
			{
                $txtworkorderno = $row[0]->workorder_no; 
                $txtworkorderdate = $row[0]->workorder_date != '0000-00-00' ? date("d-m-Y", strtotime($row[0]->workorder_date)) : '';
				$txtvendorid = $row[0]->vendor_id; 
                $txtvendorcontactid = $row[0]->vendorcontact_id; 
                $txtproductgroup = $row[0]->product_group; 
                $txtprojectmanager = $row[0]->project_manager; 
                $txtremark = $row[0]->remark; 
				$txtworkflowmilestoneid = $row[0]->work_flow_milestone_id;
                $txtstatusid = $row[0]->status_id;
            }
			$option_vendor = '<option value="'.$txtvendorid.'" selected >'.$this->Model_table_vendor->get_name_basedon_row_id($txtvendorid)[0]->name.'</option>';	
			$option_vendor_contact = '<option value="'.$txtvendorcontactid.'" selected >'.$this->Model_table_vendor_contact->get_name_basedon_row_id($txtvendorcontactid)[0]->name.'</option>';	
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';						
        }

        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />
                    <input type="hidden" id="txtworkflowmilestoneid" name="txtworkflowmilestoneid" class="form-control" value="'.$txtworkflowmilestoneid.'" />
                
                    <div class="col-md-6">
						<div class="form-group">
							<label>Work Order No.: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtworkorderno_old" name="txtworkorderno_old" class="form-control" value="'.$txtworkorderno.'" />
							<input type="text" id="txtworkorderno" name="txtworkorderno" class="form-control" value="'.$txtworkorderno.'" readonly/>
						</div>
                    </div>
                  
                    <div class="col-md-6">
						<div class="form-group">
							<label>Work Order Date: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtworkorderdate_old" name="txtworkorderdate_old" class="form-control" value="'.$txtworkorderdate.'" />
							<input type="text" id="txtworkorderdate" name="txtworkorderdate" class="form-control" value="'.$txtworkorderdate.'" />
						</div>
                    </div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Vendor: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtvendorid_old" name="txtvendorid_old" class="form-control" value="'.$txtvendorid.'" />
							<select id="txtvendorid" name="txtvendorid" class="form-control select2"  required>
								'.$option_vendor.'
							</select>
						</div>
					</div>
                  
					<div class="col-md-6">
						<div class="form-group">
							<label>Contact Person: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtvendorcontactid_old" name="txtvendorcontactid_old" class="form-control" value="'.$txtvendorcontactid.'" />
							<select id="txtvendorcontactid" name="txtvendorcontactid" class="form-control select2"  required>
								'.$option_vendor_contact.'
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label>Product Group: </label>
							<input type="hidden" id="txtproductgroup_old" name="txtproductgroup_old" class="form-control" value="'.$txtproductgroup.'" />
							<input type="text" id="txtproductgroup" name="txtproductgroup" class="form-control" value="'.$txtproductgroup.'" />
						</div>
					</div>
                  
					<div class="col-md-6">
						<div class="form-group">
							<label>Project Manager: <span style="color: #ff0000">*</label>
							<input type="hidden" id="txtprojectmanager_old" name="txtprojectmanager_old" class="form-control" value="'.$txtprojectmanager.'" />
							<input type="text" id="txtprojectmanager" name="txtprojectmanager" class="form-control" value="'.$txtprojectmanager.'" />
						</div>
					</div>
                  
					<div class="col-md-6">
						<div class="form-group">
							<label>Remark: </label>
							<input type="hidden" id="txtremark_old" name="txtremark_old" class="form-control" value="'.$txtremark.'" />
							<input type="text" id="txtremark" name="txtremark" class="form-control" value="'.$txtremark.'" />
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Status: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
							<select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
								'.$option_status.'
							</select>
						</div>
					</div>
                  
				  
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $txtrowid = $this->input->post("txtrowid");	
        $txtworkorderno = ''; 
        $txtworkorderdate = ''; 
        $txtvendortable_name = ''; 
        $txtvendorcontacttable_name = ''; 
        $txtvendorcontacttable_mobile_no = ''; 
        $txtvendorcontacttable_email = ''; 
        $txtproductgroup = ''; 
        $txtprojectmanager = ''; 
        $txtremark = ''; 
		$txtworkflowmilestonetable_name = '';
        $txtstatustable_name = ''; 
		
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
		$row = $this->Model_table_work_order->get_rows($where);
		
        if($row != "") {
                $txtworkorderno = $row[0]->workorder_no; 
                $txtworkorderdate = $row[0]->workorder_date == "0000-00-00" ? ' - ' : date("d-m-Y", strtotime($row[0]->workorder_date));
				$txtvendortable_name = $this->Model_table_vendor->get_name_basedon_row_id($row[0]->vendor_id)[0]->name; 
                $txtvendorcontacttable_name = $this->Model_table_vendor_contact->get_name_basedon_row_id($row[0]->vendorcontact_id)[0]->name;
                $txtvendorcontacttable_mobile_no = $this->Model_table_vendor_contact->get_mobile_no_basedon_row_id($row[0]->vendorcontact_id)[0]->mobile_no;
                $txtvendorcontacttable_email = $this->Model_table_vendor_contact->get_email_basedon_row_id($row[0]->vendorcontact_id)[0]->email;
                $txtproductgroup = $row[0]->product_group; 
                $txtprojectmanager = $row[0]->project_manager; 
                $txtremark = $row[0]->remark; 
				$txtworkflowmilestonetable_name = $this->Model_table_work_flow_milestone->get_name_basedon_id($row[0]->work_flow_milestone_id)[0]->name;
                $txtstatustable_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Work Order No.:</td>
						<td class="view-txt">'.$txtworkorderno.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Work Order Date:</td>
                        <td class="view-txt">'.$txtworkorderdate.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Vendor:</td>
                        <td class="view-txt">'.$txtvendortable_name.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Contact</td>
                        <td class="view-txt">'.$txtvendorcontacttable_name.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">CP Email:</td>
                        <td class="view-txt">'.$txtvendorcontacttable_email.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">CP Phone:</td>
                        <td class="view-txt">'.$txtvendorcontacttable_mobile_no.'</td>
                    </tr>
                  
                  <tr>
                      <td class="view-title" style="width: 30%">Product Group:</td>
                      <td class="view-txt">'.$txtproductgroup.'</td>
                  </tr>
				  
                  <tr>
                      <td class="view-title" style="width: 30%">Project Manager:</td>
                      <td class="view-txt">'.$txtprojectmanager.'</td>
                  </tr>

                  <tr>
                      <td class="view-title" style="width: 30%">Remark</td>
                      <td class="view-txt">'.$txtremark.'</td>
                  </tr>
				  
                <tr>
                    <td class="view-title" style="width: 30%">Workorder Milestone:</td>
                    <td class="view-txt">'.$txtworkflowmilestonetable_name.' State</td>
                </tr>

                <tr>
                    <td class="view-title" style="width: 30%">Status:</td>
                    <td class="view-txt">'.$txtstatustable_name.'</td>
                </tr>
                
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
		
        $txtrowid = (int)$this->input->post("txtrowid");
        $txtworkorderdate = $this->input->post("txtworkorderdate") != '' ? date("Y-m-d", strtotime($this->input->post("txtworkorderdate"))) : $this->input->post("txtworkorderdate");	
        $txtworkorderno = $this->input->post("txtworkorderno"); 
        $txtvendorid = (int)$this->input->post("txtvendorid"); 
        $txtvendorcontactid = (int)$this->input->post("txtvendorcontactid"); 
        $txtproductgroup = $this->input->post("txtproductgroup"); 
        $txtprojectmanager = $this->input->post("txtprojectmanager"); 
        $txtremark = $this->input->post("txtremark"); 
		$txtworkflowmilestoneid = (int)$this->input->post("txtworkflowmilestoneid");     
		$txtstatusid = (int)$this->input->post("txtstatusid");     
		

        if($txtstate == 'add')
        {
            $insert = array(
                'workorder_no' => generateWONo($txtworkorderdate),
				'workorder_date' => $txtworkorderdate,
				'vendor_id' => $txtvendorid,
				'vendorcontact_id' => $txtvendorcontactid,
				'product_group' => $txtproductgroup,
				'project_manager' => $txtprojectmanager,
				'remark' => $txtremark,
				'work_flow_milestone_id' => $txtworkflowmilestoneid,
				'status_id' => $txtstatusid,         	
            );

			$this->Model_table_work_order->insert($insert);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            $data['workorder_no'] = $txtworkorderno;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'workorder_no' => $txtworkorderno,
				'workorder_date' => $txtworkorderdate,
				'vendor_id' => $txtvendorid,
				'vendorcontact_id' => $txtvendorcontactid,
				'product_group' => $txtproductgroup,
				'project_manager' => $txtprojectmanager,
				'remark' => $txtremark,
				'work_flow_milestone_id' => $txtworkflowmilestoneid,
				'status_id' => $txtstatusid,         				
            );
			
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$this->Model_table_work_order->update($update, $where);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            $data['workorder_no'] = $txtworkorderno;
            echo json_encode($data);
        }
    }

    public function delete()
	{
        $txtrowid = $this->input->post("txtrowid");
		
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_work_order->delete($where);
        
        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }
		
		
	public function getVendorList()
	{
		echo json_encode($this->Model_table_vendor->get_all_active());
	}

	public function getVendorContactList($vendor_id)
	{
		if($vendor_id)
		{
			$where[] = array('key' => 'vendor_id', 'value' => $vendor_id);
		}	
		echo json_encode($this->Model_table_vendor_contact->get_rows($where));
	}
	
	public function getWorkFlowMilestoneList()
	{
		echo json_encode($this->Model_table_work_flow_milestone->get_all());
	}

	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}
	
	
}	