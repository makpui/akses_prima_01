<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class password extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_status");
		$this->load->model("Model_table_role");
  		$this->load->model("Model_table_user");
    }

    public function index() {
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1))                
        );
        $this->template->display_app('page/password_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");
        
		if($this->session->userdata['SessionLogin']['SesLoginId'] <> 1 ) {
			$where[] = array('key' => 'row_id', 'value' => $this->session->userdata['SessionLogin']['SesLoginId']);
		}
				
		$list = $this->Model_table_user->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Edit Password</a></li>
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_role->get_txt_basedon_row_id($line->role_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->user_id.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->name.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->email.'</div>'; 
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_user->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");

		$txtpswrdnewa = ''; 
		$txtpswrdnewb = ''; 

        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />';

		if($this->session->userdata['SessionLogin']['SesLoginId'] <> 1) {			
		$txtpswrd = ''; 
		$table .= '                 
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Current Password:</label>
                        <input type="hidden" id="txtpswrd_old" name="txtpswrd_old" class="form-control" value="'.$txtpswrd.'" />
                        <input type="password" id="txtpswrd" name="txtpswrd" class="form-control" onblur="checkPassword()" value="'.$txtpswrd.'" />
                      </div>
                    </div>';
		}			
                                  
		$table .= '                 
                    <div class="col-md-12">
                      <div class="form-group">
                      </div>
                    </div>

					<div class="col-md-12">
                      <div class="form-group">
                        <label>New Password:</label>
                        <input type="hidden" id="txtpswrdnewa_old" name="txtpswrdnewa_old" class="form-control" value="'.$txtpswrdnewa.'" />
                        <input type="password" id="txtpswrdnewa" name="txtpswrdnewa" class="form-control" value="'.$txtpswrdnewa.'" />
                      </div>
                    </div>
					
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Confirm New Password:</label>
                        <input type="hidden" id="txtpswrdnewb_old" name="txtpswrdnewb_old" class="form-control" value="'.$txtpswrdnewb.'" />
                        <input type="password" id="txtpswrdnewb" name="txtpswrdnewb" class="form-control" onblur="checkNewPassword()" value="'.$txtpswrdnewb.'" />
                      </div>
                    </div>
					
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
		$txtroletabletxt = ''; 
		$txtname = ''; 
		$txtemail = ''; 
		$txtuserid = ''; 
		$txtpswrd = '*********'; 
		$txtphoto = ''; 
		$txtstatustablename = ''; 
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_user->get_rows($where);

        if($row != "") {
			$txtroletabletxt = $this->Model_table_role->get_txt_basedon_row_id($row[0]->role_id)[0]->txt; 
			$txtname = $row[0]->name; 
			$txtemail = $row[0]->email; 
			$txtuserid = $row[0]->user_id; 
			$txtphoto = $row[0]->photo; 
			$txtphoto_hdn = $row[0]->photo; 
			$txtstatustablename =  $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                  <tr>
                      <td class="view-title" style="width: 30%">Role:</td>
                      <td class="view-txt">'.$txtroletabletxt.'</td>
                  </tr>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Name:</td>
                        <td class="view-txt">'.$txtname.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Email:</td>
                        <td class="view-txt">'.$txtemail.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">User Id:</td>
                        <td class="view-txt">'.$txtuserid.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Password:</td>
                        <td class="view-txt">'.$txtpswrd.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Photo Profile:</td>
                        <td class="view-txt">'.$txtphoto.'</td>
                    </tr>
                  
                  <tr>
                      <td class="view-title" style="width: 30%">Status:</td>
                      <td class="view-txt">'.$txtstatustablename.'</td>
                  </tr>
                
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $controller = $this->uri->segment(1);
        $txtstate = $this->input->post("txtstate");		
        $txtrowid = (int)$this->input->post("txtrowid");
		
		$txtpswrdnewb = $this->input->post("txtpswrdnewb");
              
        if($txtstate == 'edit')
        {            
            $update = array(
				'pswrd' => md5($txtpswrdnewb),
            );

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
            $this->Model_table_user->update($update, $where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

	public function isPasswordExist() {
        $txtrowid = $this->input->post("txtrowid");
        $txtpswrd = $this->input->post("txtpswrd");		
		$result = array();
	    $rs_usertable = getByQuery("select * from usertable where row_id = ".$txtrowid);
        if($rs_usertable != "") {
            foreach($rs_usertable as $row_usertable) {
				if($row_usertable->pswrd == md5($txtpswrd))
				{ 
					$result = array('isexist'=>1);
				}
				else 
				{
					$result = array('isexist'=>0);				
				}
            }
			echo json_encode($result);
        }
	}

}	