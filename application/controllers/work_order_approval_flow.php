<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class work_order_approval_flow extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
 		$this->load->model("Model_table_role");	
 		$this->load->model("Model_table_user");	
 		$this->load->model("Model_table_work_order_approval_flow");		
    }
    
    public function index() {
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1))                
        );
        $this->template->display_app('page/work_order_approval_flow_vw', $data);
    }

    public function gridview() {        
        $where = $this->input->post("where");
        
		$list = $this->Model_table_work_order_approval_flow->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		

		$data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->ordering.'</div>'; 
            $row[] = '<div style="text-align:center">'.$this->Model_table_role->get_txt_basedon_row_id($line->role_id)[0]->txt.'</div>'; 
            //$row[] = '<div style="text-align:center">'.$this->Model_table_user->get_name_basedon_role_id($line->role_id)[0]->name.'</div>'; 
            //$row[] = '<div style="text-align:center">'.$this->Model_table_user->get_email_basedon_role_id($line->role_id)[0]->email.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->remark.'</div>'; 


            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_work_order_approval_flow->get_all()),
            "recordsFiltered" => sizeof($list),

            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
		
        $txtordering = '';
		$txtroleid = '';
        $txtremark = ''; 

		
		$option_role = '<option value="">--Select a Role--</option>';

        if($txtstate == "edit") {
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);			
			$row = $this->Model_table_work_order_approval_flow->get_rows($where);
					
            if($row != "") {
                $txtordering = $row[0]->ordering; 
                $txtroleid = $row[0]->role_id; 
                $txtremark = $row[0]->remark;
            }
			$option_role = '<option value="'.$txtroleid.'" selected >'.$this->Model_table_role->get_txt_basedon_row_id($txtroleid)[0]->txt.'</option>';
		}
       
        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

					<div class="col-md-12">
						<div class="form-group">
						  <label>Approval State: <span style="color: #ff0000">*</span></label>
						  <input type="hidden" id="txtordering_old" name="txtordering_old" class="form-control" value="'.$txtordering.'" />
						  <input type="text" id="txtordering" name="txtordering" class="form-control" value="'.$txtordering.'" />
						</div>
					</div>
					
					<div class="col-md-12">
					  <div class="form-group">
						<label>Role: <span style="color: #ff0000">*</span></label>
						<input type="hidden" id="txtroleid_old" name="txtroleid_old" class="form-control" value="'.$txtroleid.'" />
						<select id="txtroleid" name="txtroleid" class="form-control select2"  required>
						  '.$option_role.'
						</select>
					  </div>
					</div>
						
					<div class="col-md-12">
						<div class="form-group">
						  <label>Remark: <span style="color: #ff0000">*</span></label>
						  <input type="hidden" id="txtremark_old" name="txtremark_old" class="form-control" value="'.$txtremark.'" />
						  <input type="text" id="txtremark" name="txtremark" class="form-control" value="'.$txtremark.'" />
						</div>
					</div>
              
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
		
        $txtordering = '';
		$txtroletable_txt = '';
		$txtusertable_name = '';
		$txtusertable_email = '';
        $txtremark = ''; 		
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_work_order_approval_flow->get_rows($where);

        if($row != "") {
			$txtordering = $row[0]->ordering;
			$txtroletable_txt = $this->Model_table_role->get_txt_basedon_row_id($row[0]->role_id)[0]->txt;
			$txtusertable_name = $this->Model_table_user->get_name_basedon_role_id($row[0]->role_id)[0]->name;
			$txtusertable_email = $this->Model_table_user->get_email_basedon_role_id($row[0]->role_id)[0]->email;
			$txtremark = $row[0]->remark;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                <tr>
                    <td class="view-title" style="width: 30%">Approval State:</td>
                    <td class="view-txt">'.$txtordering.'</td>
                </tr>
				
                <tr>
                    <td class="view-title" style="width: 30%">Role Name:</td>
                    <td class="view-txt">'.$txtroletable_txt.'</td>
                </tr>
                
                <tr>
                    <td class="view-title" style="width: 30%">User Name:</td>
                    <td class="view-txt">'.$txtusertable_name.'</td>
                </tr>

                <tr>
                    <td class="view-title" style="width: 30%">Email:</td>
                    <td class="view-txt">'.$txtusertable_email.'</td>
                </tr>
                              
                <tr>
                    <td class="view-title" style="width: 30%">Remark:</td>
                    <td class="view-txt">'.$txtremark.'</td>
                </tr>
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");	
		
        $txtrowid = (int)$this->input->post("txtrowid");
        $txtordering = (int)$this->input->post("txtordering");
		$txtroleid = (int)$this->input->post("txtroleid");
        $txtremark = $this->input->post("txtremark"); 

        if($txtstate == 'add')
        {
            $insert = array(
                'ordering' => $txtordering,
				'role_id' => $txtroleid,
                'remark' => $txtremark,
            );
			
			$this->Model_table_work_order_approval_flow->insert($insert);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'ordering' => $txtordering,
				'role_id' => $txtroleid,
                'remark' => $txtremark,
            );
			
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
            $this->Model_table_work_order_approval_flow->update($update, $where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $controller = $this->uri->segment(1);
        $txtrowid = $this->input->post("txtrowid");
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_work_order_approval_flow->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }
	
	public function getRoleList()
	{	
		$rows = $this->Model_table_work_order_approval_flow->get_all();

		foreach($rows as $row)
		{
			$where[] = array('key' => 'row_id != ', 'value' => $row->role_id);
			unset($row);
		}
		unset($rows);
		echo json_encode($this->Model_table_role->get_rows($where));
		unset($where);		
	}
		
}	