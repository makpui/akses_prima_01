<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class itemunit extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
    }

    public $table_db = "itemunittable";
    
    public function index() {
        $controller = $this->uri->segment(1);
        $lbl_controller = str_replace("_", " ", $controller);
        $page_url = base_url($controller);
		$data = array(
            'controller' => $this->uri->segment(1),
            'lbl_controller' => $lbl_controller,
            'page_url' => $page_url,
            'rs_statustable' => getByQuery('select * from statustable'),
                
        );
        $this->template->display_app('page/itemunit_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");        
        $database = "default";
        $column = array('row_id', 'txt', 'status_id');
        $table = "vw_itemunittablelist";
        $list = $this->datatables_mdl->get_datatables($database, $table, $column, $where);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="#" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
                    <li><a href="#" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
                    <li><a href="#" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->txt.'</div>'; 
$row[] = '<div style="text-align:center">'.showStatusIconActive($line->statustable_name).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatables_mdl->count_all($database, $table, $column),
            "recordsFiltered" => $this->datatables_mdl->count_filtered($database, $table, $column, $where),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
        $txttxt = ''; 
$txtstatusid = ''; 
$statustable_list = "";$txtstatustablename = ''; 


        if($txtstate == "edit") {
            $qry = "select * from vw_itemunittablelist where row_id = '".$txtrowid."'";
            $rs = getByQuery($qry);
            if($rs != "") {
                $txttxt = $rs[0]->txt; 
$txtstatusid = $rs[0]->status_id; 
$txtstatustablename = $rs[0]->statustable_name; 

            }
        }

        
                  $rs_statustable = getByQuery("select * from statustable");
                  if($rs_statustable != "") {
                      foreach($rs_statustable as $row_statustable) {
                          $selected = ($row_statustable->row_id == $txtstatusid) ? "selected" : "";
                          $statustable_list .= "<option value='".$row_statustable->row_id."' ".$selected." >".$row_statustable->name."</option>";
                      }
                  }
                

        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

                    
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Item Unit:</label>
                        <input type="hidden" id="txttxt_old" name="txttxt_old" class="form-control" value="'.$txttxt.'" />
                        <input type="text" id="txttxt" name="txttxt" class="form-control" value="'.$txttxt.'" />
                      </div>
                    </div>
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Status: <span style="color: #ff0000">*</span></label>
                      <input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
                      <select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
                        <option value=""></option>
                        '.$statustable_list.'
                      </select>
                    </div>
                  </div>
                
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
        $txttxt = ''; 
$txtstatusid = ''; 
$statustable_list = "";$txtstatustablename = ''; 

        
        $qry = "select * from vw_itemunittablelist where row_id = '".$txtrowid."'";
        $rs = getByQuery($qry);
        if($rs != "") {
                $txttxt = $rs[0]->txt; 
$txtstatusid = $rs[0]->status_id; 
$txtstatustablename = $rs[0]->statustable_name; 

        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Item Unit:</td>
                        <td class="view-txt">'.$txttxt.'</td>
                    </tr>
                  
                  <tr>
                      <td class="view-title" style="width: 30%">Status:</td>
                      <td class="view-txt">'.$txtstatustablename.'</td>
                  </tr>
                
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $controller = $this->uri->segment(1);
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
        $txttxt = $this->input->post("txttxt"); $txtstatusid = $this->input->post("txtstatusid"); 

        

        if($txtstate == 'add')
        {
            $insert = array(
                'txt' => $txttxt, 'status_id' => $txtstatusid,         
            );
            insertData("Insert ".$this->table_db, $controller, $this->table_db, $insert);
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'txt' => $txttxt, 'status_id' => $txtstatusid,              
            );
            updateData("Update ".$this->table_db, $controller, $this->table_db, $update, "row_id = '".$txtrowid."'", $txtrowid);
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $controller = $this->uri->segment(1);
        $txtrowid = $this->input->post("txtrowid");
        
        deleteData("Delete ".$this->table_db, $controller, $this->table_db, "row_id = '".$txtrowid."'", $txtrowid);
        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }
}	