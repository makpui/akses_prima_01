<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tesprint extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function tes() {
        $data = array();
        $this->load->view('report/print_rpt', $data);
        $html = $this->output->get_output();
        $pdfFilePath = "print_entertain.pdf";
        $this->load->library('m_pdf');
        $pdf = $this->m_pdf->load("A4");
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, "D");
    }

    public function tes2() {
        $data = array();
        generatePDF("print_rpt", "tsds", $data);
    }

    
    public function aaa() {
        echo generateSONo("2018");
    }

    public function tesemail() {
        $this->load->library('sendemail');
        $data = array(
            "to" => "yusa.febriyan@balitower.co.id", 
            "subject" => "Yusa Febriyan", 
            "attachment" => "", 
            "nama_customer" => "Yusa Febriyan", 
            "no_ktp" => "28348239432432",
            "alamat" => "Bekasi",
            "service" => "Service",
            "pembayaran" => "CC"
        );
        $this->sendemail->registrationPersonal($data);
    }
}	