<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_territory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_user_territory");
		$this->load->model("Model_table_user");
		$this->load->model("Model_table_area");
		$this->load->model("Model_table_role");
		$this->load->model("Model_table_status");
    }

    public function index()
	{
        $usertable_row_id = '';
        $usertable_role_name = '';
        $usertable_name = '';
        $usertable_email = '';
        $usertable_user_id = '';
        $usertable_status = '';

		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));
		$row = $this->Model_table_user->get_rows($where);
		unset($where);

        if($row != "")
		{
			$usertable_row_id = $row[0]->row_id;
			$usertable_role_txt = $this->Model_table_role->get_txt_basedon_row_id($row[0]->role_id)[0]->txt;
			$usertable_name = $row[0]->name;
			$usertable_email = $row[0]->email;
			$usertable_user_id = $row[0]->user_id;
			$usertable_status_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }
		unset($row);

		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),

			'usertable_row_id' => $usertable_row_id,
			'usertable_role_txt' => $usertable_role_txt,
			'usertable_name' => $usertable_name,
			'usertable_email' => $usertable_email,
			'usertable_user_id' => $usertable_user_id,
			'usertable_status_name' => $usertable_status_name,
        );
        $this->template->display_app('page/user_territory_vw', $data);
    }

    public function gridview()
	{
        $where = $this->input->post("where");
		$list = $this->Model_table_user_territory->get_rows($where);
		unset($where);

		if(!is_array($list)) { $list = array(); }

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line)
        {
            $no++;
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->user_row_id.'\', \''.$line->territory_id.'\')">View</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->user_row_id.'\', \''.$line->territory_id.'\')">Delete</a></li>
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:left">'.$this->Model_table_area->get_txt_basedon_row_id($line->territory_id)[0]->txt.'</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_user_territory->get_all()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function form()
	{
        $txtstate = $this->input->post("txtstate");

        $txtuserrowid = $this->input->post("txtuserrowid");
        $txtterritoryid = '';

		$option_territory = '<option value="">--Select a Territory--</option>';

        if($txtstate == "edit")
		{
			$where[] = array('key' => 'user_row_id', 'value' => $txtuserrowid);
			$where[] = array('key' => 'territory_id', 'value' => $this->input->post("txtterritoryid"));
			$row = $this->Model_table_user_territory->get_rows($where);

            if($row != "") {
                $txtuserrowid = $row[0]->user_row_id;
                $txtterritoryid = $row[0]->territory_id;
            }
			unset($row);
			$option_territory = '<option value="'.$txtterritoryid.'" selected >'.$this->Model_table_area->get_txt_basedon_row_id($txtterritoryid)[0]->txt.'</option>';
        }


        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

                <div class="col-md-6">
                  <div class="form-group">
                    <label>User: <span style="color: #ff0000">*</span></label>
                    <input type="hidden" id="txtuserrowid_old" name="txtuserrowid_old" class="form-control" value="'.$txtuserrowid.'" />
                    <input type="text" id="txtusername" name="txtusername" class="form-control" value="'.$this->Model_table_user->get_name_basedon_row_id($txtuserrowid)[0]->name.'" readonly/>
                    <input type="hidden" id="txtuserrowid" name="txtuserrowid" class="form-control" value="'.$txtuserrowid.'" />
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Territory: <span style="color: #ff0000">*</span></label>
                    <input type="hidden" id="txtterritoryid_old" name="txtterritoryid_old" class="form-control" value="'.$txtterritoryid.'" />
                    <select id="txtterritoryid" name="txtterritoryid" class="form-control select2"  required>
                      '.$option_territory.'
                    </select>
                  </div>
                </div>

                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $usertable_name = $this->Model_table_user->get_name_basedon_row_id($this->input->post("txtuserrowid"))[0]->name;
        $territory_txt = $this->Model_table_area->get_txt_basedon_row_id($this->input->post("txtterritoryid"))[0]->txt;

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>

                  <tr>
                      <td class="view-title" style="width: 30%">User</td>
                      <td class="view-txt">: '.$usertable_name.'</td>
                  </tr>

                  <tr>
                      <td class="view-title" style="width: 30%">Territory</td>
                      <td class="view-txt">: '.$territory_txt.'</td>
                  </tr>

              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");

        $txtuserrowid = $this->input->post("txtuserrowid");
        $txtterritoryid = $this->input->post("txtterritoryid");

        if($txtstate == 'add')
        {
            $insert = array(
                'user_row_id' => $txtuserrowid,
				'territory_id' => $txtterritoryid,
            );

			$this->Model_table_user_territory->insert($insert);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {
            $update = array(
                'user_row_id' => $txtuserrowid,
				'territory_id' => $txtterritoryid,
            );

			$where[] = array('key' => 'user_row_id', 'value' => $txtuserrowid);
			$where[] = array('key' => 'territory_id', 'value' => $txtterritoryid);
            $this->Model_table_user_territory->update($update, $where);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete()
	{
        $txtuserrowid = $this->input->post("txtuserrowid");
        $txtterritoryid = $this->input->post("txtterritoryid");

		$where[] = array('key' => 'user_row_id', 'value' => $txtuserrowid);
		$where[] = array('key' => 'territory_id', 'value' => $txtterritoryid);
		$this->Model_table_user_territory->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }

	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}

	public function getRoleList()
	{
		echo json_encode($this->Model_table_role->get_all());
	}

	public function getTerritoryList()
	{
    $where = array();
		$where[] = array('key' => 'user_row_id', 'value' => $this->input->post("txtuserrowid"));
		$rows =	$this->Model_table_user_territory->get_rows($where);
		$where = array();

		if(is_array($rows))
		{
			foreach($rows as $row)
			{
				$where[] = array('key' => 'row_id != ', 'value' => $row->territory_id);
				unset($row);
			}
		}
		echo json_encode($this->Model_table_area->get_rows($where));
		unset($where);

		//echo json_encode($this->Model_table_area->get_all());
	}

}
