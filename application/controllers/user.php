<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_status");
		$this->load->model("Model_table_role");
  		$this->load->model("Model_table_user");
     }

    public function index() {
		$data = array(
            'lbl_controller' => str_replace("_", " ", $this->uri->segment(1))                
        );
        $this->template->display_app('page/user_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");
		
		$list = $this->Model_table_user->get_rows($where);				
		if(!is_array($list)) { $list = array(); }		
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
					<li><a href="'.base_url("user_territory/index")."/".$line->row_id.'" style="color:#111" >User Territory</a></li>					
                    <li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
                    <li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
                  </ul>
                </div>
            ';
						
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$this->Model_table_role->get_txt_basedon_row_id($line->role_id)[0]->txt.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->user_id.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->name.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->email.'</div>'; 
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_user->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form()
	{
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
        $txtroleid = ''; 
		$txtname = ''; 
		$txtemail = ''; 
		$txtuserid = ''; 
		$txtpswrd = ''; 
		$txtphoto = ''; 
		$txtstatusid = '';
		
		$readonly = '';

		$option_role = '<option value="">--Select a Role--</option>';
		$option_status = '<option value="1">Active</option>';
		
        if($txtstate == "edit") {
			$readonly = 'readonly';
		
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$row = $this->Model_table_user->get_rows($where);
			
            if($row != "")
			{
                $txtroleid = $row[0]->role_id; 
				$txtname = $row[0]->name; 
				$txtemail = $row[0]->email; 
				$txtuserid = $row[0]->user_id; 
				//$txtpswrd = $row[0]->pswrd; 
				$txtphoto = $row[0]->photo; 
				$txtphoto_hdn = $row[0]->photo; 
				$txtstatusid = $row[0]->status_id; 

            }
			$option_role = '<option value="'.$txtroleid.'" selected >'.$this->Model_table_role->get_txt_basedon_row_id($txtroleid)[0]->txt.'</option>';
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';			
        }
        
        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />
                                        
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Role: <span style="color: #ff0000">*</span></label>
                      <input type="hidden" id="txtroleid_old" name="txtroleid_old" class="form-control" value="'.$txtroleid.'" />
                      <select id="txtroleid" name="txtroleid" class="form-control select2"  required>
							'.$option_role.'
                      </select>
                    </div>
                  </div>
                
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Name: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtname_old" name="txtname_old" class="form-control" value="'.$txtname.'" />
                        <input type="text" id="txtname" name="txtname" class="form-control" value="'.$txtname.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Email: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtemail_old" name="txtemail_old" class="form-control" value="'.$txtemail.'" />
                        <input type="text" id="txtemail" name="txtemail" class="form-control" value="'.$txtemail.'" />
                      </div>
                    </div>
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>User Id: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtuserid_old" name="txtuserid_old" class="form-control" value="'.$txtuserid.'"  />
                        <input type="text" id="txtuserid" name="txtuserid" class="form-control" value="'.$txtuserid.'"  />
                      </div>
                    </div>
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Password:</label>
                        <input type="hidden" id="txtpswrd_old" name="txtpswrd_old" class="form-control" value="'.$txtpswrd.'" />
                        <input type="password" id="txtpswrd" name="txtpswrd" class="form-control" value="'.$txtpswrd.'" '.$readonly.'/>
                      </div>
                    </div>
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Photo Profile: <span style="font-size:10px">(leave it blank if you do not want to update)</span></label>
                        <input type="file" id="txtphoto" name="txtphoto" class="form-control" />
                        <input type="hidden" id="txtphoto_hdn" name="txtphoto_hdn" class="form-control" value="'.$txtphoto.'" />
                        <input type="hidden" id="txtphoto_old" name="txtphoto_old" class="form-control" value="'.$txtphoto.'" />
                      </div>
                    </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Status: <span style="color: #ff0000">*</span></label>
                      <input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
                      <select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
							'.$option_status.'
                      </select>
                    </div>
                  </div>
                
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{
        $txtrowid = $this->input->post("txtrowid");
		$txtroletabletxt = ''; 
		$txtname = ''; 
		$txtemail = ''; 
		$txtuserid = ''; 
		$txtpswrd = '**********'; 
		$txtphoto = ''; 
		$txtstatustablename = ''; 
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_user->get_rows($where);

        if($row != "")
		{
			$txtroletabletxt = $this->Model_table_role->get_txt_basedon_row_id($row[0]->role_id)[0]->txt; 
			$txtname = $row[0]->name; 
			$txtemail = $row[0]->email; 
			$txtuserid = $row[0]->user_id; 
			//$txtpswrd = $row[0]->pswrd; 
			$txtphoto = $row[0]->photo; 
			$txtphoto_hdn = $row[0]->photo; 
			$txtstatustablename =  $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                					
                    <tr>
                      <td class="view-title" style="width: 30%">Role:</td>
                      <td class="view-txt">'.$txtroletabletxt.'</td>
                    </tr>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Name:</td>
                        <td class="view-txt">'.$txtname.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Email:</td>
                        <td class="view-txt">'.$txtemail.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">User Id:</td>
                        <td class="view-txt">'.$txtuserid.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Password:</td>
                        <td class="view-txt">'.$txtpswrd.'</td>
                    </tr>
                  
                    <tr>
                        <td class="view-title" style="width: 30%">Photo Profile:</td>
                        <td class="view-txt">'.$txtphoto.'</td>
                    </tr>
                  
                  <tr>
                      <td class="view-title" style="width: 30%">Status:</td>
                      <td class="view-txt">'.$txtstatustablename.'</td>
                  </tr>
                
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
		
        $txtrowid = (int)$this->input->post("txtrowid");
        $txtroleid = (int)$this->input->post("txtroleid");
		$txtname = $this->input->post("txtname");
		$txtemail = $this->input->post("txtemail");
		$txtuserid = $this->input->post("txtuserid");
		$txtpswrd = $this->input->post("txtpswrd");
		$txtphoto = $this->input->post("txtphoto");
		$txtphoto_hdn = $this->input->post("txtphoto_hdn");
		$txtstatusid = (int)$this->input->post("txtstatusid"); 

        
		$txtphoto =  $txtphoto_hdn;
		if($_FILES['txtphoto']['name'] != '')
		{
			 $ext = pathinfo($_FILES['txtphoto']['name'], PATHINFO_EXTENSION);
			 $file_name = date('YmdHis').'.'.$ext;
			 $config = array(
				'upload_path' => 'uploaded',
				'allowed_types' => 'gif|jpg|png|jpeg|pdf|docx|xls|xlsx|doc|txt',
				'overwrite' => TRUE,
				'file_name' =>  $file_name
			);
			
			$this->upload->initialize( $config);
			if( $this->upload->do_upload('txtphoto'))
			{
				 $data = $this->upload->data();
				$txtphoto =  $file_name;
				if($txtphoto_hdn != '')
				  unlink('uploaded/'.$txtphoto_hdn);
			}
			else
			{
				$data['msg'] = $this->upload->display_errors();
				$data['type'] = 0;
				echo json_encode($data);
				exit();
			}
		}
              

        if($txtstate == 'add')
        {
            $insert = array(
                'role_id' => $txtroleid,
				'name' => $txtname,
				'email' => $txtemail,
				'user_id' => $txtuserid,
				'pswrd' => md5($txtpswrd),
				'photo' => $txtphoto,
				'status_id' => $txtstatusid,         
            );

			$this->Model_table_user->insert($insert);			

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'role_id' => $txtroleid,
				'name' => $txtname,
				'email' => $txtemail,
				'user_id' => $txtuserid,
				//'pswrd' => md5($txtpswrd),
				'photo' => $txtphoto,
				'status_id' => $txtstatusid,              
            );

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
            $this->Model_table_user->update($update, $where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");
        
		/* 
		$rs_usertable = getByQuery("select * from usertable where row_id = ".$txttransid);
		if($rs_usertable != "") {
			foreach($rs_usertable as $row) {
				
				$rs_usertable = getByQuery("select * from usertable where row_id = ".$txttransid);
				if($rs_usertable != "") {
					foreach($rs_usertable as $row) {
						unlink('uploaded/'.$row->photo); 

					}
				}
	  
			}
		}
		*/

		
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_user->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }

	public function getStatusList()
	{
			echo json_encode($this->Model_table_status->get_all());
	}

	public function getRoleList()
	{
			echo json_encode($this->Model_table_role->get_all());
	}
	
}	