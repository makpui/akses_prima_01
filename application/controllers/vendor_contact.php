<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class vendor_contact extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
		$this->load->model("Model_table_status");
 		$this->load->model("Model_table_vendor_type");		
 		$this->load->model("Model_table_vendor");		
 		$this->load->model("Model_table_vendor_contact");
 		$this->load->model("Model_table_role");
 		$this->load->model("Model_table_user");			
    }
    
    public function index()
	{		
		$vendortypetable_txt = "";		
        $id = "";
        $name = "";
        $email = "";		
        $phone_no = "";
        $address = "";
		$statustable_name = "";	
		
		$where[] = array('key' => 'row_id', 'value' => $this->uri->segment(3));
		$row = $this->Model_table_vendor->get_rows($where);		
		
        if($row != "") {
			$row_id = $row[0]->row_id;
			$vendortypetable_txt = $this->Model_table_vendor_type->get_txt_basedon_row_id($row[0]->vendortype_id)[0]->txt;
            $id = $row[0]->id;
            $name= $row[0]->name;
            $address = $row[0]->address;
            $phone_no = $row[0]->phone_no;
            $email = $row[0]->email;
			$statustable_name = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
        }
		
		$data = array(
		    'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),             
            'user_role_id' => $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id,
						
            'row_id' 				=> $row_id,
            'vendortypetable_txt' 	=> $vendortypetable_txt,
            'id' 					=> $id,
            'name' 					=> $name,
            'address' 				=> $address,
            'phone_no'	 			=> $phone_no,
            'email' 				=> $email,
			'statustable_name' 		=> $statustable_name,
			
        );
        $this->template->display_app('page/vendor_contact_vw', $data);
    }

    public function gridview() {
        $where = $this->input->post("where");
				
		$list = $this->Model_table_vendor_contact->get_rows($where);				
		if(!is_array($list)) { $list = array(); }
		
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $line) 
        {
            $no++;          
            $row = array();
            $link = '
                <div class="btn-group">
                  <a href="#" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
					';
					
			if($this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 1 || $this->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id == 5)
			{					
            $link .= '
                    <li><a href="#" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
                    <li><a href="#" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					';
			}
			
            $link .= '
                  </ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->name.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->position.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->organization.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->email.'</div>'; 
            $row[] = '<div style="text-align:center">'.$line->mobile_no.'</div>';
            $row[] = '<div style="text-align:center">'.showStatusIconActive($line->status_id).'</div>'; 

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_vendor_contact->get_all_active()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function form() {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
        $txtvendorid = $this->input->post("txtfilter_vendor_id"); 
		
		$txtname = '';
		$txtposition = '';
		$txtorganization = '';
		$txtemail = '';
		$txtmobileno = '';
		$txtstatusid = '';
		
		$txtvendortablename = ''; 
		
		$option_status = '<option value="1">Active</option>';

        if($txtstate == "edit") {

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$row = $this->Model_table_vendor_contact->get_rows($where);
		
            if($row != "") {
                $txtvendorid = $row[0]->vendor_id; 
                $txtname = $row[0]->name; 
                $txtposition = $row[0]->position;		
				$txtorganization = $row[0]->organization; 
				$txtemail = $row[0]->email; 
				$txtmobileno = $row[0]->mobile_no; 
				$txtstatusid = $row[0]->status_id;
            }
			$option_status = '<option value="'.$txtstatusid.'" selected >'.$this->Model_table_status->get_name_basedon_row_id($txtstatusid)[0]->name.'</option>';
         }

        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" style="background:#fff" readonly />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />
                    
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Vendor: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtvendorid_old" name="txtvendorid_old" class="form-control" value="'.$txtvendorid.'" />
                        <input type="hidden" id="txtvendorid" name="txtvendorid" class="form-control" value="'.$txtvendorid.'"  readonly />
							<br>'.$this->Model_table_vendor->get_name_basedon_row_id($txtvendorid)[0]->name.'
                      </div>
                    </div>
                                  
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Name: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtname_old" name="txtname_old" class="form-control" value="'.$txtname.'" />
                        <input type="text" id="txtname" name="txtname" class="form-control" value="'.$txtname.'" />
                      </div>
                    </div>
					
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Position: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtposition_old" name="txtposition_old" class="form-control" value="'.$txtposition.'" />
                        <input type="text" id="txtposition" name="txtposition" class="form-control" value="'.$txtposition.'" />
                      </div>
                    </div>
					
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Organization: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtorganization_old" name="txtorganization_old" class="form-control" value="'.$txtorganization.'" />
                        <input type="text" id="txtorganization" name="txtorganization" class="form-control" value="'.$txtorganization.'" />
                      </div>
                    </div>

					<div class="col-md-12">
                      <div class="form-group">
                        <label>Email: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtemail_old" name="txtemail_old" class="form-control" value="'.$txtemail.'" />
                        <input type="text" id="txtemail" name="txtemail" class="form-control" value="'.$txtemail.'" />
                      </div>
                    </div>

					<div class="col-md-12">
                      <div class="form-group">
                        <label>Mobile No: <span style="color: #ff0000">*</span></label>
                        <input type="hidden" id="txtmobileno_old" name="txtmobileno_old" class="form-control" value="'.$txtmobileno.'" />
                        <input type="text" id="txtmobileno" name="txtmobileno" class="form-control" value="'.$txtmobileno.'" />
                      </div>
                    </div>
					
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Status: <span style="color: #ff0000">*</span></label>
                    <input type="hidden" id="txtstatusid_old" name="txtstatusid_old" class="form-control" value="'.$txtstatusid.'" />
                    <select id="txtstatusid" name="txtstatusid" class="form-control select2"  required>
						'.$option_status.'                    
                    </select>
                  </div>
                </div>
                  
                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function view()
	{	
        $txtrowid = $this->input->post("txtrowid");
		$txtvendortablename = ''; 		
		$txtname = '';
		$txtposition = '';
		$txtorganization = '';
		$txtemail = '';
		$txtmobileno = '';		
		$txtstatustablename = ''; 

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_vendor_contact->get_rows($where);
        
        if($row != "")
		{
			$txtvendorid = $row[0]->vendor_id; 
			$txtvendortablename = $this->Model_table_vendor->get_name_basedon_row_id($row[0]->vendor_id)[0]->name; 								
			$txtname = $row[0]->name; 
			$txtposition = $row[0]->position;		
			$txtorganization = $row[0]->organization; 
			$txtemail = $row[0]->email; 
			$txtmobileno = $row[0]->mobile_no; 
			$txtstatustablename = $this->Model_table_status->get_name_basedon_row_id($row[0]->status_id)[0]->name;
		}        

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>
                
                    <tr>
                        <td class="view-title" style="width: 30%">Vendor:</td>
                        <td class="view-txt">'.$txtvendortablename.'</td>
                    </tr>
                  
                  <tr>
                      <td class="view-title" style="width: 30%">Name:</td>
                      <td class="view-txt">'.$txtname.'</td>
                  </tr>

					<tr>
						<td class="view-title" style="width: 30%">Position:</td>
						<td class="view-txt">'.$txtposition.'</td>
					</tr>
					<tr>
						<td class="view-title" style="width: 30%">Organization:</td>
						<td class="view-txt">'.$txtorganization.'</td>
					</tr>
					
					<tr>
						<td class="view-title" style="width: 30%">Email:</td>
						<td class="view-txt">'.$txtemail.'</td>
					</tr>
					<tr>
						<td class="view-title" style="width: 30%">Mobile No.:</td>
						<td class="view-txt">'.$txtmobileno.'</td>
					</tr>

                <tr>
                    <td class="view-title" style="width: 30%">Status:</td>
                    <td class="view-txt">'.$txtstatustablename.'</td>
                </tr>
                  
              </tbody>
            </table>
        ';

        $data['table'] = $table;
        echo json_encode($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = (int)$this->input->post("txtrowid");
        $txtvendorid = (int)$this->input->post("txtvendorid");
		$txtname = $this->input->post("txtname");
		$txtposition = $this->input->post("txtposition");
		$txtorganization = $this->input->post("txtorganization"); 
		$txtemail = $this->input->post("txtemail"); 
		$txtmobileno = $this->input->post("txtmobileno"); 
		$txtstatusid = (int)$this->input->post("txtstatusid");      

        if($txtstate == 'add')
        {
            $insert = array(
                'vendor_id' => $txtvendorid,
				'name' => $txtname,
				'position' => $txtposition,
				'organization' => $txtorganization,
				'email' => $txtemail,
				'mobile_no' => $txtmobileno,
				'status_id' => $txtstatusid,				
            );
			
			$this->Model_table_vendor_contact->insert($insert);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {            
            $update = array(
                'vendor_id' => $txtvendorid,
				'name' => $txtname,
				'position' => $txtposition,
				'organization' => $txtorganization,
				'email' => $txtemail,
				'mobile_no' => $txtmobileno,
				'status_id' => $txtstatusid,				
            );

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
            $this->Model_table_vendor_contact->update($update, $where);
			
            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");
        
		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_vendor_contact->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }
	
	public function getStatusList()
	{
		echo json_encode($this->Model_table_status->get_all());
	}

}	