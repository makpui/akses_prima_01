<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class menu extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library("template");
		$this->load->model("Model_table_menu");
	}

    public function index()
	{
		$data = array( 
			'lbl_controller' => str_replace("_", " ", $this->uri->segment(1)),						
		);

		$this->template->display_app('page/menu_vw', $data);

		unset($lbl_controller);
		unset($data);
	}

    public function gridview()
	{
		$where = $this->input->post("where");
		
		$list = $this->Model_table_menu->get_rows($where);
		if(!is_array($list)) { $list = array(); }
		
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $line)
        {
            $no++;
            $row = array();
            $link = '
                <div class="btn-group">
					<a href="javascript:void()" data-toggle="dropdown" style="color:#111"><i class="fa fa-folder-open"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="javascript:void()" style="color:#111" data-toggle="modal" data-target="#modalForm" onclick="generateModalView(\''.$line->row_id.'\')">View</a></li>
						<li><a href="javascript:void()" style="color:#111" onclick="generateModalForm(\'edit\', \''.$line->row_id.'\')">Update</a></li>
						<li><a href="javascript:void()" style="color:#111" onclick="doDelete(\''.$line->row_id.'\')">Delete</a></li>
					</ul>
                </div>
            ';
            $row[] = '<div style="text-align:center">'.$link.'</div>';
            $row[] = '<div style="text-align:center">'.$line->code.'</div>';
            $row[] = '<div style="text-align:center">'.$line->name.'</div>';
            $row[] = '<div style="text-align:center">'.$line->remark.'</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => sizeof($this->Model_table_menu->get_all()),
            "recordsFiltered" => sizeof($list),
            "data" => $data,
        );

        echo json_encode($output);

		unset($where);
		unset($list);
		unset($row);
		unset($data);
		unset($output);
    }

    public function form()
	{
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
		$txtcode = '';
		$txtname = '';
		$txtremark = '';
		
        if($txtstate == "edit")
		{
			$where[] = array('key' => 'row_id', 'value' => $txtrowid);
			$row = $this->Model_table_menu->get_rows($where);
            if($row != "")
			{
                $txtcode = $row[0]->code;
                $txtname = $row[0]->name;
				$txtremark = $row[0]->remark;
            }
        }
		unset($where);
		unset($row);

        $table = '
            <div class="row">
                <form method="POST" class="formInput" enctype="multipart/form-data">
                    <input type="hidden" id="txtrowid" name="txtrowid" class="form-control" value="'.$txtrowid.'" />
                    <input type="hidden" id="txtstate" name="txtstate" class="form-control" value="'.$txtstate.'" />

					<div class="col-md-12">
						<div class="form-group">
							<label>Code: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtcode_old" name="txtcode_old" class="form-control" value="'.$txtcode.'" />
							<input type="text" id="txtcode" name="txtcode" class="form-control" value="'.$txtcode.'" />
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Menu Name: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtname_old" name="txtname_old" class="form-control" value="'.$txtname.'" />
							<input type="text" id="txtname" name="txtname" class="form-control" value="'.$txtname.'" />
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Remark: <span style="color: #ff0000">*</span></label>
							<input type="hidden" id="txtremark_old" name="txtremark_old" class="form-control" value="'.$txtremark.'" />
							<input type="text" id="txtremark" name="txtremark" class="form-control" value="'.$txtremark.'" />
						</div>
					</div>

                </form>
            </div>
        ';

        $data['table'] = $table;
        echo json_encode($data);

		unset($table);
		unset($data);
    }

    public function view() {
        $txtrowid = $this->input->post("txtrowid");
		$txtcode = '';
		$txtname = '';
		$txtremark = '';

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$row = $this->Model_table_menu->get_rows($where);

        if($row != "") {
			$txtcode = $row[0]->code;
			$txtname = $row[0]->name;
			$txtremark = $row[0]->remark;
        }

        $table = '
            <table id="tablelist" class="table table-striped">
              <tbody>

					<tr>
						<td class="view-title" style="width: 30%">Code:</td>
						<td class="view-txt">'.$txtcode.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Menu:</td>
						<td class="view-txt">'.$txtname.'</td>
					</tr>

					<tr>
						<td class="view-title" style="width: 30%">Remark:</td>
						<td class="view-txt">'.$txtremark.'</td>
					</tr>

              </tbody>
            </table>
        ';

        $data['table'] = $table;

        echo json_encode($data);

		unset($where);
		unset($row);
		unset($table);
		unset($data);
    }

    public function save()
    {
        $txtstate = $this->input->post("txtstate");
        $txtrowid = $this->input->post("txtrowid");
		$txtcode = $this->input->post("txtcode");
		$txtname = $this->input->post("txtname");
		$txtremark = $this->input->post("txtremark");

        if($txtstate == 'add')
        {
            $insert = array(
							'code' => $txtcode,
							'name' => $txtname,
							'remark' => $txtremark,
						);

			$this->Model_table_menu->insert($insert);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
        else
        {
            $update = array(
							'code' => $txtcode,
							'name' => $txtname,
							'remark' => $txtremark,
							);

			$where[] = array('key' => 'row_id', 'value' => $txtrowid);

            $this->Model_table_menu->update($update, $where);

            $data['msg'] = "Process successful";
            $data['type'] = 1;
            echo json_encode($data);
        }
    }

    public function delete(){
        $txtrowid = $this->input->post("txtrowid");

		$where[] = array('key' => 'row_id', 'value' => $txtrowid);
		$this->Model_table_menu->delete($where);

        $data['msg'] = "Process successful";
        $data['type'] = 1;
        echo json_encode($data);
    }


}
