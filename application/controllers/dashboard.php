<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library("template");
    }
    
    public function index() {
        $controller = $this->uri->segment(1);
        $lbl_controller = str_replace("_", " ", $controller);
        $page_url = base_url($controller);
		
		
		$data = array(
            'controller' => $this->uri->segment(1),
            'lbl_controller' => $lbl_controller,
            'page_url' => $page_url,
        );		
		
        $this->template->display_app('page/dashboard_vw', $data);
    }


}	