<?php
	if(!$this->session->userdata['SessionLogin']['SesLoginId'])
	{
		redirect(base_url());
	}
?>

<header class="main-header">
  <a href="#" class="logo">
    <span class="logo-mini logo-title">..</span>
    <span class="logo-lg logo-title"><img src="<?= base_url("assets/backend/dist/img") ?>/logo-w.png" style="width: 170px" /></span>
  </a>
  <nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <div class="navbar-custom-menu">
    </div>
  </nav>
</header>