<?php
	$CI =& get_instance();	
	$CI->load->model("Model_table_user");
	$CI->load->model("Model_table_role_menu");
	$CI->load->helper("my_helper");
	
	$my_role_id = $CI->Model_table_user->get_role_id_basedon_row_id($this->session->userdata['SessionLogin']['SesLoginId'])[0]->role_id;
	$my_menu_list = $CI->Model_table_role_menu->get_menu_id_basedon_role_id($my_role_id);
	unset($my_role_id);
	
	// Number of Pending Approval
	$color = '';
	$number_of_pending_approval = array();
	$sessionroleid = $this->session->userdata['SessionLogin']['SesUserRoleId'];
	if($sessionroleid == 1)
	{
		$number_of_pending_approval = my_get_all_pending_approval_list();
	}
	elseif($sessionroleid == 2)
	{
		foreach(my_get_all_pending_approval_list() as $row)
		{
			if($row->main_controller == 'sales_order')
			{
				$number_of_pending_approval[] = $row;
			}
			unset($row);
		}
	}
	elseif($sessionroleid == 5)
	{
		foreach(my_get_all_pending_approval_list() as $row)
		{
			if($row->main_controller == 'work_order')
			{
				$number_of_pending_approval[] = $row;
			}
			unset($row);
		}
	}	
	else
	{
		$number_of_pending_approval = my_get_pending_approval_list($this->session->userdata['SessionLogin']['SesLoginId']);	
		$color = 'red';
	}
	
	if($number_of_pending_approval) {
		$number_of_pending_approval = "<strong style='color:".$color."'>(".sizeof($number_of_pending_approval).")</strong>";
	} else {
		$number_of_pending_approval = "<strong>(0)</strong>";	
	}
	
	// to control hide or show menu
	$hide_start = '<!-- ';
	$hide_end = '--> ';
	
?>
		
<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url("assets/backend") ?>/dist/img/user.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?= $this->session->userdata['SessionLogin']['SesUserName'] ?></p>
        <a href="#"><i class="fa fa-suitcase"></i> <?= $this->session->userdata['SessionLogin']['SesUserRoleTxt'] ?></a>
		
		<input type="hidden" id="txtsession_loginid" name="txtsession_loginid" class="form-control" value="<?= $this->session->userdata['SessionLogin']['SesLoginId'] ?>" />
		
      </div>
    </div>

    <ul class="sidebar-menu">
      <li class="header" style="color:#FFF"><h4><strong>MENU</strong></h4></li>
      <?php if(!my_menu($my_menu_list,1)){echo $hide_start;} ?> <li><a href="<?= base_url("dashboard") ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li> <?php if(!my_menu($my_menu_list,1)){echo $hide_end;} ?> 
      <?php if(!my_menu($my_menu_list,2)){echo $hide_start;} ?> <li><a href="<?= base_url('pending_approval')?>"><i class="fa fa-check-square-o"></i> <span>Pending Approval <?= $number_of_pending_approval; ?></span></a></li> <?php if(!my_menu($my_menu_list,2)){echo $hide_end;} ?> 
      <?php if(!my_menu($my_menu_list,3)){echo $hide_start;} ?> <li><a href="<?= base_url("site") ?>"><i class="fa fa-rss"></i> <span>Site</span></a></li> <?php if(!my_menu($my_menu_list,3)){echo $hide_end;} ?> 

	  <?php if(!my_menu($my_menu_list,4)){echo $hide_start;} ?>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-list-alt"></i> <span>Project</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
	  <?php if(!my_menu($my_menu_list,4)){echo $hide_end;} ?>
		
          <?php if(!my_menu($my_menu_list,5)){echo $hide_start;} ?> <li><a href="<?= base_url("sales_order") ?>"><i class="fa fa-long-arrow-right"></i>Sales Order</a></li> <?php if(!my_menu($my_menu_list,5)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,6)){echo $hide_start;} ?> <li><a href="<?= base_url("work_order") ?>"><i class="fa fa-long-arrow-right"></i>Work Order</a></li> <?php if(!my_menu($my_menu_list,6)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,7)){echo $hide_start;} ?> <li><a href="<?= base_url("survey") ?>"><i class="fa fa-long-arrow-right"></i>Site Survey</a></li> <?php if(!my_menu($my_menu_list,7)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,8)){echo $hide_start;} ?> <li><a href="<?= base_url("sitac") ?>"><i class="fa fa-long-arrow-right"></i>Site Acquisition</a></li><?php if(!my_menu($my_menu_list,8)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,9)){echo $hide_start;} ?> <li><a href="<?= base_url("soiltest") ?>"><i class="fa fa-long-arrow-right"></i>Soil Test</a></li> <?php if(!my_menu($my_menu_list,9)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,10)){echo $hide_start;} ?> <li><a href="<?= base_url("cme") ?>"><i class="fa fa-long-arrow-right"></i>CME</a></li> <?php if(!my_menu($my_menu_list,10)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,11)){echo $hide_start;} ?> <li><a href="<?= base_url("atponair") ?>"><i class="fa fa-long-arrow-right"></i>ATP & On Air</a></li>  <?php if(!my_menu($my_menu_list,11)){echo $hide_end;} ?>
		  <?php if(!my_menu($my_menu_list,12)){echo $hide_start;} ?> <li><a href="<?= base_url("project_activity") ?>"><i class="fa fa-long-arrow-right"></i>Project Activity</a></li> <?php if(!my_menu($my_menu_list,12)){echo $hide_end;} ?> 

	  <?php if(!my_menu($my_menu_list,4)){echo $hide_start;} ?>
		</ul>
      </li>
	  <?php if(!my_menu($my_menu_list,4)){echo $hide_end;} ?>
	  
      <?php if(!my_menu($my_menu_list,13)){echo $hide_start;} ?> <li><a href="<?= base_url("tenant") ?>"><i class="fa fa-users"></i> <span>Tenant</span></a></li> <?php if(!my_menu($my_menu_list,13)){echo $hide_end;} ?> 
      <?php if(!my_menu($my_menu_list,14)){echo $hide_start;} ?> <li><a href="<?= base_url("vendor") ?>"><i class="fa fa-wrench"></i> <span>Vendor</span></a></li> <?php if(!my_menu($my_menu_list,14)){echo $hide_end;} ?> 

	  <?php if(!my_menu($my_menu_list,15)){echo $hide_start;} ?> 
      <li class="treeview">
        <a href="#">
          <i class="fa fa-cubes"></i> <span>Master Data</span> 
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
      <?php if(!my_menu($my_menu_list,15)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,16)){echo $hide_start;} ?> <li><a href="<?= base_url("product") ?>"><i class="fa fa-long-arrow-right"></i>Product</a></li> <?php if(!my_menu($my_menu_list,16)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,17)){echo $hide_start;} ?> <li><a href="<?= base_url("tower_type") ?>"><i class="fa fa-long-arrow-right"></i>Tower Type</a></li> <?php if(!my_menu($my_menu_list,17)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,18)){echo $hide_start;} ?> <li><a href="<?= base_url("area") ?>"><i class="fa fa-long-arrow-right"></i>Area</a></li> <?php if(!my_menu($my_menu_list,18)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,19)){echo $hide_start;} ?> <li><a href="<?= base_url("region") ?>"><i class="fa fa-long-arrow-right"></i>Region</a></li> <?php if(!my_menu($my_menu_list,19)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,20)){echo $hide_start;} ?> <li><a href="<?= base_url("province") ?>"><i class="fa fa-long-arrow-right"></i>Province</a></li> <?php if(!my_menu($my_menu_list,20)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,21)){echo $hide_start;} ?> <li><a href="<?= base_url("city") ?>"><i class="fa fa-long-arrow-right"></i>City</a></li> <?php if(!my_menu($my_menu_list,21)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,22)){echo $hide_start;} ?> <li><a href="<?= base_url("site_type") ?>"><i class="fa fa-long-arrow-right"></i>Site Type</a></li> <?php if(!my_menu($my_menu_list,22)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,23)){echo $hide_start;} ?> <li><a href="<?= base_url("tower_height") ?>"><i class="fa fa-long-arrow-right"></i>Tower Height</a></li> <?php if(!my_menu($my_menu_list,23)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,24)){echo $hide_start;} ?> <li><a href="<?= base_url("vendor_type") ?>"><i class="fa fa-long-arrow-right"></i>Vendor Type</a></li>  <?php if(!my_menu($my_menu_list,24)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,25)){echo $hide_start;} ?> <li><a href="<?= base_url("transmission_type") ?>"><i class="fa fa-long-arrow-right"></i>Transmission Type</a></li> <?php if(!my_menu($my_menu_list,25)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,26)){echo $hide_start;} ?> <li><a href="<?= base_url("drop_category") ?>"><i class="fa fa-long-arrow-right"></i>Drop Category</a></li> <?php if(!my_menu($my_menu_list,26)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,27)){echo $hide_start;} ?> <li><a href="<?= base_url("itemunit") ?>"><i class="fa fa-long-arrow-right"></i>Item Unit</a></li> <?php if(!my_menu($my_menu_list,27)){echo $hide_end;} ?> 	  
	  <?php if(!my_menu($my_menu_list,15)){echo $hide_start;} ?> 
        </ul>
      </li>
      <?php if(!my_menu($my_menu_list,15)){echo $hide_end;} ?>
	  
      <?php if(!my_menu($my_menu_list,28)){echo $hide_start;} ?> 
      <li class="treeview">
		<a href="#">
          <i class="fa fa-cogs"></i> <span>System Administrator</span> 
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
      <?php if(!my_menu($my_menu_list,28)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,29)){echo $hide_start;} ?> <li><a href="<?= base_url("sales_order_approval_flow") ?>"><i class="fa fa-long-arrow-right"></i>Sales Order Approval Flow</a></li> <?php if(!my_menu($my_menu_list,29)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,30)){echo $hide_start;} ?> <li><a href="<?= base_url("work_order_approval_flow") ?>"><i class="fa fa-long-arrow-right"></i>Work Order Approval Flow</a></li>  <?php if(!my_menu($my_menu_list,30)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,31)){echo $hide_start;} ?> <li><a href="<?= base_url("role") ?>"><i class="fa fa-long-arrow-right"></i>Role</a></li>  <?php if(!my_menu($my_menu_list,31)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,32)){echo $hide_start;} ?> <li><a href="<?= base_url("user") ?>"><i class="fa fa-long-arrow-right"></i>User</a></li>  <?php if(!my_menu($my_menu_list,32)){echo $hide_end;} ?>

          <?php if(!my_menu($my_menu_list,33)){echo $hide_start;} ?> <li><a href="<?= base_url("password") ?>"><i class="fa fa-long-arrow-right"></i>Password</a></li>  <?php if(!my_menu($my_menu_list,33)){echo $hide_end;} ?>
          <?php if(!my_menu($my_menu_list,34)){echo $hide_start;} ?> <li><a href="<?= base_url("menu") ?>"><i class="fa fa-long-arrow-right"></i>Menu</a></li>  <?php if(!my_menu($my_menu_list,34)){echo $hide_end;} ?>

          <?php if(!my_menu($my_menu_list,35)){echo $hide_start;} ?> <li><a href="<?= base_url("generatorscript") ?>"><i class="fa fa-long-arrow-right"></i>Generator</a></li>  <?php if(!my_menu($my_menu_list,35)){echo $hide_end;} ?> 
          <?php if(!my_menu($my_menu_list,36)){echo $hide_start;} ?> <li><a href="<?= base_url("systemlog") ?>"><i class="fa fa-long-arrow-right"></i>System Log</a></li> <?php if(!my_menu($my_menu_list,36)){echo $hide_end;} ?> 
      <?php if(!my_menu($my_menu_list,28)){echo $hide_start;} ?> 
        </ul>
      </li>
      <?php if(!my_menu($my_menu_list,28)){echo $hide_end;} ?> 
	  
      <?php if(!my_menu($my_menu_list,37)){echo $hide_start;} ?> <li><a href="<?= base_url("login/logout") ?>"><i class="fa fa-power-off"></i> <span>Logout</span></a></li> <?php if(!my_menu($my_menu_list,37)){echo $hide_end;} ?> 
    </ul>
  </section>
</aside>