<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-rss"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small>List of Data</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-rss"></i> <?= ucwords($lbl_controller) ?></a></li>
      <li class="active">List of data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
    
      <div class="col-xs-12">
        <div  style="clear:both; border-top: 3px solid #d2d6de; height: 17px"></div>

        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtstatusidfilter" name="txtstatusidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Status</option>
            </select>
          </div>
        </div>
		
        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtproductidfilter" name="txtproductidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Product</option>
            </select>
          </div>
        </div>


        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txttenantidfilter" name="txttenantidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Tenant</option>
            </select>
          </div>
        </div>

        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtareaidfilter" name="txtareaidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Area</option>
            </select>
          </div>
        </div>

        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtregionidfilter" name="txtregionidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Region</option>
            </select>
          </div>
        </div>		

        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtprovinceidfilter" name="txtprovinceidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Province</option>
            </select>
          </div>
        </div>		

        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtcityidfilter" name="txtcityidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All City</option>
            </select>
          </div>
        </div>		

        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtsitetypeidfilter" name="txtsitetypeidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Site Type</option>
            </select>
          </div>
        </div>
        
        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txttowerheightidfilter" name="txttowerheightidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Tower Height</option>
            </select>
          </div>
        </div>
        
        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txttransmissiontypeidfilter" name="txttransmissiontypeidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Transmission Type</option>
            </select>
          </div>
        </div>
		        
      </div>
            
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <div id='callback_msg'></div>
            <table id="tablelist" class="table table-bordered table-striped" style="width: 125%">
              <thead>
                <tr>
                  <th style="width:3%"></th>
                  <th style='width:5%'>Product</th> 
                  <th style='width:5%'>Tower Type</th> 
                  <th style='width:10%'>Tenant</th> 
                  <th style='width:10%'>Site Id API</th> 
                  <th style='width:10%'>Site Name API</th> 
                  <th style='width:10%'>Site Id Tenant</th> 
                  <th style='width:10%'>Site Name Tenant</th> 
                  <th style='width:5%'>Area</th> 
                  <th style='width:5%'>Region</th> 
                  <th style='width:5%'>Province</th> 
                  <th style='width:5%'>City</th> 
                  <th style='width:10%'>Address</th> 
                  <th style='width:5%'>Longitude</th> 
                  <th style='width:5%'>Latitude</th> 
                  <th style='width:5%'>Type</th> 
                  <th style='width:5%'>Height</th> 
                  <th style='width:5%'>Transmission</th> 
                  <th style='width:10%'>Remark</th>				  
				  <th style='width:2%'></th> 
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="modalForm" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-rss"></i> <?= strtoupper($lbl_controller) ?> FORM</h4>
      </div>
      <div class="modal-body">
        <div id="tableModal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
        <button type="button" id="btnSave" class="btn btn-success">SAVE</button>
      </div>
    </div>
  </div>
</div>

<script src="<?= base_url("assets/backend") ?>/controller/site.js"></script>