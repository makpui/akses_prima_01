<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> <?= ucwords($lbl_controller) ?></a></li>
      <li class="active"></li>
    </ol>
  </section>

  <section class="content">
	<div class="col-xs-12">
		<div class="box">
		  <div class="box-body">
			<div id='callback_msg'></div>

			<h4>
			  &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>&nbsp; <span class="title-page">Approval Pending</span>
			</h4>	
				
			<table id="tablelist" class="table table-bordered table-striped" style="width: 100%">
			  <thead>
				<tr>
				  <th style="width:17%">Action</th>
				  
				  <?php
					  if($lbl_session_role_id == 1 || $lbl_session_role_id == 2 || $lbl_session_role_id == 5) { 
						echo '<th style="width:10%">Approver</th>';
					  }
				  ?>
				  			  
				  <th style='width:10%'>Document</th> 
				  <th style='width:10%'>Date</th> 
				  <th style='width:10%'>Number</th> 
				  <th style='width:10%'>Number of Line</th>
				  
				  <?php
						if($lbl_session_role_id == 1 || $lbl_session_role_id == 2 || $lbl_session_role_id == 5) { 
							echo '<th style="width:30%">Customer</th>';
						}
						else
						{
							echo '<th style="width:40%">Customer</th>';
						}
				   ?>
				  
				  
				</tr>
			  </thead>
			  <tbody></tbody>
			</table>
		  </div>
		</div>
	</div>		
  </section>  
</div>


<div class="modal fade" id="modalForm1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-cubes"></i> APPROVAL / REJECT FORM</h4>
      </div>
      <div class="modal-body">
        <div id="tableModal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
        <button type="button" id="btnSave" class="btn btn-success">SAVE</button>
      </div>
    </div>
  </div>
</div>

<script src="<?= base_url("assets/backend") ?>/controller/pending_approval.js"></script>
