<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small>Graphic & Summary</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> <?= ucwords($lbl_controller) ?></a></li>
      <li class="active">Graphic & Summary</li>
    </ol>
	
  </section>

  <section class="content">
	
	
    <div class="row">

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-rss"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Total Site</span>
            <span class="info-box-number">1040</span>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-rss"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">MCP</span>
            <span class="info-box-number">41,410</span>
          </div>
        </div>
      </div>

      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-rss"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">MCP+</span>
            <span class="info-box-number">760</span>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="fa fa-rss"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">MACRO SST</span>
            <span class="info-box-number">2,000</span>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <p class="text-center">
                  <strong>Monthly Sales Report: 1 Jan, 2014 - 30 Jul, 2014</strong>
                </p>
                <div class="chart">
                  <canvas id="salesChart" style="height: 185px;"></canvas>
                </div>
              </div>

              <div class="col-md-4">
                <p class="text-center">
                  <strong>Site Completion</strong>
                </p>

                <div class="progress-group">
                  <span class="progress-text">Site Survey</span>
                  <span class="progress-number"><b>40</b>/200</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 40%"></div>
                  </div>
                </div>

                <div class="progress-group">
                  <span class="progress-text">SITAC</span>
                  <span class="progress-number"><b>30</b>/200</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 30%"></div>
                  </div>
                </div>

                <div class="progress-group">
                  <span class="progress-text">CME</span>
                  <span class="progress-number"><b>60</b>/200</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-green" style="width: 60%"></div>
                  </div>
                </div>

                <div class="progress-group">
                  <span class="progress-text">ATP & On Air</span>
                  <span class="progress-number"><b>70</b>/200</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">View All Sites</h3>
          </div>
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="pad">
                  <div id="world-map-markers" style="height: 325px;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Latest Orders</h3>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Sales No</th>
                    <th>Tenant</th>
                    <th>Total Sites</th>
                    <th>Site Completion</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="text-align: center"><a href="#">OR9842</a></td>
                    <td style="text-align: center">Telkomsel</td>
                    <td style="text-align: center">200</td>
                    <td style="text-align: center">
                      <div class="progress-group">
                        <div class="progress sm">
                          <div class="progress-bar progress-bar-green" style="width: 60%"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td style="text-align: center"><a href="#">OR9844</a></td>
                    <td style="text-align: center">XL</td>
                    <td style="text-align: center">32</td>
                    <td style="text-align: center">
                      <div class="progress-group">
                        <div class="progress sm">
                          <div class="progress-bar progress-bar-green" style="width: 70%"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td style="text-align: center"><a href="#">OR9842</a></td>
                    <td style="text-align: center">Indosat</td>
                    <td style="text-align: center">320</td>
                    <td style="text-align: center">
                      <div class="progress-group">
                        <div class="progress sm">
                          <div class="progress-bar progress-bar-green" style="width: 20%"></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Tenant</h3>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <div class="chart-responsive">
                  <canvas id="pieChart" height="150"></canvas>
                </div>
              </div>
              <div class="col-md-4">
                <ul class="chart-legend clearfix">
                  <li><i class="fa fa-circle-o text-red"></i> Telkomsel</li>
                  <li><i class="fa fa-circle-o text-green"></i> Indosat</li>
                  <li><i class="fa fa-circle-o text-yellow"></i> XL</li>
                  <li><i class="fa fa-circle-o text-aqua"></i> 3</li>
                  <li><i class="fa fa-circle-o text-light-blue"></i> Axis</li>
                  <li><i class="fa fa-circle-o text-gray"></i> Bolt</li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <BR>

        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Top 7 Sites with the Highest Cost</h3>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Tenant</th>
                    <th>Site Id</th>
                    <th>Cost</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="text-align: center">Telkomsel</td>
                    <td style="text-align: center">432312</td>
                    <td style="text-align: center">10000000</td>
                  </tr>
                  <tr>
                    <td style="text-align: center">Telkomsel</td>
                    <td style="text-align: center">432312</td>
                    <td style="text-align: center">10000000</td>
                  </tr>
                  <tr>
                    <td style="text-align: center">Telkomsel</td>
                    <td style="text-align: center">432312</td>
                    <td style="text-align: center">10000000</td>
                  </tr>
                  <tr>
                    <td style="text-align: center">Telkomsel</td>
                    <td style="text-align: center">432312</td>
                    <td style="text-align: center">10000000</td>
                  </tr>
                  <tr>
                    <td style="text-align: center">Telkomsel</td>
                    <td style="text-align: center">432312</td>
                    <td style="text-align: center">10000000</td>
                  </tr>
                  <tr>
                    <td style="text-align: center">Telkomsel</td>
                    <td style="text-align: center">432312</td>
                    <td style="text-align: center">10000000</td>
                  </tr>
                  <tr>
                    <td style="text-align: center">Telkomsel</td>
                    <td style="text-align: center">432312</td>
                    <td style="text-align: center">10000000</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </div>	

	
  </section>
  
  
  
  
</div>

<script src="<?= base_url("assets/backend") ?>/controller/dashboard.js"></script>
