<input type="hidden" id="txtfilter_user_row_id" value="<?= $usertable_row_id ?>" />

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-cubes"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small>List of Data</small>
    </h1>
    <div class="buttonaction">
      <a href="<?= base_url("user") ?>" class="btn btn-default btn-sm"><i class="fa fa-backward"></i> back</a>
    </div>
  </section>

  <section class="content">
    <div class="row">
    
      <div class="col-xs-12">
        <div  style="clear:both; border-top: 3px solid #d2d6de; height: 17px"></div>
        
        <table class="table" style="width: 100%;">
          <thead>
            <tr>
              <td style="text-align: left; width:15%"><b>Role</b></td>
              <td style="text-align: left; width:35%"><strong>: <?= $usertable_role_txt ?></strong></td>			
              <td style="text-align: leftt; width:15%"><b>User Name</b></td>
              <td style="text-align: left; width:35%"><strong>: <?= $usertable_name ?></strong></td>
            </tr>

            <tr>
              <td style="text-align: left; width:15%"></td>
              <td style="text-align: left; width:35%"></td>			
              <td style="text-align: left; width:15%">User Id</td>
              <td style="text-align: left; width:35%">: <?= $usertable_user_id ?></td>
            </tr>

            <tr>
              <td style="text-align: left; width:15%"></td>
              <td style="text-align: left; width:35%"></td>			
              <td style="text-align: left; width:15%">Email</td>
              <td style="text-align: left; width:35%">: <?= $usertable_email ?></td>
            </tr>
			
            <tr>
              <td style="text-align: left; width:15%"></td>
              <td style="text-align: left; width:35%"></td>			
              <td style="text-align: left; width:15%">Status</td>
              <td style="text-align: left; width:35%">: <?= $usertable_status_name ?></td>
            </tr>

            <tr>
              <td colspan="4"></td>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
        
      </div>
            
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <div id='callback_msg'></div>
            <table id="tablelist" class="table table-bordered table-striped" style="width: 100%">
              <thead>
                <tr>
                  <th style="width:5%"></th>
                  <th style="width:95%; text-align:left">Territory</th>				  
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="modalForm" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-cubes"></i> <?= strtoupper($lbl_controller) ?> FORM</h4>

      </div>
      <div class="modal-body">
        <div id="tableModal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
        <button type="button" id="btnSave" class="btn btn-success">SAVE</button>
      </div>
    </div>
  </div>
</div>

<script src="<?= base_url("assets/backend") ?>/controller/user_territory.js"></script>