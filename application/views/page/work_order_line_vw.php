<input type="hidden" id="txtfilter_workordertable_row_id" value="<?= $workordertable_row_id ?>" />
<input type="hidden" id="txtfilter_workordertable_workorder_no" value="<?= $workordertable_workorder_no ?>" />

<input type="hidden" id="workordertable_work_flow_milestone_id" value="<?= $workordertable_work_flow_milestone_id ?>" />
<input type="hidden" id="workflowmilestonetable_creation_state" value="<?= $workflowmilestonetable_creation_state ?>" />
<input type="hidden" id="workflowmilestonetable_ending_state" value="<?= $workflowmilestonetable_ending_state ?>" />
<input type="hidden" id="salesorderlinetable_work_order_isexist" value="<?= $workorderlinetable_work_order_isexist ?>" />
<input type="hidden" id="salesorderapprovaltable_work_order_isexist" value="<?= $workorderapprovaltable_work_order_isexist ?>" />
<input type="hidden" id="is_so_maker" value="<?= $is_so_maker ?>" />

<input type="hidden" id="workordertable_work_order_date" value="<?= $workordertable_workorder_date ?>" />


<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-table"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small>List of Data</small>
    </h1>
    <div class="buttonaction">
      <a href="<?= base_url("work_order") ?>" class="btn btn-default btn-sm"><i class="fa fa-backward"></i> back</a>
      <a href="<?= base_url("work_order_approval/index")."/".$workordertable_row_id ?>" class="btn btn-default btn-sm">Approval</a>
      <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-send"></i> submit</a>
      <a href="<?= base_url("work_order_line")."/printwo/".$workordertable_row_id ?>" class="btn btn-success btn-sm"><i class="fa fa-print"></i> print</a>
    </div>
  </section>

  <section class="content">
    <div class="row">
    
      <div class="col-xs-12">
        <div  style="clear:both; border-top: 3px solid #d2d6de; height: 17px"></div>
        
        <table class="table" style="width: 100%;">
          <thead>
            <tr>
              <td style="width:15%; text-align: right"><b>Pending Approval Role</td>
              <td style="width:25%"><strong><?= $workordertable_pending_approval_role?></strong></td>			
              <td style="text-align: right"><b>Vendor Name</td>
              <td><?= $workordertable_vendor_name?></td>
            </tr>
            <tr>
              <td style="width:15%; text-align: right"><b>Work Order No.</b></td>
              <td style="width:25%"><?= $workordertable_workorder_no ?></td>
              <td style="text-align: right"><b>Vendor Address </b></td>
              <td><?= $workordertable_vendor_address ?></td>
            </tr>
            <tr>
              <td style="width:15%; text-align: right"><b>Work Order Date</b></td>
              <td style="width:15%"><?= $workordertable_workorder_date ?></td>
              <td style="text-align: right"><b>Vendor Office Phone</b></td>
              <td><?= $workordertable_vendor_phone_no ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Project Manager</td>
              <td><?= $workordertable_project_manager ?></td>
              <td style="text-align: right"><b>Vendor Contact Name </b></td>
              <td><?= $workordertable_vendor_contact_name ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Milestone</td>
              <td><?= $workordertable_work_flow_milestone ?></td>
              <td style="text-align: right"><b>Vendor Contact Mobile Phone </b></td>
              <td><?= $workordertable_vendor_contact_mobile_no ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Approval Progress</td>
              <td><?= $workordertable_approval_progress ?></td>
              <td style="text-align: right"><b>Vendor Contact Email</b></td>
              <td><?= $workordertable_vendor_contact_email ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Approval Result</td>
              <td><?= $workordertable_approval_result ?></td>
              <td style="text-align: right"><b>Status</td>
              <td><?= $workordertable_status_name ?></td>
            </tr>			
            <tr>
              <td colspan="4"></td>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
        
      </div>
            
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <div id='callback_msg'></div>
            <table id="tablelist" class="table table-bordered table-striped" style="width: 100%">
              <thead>
                <tr>
                  <th style="width:3%"></th>
                  <th style='width:10%'>Sales Order No.</th> 
                  <th style='width:10%'>Site Id API</th> 
                  <th style='width:10%'>Site Name API</th> 
                  <th style='width:20%'>Address</th>
                  <th style='width:5%'>Longitude</th> 
                  <th style='width:5%'>Latitude</th> 
                  <th style='width:15%'>Scope</th>
                  <th style='width:10%'>Target Started</th>
                  <th style='width:10%'>Target Finished</th>				  
                  <th style='width:20%'>Remark</th>				  
                  <th style='width:2%'></th>
                </tr>				
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="modalForm" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-list-alt"></i> WORK ORDER LINE FORM</h4>
      </div>
      <div class="modal-body">
        <div id="tableModal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
        <button type="button" id="btnSave" class="btn btn-success">SAVE</button>
      </div>
    </div>
  </div>
</div>

<script src="<?= base_url("assets/backend") ?>/controller/work_order_line.js"></script>