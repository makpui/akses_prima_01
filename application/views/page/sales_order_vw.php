<input type="hidden" id="is_so_maker" value="<?= $is_so_maker ?>" />

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-table"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small>List of Data</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-table"></i> <?= ucwords($lbl_controller) ?></a></li>
      <li class="active">List of data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
    
      <div class="col-xs-12">
        <div  style="clear:both; border-top: 3px solid #d2d6de; height: 17px"></div>
        
        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtstatusidfilter" name="txtstatusidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Status</option>
            </select>
          </div>
        </div>

        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txtworkflowmilestoneidfilter" name="txtworkflowmilestoneidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Milestone</option>
            </select>
          </div>
        </div>

        <div class="col-md-2" style="padding-left: 0px;">
          <div class="form-group">
            <select id="txttenantidfilter" name="txttenantidfilter" class="form-control" onchange="generateTable()">
              <option value="">View All Tenant</option>
            </select>
          </div>
        </div>
        
      </div>
	  
	  
	  
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <div id='callback_msg'></div>
            <table id="tablelist" class="table table-bordered table-striped" style="width: 100%">
              <thead>
                <tr>
                  <th style="width:3%"></th>
                  <th style='width:15%'>SO No.</th> 
                  <th style='width:10%'>SO Date</th> 
                  <th style='width:10%'>DRM Date</th> 
                  <th style='width:5%'>Batch</th> 
                  <th style='width:15%'>PO No. Tenant</th> 
                  <th style='width:10%'>PO Date Tenant</th> 
                  <th style='width:15%'>Tenant</th>				  
				  <th style='width:15%'>Number Of Line</th>
				  
                  <th style='width:10%'>SO Milestone</th>
                  <th style='width:10%'>Approval Progress</th>                  
                  <th style='width:20%'>Approval Pending At</th>				  
				  <th style='width:10%'>Approval Result</th>			  
                  <th style='width:2%'></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="modalForm" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-table"></i> <?= strtoupper($lbl_controller) ?> FORM</h4>
      </div>
      <div class="modal-body">
        <div id="tableModal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
        <button type="button" id="btnSave" class="btn btn-success">SAVE</button>
      </div>
    </div>
  </div>
</div>

<script src="<?= base_url("assets/backend") ?>/controller/sales_order.js"></script>