<!-- -->
<input type="hidden" id="user_role_id" value="<?= $user_role_id ?>" />
<input type="hidden" id="txtfilter_vendor_id" value="<?= $row_id ?>" />

<!-- -->

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-cubes"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small>List of Data</small>
    </h1>
    <div class="buttonaction">
      <a href="<?= base_url("vendor") ?>" class="btn btn-default btn-sm"><i class="fa fa-backward"></i> back</a>
    </div>
  </section>

  <section class="content">
    <div class="row">
    
      <div class="col-xs-12">
        <div  style="clear:both; border-top: 3px solid #d2d6de; height: 17px"></div>
        
        <table class="table" style="width: 100%;">
          <thead>
            <tr>
              <td style="text-align: right"><b>Vendor Type</b></td>
              <td><?= $vendortypetable_txt ?></td>
              <td style="width:15%; text-align: right"><b>Vendor Id</b></td>
              <td style="width:35%"><?= $id ?> </td>
            </tr>
            <tr>
              <td style="width:15%; text-align: right"><b>Name</b></td>
              <td style="width:35%"><?= $name ?></td>
              <td style="text-align: right"><b>Email</b></td>
              <td><?= $email ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Address</b></td>
              <td><?= $address ?></td>
              <td style="text-align: right"><b>Phone</b></td>
              <td><?= $phone_no ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Status</td>
              <td><?= $statustable_name ?></td>
            </tr>
            <tr>
              <td colspan="4"></td>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
        
      </div>
            
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <div id='callback_msg'></div>
            <table id="tablelist" class="table table-bordered table-striped" style="width: 100%">
              <thead>
                <tr>
                  <th style="width:3%"></th>
                  <th style='width:20%'>Name</th> 
                  <th style='width:20%'>Position</th> 
                  <th style='width:20%'>Organization</th> 
                  <th style='width:20%'>Email</th> 
                  <th style='width:15%'>Mobile Number</th>
                  <th style='width:2%'></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="modalForm" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-cubes"></i> <?= strtoupper($lbl_controller) ?> FORM</h4>
		
      </div>
      <div class="modal-body">
        <div id="tableModal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
        <button type="button" id="btnSave" class="btn btn-success">SAVE</button>
      </div>
    </div>
  </div>
</div>

<script src="<?= base_url("assets/backend") ?>/controller/vendor_contact.js"></script>