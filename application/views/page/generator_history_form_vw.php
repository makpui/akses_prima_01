<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-table"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small>List of Data</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-table"></i> <?= ucwords($lbl_controller) ?></a></li>
      <li class="active">List of data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div style="clear:both; height:20px"></div>
          <b>Script Controller</b>: <?= $controller ?>.php
          <div style="clear:both; height:5px"></div>
          <iframe src="<?= base_url() ?>assets/generate/<?= $controller ?>.txt" style="border:none; background:#FFF; width:100%; height:400px"></iframe>

          <div style="clear:both; height:50px"></div>
          <b>Script View</b>: <?= $controller ?>_vw.php
          <div style="clear:both; height:5px"></div>
          <iframe src="<?= base_url() ?>assets/generate/<?= $controller ?>_vw.txt" style="border:none; background:#FFF; width:100%; height:400px"></iframe>

          <div style="clear:both; height:50px"></div>
          <b>Script Create Table</b>: <?= $controller ?>.sql
          <div style="clear:both; height:5px"></div>
          <iframe src="<?= base_url() ?>assets/generate/<?= $controller ?>_tbl.txt" style="border:none; background:#FFF; width:100%; height:400px"></iframe>

          <div style="clear:both; height:50px"></div>
          <b>Script JS</b>: <?= $controller ?>.js
          <div style="clear:both; height:5px"></div>
          <iframe src="<?= base_url() ?>assets/generate/<?= $controller ?>_js.txt" style="border:none; background:#FFF; width:100%; height:400px"></iframe>
        </div>
      </div>
    </div>
  </section>
</div>