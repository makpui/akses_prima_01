<input type="hidden" id="txtfilter_salesordertable_row_id" value="<?= $salesordertable_row_id ?>" />
<input type="hidden" id="txtfilter_salesordertable_salesorder_no" value="<?= $salesordertable_salesorder_no ?>" />

<input type="hidden" id="show_button_add" value="<?= $show_button_add  ?>" />

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-table"></i>&nbsp; <span class="title-page"><?= strtoupper($lbl_controller) ?></span>
      <small>List of Data</small>
    </h1>
		
    <div class="buttonaction">
      <a href="<?= base_url("sales_order") ?>" class="btn btn-default btn-sm"><i class="fa fa-backward"></i> back</a> 
     <a href="<?= base_url("sales_order_line/index")."/".$salesordertable_row_id ?>" class="btn btn-default btn-sm">Line</a>
	  <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-send"></i> submit</a>
      <a href="<?= base_url("sales_order_approval")."/printso/".$salesordertable_row_id ?>" class="btn btn-success btn-sm"><i class="fa fa-print"></i> print</a>
    </div>
  </section>

  <section class="content">
    <div class="row">
    
      <div class="col-xs-12">
        <div  style="clear:both; border-top: 3px solid #d2d6de; height: 17px"></div>
        
        <table class="table" style="width: 100%;">
          <thead>
            <tr>
              <td style="width:15%; text-align: right"><b>Pending Approvel Role</td>
              <td style="width:25%"><strong><?= $salesordertable_pending_approval_role ?></strong></td>			
              <td style="text-align: right"><b>Tenant Name</td>
              <td><?= $salesordertable_tenant_name ?></td>
            </tr>
            <tr>
              <td style="width:15%; text-align: right"><b>Sales Order No.</b></td>
              <td style="width:25%"><?= $salesordertable_salesorder_no ?></td>
              <td style="text-align: right"><b>Tenant's PO No. </b></td>
              <td><?= $salesordertable_po_tenant_no ?></td>
            </tr>
            <tr>
              <td style="width:15%; text-align: right"><b>Sales Order Date</b></td>
              <td style="width:25%"><?= $salesordertable_salesorder_date ?></td>
              <td style="text-align: right"><b>Tenant's PO Date </b></td>
              <td><?= $salesordertable_po_tenant_date ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Milestone</td>
              <td><?= $salesordertable_work_flow_milestone ?></td>
              <td style="text-align: right"><b>DRM Date</b></td>
              <td><?= $salesordertable_drm_date ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Approval Progress</td>
              <td><?= $salesordertable_approval_progress ?></td>
              <td style="text-align: right"><b>Batch</b></td>
              <td><?= $salesordertable_batch ?></td>
            </tr>
            <tr>
              <td style="text-align: right"><b>Approval Result</td>
              <td><?= $salesordertable_approval_result ?></td>
              <td style="text-align: right"><b>Status</td>
              <td><?= $salesordertable_status_name ?></td>
            </tr>
            <tr>
              <td colspan="4"></td>
            </tr>
          </thead>
          <tbody></tbody>
        </table>       
      </div>
            
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <div id='callback_msg'></div>
            <table id="tablelist" class="table table-bordered table-striped" style="width: 100%">
              <thead>
                <tr>
                  <th style="width:3%"></th>
                  <th style='width:5%'>No.</th> 
				  <th style='width:15%'>Date</th>
                  <th style='width:15%'>Role Name</th> 
                  <th style='width:10%'>Action</th> 
                  <th style='width:37%'>Remark</th> 
                  <th style='width:2%'></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="modalForm" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-table"></i> <?= strtoupper($lbl_controller) ?> FORM</h4>
      </div>
      <div class="modal-body">
        <div id="tableModal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
        <button type="button" id="btnSave" class="btn btn-success">SAVE</button>
      </div>
    </div>
  </div>
</div>

<script src="<?= base_url("assets/backend") ?>/controller/sales_order_approval.js"></script>