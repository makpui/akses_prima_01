<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login - <?= PAGE_TITLE ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url("assets/backend") ?>/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url("assets/backend") ?>/plugins/custom/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url("assets/backend") ?>/plugins/custom/ionicons.min.css">
  <link rel="stylesheet" href="<?= base_url("assets/backend") ?>/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?= base_url("assets/backend") ?>/dist/css/custom.css">
  <link rel="stylesheet" href="<?= base_url("assets/backend") ?>/plugins/iCheck/square/blue.css">
  <link rel="shortcut icon" href="<?= base_url("assets/backend") ?>/dist/img/icon-aksesprima.png"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-box-body" style="background: rgba(255, 255, 255, 0.9); border-radius: 10px">
    <div class="login-logo">
      <img class="logo-title" src="<?= base_url("assets/backend/dist/img") ?>/logo.png" style="width: 250px" />
    </div>
    <form action="<?= base_url("login") ?>/doLogin" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="txtusername" class="form-control" placeholder="ID">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="txtpassword" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
        
      <span style="font-size:12px">Prove us you are not a robot!</span>
      <div class="row">
        <div class="col-xs-8">
          <input type="text" name="txtl" class="form-control" readonly="" value="<?= $question ?>">
          <input type="hidden" name="txtprovequestion" class="form-control" readonly="" value="<?= $answer ?>">
        </div>
        <div class="col-xs-4">
          <input type="text" name="txtproveanswer" class="form-control" placeholder="Result">
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <div style="clear:both; height:25px"></div>
          <button type="submit" class="btn btn-danger btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>
</div>

<?php
  if(isset($_GET['msg'])) {
?>
    <div class="alert alert-danger alert-dismissible" style="position: absolute; top:20px; right:20px">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?= $_GET['msg'] ?>
    </div>
<?php
  }
?>

<script src="<?= base_url("assets/backend") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?= base_url("assets/backend") ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url("assets/backend") ?>/plugins/iCheck/icheck.min.js"></script>
</body>
</html>
