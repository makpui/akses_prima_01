<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class m_pdf {

  protected $_ci;

  function __construct()
  {
      $this->_ci = &get_instance();
      log_message('Debug', 'mPDF class is loaded.');
  }

  function load($param = "A4-L") {
    include_once APPPATH . '/third_party/mpdf/mpdf.php';
    $margin_top = 25;
	return new mPDF("en-GB", $param, "", "", 5, 5, $margin_top, 10, 6, 3);
  }

}
