<?php

class sendmail{
  protected $_ci;

  function __construct() {
      $this->_ci = &get_instance();

      $this->_ci->load->library('email');
      $config = array('protocal' => EMAIL_PROTOCOL,
                      'smtp_host' => SMTP_HOST,
                      'smtp_user' => SMTP_USER,
                      'smtp_pass' => SMTP_PASS,
                      'smtp_port' => SMTP_PORT,
                      'smtp_crypto' => SMTP_CRYPTO,
                      'mailtype' => 'html'
                    );
      $this->_ci->email->initialize($config);
  }

  public function sales_order_approval($to="",
										$name,
										$pending_approval_link,
										$salesorder_no,
										$salesorder_date,
										$drm_date,
										$batch,
										$po_tenant_no,
										$po_tenant_date,
										$tenanttable_name,
										$approval_status,
										$approval_remark										
										)
 {	
 	
    if($to != ""){
      $data = array();
      $data['name'] = $name;
	  $data['pending_approval_link'] = $pending_approval_link;	  
      $data['salesorder_no'] = $salesorder_no;	  
      $data['salesorder_date'] = $salesorder_date;
      $data['drm_date'] = $drm_date;
      $data['batch'] = $batch;
      $data['po_tenant_no'] = $po_tenant_no;
      $data['po_tenant_date'] = $po_tenant_date;
      $data['tenanttable_name'] = $tenanttable_name;
      $data['approval_status'] = $approval_status;
      $data['approval_remark'] = $approval_remark;
	  
	  $this->_ci->email->from(SMTP_USER);
      $this->_ci->email->to($to);
      $this->_ci->email->subject(($approval_status == 0 ? 'Rejection' : 'Approval' ) .' Sales Order No. ' .$salesorder_no);
      $this->_ci->email->message($this->_ci->load->view('mail/so_approval', $data, true));

      if(!$this->_ci->email->send()){
		$this->email->print_debugger(array('headers'));
	  }
    }
  }

  public function work_order_approval(  $to="",
										$name,
										$pending_approval_link,
										$workorder_no,
										$workorder_date,
										$vendor_name,
										$vendor_contact_name,
										$vendor_contact_mobile_no,
										$vendor_contact_email,
										$product_group,
										$project_manager,
										$approval_status,
										$approval_remark										
										)
 {

    if($to != ""){
      $data = array();
      $data['name'] = $name;
	  $data['pending_approval_link'] = $pending_approval_link;	  
      $data['workorder_no'] = $workorder_no;	  
      $data['workorder_date'] = $workorder_date;
      $data['vendor_name'] = $vendor_name;
      $data['vendor_contact_name'] = $vendor_contact_name;
      $data['vendor_contact_mobile_no'] = $vendor_contact_mobile_no;
      $data['vendor_contact_email'] = $vendor_contact_email;
      $data['product_group'] = $product_group;	  
      $data['project_manager'] = $project_manager;
      $data['approval_status'] = $approval_status;
      $data['approval_remark'] = $approval_remark;	  
	  
      $this->_ci->email->from(SMTP_USER);
      $this->_ci->email->to($to);
      $this->_ci->email->subject(($approval_status == 0 ? 'Rejection' : 'Approval' )  .' Work Order No. '.$workorder_no);
      $this->_ci->email->message($this->_ci->load->view('mail/wo_approval', $data, true));
      $this->_ci->email->send();
    }
  }  

}
