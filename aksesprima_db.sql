-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2017 at 04:39 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aksesprima_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `areatable`
--

CREATE TABLE IF NOT EXISTS `areatable` (
`row_id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `areatable`
--

INSERT INTO `areatable` (`row_id`, `region_id`, `txt`, `status_id`) VALUES
(1, 1, 'Area 1', 1),
(2, 1, 'Area 2', 2);

-- --------------------------------------------------------

--
-- Table structure for table `dropcategorytable`
--

CREATE TABLE IF NOT EXISTS `dropcategorytable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dropcategorytable`
--

INSERT INTO `dropcategorytable` (`row_id`, `txt`, `is_active`) VALUES
(1, 'High Rental Price', 1),
(2, 'Community Issue', 1),
(3, 'Rejected by Landlord', 1),
(4, 'Rejected by Government', 1),
(5, 'Canceled PO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `generatetable`
--

CREATE TABLE IF NOT EXISTS `generatetable` (
`row_id` int(11) NOT NULL,
  `generate_controller` varchar(100) DEFAULT NULL,
  `generate_table` varchar(100) DEFAULT NULL,
  `generate_field` varchar(100) DEFAULT NULL,
  `generate_field_descr` varchar(100) DEFAULT NULL,
  `generate_type` varchar(100) DEFAULT NULL,
  `generate_field_index` int(11) DEFAULT '0',
  `generate_field_mandatory` int(11) DEFAULT '0',
  `generate_relation_table` varchar(100) DEFAULT NULL,
  `generate_relation_field` varchar(100) DEFAULT NULL,
  `generate_relation_fieldtxt` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `itemunittable`
--

CREATE TABLE IF NOT EXISTS `itemunittable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itemunittable`
--

INSERT INTO `itemunittable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'Kg', 1),
(2, 'Ton', 1);

-- --------------------------------------------------------

--
-- Table structure for table `producttable`
--

CREATE TABLE IF NOT EXISTS `producttable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `producttable`
--

INSERT INTO `producttable` (`row_id`, `txt`, `is_active`) VALUES
(1, 'B2S MCP', 1),
(2, 'B2S BTSH', 1),
(3, 'COLO MCP', 1),
(4, 'COLO BTSH', 1),
(5, 'B2S Macro', 1),
(6, 'COLO Macro', 1);

-- --------------------------------------------------------

--
-- Table structure for table `regiontable`
--

CREATE TABLE IF NOT EXISTS `regiontable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regiontable`
--

INSERT INTO `regiontable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'Jawa Barat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roletable`
--

CREATE TABLE IF NOT EXISTS `roletable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roletable`
--

INSERT INTO `roletable` (`row_id`, `txt`) VALUES
(1, 'Administrator'),
(2, 'Sales'),
(3, 'Solution'),
(4, 'PMO'),
(5, 'PM Regional'),
(6, 'Project Support'),
(7, 'SCM'),
(8, 'Legal'),
(9, 'Finance'),
(10, 'Engineering');

-- --------------------------------------------------------

--
-- Table structure for table `salesorderline`
--

CREATE TABLE IF NOT EXISTS `salesorderline` (
`row_id` int(11) NOT NULL,
  `salesorder_no` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `site_tenant_id` varchar(255) DEFAULT NULL,
  `rent_period` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesorderline`
--

INSERT INTO `salesorderline` (`row_id`, `salesorder_no`, `product_id`, `site_tenant_id`, `rent_period`) VALUES
(1, 'SO15000008', 1, '884938', 5);

-- --------------------------------------------------------

--
-- Table structure for table `salesordertable`
--

CREATE TABLE IF NOT EXISTS `salesordertable` (
`row_id` int(11) NOT NULL,
  `salesorder_no` varchar(255) DEFAULT NULL,
  `salesorder_date` datetime DEFAULT NULL,
  `drm_date` datetime DEFAULT NULL,
  `batch` varchar(255) DEFAULT NULL,
  `po_tenant_no` varchar(255) DEFAULT NULL,
  `po_tenant_date` datetime DEFAULT NULL,
  `tenant_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesordertable`
--

INSERT INTO `salesordertable` (`row_id`, `salesorder_no`, `salesorder_date`, `drm_date`, `batch`, `po_tenant_no`, `po_tenant_date`, `tenant_id`) VALUES
(1, 'SO15000001', '2017-03-15 00:00:00', '2017-03-15 00:00:00', '1', 'PO000001', '2017-03-10 00:00:00', 1),
(2, 'SO17000002', '2017-03-17 00:00:00', '2017-03-10 00:00:00', '1', 'PO000001', '2017-03-10 00:00:00', 1),
(3, 'SO10000003', '2017-03-10 00:00:00', '2017-03-11 00:00:00', '1', '2', '2017-03-08 00:00:00', 1),
(4, 'SO15000004', '2017-03-15 00:00:00', '2017-03-15 00:00:00', '2', 'PO023093', '2017-03-15 00:00:00', 1),
(5, 'SO15000005', '2017-03-15 00:00:00', '2017-03-04 00:00:00', 'SO02399493', 'PO9490303', '2017-03-10 00:00:00', 1),
(6, 'SO15000006', '2017-03-15 00:00:00', '2017-03-04 00:00:00', '2', 'PO0994392', '2017-03-15 00:00:00', 1),
(7, 'SO15000007', '2017-03-15 00:00:00', '2017-03-11 00:00:00', '1', 'PO094493', '2017-03-15 00:00:00', 1),
(8, 'SO15000008', '2017-03-15 00:00:00', '2017-03-11 00:00:00', '3', 'PO0490334', '2017-03-11 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sitetable`
--

CREATE TABLE IF NOT EXISTS `sitetable` (
`row_id` int(11) NOT NULL,
  `site_id` varchar(255) DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `sitetype_id` int(11) DEFAULT NULL,
  `towerheight_id` int(11) DEFAULT NULL,
  `transmissiontype_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `address` text,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `milestone_status_id` int(11) DEFAULT NULL,
  `inchstone_status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sitetable`
--

INSERT INTO `sitetable` (`row_id`, `site_id`, `site_name`, `sitetype_id`, `towerheight_id`, `transmissiontype_id`, `region_id`, `area_id`, `address`, `latitude`, `longitude`, `milestone_status_id`, `inchstone_status_id`) VALUES
(1, '210321', 'Site Bekasi', 1, 1, 1, 1, 1, 'Jalan raya bekasi', '123', '321', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sitetypetable`
--

CREATE TABLE IF NOT EXISTS `sitetypetable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sitetypetable`
--

INSERT INTO `sitetypetable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'MCP', 1),
(2, 'MCP+', 1),
(3, 'Macro/SST', 1);

-- --------------------------------------------------------

--
-- Table structure for table `statustable`
--

CREATE TABLE IF NOT EXISTS `statustable` (
`row_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `txt` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statustable`
--

INSERT INTO `statustable` (`row_id`, `name`, `txt`) VALUES
(1, 'Active', 'Stated that the data is active'),
(2, 'Not Active', 'Stated that the data is not active');

-- --------------------------------------------------------

--
-- Table structure for table `systemlogtable`
--

CREATE TABLE IF NOT EXISTS `systemlogtable` (
`row_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `key_id` int(11) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `data` text
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `systemlogtable`
--

INSERT INTO `systemlogtable` (`row_id`, `created_by`, `created_date`, `module`, `key_id`, `action`, `data`) VALUES
(1, 1, '2017-03-12 22:44:12', 'region', 1, 'Insert regiontable', '{"txt":"Jawa Barat","status_id":"1"}'),
(2, 1, '2017-03-12 22:44:38', 'region', 1, 'Update regiontable', '{"txt":"Jawa Barat 1","status_id":"1"}'),
(3, 1, '2017-03-12 22:45:07', 'region', 1, 'Update regiontable', '{"txt":"Jawa Barat","status_id":"1"}'),
(4, 1, '2017-03-12 23:17:46', 'sitetype', 1, 'Update sitetypetable', '{"txt":"MCP 1","status_id":"1"}'),
(5, 1, '2017-03-12 23:17:52', 'sitetype', 1, 'Update sitetypetable', '{"txt":"MCP","status_id":"1"}'),
(6, 1, '2017-03-13 22:11:46', 'towerheight', 1, 'Insert towerheighttable', '{"txt":"RT - 6 Meter","status_id":"1"}'),
(7, 1, '2017-03-13 22:11:56', 'towerheight', 2, 'Insert towerheighttable', '{"txt":"RT - 9 Meter","status_id":"1"}'),
(8, 1, '2017-03-13 22:12:12', 'towerheight', 3, 'Insert towerheighttable', '{"txt":"GF - 20 Meter","status_id":"1"}'),
(9, 1, '2017-03-13 22:13:38', 'site', 1, 'Insert sitetable', '{"site_id":"201232","site_name":"Site Bekasi","sitetype_id":"1","towerheight_id":"1","region_id":"1","area_id":"1","address":"Jalan Raya Bekasi","latitude":"123","longitude":"321","status_id":"1"}'),
(10, 1, '2017-03-13 22:23:22', 'vendortype', 1, 'Insert vendortypetable', '{"txt":"Construction","status_id":"1"}'),
(11, 1, '2017-03-13 22:24:11', 'vendortype', 2, 'Insert vendortypetable', '{"txt":"Fabrication","status_id":"1"}'),
(12, 1, '2017-03-13 22:38:51', 'vendor', 1, 'Insert vendortable', '{"vendortype_id":"1","name":"PT. WIKA","email":"wika@email.com","phone_no":"112233","address":"Jalan Raya Bogor","status_id":"1"}'),
(13, 1, '2017-03-13 22:55:38', 'transmissiontype', 1, 'Insert transmissiontypetable', '{"txt":"FSO","status_id":"1"}'),
(14, 1, '2017-03-13 22:55:44', 'transmissiontype', 2, 'Insert transmissiontypetable', '{"txt":"FO","status_id":"1"}'),
(15, 1, '2017-03-13 23:16:01', 'site', 1, 'Insert sitetable', '{"site_id":"210321","site_name":"Site Bekasi","sitetype_id":"1","towerheight_id":"1","transmissiontype_id":"1","region_id":"1","area_id":"1","address":"Jalan raya bekasi","latitude":"123","longitude":"321","milestone_status_id":"1","inchstone_status_id":"1"}'),
(16, 1, '2017-03-14 22:17:42', 'product', 1, 'Insert producttable', '{"txt":"B2S MCP","is_active":"1"}'),
(17, 1, '2017-03-14 22:17:56', 'product', 2, 'Insert producttable', '{"txt":"B2S BTSH","is_active":"1"}'),
(18, 1, '2017-03-14 22:18:05', 'product', 3, 'Insert producttable', '{"txt":"COLO MCP","is_active":"1"}'),
(19, 1, '2017-03-14 22:18:19', 'product', 4, 'Insert producttable', '{"txt":"COLO BTSH","is_active":"1"}'),
(20, 1, '2017-03-14 22:18:33', 'product', 5, 'Insert producttable', '{"txt":"B2S Macro","is_active":"1"}'),
(21, 1, '2017-03-14 22:18:48', 'product', 6, 'Insert producttable', '{"txt":"COLO Macro","is_active":"1"}'),
(22, 1, '2017-03-14 22:31:06', 'dropcategory', 1, 'Insert dropcategorytable', '{"txt":"High Rental Price","is_active":"1"}'),
(23, 1, '2017-03-14 22:31:21', 'dropcategory', 2, 'Insert dropcategorytable', '{"txt":"Community Issue","is_active":"1"}'),
(24, 1, '2017-03-14 22:31:37', 'dropcategory', 3, 'Insert dropcategorytable', '{"txt":"Rejected by Landlord","is_active":"1"}'),
(25, 1, '2017-03-14 22:31:54', 'dropcategory', 4, 'Insert dropcategorytable', '{"txt":"Rejected by Government","is_active":"1"}'),
(26, 1, '2017-03-14 22:32:09', 'dropcategory', 5, 'Insert dropcategorytable', '{"txt":"Canceled PO","is_active":"1"}'),
(27, 1, '2017-03-15 18:12:25', 'salesorder', 1, 'Insert salesordertable', '{"salesorder_no":"SO15000001","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-15 00:00:00","batch":"1","po_tenant_no":"PO000001","po_tenant_date":"2017-03-10 00:00:00","tenant_id":"1"}'),
(28, 1, '2017-03-15 18:13:06', 'salesorder', 2, 'Insert salesordertable', '{"salesorder_no":"SO17000002","salesorder_date":"2017-03-17 00:00:00","drm_date":"2017-03-10 00:00:00","batch":"1","po_tenant_no":"PO000001","po_tenant_date":"2017-03-10 00:00:00","tenant_id":"1"}'),
(29, 1, '2017-03-15 18:14:37', 'salesorder', 3, 'Insert salesordertable', '{"salesorder_no":"SO10000003","salesorder_date":"2017-03-10 00:00:00","drm_date":"2017-03-11 00:00:00","batch":"1","po_tenant_no":"2","po_tenant_date":"2017-03-08 00:00:00","tenant_id":"1"}'),
(30, 1, '2017-03-15 18:20:36', 'salesorder', 4, 'Insert salesordertable', '{"salesorder_no":"SO15000004","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-15 00:00:00","batch":"2","po_tenant_no":"PO023093","po_tenant_date":"2017-03-15 00:00:00","tenant_id":"1"}'),
(31, 1, '2017-03-15 18:21:22', 'salesorder', 5, 'Insert salesordertable', '{"salesorder_no":"SO15000005","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-04 00:00:00","batch":"SO02399493","po_tenant_no":"PO9490303","po_tenant_date":"2017-03-10 00:00:00","tenant_id":"1"}'),
(32, 1, '2017-03-15 18:29:56', 'salesorder', 6, 'Insert salesordertable', '{"salesorder_no":"SO15000006","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-04 00:00:00","batch":"2","po_tenant_no":"PO0994392","po_tenant_date":"2017-03-15 00:00:00","tenant_id":"1"}'),
(33, 1, '2017-03-15 18:31:29', 'salesorder', 7, 'Insert salesordertable', '{"salesorder_no":"SO15000007","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-11 00:00:00","batch":"1","po_tenant_no":"PO094493","po_tenant_date":"2017-03-15 00:00:00","tenant_id":"1"}'),
(34, 1, '2017-03-15 18:39:35', 'salesorder', 8, 'Insert salesordertable', '{"salesorder_no":"SO15000008","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-11 00:00:00","batch":"3","po_tenant_no":"PO0490334","po_tenant_date":"2017-03-11 00:00:00","tenant_id":"1"}'),
(35, 1, '2017-03-15 20:19:31', 'salesorderline', 1, 'Insert salesorderline', '{"salesorder_no":"SO15000008","product_id":"1","site_tenant_id":"884938","rent_period":"5"}'),
(36, 1, '2017-04-02 20:58:37', 'itemunit', 1, 'Insert itemunittable', '{"txt":"Kg","status_id":"1"}'),
(37, 1, '2017-04-02 20:58:43', 'itemunit', 2, 'Insert itemunittable', '{"txt":"Ton","status_id":"1"}');

-- --------------------------------------------------------

--
-- Table structure for table `tenanttable`
--

CREATE TABLE IF NOT EXISTS `tenanttable` (
`row_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `address` text,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenanttable`
--

INSERT INTO `tenanttable` (`row_id`, `name`, `email`, `phone_no`, `address`, `status_id`) VALUES
(1, 'Telkomsel', 'telkomsel@email.com', '1213212312', 'Jalan Sudirman', 1);

-- --------------------------------------------------------

--
-- Table structure for table `towerheighttable`
--

CREATE TABLE IF NOT EXISTS `towerheighttable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(50) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `towerheighttable`
--

INSERT INTO `towerheighttable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'RT - 6 Meter', 1),
(2, 'RT - 9 Meter', 1),
(3, 'GF - 20 Meter', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transmissiontypetable`
--

CREATE TABLE IF NOT EXISTS `transmissiontypetable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transmissiontypetable`
--

INSERT INTO `transmissiontypetable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'FSO', 1),
(2, 'FO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `usertable`
--

CREATE TABLE IF NOT EXISTS `usertable` (
`row_id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `pswrd` varchar(100) DEFAULT NULL,
  `photo` varchar(20) DEFAULT NULL,
  `status_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usertable`
--

INSERT INTO `usertable` (`row_id`, `role_id`, `name`, `email`, `user_id`, `pswrd`, `photo`, `status_id`) VALUES
(1, 1, 'John Doe', 'ysfbryn@yahoo.com', 'administrator', 'd0970714757783e6cf17b26fb8e2298f', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `vendortable`
--

CREATE TABLE IF NOT EXISTS `vendortable` (
`row_id` int(11) NOT NULL,
  `vendortype_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `address` text,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendortable`
--

INSERT INTO `vendortable` (`row_id`, `vendortype_id`, `name`, `email`, `phone_no`, `address`, `status_id`) VALUES
(1, 1, 'PT. WIKA', 'wika@email.com', '112233', 'Jalan Raya Bogor', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendortypetable`
--

CREATE TABLE IF NOT EXISTS `vendortypetable` (
`row_id` int(11) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendortypetable`
--

INSERT INTO `vendortypetable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'Construction', 1),
(2, 'Fabrication', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_areatablelist`
--
CREATE TABLE IF NOT EXISTS `vw_areatablelist` (
`row_id` int(11)
,`region_id` int(11)
,`regiontable_txt` varchar(255)
,`txt` varchar(255)
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_dropcategorytablelist`
--
CREATE TABLE IF NOT EXISTS `vw_dropcategorytablelist` (
`row_id` int(11)
,`txt` varchar(255)
,`is_active` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_itemunittablelist`
--
CREATE TABLE IF NOT EXISTS `vw_itemunittablelist` (
`row_id` int(11)
,`txt` varchar(255)
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_producttablelist`
--
CREATE TABLE IF NOT EXISTS `vw_producttablelist` (
`row_id` int(11)
,`txt` varchar(255)
,`is_active` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_regiontablelist`
--
CREATE TABLE IF NOT EXISTS `vw_regiontablelist` (
`row_id` int(11)
,`txt` varchar(255)
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_salesorderlinelist`
--
CREATE TABLE IF NOT EXISTS `vw_salesorderlinelist` (
`row_id` int(11)
,`salesorder_no` varchar(255)
,`product_id` int(11)
,`producttable_txt` varchar(255)
,`site_tenant_id` varchar(255)
,`rent_period` double
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_salesordertablelist`
--
CREATE TABLE IF NOT EXISTS `vw_salesordertablelist` (
`row_id` int(11)
,`salesorder_no` varchar(255)
,`salesorder_date` datetime
,`drm_date` datetime
,`batch` varchar(255)
,`po_tenant_no` varchar(255)
,`po_tenant_date` datetime
,`tenant_id` int(11)
,`tenanttable_txt` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_sitetablelist`
--
CREATE TABLE IF NOT EXISTS `vw_sitetablelist` (
`row_id` int(11)
,`site_id` varchar(255)
,`site_name` varchar(255)
,`sitetype_id` int(11)
,`sitetypetable_txt` varchar(255)
,`towerheight_id` int(11)
,`towerheighttable_txt` varchar(50)
,`transmissiontype_id` int(11)
,`transmissiontypetable_txt` varchar(255)
,`region_id` int(11)
,`regiontable_txt` varchar(255)
,`area_id` int(11)
,`areatable_txt` varchar(255)
,`address` text
,`latitude` varchar(255)
,`longitude` varchar(255)
,`milestone_status_id` int(11)
,`statustable_milestone_name` varchar(100)
,`inchstone_status_id` int(11)
,`statustable_inchstone_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_sitetypetablelist`
--
CREATE TABLE IF NOT EXISTS `vw_sitetypetablelist` (
`row_id` int(11)
,`txt` varchar(255)
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_systemlogtablelist`
--
CREATE TABLE IF NOT EXISTS `vw_systemlogtablelist` (
`row_id` int(11)
,`created_by` int(11)
,`usertable_name` varchar(100)
,`created_date` datetime
,`module` varchar(255)
,`key_id` int(11)
,`action` varchar(255)
,`data` text
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_tenanttablelist`
--
CREATE TABLE IF NOT EXISTS `vw_tenanttablelist` (
`row_id` int(11)
,`name` varchar(255)
,`email` varchar(255)
,`phone_no` varchar(255)
,`address` text
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_towerheighttablelist`
--
CREATE TABLE IF NOT EXISTS `vw_towerheighttablelist` (
`row_id` int(11)
,`txt` varchar(50)
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_transmissiontypetablelist`
--
CREATE TABLE IF NOT EXISTS `vw_transmissiontypetablelist` (
`row_id` int(11)
,`txt` varchar(255)
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_usertablelist`
--
CREATE TABLE IF NOT EXISTS `vw_usertablelist` (
`row_id` int(11)
,`role_id` int(11)
,`roletable_txt` varchar(45)
,`name` varchar(100)
,`email` varchar(100)
,`user_id` varchar(100)
,`pswrd` varchar(100)
,`photo` varchar(20)
,`status_id` varchar(45)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_vendortablelist`
--
CREATE TABLE IF NOT EXISTS `vw_vendortablelist` (
`row_id` int(11)
,`vendortype_id` int(11)
,`vendortypetable_txt` varchar(255)
,`name` varchar(255)
,`email` varchar(255)
,`phone_no` varchar(255)
,`address` text
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_vendortypetablelist`
--
CREATE TABLE IF NOT EXISTS `vw_vendortypetablelist` (
`row_id` int(11)
,`txt` varchar(255)
,`status_id` int(11)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Structure for view `vw_areatablelist`
--
DROP TABLE IF EXISTS `vw_areatablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_areatablelist` AS select `areatable`.`row_id` AS `row_id`,`areatable`.`region_id` AS `region_id`,`regiontable`.`txt` AS `regiontable_txt`,`areatable`.`txt` AS `txt`,`areatable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from ((`areatable` join `regiontable` on((`regiontable`.`row_id` = `areatable`.`region_id`))) join `statustable` on((`statustable`.`row_id` = `areatable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_dropcategorytablelist`
--
DROP TABLE IF EXISTS `vw_dropcategorytablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_dropcategorytablelist` AS select `dropcategorytable`.`row_id` AS `row_id`,`dropcategorytable`.`txt` AS `txt`,`dropcategorytable`.`is_active` AS `is_active`,`statustable`.`name` AS `statustable_name` from (`dropcategorytable` join `statustable` on((`statustable`.`row_id` = `dropcategorytable`.`is_active`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_itemunittablelist`
--
DROP TABLE IF EXISTS `vw_itemunittablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_itemunittablelist` AS select `itemunittable`.`row_id` AS `row_id`,`itemunittable`.`txt` AS `txt`,`itemunittable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from (`itemunittable` join `statustable` on((`statustable`.`row_id` = `itemunittable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_producttablelist`
--
DROP TABLE IF EXISTS `vw_producttablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_producttablelist` AS select `producttable`.`row_id` AS `row_id`,`producttable`.`txt` AS `txt`,`producttable`.`is_active` AS `is_active`,`statustable`.`name` AS `statustable_name` from (`producttable` join `statustable` on((`statustable`.`row_id` = `producttable`.`is_active`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_regiontablelist`
--
DROP TABLE IF EXISTS `vw_regiontablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_regiontablelist` AS select `regiontable`.`row_id` AS `row_id`,`regiontable`.`txt` AS `txt`,`regiontable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from (`regiontable` join `statustable` on((`statustable`.`row_id` = `regiontable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_salesorderlinelist`
--
DROP TABLE IF EXISTS `vw_salesorderlinelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_salesorderlinelist` AS select `salesorderline`.`row_id` AS `row_id`,`salesorderline`.`salesorder_no` AS `salesorder_no`,`salesorderline`.`product_id` AS `product_id`,`producttable`.`txt` AS `producttable_txt`,`salesorderline`.`site_tenant_id` AS `site_tenant_id`,`salesorderline`.`rent_period` AS `rent_period` from (`salesorderline` join `producttable` on((`producttable`.`row_id` = `salesorderline`.`product_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_salesordertablelist`
--
DROP TABLE IF EXISTS `vw_salesordertablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_salesordertablelist` AS select `salesordertable`.`row_id` AS `row_id`,`salesordertable`.`salesorder_no` AS `salesorder_no`,`salesordertable`.`salesorder_date` AS `salesorder_date`,`salesordertable`.`drm_date` AS `drm_date`,`salesordertable`.`batch` AS `batch`,`salesordertable`.`po_tenant_no` AS `po_tenant_no`,`salesordertable`.`po_tenant_date` AS `po_tenant_date`,`salesordertable`.`tenant_id` AS `tenant_id`,`tenanttable`.`name` AS `tenanttable_txt` from (`salesordertable` join `tenanttable` on((`tenanttable`.`row_id` = `salesordertable`.`tenant_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_sitetablelist`
--
DROP TABLE IF EXISTS `vw_sitetablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sitetablelist` AS select `sitetable`.`row_id` AS `row_id`,`sitetable`.`site_id` AS `site_id`,`sitetable`.`site_name` AS `site_name`,`sitetable`.`sitetype_id` AS `sitetype_id`,`sitetypetable`.`txt` AS `sitetypetable_txt`,`sitetable`.`towerheight_id` AS `towerheight_id`,`towerheighttable`.`txt` AS `towerheighttable_txt`,`sitetable`.`transmissiontype_id` AS `transmissiontype_id`,`transmissiontypetable`.`txt` AS `transmissiontypetable_txt`,`sitetable`.`region_id` AS `region_id`,`regiontable`.`txt` AS `regiontable_txt`,`sitetable`.`area_id` AS `area_id`,`areatable`.`txt` AS `areatable_txt`,`sitetable`.`address` AS `address`,`sitetable`.`latitude` AS `latitude`,`sitetable`.`longitude` AS `longitude`,`sitetable`.`milestone_status_id` AS `milestone_status_id`,`a`.`name` AS `statustable_milestone_name`,`sitetable`.`inchstone_status_id` AS `inchstone_status_id`,`b`.`name` AS `statustable_inchstone_name` from (((((((`sitetable` join `sitetypetable` on((`sitetypetable`.`row_id` = `sitetable`.`sitetype_id`))) join `towerheighttable` on((`towerheighttable`.`row_id` = `sitetable`.`towerheight_id`))) join `transmissiontypetable` on((`transmissiontypetable`.`row_id` = `sitetable`.`transmissiontype_id`))) join `regiontable` on((`regiontable`.`row_id` = `sitetable`.`region_id`))) join `areatable` on((`areatable`.`row_id` = `sitetable`.`area_id`))) join `statustable` `a` on((`a`.`row_id` = `sitetable`.`milestone_status_id`))) join `statustable` `b` on((`b`.`row_id` = `sitetable`.`inchstone_status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_sitetypetablelist`
--
DROP TABLE IF EXISTS `vw_sitetypetablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sitetypetablelist` AS select `sitetypetable`.`row_id` AS `row_id`,`sitetypetable`.`txt` AS `txt`,`sitetypetable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from (`sitetypetable` join `statustable` on((`statustable`.`row_id` = `sitetypetable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_systemlogtablelist`
--
DROP TABLE IF EXISTS `vw_systemlogtablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_systemlogtablelist` AS select `systemlogtable`.`row_id` AS `row_id`,`systemlogtable`.`created_by` AS `created_by`,`usertable`.`name` AS `usertable_name`,`systemlogtable`.`created_date` AS `created_date`,`systemlogtable`.`module` AS `module`,`systemlogtable`.`key_id` AS `key_id`,`systemlogtable`.`action` AS `action`,`systemlogtable`.`data` AS `data` from (`systemlogtable` join `usertable` on((`usertable`.`row_id` = `systemlogtable`.`created_by`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_tenanttablelist`
--
DROP TABLE IF EXISTS `vw_tenanttablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_tenanttablelist` AS select `tenanttable`.`row_id` AS `row_id`,`tenanttable`.`name` AS `name`,`tenanttable`.`email` AS `email`,`tenanttable`.`phone_no` AS `phone_no`,`tenanttable`.`address` AS `address`,`tenanttable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from (`tenanttable` join `statustable` on((`statustable`.`row_id` = `tenanttable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_towerheighttablelist`
--
DROP TABLE IF EXISTS `vw_towerheighttablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_towerheighttablelist` AS select `towerheighttable`.`row_id` AS `row_id`,`towerheighttable`.`txt` AS `txt`,`towerheighttable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from (`towerheighttable` join `statustable` on((`statustable`.`row_id` = `towerheighttable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_transmissiontypetablelist`
--
DROP TABLE IF EXISTS `vw_transmissiontypetablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_transmissiontypetablelist` AS select `transmissiontypetable`.`row_id` AS `row_id`,`transmissiontypetable`.`txt` AS `txt`,`transmissiontypetable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from (`transmissiontypetable` join `statustable` on((`statustable`.`row_id` = `transmissiontypetable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_usertablelist`
--
DROP TABLE IF EXISTS `vw_usertablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_usertablelist` AS select `usertable`.`row_id` AS `row_id`,`usertable`.`role_id` AS `role_id`,`roletable`.`txt` AS `roletable_txt`,`usertable`.`name` AS `name`,`usertable`.`email` AS `email`,`usertable`.`user_id` AS `user_id`,`usertable`.`pswrd` AS `pswrd`,`usertable`.`photo` AS `photo`,`usertable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from ((`usertable` join `roletable` on((`roletable`.`row_id` = `usertable`.`role_id`))) join `statustable` on((`statustable`.`row_id` = `usertable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_vendortablelist`
--
DROP TABLE IF EXISTS `vw_vendortablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_vendortablelist` AS select `vendortable`.`row_id` AS `row_id`,`vendortable`.`vendortype_id` AS `vendortype_id`,`vendortypetable`.`txt` AS `vendortypetable_txt`,`vendortable`.`name` AS `name`,`vendortable`.`email` AS `email`,`vendortable`.`phone_no` AS `phone_no`,`vendortable`.`address` AS `address`,`vendortable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from ((`vendortable` join `vendortypetable` on((`vendortypetable`.`row_id` = `vendortable`.`vendortype_id`))) join `statustable` on((`statustable`.`row_id` = `vendortable`.`status_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_vendortypetablelist`
--
DROP TABLE IF EXISTS `vw_vendortypetablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_vendortypetablelist` AS select `vendortypetable`.`row_id` AS `row_id`,`vendortypetable`.`txt` AS `txt`,`vendortypetable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from (`vendortypetable` join `statustable` on((`statustable`.`row_id` = `vendortypetable`.`status_id`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areatable`
--
ALTER TABLE `areatable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`region_id`,`status_id`);

--
-- Indexes for table `dropcategorytable`
--
ALTER TABLE `dropcategorytable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`is_active`);

--
-- Indexes for table `generatetable`
--
ALTER TABLE `generatetable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`generate_controller`,`generate_field`,`generate_field_descr`,`generate_type`);

--
-- Indexes for table `itemunittable`
--
ALTER TABLE `itemunittable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`status_id`);

--
-- Indexes for table `producttable`
--
ALTER TABLE `producttable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`is_active`);

--
-- Indexes for table `regiontable`
--
ALTER TABLE `regiontable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`status_id`);

--
-- Indexes for table `roletable`
--
ALTER TABLE `roletable`
 ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `salesorderline`
--
ALTER TABLE `salesorderline`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`salesorder_no`,`product_id`,`site_tenant_id`);

--
-- Indexes for table `salesordertable`
--
ALTER TABLE `salesordertable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`salesorder_no`,`salesorder_date`,`drm_date`,`po_tenant_no`,`po_tenant_date`,`tenant_id`);

--
-- Indexes for table `sitetable`
--
ALTER TABLE `sitetable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`site_id`,`sitetype_id`,`towerheight_id`,`transmissiontype_id`,`region_id`,`area_id`,`milestone_status_id`,`inchstone_status_id`);

--
-- Indexes for table `sitetypetable`
--
ALTER TABLE `sitetypetable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`status_id`);

--
-- Indexes for table `statustable`
--
ALTER TABLE `statustable`
 ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `systemlogtable`
--
ALTER TABLE `systemlogtable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`created_by`,`created_date`,`module`,`key_id`,`action`);

--
-- Indexes for table `tenanttable`
--
ALTER TABLE `tenanttable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`status_id`);

--
-- Indexes for table `towerheighttable`
--
ALTER TABLE `towerheighttable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`status_id`);

--
-- Indexes for table `transmissiontypetable`
--
ALTER TABLE `transmissiontypetable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`status_id`);

--
-- Indexes for table `usertable`
--
ALTER TABLE `usertable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`user_id`,`pswrd`,`role_id`,`status_id`);

--
-- Indexes for table `vendortable`
--
ALTER TABLE `vendortable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`vendortype_id`,`status_id`);

--
-- Indexes for table `vendortypetable`
--
ALTER TABLE `vendortypetable`
 ADD PRIMARY KEY (`row_id`), ADD KEY `IDX` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areatable`
--
ALTER TABLE `areatable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dropcategorytable`
--
ALTER TABLE `dropcategorytable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `generatetable`
--
ALTER TABLE `generatetable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `itemunittable`
--
ALTER TABLE `itemunittable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `producttable`
--
ALTER TABLE `producttable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `regiontable`
--
ALTER TABLE `regiontable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roletable`
--
ALTER TABLE `roletable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `salesorderline`
--
ALTER TABLE `salesorderline`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `salesordertable`
--
ALTER TABLE `salesordertable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sitetable`
--
ALTER TABLE `sitetable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sitetypetable`
--
ALTER TABLE `sitetypetable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `statustable`
--
ALTER TABLE `statustable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `systemlogtable`
--
ALTER TABLE `systemlogtable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tenanttable`
--
ALTER TABLE `tenanttable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `towerheighttable`
--
ALTER TABLE `towerheighttable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transmissiontypetable`
--
ALTER TABLE `transmissiontypetable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usertable`
--
ALTER TABLE `usertable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vendortable`
--
ALTER TABLE `vendortable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vendortypetable`
--
ALTER TABLE `vendortypetable`
MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
