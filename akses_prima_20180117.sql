-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2018 at 08:17 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `akses_prima`
--

-- --------------------------------------------------------

--
-- Table structure for table `areatable`
--

CREATE TABLE IF NOT EXISTS `areatable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `short_txt` varchar(1) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `areatable`
--

INSERT INTO `areatable` (`row_id`, `short_txt`, `txt`, `status_id`) VALUES
(1, 'A', 'Area 1 (Sumatera)', 1),
(2, 'B', 'Area 2 (Jabo Jabar)', 1),
(3, 'C', 'Area 3 (Jateng Jatim Balnus)', 1),
(4, 'D', 'Area 4 (Kalimantan Sulawesi Puma)', 1);

-- --------------------------------------------------------

--
-- Table structure for table `citytable`
--

CREATE TABLE IF NOT EXISTS `citytable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `short_txt` varchar(3) NOT NULL,
  `txt` varchar(125) NOT NULL,
  `area_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=484 ;

--
-- Dumping data for table `citytable`
--

INSERT INTO `citytable` (`row_id`, `short_txt`, `txt`, `area_id`, `region_id`, `province_id`, `status_id`) VALUES
(1, 'BNA', 'Banda Aceh', 1, 1, 1, 1),
(2, 'BIR', 'Bireuen', 1, 1, 1, 1),
(3, 'BKJ', 'Blangkejeren', 1, 1, 1, 1),
(4, 'BPD', 'Blangpidie', 1, 1, 1, 1),
(5, 'CAG', 'Calang', 1, 1, 1, 1),
(6, 'JTH', 'Jantho', 1, 1, 1, 1),
(7, 'KRB', 'Karang Baru', 1, 1, 1, 1),
(8, 'KTN', 'Kutacane', 1, 1, 1, 1),
(9, 'LGS', 'Langsa', 1, 1, 1, 1),
(10, 'LSM', 'Lhoksumawe', 1, 1, 1, 1),
(11, 'LSK', 'Lhoksukon', 1, 1, 1, 1),
(12, 'MBO', 'Meulaboh', 1, 1, 1, 1),
(13, 'MRN', 'Meureunde', 1, 1, 1, 1),
(14, 'SAB', 'Sabang', 1, 1, 1, 1),
(15, 'SGI', 'Sigli', 1, 1, 1, 1),
(16, 'STR', 'Simpang Tiga Redelog', 1, 1, 1, 1),
(17, 'SNB', 'Sinabang', 1, 1, 1, 1),
(18, 'SKL', 'Singkil', 1, 1, 1, 1),
(19, 'SUS', 'Subulussalam', 1, 1, 1, 1),
(20, 'SKM', 'Suka Makmue', 1, 1, 1, 1),
(21, 'TKN', 'Takengon', 1, 1, 1, 1),
(22, 'TTN', 'Tapak Tuan', 1, 1, 1, 1),
(23, 'AKK', 'Aek Kanopan', 1, 1, 2, 1),
(24, 'BLG', 'Balige', 1, 1, 2, 1),
(25, 'BNJ', 'Binjai', 1, 1, 2, 1),
(26, 'DLS', 'Dolok Sanggul', 1, 1, 2, 1),
(27, 'GNT', 'Gunung Tua', 1, 1, 2, 1),
(28, 'GST', 'Gunungsitoli', 1, 1, 2, 1),
(29, 'KBJ', 'Kabanjahe', 1, 1, 2, 1),
(30, 'KIS', 'Kisaran', 1, 1, 2, 1),
(31, 'KPI', 'Kota Pinang', 1, 1, 2, 1),
(32, 'LHM', 'Lahomi', 1, 1, 2, 1),
(33, 'LMP', 'Lima Puluh', 1, 1, 2, 1),
(34, 'LTU', 'Lotu', 1, 1, 2, 1),
(35, 'LBP', 'Lubuk Pakam', 1, 1, 2, 1),
(36, 'MDN', 'Medan', 1, 1, 2, 1),
(37, 'PSP', 'Padang Sidempuan', 1, 1, 2, 1),
(38, 'PRR', 'Pangururuan', 1, 1, 2, 1),
(39, 'PYB', 'Panyabungan', 1, 1, 2, 1),
(40, 'PMS', 'Pematangsiantar', 1, 1, 2, 1),
(41, 'RAP', 'Rantau Prapat', 1, 1, 2, 1),
(42, 'SAL', 'Salak', 1, 1, 2, 1),
(43, 'SRH', 'Sei Rampah', 1, 1, 2, 1),
(44, 'SBG', 'Sibolga', 1, 1, 2, 1),
(45, 'SBH', 'Sibuhuan', 1, 1, 2, 1),
(46, 'SDK', 'Sidikalang', 1, 1, 2, 1),
(47, 'STB', 'Stabat', 1, 1, 2, 1),
(48, 'TJB', 'Tanjung Balai', 1, 1, 2, 1),
(49, 'TRT', 'Tarutung', 1, 1, 2, 1),
(50, 'TBT', 'Tebing Tinggi', 1, 1, 2, 1),
(51, 'TLD', 'Teluk Dalam', 1, 1, 2, 1),
(52, 'ARS', 'Arosuka', 1, 10, 3, 1),
(53, 'BSK', 'Batusangkat', 1, 10, 3, 1),
(54, 'BKT', 'Bukittinggi', 1, 10, 3, 1),
(55, 'LBB', 'Lubuk Basung', 1, 10, 3, 1),
(56, 'LBS', 'Lubuk Sikaping', 1, 10, 3, 1),
(57, 'MRJ', 'Muaro Sijunjung', 1, 10, 3, 1),
(58, 'NPM', 'Nagari Parit Malintang', 1, 10, 3, 1),
(59, 'PAD', 'Padang', 1, 10, 3, 1),
(60, 'PDA', 'Padang Aro', 1, 10, 3, 1),
(61, 'PDP', 'Padang Panjang', 1, 10, 3, 1),
(62, 'PNN', 'Painan', 1, 10, 3, 1),
(63, 'PMN', 'Pariaman', 1, 10, 3, 1),
(64, 'PYH', 'Payakumbuh', 1, 10, 3, 1),
(65, 'PLJ', 'Pulau Punjung', 1, 10, 3, 1),
(66, 'SRK', 'Sarilamak', 1, 10, 3, 1),
(67, 'SWL', 'Sawahlunto', 1, 10, 3, 1),
(68, 'SPE', 'Simpang Empat', 1, 10, 3, 1),
(69, 'SLK', 'Solok', 1, 10, 3, 1),
(70, 'TPT', 'Tuapejat', 1, 10, 3, 1),
(71, 'BKN', 'Bengkinang', 1, 10, 4, 1),
(72, 'BLS', 'Bengkalis', 1, 10, 4, 1),
(73, 'DUM', 'Dumai', 1, 10, 4, 1),
(74, 'PKK', 'Pangkalan Kerinci', 1, 10, 4, 1),
(75, 'PRP', 'Pasir Pangarairan', 1, 10, 4, 1),
(76, 'PBR', 'Pekanbaru', 1, 10, 4, 1),
(77, 'RGT', 'Rengat', 1, 10, 4, 1),
(78, 'SAK', 'Siak Sriindrapura', 1, 10, 4, 1),
(79, 'TTG', 'Tebing Tinggi', 1, 10, 4, 1),
(80, 'TLK', 'Teluk Kuantan', 1, 10, 4, 1),
(81, 'TBH', 'Tembilahan', 1, 10, 4, 1),
(82, 'UJT', 'Ujung Tanjung', 1, 10, 4, 1),
(83, 'BSB', 'Bandar Seri Bentan', 1, 10, 5, 1),
(84, 'BTM', 'Batam', 1, 10, 5, 1),
(85, 'DKL', 'Daik Lingga', 1, 10, 5, 1),
(86, 'RAN', 'Ranai', 1, 10, 5, 1),
(87, 'TBK', 'Tanjung Balai Karimun', 1, 10, 5, 1),
(88, 'TPG', 'Tanjung Pinang', 1, 10, 5, 1),
(89, 'TRP', 'Tarempa', 1, 10, 5, 1),
(90, 'BTA', 'Baturaja', 1, 2, 6, 1),
(91, 'IDL', 'Indralaya', 1, 2, 6, 1),
(92, 'KAG', 'Kayu Agung', 1, 2, 6, 1),
(93, 'LHT', 'Lahat', 1, 2, 6, 1),
(94, 'LLG', 'Lubuk Linggau', 1, 2, 6, 1),
(95, 'MPR', 'Martapura', 1, 2, 6, 1),
(96, 'MRE', 'Muara Enim', 1, 2, 6, 1),
(97, 'MBL', 'Muarabeliti', 1, 2, 6, 1),
(98, 'MRD', 'Muara Dua', 1, 2, 6, 1),
(99, 'PGA', 'Pagar Alam', 1, 2, 6, 1),
(100, 'PLG', 'Palembang', 1, 2, 6, 1),
(101, 'PKB', 'Pangkalan Balai', 1, 2, 6, 1),
(102, 'PBM', 'Prabumulih', 1, 2, 6, 1),
(103, 'SKL', 'Sekayu', 1, 2, 6, 1),
(104, 'TBG', 'Tebing Tinggi', 1, 2, 6, 1),
(105, 'AGM', 'Arga Makmur', 1, 2, 7, 1),
(106, 'BGL', 'Bengkulu', 1, 2, 7, 1),
(107, 'BHN', 'Bintuhan', 1, 2, 7, 1),
(108, 'CRP', 'Curup', 1, 2, 7, 1),
(109, 'KPH', 'Kapahiang', 1, 2, 7, 1),
(110, 'KRT', 'Karang Tinggi', 1, 2, 7, 1),
(111, 'MNA', 'Manna', 1, 2, 7, 1),
(112, 'MKM', 'Mukomuko', 1, 2, 7, 1),
(113, 'TAS', 'Tais', 1, 2, 7, 1),
(114, 'TUB', 'Tubei', 1, 2, 7, 1),
(115, 'BKO', 'Bangko', 1, 2, 8, 1),
(116, 'JMB', 'Jambi', 1, 2, 8, 1),
(117, 'KLT', 'Kuala Tungkai', 1, 2, 8, 1),
(118, 'MBN', 'Muara Bulian', 1, 2, 8, 1),
(119, 'MRB', 'Muara Bungo', 1, 2, 8, 1),
(120, 'MSK', 'Muara Sabak', 1, 2, 8, 1),
(121, 'MRT', 'Muara Tebo', 1, 2, 8, 1),
(122, 'SRL', 'Sorolangun', 1, 2, 8, 1),
(123, 'SNT', 'Sengeti', 1, 2, 8, 1),
(124, 'SPN', 'Sungai Penuh', 1, 2, 8, 1),
(125, 'BDL', 'Bandar Lampung', 1, 2, 9, 1),
(126, 'BBU', 'Blambangan Umpu', 1, 2, 9, 1),
(127, 'GDT', 'Gedong Tataan', 1, 2, 9, 1),
(128, 'GNS', 'Gunung Sugih', 1, 2, 9, 1),
(129, 'KLA', 'Kalianda', 1, 2, 9, 1),
(130, 'KOT', 'Kota Agung', 1, 2, 9, 1),
(131, 'KTB', 'Kotabumi', 1, 2, 9, 1),
(132, 'LIW', 'Liwa', 1, 2, 9, 1),
(133, 'MGL', 'Menggala', 1, 2, 9, 1),
(134, 'MSJ', 'Mesuji', 1, 2, 9, 1),
(135, 'MET', 'Metro', 1, 2, 9, 1),
(136, 'PRW', 'Pringsewu', 1, 2, 9, 1),
(137, 'SDN', 'Sukadana', 1, 2, 9, 1),
(138, 'TWG', 'Tulang Bawang Tengah', 1, 2, 9, 1),
(139, 'KBA', 'Koba', 1, 2, 10, 1),
(140, 'MGR', 'Manggar', 1, 2, 10, 1),
(141, 'MTR', 'Mentok', 1, 2, 10, 1),
(142, 'PGP', 'Pangkal Pinang', 1, 2, 10, 1),
(143, 'SGL', 'Sungai Liat', 1, 2, 10, 1),
(144, 'TDN', 'Tanjung Pandan', 1, 2, 10, 1),
(145, 'TBL', 'Toboali', 1, 2, 10, 1),
(146, 'CKG', 'Cakung', 1, 2, 11, 1),
(147, 'JKT', 'Jakarta', 2, 3, 11, 1),
(148, 'GGP', 'Grogol Petamburan', 2, 3, 11, 1),
(149, 'KSU', 'Kepulauan Seribu Utara', 2, 3, 11, 1),
(150, 'KYB', 'Kebayoran Baru', 2, 3, 11, 1),
(151, 'TJP', 'Tanjungpriok', 2, 3, 11, 1),
(152, 'TNA', 'Tananh Abang', 2, 3, 11, 1),
(153, 'CLG', 'Cilegon', 2, 4, 12, 1),
(154, 'CPT', 'Ciputat', 2, 4, 12, 1),
(155, 'PDG', 'Pandelang', 3, 5, 12, 1),
(156, 'RKB', 'Rangkas Bitung', 3, 5, 12, 1),
(157, 'SRG', 'Serang', 3, 5, 12, 1),
(158, 'TNG', 'Tangerang', 3, 5, 12, 1),
(159, 'TGR', 'Tigaraksa', 3, 5, 12, 1),
(160, 'BDG', 'Bandung', 3, 5, 13, 1),
(161, 'BJR', 'Banjar', 3, 5, 13, 1),
(162, 'BKS', 'Bekasi', 3, 5, 13, 1),
(163, 'BGR', 'Bogor', 3, 5, 13, 1),
(164, 'CMS', 'Ciamis', 3, 5, 13, 1),
(165, 'CJR', 'Cianjur', 3, 5, 13, 1),
(166, 'CBI', 'Cibinong', 3, 5, 13, 1),
(167, 'CKR', 'Cikarang', 3, 5, 13, 1),
(168, 'CMH', 'Cimahi', 3, 5, 13, 1),
(169, 'CBN', 'Cirebon', 3, 5, 13, 1),
(170, 'DPK', 'Depok', 3, 5, 13, 1),
(171, 'GRT', 'Garut', 3, 5, 13, 1),
(172, 'IDM', 'Indramayu', 3, 5, 13, 1),
(173, 'KWG', 'Karawang', 3, 5, 13, 1),
(174, 'KNG', 'Kuningan', 3, 5, 13, 1),
(175, 'MJL', 'Majalengka', 3, 5, 13, 1),
(176, 'NPH', 'Ngamprah', 3, 5, 13, 1),
(177, 'PWK', 'Purwakarta', 3, 5, 13, 1),
(178, 'SPA', 'Singaparna', 3, 5, 13, 1),
(179, 'SOR', 'Soreang', 3, 5, 13, 1),
(180, 'SNG', 'Subang', 3, 5, 13, 1),
(181, 'SKB', 'Sukabumi', 3, 5, 13, 1),
(182, 'SBR', 'Sumber', 3, 5, 13, 1),
(183, 'SMD', 'Sumedang', 3, 5, 13, 1),
(184, 'TSM', 'Tasikmalaya', 3, 5, 13, 1),
(185, 'BNR', 'Banjarnegara', 3, 5, 14, 1),
(186, 'BTG', 'Batang', 3, 5, 14, 1),
(187, 'BLA', 'Blora', 3, 5, 14, 1),
(188, 'BYL', 'Boyolali', 3, 5, 14, 1),
(189, 'BBS', 'Brebes', 3, 5, 14, 1),
(190, 'SLO', 'Cilacap', 3, 5, 14, 1),
(191, 'DMK', 'Demak', 3, 5, 14, 1),
(192, 'JPA', 'Jepara', 3, 5, 14, 1),
(193, 'KJN', 'Kajen', 3, 5, 14, 1),
(194, 'KRB', 'Karang Anyar', 3, 5, 14, 1),
(195, 'KBM', 'Kebumen', 3, 5, 14, 1),
(196, 'KDL', 'Kendal', 3, 5, 14, 1),
(197, 'KLN', 'Klaten', 3, 5, 14, 1),
(198, 'KDS', 'Kudus', 3, 5, 14, 1),
(199, 'MGG', 'Mangelang', 3, 5, 14, 1),
(200, 'MKD', 'Mungkid', 3, 5, 14, 1),
(201, 'PTI', 'Pati', 3, 5, 14, 1),
(202, 'PKL', 'Pekalongan', 3, 5, 14, 1),
(203, 'PML', 'Pemalang', 3, 5, 14, 1),
(204, 'PBG', 'Purbalingga', 3, 5, 14, 1),
(205, 'PWD', 'Purwodadi', 3, 5, 14, 1),
(206, 'PWT', 'Purwokerto', 3, 5, 14, 1),
(207, 'PWR', 'Purworejo', 3, 5, 14, 1),
(208, 'RBG', 'Rembang', 3, 5, 14, 1),
(209, 'SLT', 'Salatiga', 3, 5, 14, 1),
(210, 'SMG', 'Semarang', 3, 5, 14, 1),
(211, 'SLW', 'Slawi', 3, 5, 14, 1),
(212, 'SGN', 'Sragen', 3, 5, 14, 1),
(213, 'SKH', 'Sukoharjo', 3, 5, 14, 1),
(214, 'SKT', 'Surakarta', 3, 5, 14, 1),
(215, 'TGL', 'Tegal', 3, 5, 14, 1),
(216, 'TMG', 'Temanggung', 3, 5, 14, 1),
(217, 'UNR', 'Ungaran', 3, 5, 14, 1),
(218, 'WNG', 'Wonogiri', 3, 5, 14, 1),
(219, 'WSB', 'Wonosobo', 3, 5, 14, 1),
(220, 'BTL', 'Bantul', 3, 5, 15, 1),
(221, 'SMN', 'Sleman', 3, 5, 15, 1),
(222, 'WAT', 'Wates', 3, 5, 15, 1),
(223, 'WNO', 'Wonosari', 3, 5, 15, 1),
(224, 'YYK', 'Yogyakarta', 3, 5, 15, 1),
(225, 'BKL', 'Bangkalan', 3, 6, 16, 1),
(226, 'BTU', 'Batu', 3, 6, 16, 1),
(227, 'BYW', 'Banyuwangi', 3, 6, 16, 1),
(228, 'BLT', 'Blitar', 3, 6, 16, 1),
(229, 'BJN', 'Bojonegoro', 3, 6, 16, 1),
(230, 'BDW', 'Bondowoso', 3, 6, 16, 1),
(231, 'GSK', 'Gresik', 3, 6, 16, 1),
(232, 'JMR', 'Jember', 3, 6, 16, 1),
(233, 'JBG', 'Jombang', 3, 6, 16, 1),
(234, 'KNR', 'Kanigoro', 3, 6, 16, 1),
(235, 'KDR', 'Kediri', 3, 6, 16, 1),
(236, 'KPN', 'Kepanjen', 3, 6, 16, 1),
(237, 'KRS', 'Kraksan', 3, 6, 16, 1),
(238, 'LMG', 'Lamongan', 3, 6, 16, 1),
(239, 'LMJ', 'Lumajang', 3, 6, 16, 1),
(240, 'MAD', 'Madiun', 3, 6, 16, 1),
(241, 'MGT', 'Magetan', 3, 6, 16, 1),
(242, 'MLG', 'Malang', 3, 6, 16, 1),
(243, 'MJY', 'Majeyan', 3, 6, 16, 1),
(244, 'MJK', 'Mojokerto', 3, 6, 16, 1),
(245, 'NGW', 'Ngawi', 3, 6, 16, 1),
(246, 'NJK', 'Nganjuk', 3, 6, 16, 1),
(247, 'PCT', 'Pacitan', 3, 6, 16, 1),
(248, 'PMK', 'Pamekasan', 3, 6, 16, 1),
(249, 'PSN', 'Pasuruan', 3, 6, 16, 1),
(250, 'PNG', 'Ponorogo', 3, 6, 16, 1),
(251, 'PBL', 'Probolinggo', 3, 6, 16, 1),
(252, 'SPG', 'Sampang', 3, 6, 16, 1),
(253, 'SDA', 'Sidoarjo', 3, 6, 16, 1),
(254, 'SIT', 'Situbondo', 3, 6, 16, 1),
(255, 'SMP', 'Sumenep', 3, 6, 16, 1),
(256, 'SBY', 'Surabaya', 3, 6, 16, 1),
(257, 'TBN', 'Tuban', 3, 6, 16, 1),
(258, 'TLG', 'Tulung Agung', 3, 6, 16, 1),
(259, 'TRK', 'Trenggalek', 3, 6, 16, 1),
(260, 'BLI', 'Bangli', 3, 7, 17, 1),
(261, 'DPR', 'Denpasar', 3, 7, 17, 1),
(262, 'GIN', 'Gianyar', 3, 7, 17, 1),
(263, 'KRA', 'Karangasem', 3, 7, 17, 1),
(264, 'MGW', 'Mengwi', 3, 7, 17, 1),
(265, 'NGA', 'Negara', 3, 7, 17, 1),
(266, 'SRP', 'Semarapura', 3, 7, 17, 1),
(267, 'SGR', 'Singaraja', 3, 7, 17, 1),
(268, 'TAB', 'Tabanan', 3, 7, 17, 1),
(269, 'ATB', 'Atambua', 3, 7, 18, 1),
(270, 'BAA', 'Baa', 3, 7, 18, 1),
(271, 'BJW', 'Bajawa', 3, 7, 18, 1),
(272, 'BRG', 'Borong', 3, 7, 18, 1),
(273, 'END', 'Ende', 3, 7, 18, 1),
(274, 'KLB', 'Kalabahi', 3, 7, 18, 1),
(275, 'KFM', 'Kefamenamu', 3, 7, 18, 1),
(276, 'KPG', 'Kupang', 3, 7, 18, 1),
(277, 'LBJ', 'Labuan Bajo', 3, 7, 18, 1),
(278, 'LRT', 'Larantuka', 3, 7, 18, 1),
(279, 'LWL', 'Lewoleba', 3, 7, 18, 1),
(280, 'MME', 'Maumere', 3, 7, 18, 1),
(281, 'MBY', 'Mbay', 3, 7, 18, 1),
(282, 'RTG', 'Ruteng', 3, 7, 18, 1),
(283, 'SBB', 'Sabu Barat', 3, 7, 18, 1),
(284, 'SOE', 'Soe', 3, 7, 18, 1),
(285, 'TAM', 'Tambolaka', 3, 7, 18, 1),
(286, 'WBL', 'Waibakul', 3, 7, 18, 1),
(287, 'WKB', 'Waikabubak', 3, 7, 18, 1),
(288, 'WGP', 'Waingapu', 3, 7, 18, 1),
(289, 'BIM', 'Bima', 3, 7, 19, 1),
(290, 'DPU', 'Dompu', 3, 7, 19, 1),
(291, 'GRG', 'Gerung', 3, 7, 19, 1),
(292, 'MTR', 'Mataram', 3, 7, 19, 1),
(293, 'PYA', 'Praya', 3, 7, 19, 1),
(294, 'SEL', 'Selong', 3, 7, 19, 1),
(295, 'SBW', 'Sumbawa Besar', 3, 7, 19, 1),
(296, 'TLW', 'Taliwang', 3, 7, 19, 1),
(297, 'TJN', 'Tanjung', 3, 7, 19, 1),
(298, 'WHO', 'Woha', 3, 7, 19, 1),
(299, 'GTO', 'Gorontalo', 4, 9, 20, 1),
(300, 'KWD', 'Kwandang', 4, 9, 20, 1),
(301, 'MAR', 'Marisa', 4, 9, 20, 1),
(302, 'SWW', 'Suwawa', 4, 9, 20, 1),
(303, 'TMT', 'Tilamuta', 4, 9, 20, 1),
(304, 'MJN', 'Majene', 4, 9, 21, 1),
(305, 'MMS', 'Mamasa', 4, 9, 21, 1),
(306, 'MAM', 'Mamuju', 4, 9, 21, 1),
(307, 'PKY', 'Pasangkayu', 4, 9, 21, 1),
(308, 'PLW', 'Polewali', 4, 9, 21, 1),
(309, 'APN', 'Ampana', 4, 9, 22, 1),
(310, 'BGK', 'Bungku', 4, 9, 22, 1),
(311, 'BUL', 'Buol', 4, 9, 22, 1),
(312, 'DGL', 'Donggala', 4, 9, 22, 1),
(313, 'LWK', 'Luwuk', 4, 9, 22, 1),
(314, 'PAL', 'Palu', 4, 9, 22, 1),
(315, 'PRG', 'Parigi', 4, 9, 22, 1),
(316, 'PSO', 'Poso', 4, 9, 22, 1),
(317, 'SKN', 'Salakan', 4, 9, 22, 1),
(318, 'SGB', 'Sigi Biromaru', 4, 9, 22, 1),
(319, 'TLI', 'Toli Toli', 4, 9, 22, 1),
(320, 'ARM', 'Air Madidi', 4, 9, 23, 1),
(321, 'AMR', 'Amurang', 4, 9, 23, 1),
(322, 'BIT', 'Bitung', 4, 9, 23, 1),
(323, 'BLU', 'Bolang Uki', 4, 9, 23, 1),
(324, 'BRK', 'Boroko', 4, 9, 23, 1),
(325, 'KTG', 'Kotamobagu', 4, 9, 23, 1),
(326, 'LLK', 'Lolak', 4, 9, 23, 1),
(327, 'MND', 'Manado', 4, 9, 23, 1),
(328, 'MGN', 'Melongguane', 4, 9, 23, 1),
(329, 'ODS', 'Odong Siau', 4, 9, 23, 1),
(330, 'RTN', 'Ratahan', 4, 9, 23, 1),
(331, 'THN', 'Tahuna', 4, 9, 23, 1),
(332, 'TMH', 'Tomohon', 4, 9, 23, 1),
(333, 'TNN', 'Tondano', 4, 9, 23, 1),
(334, 'TTY', 'Tutuyan', 4, 9, 23, 1),
(335, 'ADL', 'Andoolo', 4, 9, 24, 1),
(336, 'BAU', 'Bau-Bau', 4, 9, 24, 1),
(337, 'BNG', 'Buranga', 4, 9, 24, 1),
(338, 'KDI', 'Kendari', 4, 9, 24, 1),
(339, 'KKA', 'Kolaka', 4, 9, 24, 1),
(340, 'LSS', 'Lasusua', 4, 9, 24, 1),
(341, 'PSW', 'Pasar Wajo', 4, 9, 24, 1),
(342, 'RAH', 'Raha', 4, 9, 24, 1),
(343, 'RMB', 'Rumbia', 4, 9, 24, 1),
(344, 'UNH', 'Unaaha', 4, 9, 24, 1),
(345, 'WGD', 'Wanggudu', 4, 9, 24, 1),
(346, 'WGW', 'Wangi Wangi', 4, 9, 24, 1),
(347, 'BAN', 'Bantaeng', 4, 9, 25, 1),
(348, 'BAR', 'Barru', 4, 9, 25, 1),
(349, 'BEN', 'Benteng', 4, 9, 25, 1),
(350, 'BLK', 'Bulukumba', 4, 9, 25, 1),
(351, 'ENR', 'Enrekang', 4, 9, 25, 1),
(352, 'JNP', 'Jeneponnto', 4, 9, 25, 1),
(353, 'MAK', 'Makale', 4, 9, 25, 1),
(354, 'MKS', 'Makassar', 4, 9, 25, 1),
(355, 'MLL', 'Malili', 4, 9, 25, 1),
(356, 'MRS', 'Maros', 4, 9, 25, 1),
(357, 'MSB', 'Masamba', 4, 9, 25, 1),
(358, 'PLP', 'Palopo', 4, 9, 25, 1),
(359, 'PKJ', 'Pangkajene', 4, 9, 25, 1),
(360, 'PRE', 'Pare Pare', 4, 9, 25, 1),
(361, 'PIN', 'Pinrang', 4, 9, 25, 1),
(362, 'RTP', 'Rantepao', 4, 9, 25, 1),
(363, 'SKG', 'Sengkang', 4, 9, 25, 1),
(364, 'SDR', 'Sidenreng', 4, 9, 25, 1),
(365, 'SNJ', 'Sinjai', 4, 9, 25, 1),
(366, 'SGM', 'Sungguminasa', 4, 9, 25, 1),
(367, 'TKA', 'Takalar', 4, 9, 25, 1),
(368, 'WTP', 'Watampone', 4, 9, 25, 1),
(369, 'WNS', 'Watan Soppeng', 4, 9, 25, 1),
(370, 'JLL', 'Jailolo', 4, 9, 26, 1),
(371, 'LBA', 'Labuha', 4, 11, 26, 1),
(372, 'MAB', 'Maba', 4, 11, 26, 1),
(373, 'MTS', 'Morotai Selatan', 4, 11, 26, 1),
(374, 'SNN', 'Sanan', 4, 11, 26, 1),
(375, 'SFF', 'Sofifi', 4, 11, 26, 1),
(376, 'TTE', 'Ternate', 4, 11, 26, 1),
(377, 'TDR', 'Tidore', 4, 11, 26, 1),
(378, 'TOB', 'Tobelo', 4, 11, 26, 1),
(379, 'WED', 'Weda', 4, 11, 26, 1),
(380, 'AMB', 'Ambon', 4, 11, 27, 1),
(381, 'DTH', 'Dataran Hunimoa', 4, 11, 27, 1),
(382, 'DRH', 'Dataran Hunipopu', 4, 11, 27, 1),
(383, 'DOB', 'Dobo', 4, 11, 27, 1),
(384, 'MSH', 'Masohi', 4, 11, 27, 1),
(385, 'NLA', 'Namlea', 4, 11, 27, 1),
(386, 'NMR', 'Namrole', 4, 11, 27, 1),
(387, 'SML', 'Saumlaki', 4, 11, 27, 1),
(388, 'TKR', 'Tiakur', 4, 11, 27, 1),
(389, 'TUL', 'Tual', 4, 11, 27, 1),
(390, 'AFT', 'Aifat', 4, 11, 28, 1),
(391, 'AMS', 'Aimas', 4, 11, 28, 1),
(392, 'BTI', 'Bintuni', 4, 11, 28, 1),
(393, 'FFK', 'Fak Fak', 4, 11, 28, 1),
(394, 'FEF', 'Fef', 4, 11, 28, 1),
(395, 'KMN', 'Kaimana', 4, 11, 28, 1),
(396, 'MNK', 'Manokwari', 4, 11, 28, 1),
(397, 'RAS', 'Rasiei', 4, 11, 28, 1),
(398, 'SON', 'Sorong', 4, 11, 28, 1),
(399, 'TMB', 'Teminabuan', 4, 11, 28, 1),
(400, 'WAS', 'Waisai', 4, 11, 28, 1),
(401, 'AGT', 'Agats', 4, 11, 29, 1),
(402, 'BIK', 'Biak', 4, 11, 29, 1),
(403, 'BTW', 'Botawa', 4, 11, 29, 1),
(404, 'BRM', 'Burmeso', 4, 11, 29, 1),
(405, 'ELL', 'Elelim', 4, 11, 29, 1),
(406, 'ERT', 'Enarotali', 4, 11, 29, 1),
(407, 'ILG', 'Ilaga', 4, 11, 29, 1),
(408, 'JAP', 'Jayapura', 4, 11, 29, 1),
(409, 'KBG', 'Karubaga', 4, 11, 29, 1),
(410, 'KYM', 'Kenyam', 4, 11, 29, 1),
(411, 'KEP', 'Kepi', 4, 11, 29, 1),
(412, 'KGM', 'Kigamani', 4, 11, 29, 1),
(413, 'KBK', 'Kobakma', 4, 11, 29, 1),
(414, 'MEK', 'Merauke', 4, 11, 29, 1),
(415, 'MUL', 'Mulia', 4, 11, 29, 1),
(416, 'NAB', 'Nabire', 4, 11, 29, 1),
(417, 'OSB', 'Oksibil', 4, 11, 29, 1),
(418, 'SMI', 'Sarmi', 4, 11, 29, 1),
(419, 'SRU', 'Serui', 4, 11, 29, 1),
(420, 'SRW', 'Sorendiweri', 4, 11, 29, 1),
(421, 'SGP', 'Sugapa', 4, 11, 29, 1),
(422, 'SMH', 'Sumohai', 4, 11, 29, 1),
(423, 'TMR', 'Tanah Merah', 4, 11, 29, 1),
(424, 'TIG', 'Tigi', 4, 11, 29, 1),
(425, 'TIM', 'Timika', 4, 11, 29, 1),
(426, 'TOM', 'Tiom', 4, 11, 29, 1),
(427, 'WAM', 'Wamena', 4, 11, 29, 1),
(428, 'WRS', 'Waris', 4, 11, 29, 1),
(429, 'BEK', 'Bengkayang', 4, 11, 30, 1),
(430, 'KTP', 'Ketapang', 4, 8, 30, 1),
(431, 'MPW', 'Mempawah', 4, 8, 30, 1),
(432, 'NGP', 'Nanga Pinoh', 4, 8, 30, 1),
(433, 'NBA', 'Ngabang', 4, 8, 30, 1),
(434, 'PTK', 'Pontianak', 4, 8, 30, 1),
(435, 'PTS', 'Putussibau', 4, 8, 30, 1),
(436, 'SBS', 'Sambas', 4, 8, 30, 1),
(437, 'SAG', 'Sanggau', 4, 8, 30, 1),
(438, 'SED', 'Sekadau', 4, 8, 30, 1),
(439, 'SKW', 'Singkawang', 4, 8, 30, 1),
(440, 'STG', 'Sintang', 4, 8, 30, 1),
(441, 'SKD', 'Sukadane', 4, 8, 30, 1),
(442, 'SRY', 'Sungai Raya', 4, 8, 30, 1),
(443, 'BPP', 'Balikpapan', 4, 8, 31, 1),
(444, 'BON', 'Bontang', 4, 8, 31, 1),
(445, 'PNJ', 'Penajam', 4, 8, 31, 1),
(446, 'SMR', 'Samarinda', 4, 8, 31, 1),
(447, 'SGT', 'Sanggata', 4, 8, 31, 1),
(448, 'SDW', 'Sendawar', 4, 8, 31, 1),
(449, 'TGT', 'Tanah Grogot', 4, 8, 31, 1),
(450, 'TNR', 'Tanjung Reded', 4, 8, 31, 1),
(451, 'TRG', 'Tenggarong', 4, 8, 31, 1),
(452, 'TDP', 'Tideng Pele', 4, 8, 31, 1),
(453, 'AMT', 'Amuntai', 4, 8, 32, 1),
(454, 'BJB', 'Banjarbaru', 4, 8, 32, 1),
(455, 'BJM', 'Banjarmasin', 4, 8, 32, 1),
(456, 'BRB', 'Barabai', 4, 8, 32, 1),
(457, 'BLN', 'Batulicin', 4, 8, 32, 1),
(458, 'KGN', 'Kandangan', 4, 8, 32, 1),
(459, 'KBR', 'Kotabaru', 4, 8, 32, 1),
(460, 'MRH', 'Marabahan', 4, 8, 32, 1),
(461, 'MTP', 'Martapura', 4, 8, 32, 1),
(462, 'PRN', 'Paringin', 4, 8, 32, 1),
(463, 'PLI', 'Palaihari', 4, 8, 32, 1),
(464, 'RTA', 'Rantau', 4, 8, 32, 1),
(465, 'TJG', 'Tanjung', 4, 8, 32, 1),
(466, 'BNT', 'Buntok', 4, 8, 33, 1),
(467, 'KSN', 'Kasongan', 4, 8, 33, 1),
(468, 'KLK', 'Kuala Kapuas', 4, 8, 33, 1),
(469, 'KKN', 'Kuala Kurun', 4, 8, 33, 1),
(470, 'KLP', 'Kuala Pembuang', 4, 8, 33, 1),
(471, 'MTW', 'Muara Teweh', 4, 8, 33, 1),
(472, 'NGB', 'Nanga Bulik', 4, 8, 33, 1),
(473, 'PLK', 'Palangkaraya', 4, 8, 33, 1),
(474, 'PBU', 'Pangkalan Bun', 4, 8, 33, 1),
(475, 'PPS', 'Pulang Pisang', 4, 8, 33, 1),
(476, 'PRC', 'Puruk Cahu', 4, 8, 33, 1),
(477, 'SPT', 'Sampit', 4, 8, 33, 1),
(478, 'SKR', 'Sukamara', 4, 8, 33, 1),
(479, 'TML', 'Tamiang Layang', 4, 8, 33, 1),
(480, 'MLN', 'Malinau', 4, 8, 34, 1),
(481, 'NNK', 'Nunukan', 4, 8, 34, 1),
(482, 'TJS', 'Tanjung Selor', 4, 8, 34, 1),
(483, 'TAR', 'Tarakan', 4, 8, 34, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dropcategorytable`
--

CREATE TABLE IF NOT EXISTS `dropcategorytable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `dropcategorytable`
--

INSERT INTO `dropcategorytable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'High Rental Price', 1),
(2, 'Community Issue', 1),
(3, 'Rejected by Landlord', 1),
(4, 'Rejected by Government', 1),
(5, 'Canceled PO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `generatetable`
--

CREATE TABLE IF NOT EXISTS `generatetable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `generate_controller` varchar(100) DEFAULT NULL,
  `generate_table` varchar(100) DEFAULT NULL,
  `generate_field` varchar(100) DEFAULT NULL,
  `generate_field_descr` varchar(100) DEFAULT NULL,
  `generate_type` varchar(100) DEFAULT NULL,
  `generate_field_index` int(11) DEFAULT '0',
  `generate_field_mandatory` int(11) DEFAULT '0',
  `generate_relation_table` varchar(100) DEFAULT NULL,
  `generate_relation_field` varchar(100) DEFAULT NULL,
  `generate_relation_fieldtxt` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`generate_controller`,`generate_field`,`generate_field_descr`,`generate_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `itemunittable`
--

CREATE TABLE IF NOT EXISTS `itemunittable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `itemunittable`
--

INSERT INTO `itemunittable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'Kg', 1),
(2, 'Ton', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menutable`
--

CREATE TABLE IF NOT EXISTS `menutable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(4) NOT NULL,
  `name` varchar(123) NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `menutable`
--

INSERT INTO `menutable` (`row_id`, `code`, `name`, `remark`) VALUES
(1, '0000', 'Dashboard', ''),
(2, '0100', 'Pending Approval', 'List of SO/WO which not approved yet'),
(3, '0200', 'Site', 'List of site'),
(4, '0300', 'Project', 'Project Management'),
(5, '0301', '  Sales Order', 'Manage Sales Order'),
(6, '0302', '  Work Order', 'Manage Work Order'),
(7, '0303', '  Site Survey', ''),
(8, '0304', '  Site acquisition', ''),
(9, '0305', '  Soil Test', ''),
(10, '0306', '  CME', ''),
(11, '0307', '  ATP & On Air', ''),
(12, '0308', '  Project Activity', 'List of Project Activity'),
(13, '0400', 'Tenant', 'List of Tenant'),
(14, '0500', 'Vendor', 'List of Vendor'),
(15, '0600', 'Master Data', 'List of Master Data'),
(16, '0601', '   Product', 'List of Product'),
(17, '0602', '   Area', ''),
(18, '0603', '   Region', ''),
(19, '0604', '   Province', ''),
(20, '0605', ' City', ''),
(21, '0606', '   Site Type', ''),
(22, '0607', '   Tower Height', ''),
(23, '0608', '   Vendor Type', ''),
(24, '0609', '   Transmission Type', ''),
(25, '0610', '   Drop Category', ''),
(26, '0611', '   Item Unit', ''),
(27, '0700', 'System Administrator', 'Tool for System Administrator'),
(28, '0701', 'Sales Order Approval Flow', ''),
(29, '0702', 'Work Order Approval Flow', ''),
(30, '0703', 'Role', ''),
(31, '0704', 'User', ''),
(32, '0705', 'Password', ''),
(33, '0706', 'Menu', ''),
(34, '0707', 'Generator', ''),
(35, '0708', 'System Log', ''),
(36, '0800', 'Logout', '');

-- --------------------------------------------------------

--
-- Table structure for table `producttable`
--

CREATE TABLE IF NOT EXISTS `producttable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `short_txt` varchar(1) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `producttable`
--

INSERT INTO `producttable` (`row_id`, `short_txt`, `txt`, `status_id`) VALUES
(1, 'P', 'MCP', 1),
(2, 'N', 'Mini Macro', 1),
(3, 'M', 'Macro', 1),
(4, 'H', 'BTS Hotel', 1),
(5, 'C', 'Collo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `projectactivitytable`
--

CREATE TABLE IF NOT EXISTS `projectactivitytable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `isscope` int(11) NOT NULL,
  `code` varchar(12) NOT NULL,
  `txt` varchar(255) NOT NULL,
  `remark` text NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `projectactivitytable`
--

INSERT INTO `projectactivitytable` (`row_id`, `isscope`, `code`, `txt`, `remark`, `status_id`) VALUES
(1, 1, '010000', 'SIS - Site Surveys', '', 1),
(2, 1, '020000', 'SITAC - Site Acqutition', '', 1),
(3, 1, '030000', 'CME - Civil & Mechanical Engineering', '', 1),
(4, 0, '040000', 'RFC - Ready for Construction', '', 1),
(5, 0, '050000', 'RFI - Ready for Installation', '', 1),
(7, 1, '020100', 'Hammer / Soil Test', '', 1),
(8, 0, '020200', 'IW - Izin Warga', '', 1),
(9, 0, '020300', 'BAN / BAK - Berita Acea Negoisasi / Berita Acara Kesepakatan', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `provincetable`
--

CREATE TABLE IF NOT EXISTS `provincetable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `short_txt` varchar(15) NOT NULL,
  `txt` varchar(125) NOT NULL,
  `area_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `provincetable`
--

INSERT INTO `provincetable` (`row_id`, `short_txt`, `txt`, `area_id`, `region_id`, `status_id`) VALUES
(1, 'NAD', 'Nanggroe Aceh Darussalam', 1, 1, 1),
(2, 'SUMUT', 'Sumatera Utara', 1, 1, 1),
(3, 'SUMBAR', 'Sumatera Barat', 1, 10, 1),
(4, 'RIAU', 'Riau', 1, 10, 1),
(5, 'KEPRI', 'Kepulauan Riau', 1, 10, 1),
(6, 'SUMSEL', 'Sumatera Selatan', 1, 2, 1),
(7, 'BENGKU', 'Bengkulu', 1, 2, 1),
(8, 'JAMB', 'Jambi', 1, 2, 1),
(9, 'LAMP', 'Lampung', 1, 2, 1),
(10, 'BABEL', 'Bangka Belitung', 1, 2, 1),
(11, 'JKT', 'DKI Jakarta', 2, 3, 1),
(12, 'BANTEN', 'Banten', 2, 4, 1),
(13, 'JABAR', 'Jawa Barat', 2, 4, 1),
(14, 'JATENG', 'Jawa Tengah', 3, 5, 1),
(15, 'DIY', 'DI Yogyakarta', 3, 5, 1),
(16, 'JATIM', 'Jawa Timur', 3, 6, 1),
(17, 'BALI', 'Bali', 3, 7, 1),
(18, 'NTT', 'Nusa Tenggara Timue', 3, 7, 1),
(19, 'NTB', 'Nusa Tenggara Barat', 3, 7, 1),
(20, 'GORON', 'Gorontalo', 4, 9, 1),
(21, 'SULBAR', 'Sulawesi Barat', 4, 9, 1),
(22, 'SULTENG', 'Sulawesi Tengah', 4, 9, 1),
(23, 'SULUT', 'Sulawesi Utara', 4, 9, 1),
(24, 'SULTENGGA', 'Sulawesi Tenggara', 4, 9, 1),
(25, 'SULSEL', 'Sulawesi Selatan', 4, 9, 1),
(26, 'MALUT', 'Maluku Utara', 4, 11, 1),
(27, 'MALUKU', 'Maluku', 4, 11, 1),
(28, 'PABAR', 'Papua Barat', 4, 11, 1),
(29, 'PAPUA', 'Papua', 4, 11, 1),
(30, 'KALBAR', 'Kalimantan Barat', 4, 8, 1),
(31, 'KALTIM', 'Kalimantan Timur', 4, 8, 1),
(32, 'KALSEL', 'Kalimantan Selatan', 4, 8, 1),
(33, 'KALTENG', 'Kalimantan Tengah', 4, 8, 1),
(34, 'KALUT', 'Kalimantan Utara', 4, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `regiontable`
--

CREATE TABLE IF NOT EXISTS `regiontable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `short_txt` varchar(1) NOT NULL,
  `txt` varchar(255) DEFAULT NULL,
  `area_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `regiontable`
--

INSERT INTO `regiontable` (`row_id`, `short_txt`, `txt`, `area_id`, `status_id`) VALUES
(1, 'A', 'Sumbagut', 1, 1),
(2, 'B', 'Sumbagsel', 1, 1),
(3, 'C', 'Jabo', 2, 1),
(4, 'D', 'Jabar', 2, 1),
(5, 'E', 'Jateng', 3, 1),
(6, 'F', 'Jatim', 3, 1),
(7, 'G', 'Balnus', 3, 1),
(8, 'H', 'Kalimantan', 4, 1),
(9, 'I', 'Sulawesi', 4, 1),
(10, 'J', 'Sumbagteng', 1, 1),
(11, 'K', 'Puma', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rolemenutable`
--

CREATE TABLE IF NOT EXISTS `rolemenutable` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rolemenutable`
--

INSERT INTO `rolemenutable` (`role_id`, `menu_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 13),
(2, 15),
(2, 16),
(2, 27),
(2, 32),
(2, 36),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 13),
(3, 27),
(3, 32),
(3, 36),
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 13),
(4, 27),
(4, 32),
(4, 36),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 6),
(5, 14),
(5, 27),
(5, 32),
(5, 36),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 14),
(6, 27),
(6, 32),
(6, 36),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 6),
(7, 14),
(7, 27),
(7, 32),
(7, 36),
(8, 1),
(8, 2),
(8, 27),
(8, 32),
(8, 36),
(9, 1),
(9, 2),
(9, 4),
(9, 5),
(9, 6),
(9, 27),
(9, 32),
(9, 36),
(10, 1),
(10, 2),
(10, 4),
(10, 5),
(10, 6),
(10, 27),
(10, 32),
(10, 36);

-- --------------------------------------------------------

--
-- Table structure for table `roletable`
--

CREATE TABLE IF NOT EXISTS `roletable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `roletable`
--

INSERT INTO `roletable` (`row_id`, `txt`) VALUES
(1, 'Administrator'),
(2, 'Sales Admin'),
(3, 'Account Manager'),
(4, 'General Manager Sales'),
(5, 'Project Support'),
(6, 'Project Manager'),
(7, 'General Manager Project'),
(8, 'Solution Director'),
(9, 'Operation Director'),
(10, 'President Director');

-- --------------------------------------------------------

--
-- Table structure for table `salesorderapprovalflowtable`
--

CREATE TABLE IF NOT EXISTS `salesorderapprovalflowtable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `ordering` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `salesorderapprovalflowtable`
--

INSERT INTO `salesorderapprovalflowtable` (`row_id`, `ordering`, `role_id`, `remark`) VALUES
(1, 1, 3, 'Account Manager'),
(2, 2, 6, 'Project Manager'),
(3, 3, 4, 'GM Sales'),
(4, 4, 9, 'Operation Director'),
(5, 5, 10, 'President Director');

-- --------------------------------------------------------

--
-- Table structure for table `salesorderapprovaltable`
--

CREATE TABLE IF NOT EXISTS `salesorderapprovaltable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `salesorder_no` varchar(255) NOT NULL,
  `approvalflow_id` int(11) NOT NULL,
  `approved_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved_status` varchar(15) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `salesorderapprovaltable`
--

INSERT INTO `salesorderapprovaltable` (`row_id`, `salesorder_no`, `approvalflow_id`, `approved_date`, `approved_status`, `remark`, `status_id`) VALUES
(14, '0001/SO/XI/2017', 1, '2017-10-23 05:00:00', '1', 'Approved by Account Manager', 1),
(15, '0001/SO/XI/2017', 2, '2017-10-23 05:00:00', '1', 'Approved by Project Manager', 1),
(16, '0001/SO/XI/2017', 3, '2017-10-23 05:00:00', '1', 'Approved by GM Sales', 1),
(17, '0001/SO/XI/2017', 4, '2017-10-23 05:00:00', '1', 'Approved by Operation Director', 1),
(18, '0001/SO/XI/2017', 5, '2017-10-23 05:00:00', '1', 'Approved by President Director', 1),
(55, '0002/SO/XI/2017', 1, '2017-11-02 05:00:00', '1', 'Approved by Account Manager', 1),
(57, '0002/SO/XI/2017', 2, '2017-11-02 05:00:00', '1', 'Approved By Project Manager', 1),
(66, '0002/SO/XI/2017', 3, '2017-12-12 22:33:39', '1', 'Approved by GM Sales', 1),
(67, '0002/SO/XI/2017', 4, '2017-12-12 22:57:49', '1', 'Approved by Operation Director', 1),
(70, '0002/SO/XI/2017', 5, '2017-12-12 23:02:04', '1', 'Approved by President Director', 1);

-- --------------------------------------------------------

--
-- Table structure for table `salesorderlinetable`
--

CREATE TABLE IF NOT EXISTS `salesorderlinetable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `salesorder_no` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `site_id_tenant` varchar(255) DEFAULT NULL,
  `site_name_tenant` varchar(255) NOT NULL,
  `site_id_api` varchar(255) NOT NULL,
  `site_name_api` varchar(255) NOT NULL,
  `sitetype_id` int(11) NOT NULL,
  `towerheight_id` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `area_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `baks` date NOT NULL,
  `rfi` date NOT NULL,
  `baps` date NOT NULL,
  `remarks` text NOT NULL,
  `rent_period` double DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`salesorder_no`,`product_id`,`site_id_tenant`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `salesorderlinetable`
--

INSERT INTO `salesorderlinetable` (`row_id`, `salesorder_no`, `product_id`, `site_id_tenant`, `site_name_tenant`, `site_id_api`, `site_name_api`, `sitetype_id`, `towerheight_id`, `latitude`, `longitude`, `area_id`, `region_id`, `province_id`, `city_id`, `baks`, `rfi`, `baps`, `remarks`, `rent_period`, `status_id`) VALUES
(1, '0001/SO/XI/2017', 1, 'TSEL-001', 'TEBET', 'PBC-LSK-001', 'API_TEBET', 1, 6, -12.8998989, 15.87766, 2, 3, 11, 147, '2017-10-18', '2017-10-17', '2017-10-19', 'na', 5, 1),
(2, '0002/SO/XI/2017', 1, 'a', 'a', 'PBC-LSK-002', 'API_a', 1, 4, -1, 122, 2, 3, 11, 147, '2017-10-25', '2017-10-24', '2017-10-26', 'a', 5, 1),
(4, '0003/SO/XII/2017', 1, 'TLKMJKT-001', 'JKT-TEBET-001', 'PAA-BNA-001', 'API_JKT-TEBET-001', 1, 4, 1, 3, 1, 1, 1, 1, '2017-12-07', '2017-12-06', '2017-12-08', '-NA-', 5, 1),
(5, '0003/SO/XII/2017', 1, 'TLKMJKT-001', 'JKT-TEBET-001', 'PAA-BNA-001', 'API_JKT-TEBET-001', 1, 4, 1, 3, 1, 1, 1, 1, '2017-12-07', '2017-12-06', '2017-12-08', '-NA-', 5, 1),
(6, '0003/SO/XII/2017', 1, 'TLKMJKT-001', 'JKT-TEBET-001', 'PAA-BNA-001', 'API_JKT-TEBET-001', 1, 4, 1, 3, 1, 1, 1, 1, '2017-12-07', '2017-12-06', '2017-12-08', '-NA-', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `salesordertable`
--

CREATE TABLE IF NOT EXISTS `salesordertable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `salesorder_no` varchar(255) DEFAULT NULL,
  `salesorder_date` date DEFAULT NULL,
  `drm_date` date DEFAULT NULL,
  `batch` varchar(255) DEFAULT NULL,
  `po_tenant_no` varchar(255) DEFAULT NULL,
  `po_tenant_date` date DEFAULT NULL,
  `tenant_id` int(11) DEFAULT NULL,
  `work_flow_milestone_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`salesorder_no`,`salesorder_date`,`drm_date`,`po_tenant_no`,`po_tenant_date`,`tenant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `salesordertable`
--

INSERT INTO `salesordertable` (`row_id`, `salesorder_no`, `salesorder_date`, `drm_date`, `batch`, `po_tenant_no`, `po_tenant_date`, `tenant_id`, `work_flow_milestone_id`, `status_id`) VALUES
(1, '0001/SO/XI/2017', '0000-00-00', '0000-00-00', '1', '123', '0000-00-00', 1, 9, 1),
(2, '0002/SO/XI/2017', '2017-10-24', '0000-00-00', '1', '01/TSEL/PO/X/2017', '2017-10-25', 1, 9, 1),
(3, '0003/SO/XII/2017', '2017-12-05', '2017-12-05', '1', 'TSEL01', '2017-12-05', 1, 2, 1),
(6, '0004/SO/XII/2017', '2017-12-05', '0000-00-00', '1', 'saaa', '2017-12-05', 1, 1, 1),
(7, '0006/SO/XII/2017', '2017-12-18', '0000-00-00', '1', 'TSEL-XII-001', '2017-12-17', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sitetable`
--

CREATE TABLE IF NOT EXISTS `sitetable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `site_id_api` varchar(255) DEFAULT NULL,
  `site_name_api` varchar(255) DEFAULT NULL,
  `site_id_tenant` varchar(125) NOT NULL,
  `site_name_tenant` varchar(255) NOT NULL,
  `area_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `longitude_ori` double DEFAULT NULL,
  `latitude_ori` double DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `sitetype_id` int(11) NOT NULL,
  `towerheight_id` int(11) NOT NULL,
  `transmissiontype_id` int(11) NOT NULL,
  `remark` text NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`site_id_api`,`area_id`,`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sitetable`
--

INSERT INTO `sitetable` (`row_id`, `product_id`, `tenant_id`, `site_id_api`, `site_name_api`, `site_id_tenant`, `site_name_tenant`, `area_id`, `region_id`, `province_id`, `city_id`, `longitude_ori`, `latitude_ori`, `address`, `sitetype_id`, `towerheight_id`, `transmissiontype_id`, `remark`, `status_id`) VALUES
(2, 1, 1, 'PBC-LSK-002', 'API_a', 'a', 'a', 2, 3, 11, 147, 122, -1, 'Jakarta', 1, 4, 1, 'a', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sitetypetable`
--

CREATE TABLE IF NOT EXISTS `sitetypetable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sitetypetable`
--

INSERT INTO `sitetypetable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'GF - Green Field', 1),
(2, 'RT - Roof Top', 1);

-- --------------------------------------------------------

--
-- Table structure for table `statustable`
--

CREATE TABLE IF NOT EXISTS `statustable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `txt` text,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `statustable`
--

INSERT INTO `statustable` (`row_id`, `name`, `txt`) VALUES
(1, 'Active', 'Stated that the data is active'),
(2, 'Not Active', 'Stated that the data is not active');

-- --------------------------------------------------------

--
-- Table structure for table `systemlogtable`
--

CREATE TABLE IF NOT EXISTS `systemlogtable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `key_id` int(11) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`created_by`,`created_date`,`module`,`key_id`,`action`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1150 ;

--
-- Dumping data for table `systemlogtable`
--

INSERT INTO `systemlogtable` (`row_id`, `created_by`, `created_date`, `module`, `key_id`, `action`, `data`) VALUES
(1, 1, '2017-03-12 22:44:12', 'region', 1, 'Insert regiontable', '{"txt":"Jawa Barat","status_id":"1"}'),
(2, 1, '2017-03-12 22:44:38', 'region', 1, 'Update regiontable', '{"txt":"Jawa Barat 1","status_id":"1"}'),
(3, 1, '2017-03-12 22:45:07', 'region', 1, 'Update regiontable', '{"txt":"Jawa Barat","status_id":"1"}'),
(4, 1, '2017-03-12 23:17:46', 'sitetype', 1, 'Update sitetypetable', '{"txt":"MCP 1","status_id":"1"}'),
(5, 1, '2017-03-12 23:17:52', 'sitetype', 1, 'Update sitetypetable', '{"txt":"MCP","status_id":"1"}'),
(6, 1, '2017-03-13 22:11:46', 'towerheight', 1, 'Insert towerheighttable', '{"txt":"RT - 6 Meter","status_id":"1"}'),
(7, 1, '2017-03-13 22:11:56', 'towerheight', 2, 'Insert towerheighttable', '{"txt":"RT - 9 Meter","status_id":"1"}'),
(8, 1, '2017-03-13 22:12:12', 'towerheight', 3, 'Insert towerheighttable', '{"txt":"GF - 20 Meter","status_id":"1"}'),
(9, 1, '2017-03-13 22:13:38', 'site', 1, 'Insert sitetable', '{"site_id":"201232","site_name":"Site Bekasi","sitetype_id":"1","towerheight_id":"1","region_id":"1","area_id":"1","address":"Jalan Raya Bekasi","latitude":"123","longitude":"321","status_id":"1"}'),
(10, 1, '2017-03-13 22:23:22', 'vendortype', 1, 'Insert vendortypetable', '{"txt":"Construction","status_id":"1"}'),
(11, 1, '2017-03-13 22:24:11', 'vendortype', 2, 'Insert vendortypetable', '{"txt":"Fabrication","status_id":"1"}'),
(12, 1, '2017-03-13 22:38:51', 'vendor', 1, 'Insert vendortable', '{"vendortype_id":"1","name":"PT. WIKA","email":"wika@email.com","phone_no":"112233","address":"Jalan Raya Bogor","status_id":"1"}'),
(13, 1, '2017-03-13 22:55:38', 'transmissiontype', 1, 'Insert transmissiontypetable', '{"txt":"FSO","status_id":"1"}'),
(14, 1, '2017-03-13 22:55:44', 'transmissiontype', 2, 'Insert transmissiontypetable', '{"txt":"FO","status_id":"1"}'),
(15, 1, '2017-03-13 23:16:01', 'site', 1, 'Insert sitetable', '{"site_id":"210321","site_name":"Site Bekasi","sitetype_id":"1","towerheight_id":"1","transmissiontype_id":"1","region_id":"1","area_id":"1","address":"Jalan raya bekasi","latitude":"123","longitude":"321","milestone_status_id":"1","inchstone_status_id":"1"}'),
(16, 1, '2017-03-14 22:17:42', 'product', 1, 'Insert producttable', '{"txt":"B2S MCP","is_active":"1"}'),
(17, 1, '2017-03-14 22:17:56', 'product', 2, 'Insert producttable', '{"txt":"B2S BTSH","is_active":"1"}'),
(18, 1, '2017-03-14 22:18:05', 'product', 3, 'Insert producttable', '{"txt":"COLO MCP","is_active":"1"}'),
(19, 1, '2017-03-14 22:18:19', 'product', 4, 'Insert producttable', '{"txt":"COLO BTSH","is_active":"1"}'),
(20, 1, '2017-03-14 22:18:33', 'product', 5, 'Insert producttable', '{"txt":"B2S Macro","is_active":"1"}'),
(21, 1, '2017-03-14 22:18:48', 'product', 6, 'Insert producttable', '{"txt":"COLO Macro","is_active":"1"}'),
(22, 1, '2017-03-14 22:31:06', 'dropcategory', 1, 'Insert dropcategorytable', '{"txt":"High Rental Price","is_active":"1"}'),
(23, 1, '2017-03-14 22:31:21', 'dropcategory', 2, 'Insert dropcategorytable', '{"txt":"Community Issue","is_active":"1"}'),
(24, 1, '2017-03-14 22:31:37', 'dropcategory', 3, 'Insert dropcategorytable', '{"txt":"Rejected by Landlord","is_active":"1"}'),
(25, 1, '2017-03-14 22:31:54', 'dropcategory', 4, 'Insert dropcategorytable', '{"txt":"Rejected by Government","is_active":"1"}'),
(26, 1, '2017-03-14 22:32:09', 'dropcategory', 5, 'Insert dropcategorytable', '{"txt":"Canceled PO","is_active":"1"}'),
(27, 1, '2017-03-15 18:12:25', 'salesorder', 1, 'Insert salesordertable', '{"salesorder_no":"SO15000001","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-15 00:00:00","batch":"1","po_tenant_no":"PO000001","po_tenant_date":"2017-03-10 00:00:00","tenant_id":"1"}'),
(28, 1, '2017-03-15 18:13:06', 'salesorder', 2, 'Insert salesordertable', '{"salesorder_no":"SO17000002","salesorder_date":"2017-03-17 00:00:00","drm_date":"2017-03-10 00:00:00","batch":"1","po_tenant_no":"PO000001","po_tenant_date":"2017-03-10 00:00:00","tenant_id":"1"}'),
(29, 1, '2017-03-15 18:14:37', 'salesorder', 3, 'Insert salesordertable', '{"salesorder_no":"SO10000003","salesorder_date":"2017-03-10 00:00:00","drm_date":"2017-03-11 00:00:00","batch":"1","po_tenant_no":"2","po_tenant_date":"2017-03-08 00:00:00","tenant_id":"1"}'),
(30, 1, '2017-03-15 18:20:36', 'salesorder', 4, 'Insert salesordertable', '{"salesorder_no":"SO15000004","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-15 00:00:00","batch":"2","po_tenant_no":"PO023093","po_tenant_date":"2017-03-15 00:00:00","tenant_id":"1"}'),
(31, 1, '2017-03-15 18:21:22', 'salesorder', 5, 'Insert salesordertable', '{"salesorder_no":"SO15000005","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-04 00:00:00","batch":"SO02399493","po_tenant_no":"PO9490303","po_tenant_date":"2017-03-10 00:00:00","tenant_id":"1"}'),
(32, 1, '2017-03-15 18:29:56', 'salesorder', 6, 'Insert salesordertable', '{"salesorder_no":"SO15000006","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-04 00:00:00","batch":"2","po_tenant_no":"PO0994392","po_tenant_date":"2017-03-15 00:00:00","tenant_id":"1"}'),
(33, 1, '2017-03-15 18:31:29', 'salesorder', 7, 'Insert salesordertable', '{"salesorder_no":"SO15000007","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-11 00:00:00","batch":"1","po_tenant_no":"PO094493","po_tenant_date":"2017-03-15 00:00:00","tenant_id":"1"}'),
(34, 1, '2017-03-15 18:39:35', 'salesorder', 8, 'Insert salesordertable', '{"salesorder_no":"SO15000008","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-11 00:00:00","batch":"3","po_tenant_no":"PO0490334","po_tenant_date":"2017-03-11 00:00:00","tenant_id":"1"}'),
(35, 1, '2017-03-15 20:19:31', 'salesorderline', 1, 'Insert salesorderline', '{"salesorder_no":"SO15000008","product_id":"1","site_tenant_id":"884938","rent_period":"5"}'),
(36, 1, '2017-04-02 20:58:37', 'itemunit', 1, 'Insert itemunittable', '{"txt":"Kg","status_id":"1"}'),
(37, 1, '2017-04-02 20:58:43', 'itemunit', 2, 'Insert itemunittable', '{"txt":"Ton","status_id":"1"}'),
(38, 1, '2017-05-08 14:39:58', 'workorder', 0, 'Insert workordertable', '{"workorder_no":"WO-2","status_id":"1"}'),
(39, 1, '2017-05-08 14:43:52', 'workorder', 1, 'Insert workordertable', '{"workorder_no":"WO-01","status_id":"1"}'),
(40, 1, '2017-05-08 14:44:14', 'workorder', 2, 'Insert workordertable', '{"workorder_no":"WO-02","status_id":"1"}'),
(41, 1, '2017-05-08 14:44:59', 'workorder', 3, 'Insert workordertable', '{"workorder_no":"WO-03","status_id":"2"}'),
(42, 1, '2017-05-08 15:06:22', 'workorder', 3, 'Update workordertable', '{"workorder_no":"","status_id":"1"}'),
(43, 1, '2017-05-08 15:06:38', 'workorder', 3, 'Update workordertable', '{"workorder_no":"WO-03","status_id":"2"}'),
(44, 1, '2017-05-08 15:06:58', 'workorder', 3, 'Update workordertable', '{"workorder_no":"WO-03","status_id":"1"}'),
(45, 1, '2017-05-09 10:02:33', 'workorder', 3, 'Delete workordertable', '""'),
(46, NULL, '2017-05-15 16:12:09', 'workorder', 0, 'Insert workordertable', '{"id":"WO-02","tanggal":"2017-05-15","vendor_row_id":"1","contact_person":"Ali","cp_phone":"0812-123-456","cp_email":"ali@localhost.com","product_group":"Group Product 1","project_manager":"Asep Sofyan","remarks":"test 2"}'),
(47, NULL, '2017-05-15 16:19:53', 'workorder', 0, 'Update workordertable', '{"tanggal":"2017-05-09","vendor_row_id":"1","contact_person":"Adi","cp_phone":"0812-345-678","cp_email":"adi@indonesiancloud.com","product_group":"Product Group 2","project_manager":"Asep Sofyan","remarks":"Test"}'),
(48, NULL, '2017-05-15 16:21:22', 'workorder', 0, 'Delete workordertable', '""'),
(49, NULL, '2017-05-15 16:25:56', 'workorder', 0, 'Insert workordertable', '{"id":"wo-03","tanggal":"2017-05-15","vendor_row_id":"1","contact_person":"adib","cp_phone":"0812222222","cp_email":"adib@localhost.com","product_group":"Product group 3","project_manager":"asep sofyan","remarks":"test 4"}'),
(50, NULL, '2017-05-15 16:28:42', 'workorder', 0, 'Update workordertable', '{"tanggal":"2017-05-15","vendor_row_id":"1","contact_person":"adib","cp_phone":"0812222222","cp_email":"adib@localhost.com","product_group":"Product group 3","project_manager":"asep sofyan","remarks":"test 4...................."}'),
(51, NULL, '2017-05-15 16:41:51', 'workorder', 0, 'Delete workordertable', '""'),
(52, NULL, '2017-05-15 16:45:59', 'workorder', 0, 'Delete workordertable', '""'),
(53, NULL, '2017-05-15 16:50:06', 'workorder', 0, 'Delete workordertable', '""'),
(54, NULL, '2017-05-15 16:51:07', 'workorder', 0, 'Update workordertable', '{"tanggal":"2017-05-09","vendor_row_id":"1","contact_person":"Adi","cp_phone":"0812-345-678","cp_email":"adi@indonesiancloud.com","product_group":"Product Group 1","project_manager":"Asep Sofyan","remarks":"Test"}'),
(55, 1, '2017-05-16 09:14:27', 'workorder', 0, 'Update workordertable', '{"tanggal":"2017-05-09","vendor_row_id":"1","contact_person":"Adi","cp_phone":"0812-345-678","cp_email":"adi@indonesiancloud.com","product_group":"Product Group 1","project_manager":"Asep Sofyan","remarks":"Hasil editan"}'),
(56, 1, '2017-05-16 09:16:02', 'workorder', 0, 'Insert workordertable', '{"id":"WO-02","tanggal":"2017-0516","vendor_row_id":"1","contact_person":"Joni","cp_phone":"0812-345-678","cp_email":"joni@wika.co.id","product_group":"Product Group 2","project_manager":"Asep Sofyan","remarks":"New project at API"}'),
(57, 1, '2017-05-16 09:16:59', 'workorder', 0, 'Delete workordertable', '""'),
(58, 1, '2017-05-16 09:24:33', 'vendor', 2, 'Insert vendortable', '{"vendortype_id":"1","name":"PP","email":"jupri@pp.co.id","phone_no":"0811-234-567","address":"Jl Pasar Rebo Jakarta","status_id":"1"}'),
(59, 1, '2017-05-16 09:24:57', 'vendor', 2, 'Update vendortable', '{"vendortype_id":"1","name":"Pembangunan Perumahan, PT","email":"jupri@pp.co.id","phone_no":"0811-234-567","address":"Jl Pasar Rebo Jakarta","status_id":"1"}'),
(60, 1, '2017-05-16 09:47:01', 'workorder', 0, 'Insert workordertable', '{"id":"WO-02","tanggal":"2017-05-18","vendor_row_id":"2","contact_person":"aa","cp_phone":"0181231","cp_email":"mal@co.com","product_group":"asda","project_manager":"dad","remarks":"adad"}'),
(61, 1, '2017-05-16 09:47:11', 'workorder', 0, 'Update workordertable', '{"tanggal":"2017-05-18","vendor_row_id":"2","contact_person":"aa","cp_phone":"0181231","cp_email":"mal@co.com","product_group":"asda","project_manager":"dad","remarks":"adad .................."}'),
(62, 1, '2017-05-16 09:47:26', 'workorder', 0, 'Delete workordertable', '""'),
(63, 1, '2017-05-16 10:19:37', 'workorder', 0, 'Insert workordertable', '{"id":"WO-02","tanggal":"2017-05-16","vendor_row_id":"2","contact_person":"Joni","cp_phone":"08122345567","cp_email":"joni@pp.co.id","product_group":"Group 1","project_manager":"Asep Sofyan","remarks":"site jao"}'),
(64, NULL, '2017-05-16 14:35:02', 'vendor', 3, 'Insert vendortable', '{"vendortype_id":"1","name":"Waskita Karya, PT.","contact_person":"Adi Wibowo","email":"info@waskita.co.id","phone_no":"(021) 543 2100","address":"Jl MT Haryono ","status_id":"1"}'),
(65, NULL, '2017-05-16 14:36:54', 'vendor', 4, 'Insert vendortable', '{"vendortype_id":"1","name":"Waskita Karya, PT.","contact_person":"Adi Wibowo","email":"info@waskita.co.id","phone_no":"021 543 2100","address":"Jl MT Haryono\\r\\nJakarta Selatan","status_id":"1"}'),
(66, NULL, '2017-05-16 14:49:56', 'vendor', 1, 'Insert vendortable', '{"vendortype_id":"1","name":"Waskita Karya, PT.","contact_person":"Adi Wibowo","email":"info@waskita.co.id","phone_no":"021 543 2100","address":"Jl MT Haryono\\r\\nJakarta Selatan\\r\\nDKI Jakarta","status_id":"1"}'),
(67, NULL, '2017-05-16 14:52:49', 'vendor', 1, 'Update vendortable', '{"vendortype_id":"1","name":"Waskita Karya, PT.","contact_person":"Adi Wibowo","email":"info@waskita.co.id","phone_no":"021 543 2100","address":"Jl MT Haryono\\r\\nJakarta Selatan\\r\\nDKI Jakarta\\r\\nIndonesia","status_id":"1"}'),
(68, 1, '2017-05-16 14:59:13', 'vendor', 1, 'Update vendortable', '{"vendortype_id":"1","name":"Waskita Karya, PT.","contact_person":"Adi Wibowo","email":"info@waskita.co.id","phone_no":"021 543 2100","address":"Jl MT Haryono\\r\\nJakarta Selatan\\r\\nDKI Jakarta","status_id":"1"}'),
(69, 1, '2017-05-16 15:00:05', 'vendor', 1, 'Update vendortable', '{"vendortype_id":"1","name":"Waskita Karya, PT.","contact_person":"Adi Wibowo","email":"info@waskita.co.id","phone_no":"021 543 2100","address":"Jl MT Haryono\\r\\nJakarta Selatan","status_id":"1"}'),
(70, 1, '2017-05-16 15:12:45', 'vendor', 2, 'Insert vendortable', '{"vendortype_id":"1","name":"Wijaya Karya, PT.","contact_person":"Adib Muchsin","email":"info@wika.co.id","phone_no":"021 543 211","address":"Jl MT Haryono\\r\\nJakarta Selatan","status_id":"1"}'),
(71, 1, '2017-05-17 13:10:53', 'area', 2, 'Update areatable', '{"txt":"Area 2...","status_id":"2"}'),
(72, 1, '2017-05-17 13:11:05', 'area', 2, 'Delete areatable', '""'),
(73, 1, '2017-05-17 13:11:23', 'area', 3, 'Insert areatable', '{"txt":"Area 2","status_id":"1"}'),
(74, 1, '2017-05-17 13:12:20', 'area', 3, 'Update areatable', '{"txt":"Area 2","status_id":"2"}'),
(75, 1, '2017-05-17 14:14:24', 'region', 2, 'Insert regiontable', '{"area_id":"3","txt":"Jabodetabek","status_id":"1"}'),
(76, 1, '2017-05-17 14:14:47', 'region', 2, 'Update regiontable', '{"area_id":"3","txt":"Jabodetabek","status_id":"2"}'),
(77, 1, '2017-05-17 14:15:00', 'region', 2, 'Update regiontable', '{"area_id":"3","txt":"Jabodetabek","status_id":"1"}'),
(78, 1, '2017-05-17 14:16:42', 'region', 2, 'Update regiontable', '{"area_id":"3","txt":"Jabodetabek","status_id":"2"}'),
(79, 1, '2017-05-17 14:26:34', 'region', 3, 'Insert regiontable', '{"area_id":"1","txt":"jawa barat","status_id":"1"}'),
(80, 1, '2017-05-17 14:26:47', 'region', 2, 'Update regiontable', '{"area_id":"3","txt":"Jabodetabek","status_id":"1"}'),
(81, 1, '2017-05-17 14:41:44', 'region', 3, 'Update regiontable', '{"area_id":"1","txt":"jawa barat","status_id":"2"}'),
(82, 1, '2017-05-17 14:43:42', 'region', 2, 'Update regiontable', '{"area_id":"3","txt":"Jabodetabek","status_id":"1"}'),
(83, 1, '2017-05-17 14:44:41', 'region', 3, 'Update regiontable', '{"area_id":"1","txt":"jawa barat","status_id":"1"}'),
(84, 1, '2017-05-17 14:48:24', 'region', 3, 'Delete regiontable', '""'),
(85, 1, '2017-05-17 14:55:07', 'region', 2, 'Delete regiontable', '""'),
(86, 1, '2017-05-17 14:55:23', 'region', 1, 'Update regiontable', '{"area_id":"1","txt":"Jawa Barat","status_id":"2"}'),
(87, 1, '2017-05-17 14:56:46', 'area', 3, 'Update areatable', '{"txt":"Area 2","status_id":"1"}'),
(88, 1, '2017-05-17 14:57:58', 'region', 1, 'Update regiontable', '{"area_id":"1","txt":"Jawa Barat","status_id":"1"}'),
(89, 1, '2017-05-17 14:58:08', 'region', 1, 'Update regiontable', '{"area_id":"1","txt":"Jawa Barat","status_id":"2"}'),
(90, 1, '2017-05-17 14:58:47', 'region', 4, 'Insert regiontable', '{"area_id":"1","txt":"Sumatera Utara","status_id":"2"}'),
(91, 1, '2017-05-17 15:01:35', 'region', 4, 'Delete regiontable', '""'),
(92, 1, '2017-05-17 15:05:30', 'region', 1, 'Update regiontable', '{"area_id":"1","txt":"Jawa Barat","status_id":"1"}'),
(93, 1, '2017-05-17 15:06:13', 'region', 5, 'Insert regiontable', '{"area_id":"1","txt":"Jabodetabek","status_id":"1"}'),
(94, 1, '2017-05-17 15:06:24', 'region', 5, 'Update regiontable', '{"area_id":"1","txt":"Jabodetabek","status_id":"2"}'),
(95, 1, '2017-05-17 15:47:37', 'sitetype', 1, 'Update sitetypetable', '{"txt":"GF - Green Field","status_id":"1"}'),
(96, 1, '2017-05-17 15:47:57', 'sitetype', 2, 'Update sitetypetable', '{"txt":"RT - Roof Top","status_id":"1"}'),
(97, 1, '2017-05-17 15:48:02', 'sitetype', 3, 'Delete sitetypetable', '""'),
(98, 1, '2017-05-17 15:49:22', 'product', 6, 'Delete producttable', '""'),
(99, 1, '2017-05-17 15:49:37', 'product', 5, 'Delete producttable', '""'),
(100, 1, '2017-05-17 15:49:43', 'product', 4, 'Delete producttable', '""'),
(101, 1, '2017-05-17 15:50:06', 'product', 1, 'Update producttable', '{"txt":"Macro \\/ SST","is_active":"1"}'),
(102, 1, '2017-05-17 15:50:20', 'product', 2, 'Update producttable', '{"txt":"Mini Macro","is_active":"1"}'),
(103, 1, '2017-05-17 15:50:35', 'product', 3, 'Update producttable', '{"txt":"MCP","is_active":"1"}'),
(104, 1, '2017-05-17 16:13:46', 'towerheight', 1, 'Update towerheighttable', '{"txt":"6 Meter","status_id":"1"}'),
(105, 1, '2017-05-17 16:13:58', 'towerheight', 2, 'Update towerheighttable', '{"txt":"9 Meter","status_id":"1"}'),
(106, 1, '2017-05-17 16:14:11', 'towerheight', 3, 'Update towerheighttable', '{"txt":"20 Meter","status_id":"1"}'),
(107, NULL, '2017-05-18 13:49:05', 'salesorder', 8, 'Update salesordertable', '{"salesorder_no":"SO15000009","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-11 00:00:00","batch":"3","po_tenant_no":"PO0490334","po_tenant_date":"2017-03-11 00:00:00","tenant_id":"1","status_id":"2"}'),
(108, 1, '2017-05-18 13:49:47', 'salesorder', 8, 'Update salesordertable', '{"salesorder_no":"SO15000009","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-11 00:00:00","batch":"3","po_tenant_no":"PO0490334","po_tenant_date":"2017-03-11 00:00:00","tenant_id":"1","status_id":"1"}'),
(109, 1, '2017-05-18 13:52:01', 'salesorder', 9, 'Insert salesordertable', '{"salesorder_no":"SO18000009","salesorder_date":"2017-05-18 00:00:00","drm_date":"2017-05-18 00:00:00","batch":"1","po_tenant_no":"TLKM-2017-05-123","po_tenant_date":"2017-05-18 00:00:00","tenant_id":"1","status_id":"1"}'),
(110, 1, '2017-05-18 13:59:44', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"SO15000010","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-04 00:00:00","batch":"1","po_tenant_no":"PO9490303","po_tenant_date":"2017-03-10 00:00:00","tenant_id":"1","status_id":"1"}'),
(111, 1, '2017-05-23 10:10:27', 'region', 5, 'Update regiontable', '{"area_id":"1","txt":"Jabodetabek","status_id":"1"}'),
(112, 1, '2017-05-23 10:18:50', 'product', 4, 'Insert producttable', '{"txt":"Collocation","is_active":"1"}'),
(113, 1, '2017-05-23 16:30:56', 'vendor', 3, 'Insert vendortable', '{"vendortype_id":"2","vendor_id":"A001","name":"Alcatel Indonesia, PT","email":"info@alcatel.co.id","phone_no":"021-765-1234","address":"Jl Sulatan Iskandarsah Jakarta Selatan","status_id":"1"}'),
(114, 1, '2017-05-23 16:31:49', 'vendor', 3, 'Update vendortable', '{"vendortype_id":"2","vendor_id":"A001","name":"Alcatel Indonesia, PT","email":"info@alcatel.co.id","phone_no":"021-765-1234","address":"Jl Sulatan Iskandarsah Jakarta Selatan DKI Jakarta Indonesia","status_id":"1"}'),
(115, 1, '2017-05-23 16:36:31', 'vendor', 3, 'Delete vendortable', '""'),
(116, 1, '2017-05-24 14:14:39', 'vendor', 3, 'Insert vendortable', '{"vendortype_id":"2","id":"A001","name":"Alcatel Indonesia, PT","email":"info@alcatel.co.id","phone_no":"021-876-2313","address":"Jl Let Jend MT Haryono","status_id":"1"}'),
(117, 1, '2017-05-24 14:20:04', 'vendor', 3, 'Update vendortable', '{"vendortype_id":"2","id":"A001","name":"Alcatel Indonesia, PT","email":"info@alcatel.co.id","phone_no":"021-876-2313","address":"Jl Let Jend MT Haryono","status_id":"2"}'),
(118, 1, '2017-05-24 14:20:25', 'vendor', 3, 'Delete vendortable', '""'),
(119, 1, '2017-05-24 16:51:50', 'vendorcontact', 2, 'Insert vendorcontacttable', '{"vendor_id":"W002","name":"Mirza","position":"Kepala Divisi","organization":"Sales","email":"mirza@wika.co.id","mobile_no":"0811-2222-3456","status_id":"1"}'),
(120, 1, '2017-05-24 16:52:19', 'vendorcontact', 2, 'Update vendorcontacttable', '{"vendor_id":"W002","name":"Mirza","position":"Kepala Divisi","organization":"General Affair","email":"mirza@wika.co.id","mobile_no":"0811-2222-3456","status_id":"1"}'),
(121, 1, '2017-05-24 16:53:12', 'vendorcontact', 3, 'Insert vendorcontacttable', '{"vendor_id":"W002","name":"Ridho","position":"Direktur","organization":"Umum","email":"ridho@wika.co.id","mobile_no":"0812-3333-4567","status_id":"1"}'),
(122, 1, '2017-05-24 16:53:20', 'vendorcontact', 3, 'Delete vendorcontacttable', '""'),
(123, 1, '2017-05-26 09:09:26', 'vendor', 3, 'Insert vendortable', '{"vendortype_id":"2","id":"A001","name":"Alcatel Indonesia, PT","email":"info@alcatel.co.id","phone_no":"012-687-2345","address":"Menara MTH Jl MT Haryono Jakarta Selatan DKI Jakarta","status_id":"1"}'),
(124, 1, '2017-05-26 09:11:21', 'vendorcontact', 3, 'Insert vendorcontacttable', '{"vendor_id":"A001","name":"Ridho","position":"Manager","organization":"LogistiC Departemen","email":"ridho@alcatel.co.id","mobile_no":"0812-3456-789","status_id":"1"}'),
(125, 1, '2017-05-26 09:11:38', 'vendorcontact', 3, 'Update vendorcontacttable', '{"vendor_id":"A001","name":"Ridho","position":"Manager","organization":"LogistiC Departemen","email":"ridho@alcatel.co.id","mobile_no":"0812-3456-789","status_id":"2"}'),
(126, 1, '2017-05-26 09:11:53', 'vendorcontact', 3, 'Update vendorcontacttable', '{"vendor_id":"A001","name":"Ridho","position":"Manager","organization":"LogistiC Departemen","email":"ridho@alcatel.co.id","mobile_no":"0812-3456-789","status_id":"1"}'),
(127, 1, '2017-05-26 09:13:24', 'vendorcontact', 4, 'Insert vendorcontacttable', '{"vendor_id":"A001","name":"Mirza","position":"Manajer","organization":"Departemen Logistik","email":"mirza@alcatel.co.id","mobile_no":"0182-888-999","status_id":"1"}'),
(128, 1, '2017-05-26 09:13:33', 'vendorcontact', 3, 'Delete vendorcontacttable', '""'),
(129, 1, '2017-05-29 09:33:17', 'salesorderline', 2, 'Insert salesorderline', '{"salesorder_no":"SO18000009","product_id":"1","site_id_tenant":"1234","site_name_tenant":"Desa Nganjuk","site_id_api":"ajsj","site_name_api":"API_Nganjuk","sitetype_id":"1","towerheight_id":"3","latitude":"313123","longitude":"-313","area_id":"1","region_id":"1","baks":"2017-07-22","rfi":"2017-09-22","baps":"2017-11-22","rent_period":"1","status_id":"1"}'),
(130, 1, '2017-05-29 09:37:15', 'salesorderline', 2, 'Update salesorderline', '{"salesorder_no":"SO18000009","product_id":"1","site_id_tenant":"1234","site_name_tenant":"Desa Nganjuk","site_id_api":"ajsj","site_name_api":"API_Nganjuk","sitetype_id":"1","towerheight_id":"3","latitude":"313123","longitude":"-313","area_id":"1","region_id":"1","baks":"2017-07-22","rfi":"2017-09-22","baps":"2017-11-22","rent_period":"1","status_id":"1"}'),
(131, 1, '2017-05-29 09:38:12', 'salesorderline', 2, 'Update salesorderline', '{"salesorder_no":"SO18000009","product_id":"1","site_id_tenant":"1234","site_name_tenant":"Desa Nganjuk","site_id_api":"ajsj","site_name_api":"API_Nganjuk","sitetype_id":"1","towerheight_id":"3","latitude":"313123","longitude":"-313","area_id":"1","region_id":"1","baks":"2017-07-22","rfi":"2017-09-22","baps":"2017-11-22","rent_period":"1","status_id":"1"}'),
(132, 1, '2017-05-29 09:41:05', 'salesorderline', 2, 'Update salesorderline', '{"salesorder_no":"SO18000009","product_id":"1","site_id_tenant":"1234","site_name_tenant":"Desa Nganjuk","site_id_api":"ajsj","site_name_api":"API_Nganjuk","sitetype_id":"1","towerheight_id":"3","latitude":"313123","longitude":"-313","area_id":"1","region_id":"1","baks":"2017-07-22","rfi":"2017-09-22","baps":"2017-11-22","remarks":"remarkssss","rent_period":"1","status_id":"1"}'),
(133, 1, '2017-05-29 09:41:26', 'salesorderline', 2, 'Delete salesorderline', '""'),
(134, 1, '2017-05-29 09:45:21', 'salesorder', 1, 'Delete salesordertable', '""'),
(135, 1, '2017-05-29 09:45:52', 'salesorder', 2, 'Delete salesordertable', '""'),
(136, 1, '2017-05-29 09:46:05', 'salesorder', 3, 'Delete salesordertable', '""'),
(137, 1, '2017-05-29 09:50:35', 'salesorderline', 3, 'Insert salesorderline', '{"salesorder_no":"SO15000009","product_id":"1","site_id_tenant":"TLKSITE7001","site_name_tenant":"Desa Pesisir","site_id_api":"mju-2001","site_name_api":"API_Desa Pesisir","sitetype_id":"1","towerheight_id":"3","latitude":"21414","longitude":"-23141","area_id":"1","region_id":"1","baks":"2017-08-10","rfi":"2017-09-11","baps":"2017-10-11","remarks":"test juga","rent_period":"1","status_id":"1"}'),
(138, 1, '2017-05-29 10:37:40', 'salesorderline', 4, 'Insert salesorderline', '{"salesorder_no":"SO15000009","product_id":"2","site_id_tenant":"tlk012345","site_name_tenant":"Tebet Timur","site_id_api":"mjuo212131","site_name_api":"API_","sitetype_id":"1","towerheight_id":"3","latitude":"2141242","longitude":"-12313213","area_id":"1","region_id":"1","baks":"2017-08-10","rfi":"2017-09-10","baps":"2017-10-10","remarks":"adadfas","rent_period":"1","status_id":"1"}'),
(139, 1, '2017-05-29 10:54:38', 'salesorderline', 5, 'Insert salesorderline', '{"salesorder_no":"SO15000009","product_id":"1","site_id_tenant":"1233467","site_name_tenant":"DESA","site_id_api":"MJU00001","site_name_api":"","sitetype_id":"1","towerheight_id":"1","latitude":"12313","longitude":"-43424","area_id":"1","region_id":"1","baks":"2017-07-10","rfi":"2017-08-10","baps":"2017-09-10","remarks":"DADA","rent_period":"1","status_id":"1"}'),
(140, 1, '2017-05-29 11:11:02', 'salesorderline', 5, 'Delete salesorderline', '""'),
(141, 1, '2017-05-29 11:11:23', 'salesorderline', 4, 'Update salesorderline', '{"salesorder_no":"SO15000009","product_id":"2","site_id_tenant":"tlk012345","site_name_tenant":"Tebet Timur","site_id_api":"mjuo212131","site_name_api":"API_","sitetype_id":"1","towerheight_id":"3","latitude":"2141242","longitude":"-12313213","area_id":"1","region_id":"1","baks":"2017-08-10","rfi":"2017-09-10","baps":"2017-10-10","remarks":"adadfas ...................................","rent_period":"1","status_id":"1"}'),
(142, 1, '2017-05-29 11:11:33', 'salesorderline', 4, 'Delete salesorderline', '""'),
(143, 1, '2017-05-29 11:15:16', 'salesorderline', 6, 'Insert salesorderline', '{"salesorder_no":"SO15000009","product_id":"1","site_id_tenant":"TLKM-0001","site_name_tenant":"TEBET BARAT","site_id_api":"MJU-0001-28","site_name_api":"API_TEBET BARAT","sitetype_id":"2","towerheight_id":"2","latitude":"2313","longitude":"-123131","area_id":"1","region_id":"5","baks":"2017-07-22","rfi":"2017-08-22","baps":"2017-09-22","remarks":"TEST 1 2 3","rent_period":"1","status_id":"1"}'),
(144, 1, '2017-05-29 15:41:35', 'salesorderline', 7, 'Insert salesorderline', '{"salesorder_no":"SO18000009","product_id":"2","site_id_tenant":"tlkm1234567","site_name_tenant":"UDIK","site_id_api":"MJU-2001","site_name_api":"API_UDIK","sitetype_id":"1","towerheight_id":"3","latitude":"1231","longitude":"-1313","area_id":"1","region_id":"5","baks":"2017-06-29","rfi":"2017-07-29","baps":"2017-08-29","remarks":"REMARKS","rent_period":"1","status_id":"1"}'),
(145, 1, '2017-05-29 15:41:55', 'salesorderline', 7, 'Update salesorderline', '{"salesorder_no":"SO18000009","product_id":"2","site_id_tenant":"tlkm1234567","site_name_tenant":"UDIK","site_id_api":"MJU-2001","site_name_api":"API_UDIK","sitetype_id":"1","towerheight_id":"3","latitude":"1231","longitude":"-1313","area_id":"1","region_id":"5","baks":"2017-06-29","rfi":"2017-07-29","baps":"2017-08-29","remarks":"REMARKS .................","rent_period":"1","status_id":"1"}'),
(146, 1, '2017-05-29 15:42:09', 'salesorderline', 7, 'Delete salesorderline', '""'),
(147, NULL, '2017-05-30 13:12:20', 'vendor', 4, 'Insert vendortable', '{"vendortype_id":"1","id":"S001","name":"Samsung","email":"info@samsung.co.id","phone_no":"012-999","address":"Jl Mth","status_id":"1"}'),
(148, 1, '2017-05-30 13:13:44', 'vendor', 4, 'Update vendortable', '{"vendortype_id":"1","id":"S001","name":"Samsung","email":"info@samsung.co.id","phone_no":"012-999","address":"Jl Mth .....","status_id":"1"}'),
(149, 1, '2017-05-30 13:14:34', 'vendorcontact', 5, 'Insert vendorcontacttable', '{"vendor_id":"S001","name":"Asep","position":"Manajer","organization":"Procuremen","email":"asep@samsung.com","mobile_no":"083131","status_id":"1"}'),
(150, 1, '2017-05-30 13:15:10', 'vendorcontact', 6, 'Insert vendorcontacttable', '{"vendor_id":"S001","name":"Bay","position":"Manjer","organization":"Project","email":"bay@samsung.com","mobile_no":"0831349","status_id":"1"}'),
(151, 1, '2017-05-30 13:15:26', 'vendorcontact', 6, 'Update vendorcontacttable', '{"vendor_id":"S001","name":"Bay","position":"Manjer","organization":"Project","email":"bay@samsung.co.id","mobile_no":"0831349","status_id":"1"}'),
(152, 1, '2017-05-30 13:15:35', 'vendorcontact', 6, 'Delete vendorcontacttable', '""'),
(153, 1, '2017-05-30 13:16:35', 'salesorder', 10, 'Insert salesordertable', '{"salesorder_no":"SO26000010","salesorder_date":"2017-05-26 00:00:00","drm_date":"1970-01-01 07:00:00","batch":"","po_tenant_no":"","po_tenant_date":"1970-01-01 07:00:00","tenant_id":"1","status_id":"1"}'),
(154, 1, '2017-05-30 13:19:59', 'salesorderline', 7, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"0001","site_name_tenant":"DESA UWUK","site_id_api":"MJU-0128","site_name_api":"API_DESA UWUK","sitetype_id":"1","towerheight_id":"3","latitude":"1313","longitude":"-1231","area_id":"1","region_id":"1","baks":"2017-06-12","rfi":"2017-06-12","baps":"2017-06-12","remarks":"remarks","rent_period":"1","status_id":"1"}'),
(155, 1, '2017-05-30 13:36:48', 'vendorcontact', 7, 'Insert vendorcontacttable', '{"vendor_id":"S001","name":"adib","position":"ceo","organization":"manajemen","email":"adib@localhost.com","mobile_no":"0229103","status_id":"1"}'),
(156, 1, '2017-06-05 15:44:27', 'site', 3, 'Insert sitetable', '{"product_id":1,"tenant_id":1,"site_id_api":"api01","site_name_api":"API_nganjuk","site_id_tenant":"tenant01","site_name_tenant":"t001","province":"jawa barat","area_id":1,"region_id":1,"longitude_ori":-1231,"latitude_ori":123,"address":"Jl layang","sitetype_id":1,"towerheight_id":1,"transmissiontype_id":1,"milestone_id":1,"inchstone_id":1,"status_id":1}'),
(157, 1, '2017-06-05 16:01:28', 'site', 4, 'Insert sitetable', '{"product_id":"4","tenant_id":"1","site_id_api":"dada","site_name_api":"dada","site_id_tenant":"dasdsa","site_name_tenant":"dada","province":"dad","area_id":"2","region_id":"2","longitude_ori":"13132","latitude_ori":"3131","address":"sdgsgs","sitetype_id":"1","towerheight_id":"2","transmissiontype_id":"2","milestone_id":"1","inchstone_id":"1","status_id":"1"}'),
(158, 1, '2017-06-05 16:02:55', 'site', 4, 'Update sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"dada","site_name_api":"dada hdhdhdhdfhdf","site_id_tenant":"dasdsa .................","site_name_tenant":"dadahdhdhdhd","province":"dad","area_id":"2","region_id":"2","longitude_ori":"13132","latitude_ori":"3131","address":"sdgsgs","sitetype_id":"1","towerheight_id":"2","transmissiontype_id":"2","milestone_id":"1","inchstone_id":"1","status_id":"1"}'),
(159, 1, '2017-06-05 16:03:04', 'site', 4, 'Delete sitetable', '""'),
(160, 1, '2017-06-05 16:10:15', 'site', 3, 'Delete sitetable', '""'),
(161, 1, '2017-06-08 08:29:11', 'workorder', 2, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-06 00:00:00","vendor_id":false,"vendorcontact_id":"1","product_group":"TBS","project_manager":null,"remark":"WO Test","status_id":"1"}'),
(162, 1, '2017-06-08 08:33:33', 'workorder', 2, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-06 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"TBS","project_manager":null,"remark":"WO Test","status_id":"1"}'),
(163, 1, '2017-06-08 08:35:37', 'workorder', 2, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-06 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"TBS","project_manager":"Asep","remark":"WO Test .......................","status_id":"1"}'),
(164, 1, '2017-06-08 08:37:11', 'workorder', 2, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-06 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"TBS","project_manager":"Asep Sofyan","remark":"WO Test 123","status_id":"1"}'),
(165, 1, '2017-06-08 08:37:39', 'workorder', 2, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-06 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"TBS","project_manager":"Asep Sofyan","remark":"WO Test 123  ...............","status_id":"1"}'),
(166, 1, '2017-06-08 08:38:27', 'workorder', 2, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-06 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"TBS","project_manager":"Asep Sofyan","remark":"WO Test 1111111111111111111111111111111","status_id":"1"}'),
(167, 1, '2017-06-08 08:39:00', 'workorder', 2, 'Delete workordertable', '""'),
(168, 1, '2017-06-08 08:39:50', 'workorder', 3, 'Insert workordertable', '{"workorder_no":"WO-2","workorder_date":"2017-06-08 00:00:00","vendor_id":"3","vendorcontact_id":"4","product_group":"HTCM","project_manager":"Asep S","remark":"test.................","status_id":"1"}'),
(169, 1, '2017-06-08 08:56:12', 'workorder', 3, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-08 00:00:00","vendor_id":"3","vendorcontact_id":"4","product_group":"HTCM","project_manager":"Asep S","remark":"test.................","status_id":"1"}'),
(170, 1, '2017-06-08 16:18:05', 'workorderline', 0, 'Update workorderline', '{"workorder_no":false,"site_id":false,"milestone_id":false,"start_date":null,"end_date":false,"status_id":false}'),
(171, 1, '2017-06-08 16:21:33', 'workorderline', 0, 'Update workorderline', '{"workorder_no":false,"site_id":false,"milestone_id":false,"start_date":null,"end_date":false,"status_id":false}'),
(172, NULL, '2017-06-09 07:44:27', 'workorder', 3, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-08 00:00:00","vendor_id":"3","vendorcontact_id":"4","product_group":"HTCM","project_manager":"Asep S","remark":"test","status_id":"1"}'),
(173, 1, '2017-06-09 07:46:08', 'workorder', 3, 'Update workordertable', '{"workorder_no":"WO-002","workorder_date":"2017-06-08 00:00:00","vendor_id":"3","vendorcontact_id":"4","product_group":"HTCM MMM","project_manager":"Asep S","remark":"test","status_id":"1"}'),
(174, 1, '2017-06-09 07:48:23', 'workorder', 1, 'Update workordertable', '{"workorder_no":"WO-001","workorder_date":"2017-06-06 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"TBS","project_manager":"Asep","remark":"WO Test 123","status_id":"1"}'),
(175, 1, '2017-06-09 08:09:09', 'workorderline', 3, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"1","milestone_id":"3","start_date":"2017-06-21","end_date":"2017-0630","status_id":"1"}'),
(176, 1, '2017-06-09 08:11:12', 'workorderline', 3, 'Delete workorderline', '""'),
(177, 1, '2017-06-09 08:11:37', 'workorderline', 4, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"1","milestone_id":"3","start_date":"2017-06-21","end_date":"2017-06-30","status_id":"1"}'),
(178, 1, '2017-06-09 08:12:02', 'workorderline', 5, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"1","milestone_id":"3","start_date":"2017-06-22","end_date":"2017-06-30","status_id":"1"}'),
(179, 1, '2017-06-09 08:41:13', 'workorder', 4, 'Insert workordertable', '{"workorder_no":"123","workorder_date":"2017-06-11 00:00:00","vendor_id":"3","vendorcontact_id":"5","product_group":"aba","project_manager":"asep","remark":"re","status_id":"1"}'),
(180, 1, '2017-06-09 08:41:59', 'workorderline', 6, 'Insert workorderline', '{"workorder_no":"123","site_id":"2","milestone_id":"1","start_date":"2017-06-21","end_date":"2017-06-28","status_id":"1"}'),
(181, 1, '2017-06-09 08:43:30', 'workorderline', 7, 'Insert workorderline', '{"workorder_no":"123","site_id":"2","milestone_id":"2","start_date":"2017-06-29","end_date":"2017-07-04","status_id":"1"}'),
(182, 1, '2017-06-09 15:58:54', 'workorderline', 8, 'Insert workorderline', '{"workorder_no":"123","site_id":"2","milestone_id":"3","start_date":"07\\/05\\/2017","end_date":"07\\/08\\/2017","status_id":"1"}'),
(183, 1, '2017-06-13 08:28:02', 'region', 3, 'Insert regiontable', '{"area_id":"2","txt":"Medan","status_id":"1"}'),
(184, 1, '2017-06-13 08:28:17', 'region', 4, 'Insert regiontable', '{"area_id":"2","txt":"Aceh","status_id":"1"}'),
(185, 1, '2017-06-14 09:42:13', 'tenant', 2, 'Insert tenanttable', '{"name":"Indosat","email":"info@indosat.co.id","phone_no":"021 5465 76888","address":"Jl Medan Merdeka Jakarta","status_id":"1"}'),
(186, 1, '2017-06-15 09:26:17', 'salesorderline', 8, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"","site_name_tenant":"Parung Panjang","site_id_api":"API-0001","site_name_api":"API_Parung Panjang","sitetype_id":"","towerheight_id":"3","latitude":"-1317371.098998","longitude":"76767.898989","area_id":"1","region_id":"1","baks":"06\\/16\\/2017","rfi":"06\\/18\\/2017","baps":"06\\/24\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(187, 1, '2017-06-15 09:33:28', 'salesorderline', 9, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"2","site_id_tenant":false,"site_name_tenant":"Parung Bengkok","site_id_api":"API-002","site_name_api":"API_Parung Bengkok","sitetype_id":"","towerheight_id":"3","latitude":"-121371.0999","longitude":"292888.121","area_id":"1","region_id":"1","baks":"06\\/20\\/2017","rfi":"06\\/18\\/2017","baps":"06\\/28\\/2017","remarks":"qwdwqdqw","rent_period":"2","status_id":"1"}'),
(188, 1, '2017-06-15 09:39:45', 'salesorderline', 10, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":false,"site_name_tenant":"Batu Tulis","site_id_api":"API-003","site_name_api":"API_Batu Tulis","sitetype_id":"","towerheight_id":"3","latitude":"-121898989.009","longitude":"12367676.099898","area_id":"1","region_id":"1","baks":"06\\/16\\/2017","rfi":"06\\/18\\/2017","baps":"06\\/22\\/2017","remarks":"dadada","rent_period":"2","status_id":"1"}'),
(189, 1, '2017-06-15 14:15:12', 'salesorderline', 11, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"2","site_id_tenant":false,"site_name_tenant":"Parung Bengkok","site_id_api":"API-0001","site_name_api":"API_Parung Bengkok","sitetype_id":"","towerheight_id":"3","latitude":"-13891.0899","longitude":"13131.8989","area_id":"1","region_id":"1","baks":"06\\/16\\/2017","rfi":"06\\/17\\/2017","baps":"06\\/19\\/2017","remarks":"rem...","rent_period":"1","status_id":"1"}'),
(190, 1, '2017-06-15 14:20:16', 'salesorderline', 12, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":false,"site_name_tenant":"DESA","site_id_api":"API-00001","site_name_api":"API_DESA","sitetype_id":"","towerheight_id":"3","latitude":"-13.989","longitude":"12.8989","area_id":"1","region_id":"1","baks":"06\\/16\\/2017","rfi":"06\\/18\\/2017","baps":"06\\/23\\/2017","remarks":"RE,,,","rent_period":"2","status_id":"1"}'),
(191, 1, '2017-06-15 14:43:16', 'salesorderline', 0, 'Update salesorderline', '{"salesorder_no":false,"product_id":false,"site_id_tenant":false,"site_name_tenant":false,"site_id_api":false,"site_name_api":false,"sitetype_id":false,"towerheight_id":false,"latitude":false,"longitude":false,"area_id":false,"region_id":false,"baks":false,"rfi":false,"baps":false,"remarks":false,"rent_period":false,"status_id":false}'),
(192, 1, '2017-06-15 16:30:57', 'salesorderline', 13, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"ten01","site_name_tenant":"Desa Ujung","site_id_api":"api01","site_name_api":"API_Desa Ujung","sitetype_id":false,"towerheight_id":"1","latitude":"-21.089","longitude":"121.999","area_id":"1","region_id":"1","baks":"06\\/16\\/2017","rfi":"06\\/18\\/2017","baps":"06\\/21\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(193, 1, '2017-06-15 16:33:55', 'salesorderline', 14, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"t01","site_name_tenant":"desa ujung","site_id_api":"a01","site_name_api":"API_desa ujung","sitetype_id":"2","towerheight_id":"2","latitude":"-31","longitude":"31","area_id":"2","region_id":"4","baks":"06\\/16\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(194, 1, '2017-06-15 16:36:12', 'salesorderline', 7, 'Delete salesorderline', '""'),
(195, 1, '2017-06-16 08:20:33', 'salesorderline', 15, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":false,"site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/18\\/2017","rfi":"06\\/20\\/2017","baps":"06\\/22\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(196, 1, '2017-06-16 08:24:00', 'salesorderline', 16, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":false,"site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/22\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(197, 1, '2017-06-16 08:26:36', 'salesorderline', 17, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":false,"site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/18\\/2017","baps":"06\\/20\\/2017","remarks":"rem","rent_period":"2","status_id":"1"}'),
(198, 1, '2017-06-16 08:28:53', 'salesorderline', 18, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":false,"site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/18\\/2017","baps":"06\\/21\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(199, 1, '2017-06-16 08:30:25', 'salesorderline', 19, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":1231,"site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/22\\/2017","remarks":"re","rent_period":"1","status_id":"1"}'),
(200, 1, '2017-06-16 08:35:45', 'salesorderline', 20, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":false,"site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/18\\/2017","rfi":"06\\/20\\/2017","baps":"06\\/22\\/2017","remarks":"rem","rent_period":"2","status_id":"1"}'),
(201, 1, '2017-06-16 08:38:14', 'salesorderline', 21, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/22\\/2017","remarks":"rem","rent_period":"2","status_id":"1"}'),
(202, 1, '2017-06-16 08:41:47', 'salesorderline', 22, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(203, 1, '2017-06-16 08:43:19', 'salesorderline', 23, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/22\\/2017","remarks":"re","rent_period":"1","status_id":"1"}'),
(204, 1, '2017-06-16 08:45:23', 'salesorderline', 24, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/22\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(205, 1, '2017-06-16 08:47:20', 'salesorderline', 25, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"","site_name_tenant":"desa udik","site_id_api":"api-0001","site_name_api":"API_desa udik","sitetype_id":"1","towerheight_id":"1","latitude":"21","longitude":"231","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/17\\/2017","baps":"06\\/23\\/2017","remarks":"da","rent_period":"1","status_id":"1"}'),
(206, 1, '2017-06-16 08:50:09', 'salesorderline', 26, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/23\\/2017","remarks":"rem","rent_period":"2","status_id":"1"}'),
(207, 1, '2017-06-16 08:52:36', 'salesorderline', 27, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"","towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"1asdadadasddddddddddddddddddddddddddddd","rent_period":"1","status_id":"1"}'),
(208, 1, '2017-06-16 08:54:03', 'salesorderline', 28, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"tenant-abc-0001","site_name_tenant":"desa udik banget","site_id_api":"api--00001","site_name_api":"API_desa udik banget","sitetype_id":"1","towerheight_id":"2","latitude":"12321","longitude":"1231","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"asdadaaaaaaaaaaaaaaaaa","rent_period":"1","status_id":"1"}'),
(209, 1, '2017-06-16 09:16:38', 'salesorderline', 29, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":false,"towerheight_id":"","latitude":"321","longitude":"123","area_id":"","region_id":"","baks":"06\\/18\\/2017","rfi":"06\\/21\\/2017","baps":"06\\/24\\/2017","remarks":"rrrrrrrrrrrrrr","rent_period":"1","status_id":"1"}'),
(210, 1, '2017-06-16 09:19:01', 'salesorderline', 30, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"tenabc-oooo1","site_name_tenant":"desa desu","site_id_api":"apiabc-00001","site_name_api":"API_desa desu","sitetype_id":false,"towerheight_id":"1","latitude":"1","longitude":"1","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/18\\/2017","baps":"06\\/21\\/2017","remarks":"wwwwwwwwwwwwwwwww","rent_period":"1","status_id":"1"}'),
(211, 1, '2017-06-16 09:19:58', 'salesorderline', 28, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"tenant-abc-0001................","site_name_tenant":"desa udik banget","site_id_api":"api--00001","site_name_api":"API_desa udik banget","sitetype_id":false,"towerheight_id":"2","latitude":"12321","longitude":"1231","area_id":"1","region_id":"","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"asdadaaaaaaaaaaaaaaaaa ----------------------------","rent_period":"1","status_id":"1"}'),
(212, 1, '2017-06-16 09:33:43', 'salesorderline', 31, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"ten00000000001","site_name_tenant":"Tebet Timur Raya","site_id_api":"api0000000000000001","site_name_api":"API_Tebet Timur Raya","sitetype_id":"2","towerheight_id":"2","latitude":"-121.988","longitude":"2231.999978","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/22\\/2017","remarks":"rem aks","rent_period":"1","status_id":"1"}'),
(213, 1, '2017-06-16 09:55:45', 'salesorderline', 32, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"1","towerheight_id":"1","latitude":"321","longitude":"123","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/20\\/2017","baps":"06\\/23\\/2017","remarks":"xxxxxxxxxxxxxxxx","rent_period":"1","status_id":"1"}'),
(214, 1, '2017-06-16 10:49:13', 'salesorderline', 33, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"tenant000001","site_name_tenant":"Tebet timur","site_id_api":"api000001","site_name_api":"API_Tebet timur","sitetype_id":"2","towerheight_id":"3","latitude":"-1321.12313","longitude":"131.32131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/22\\/2017","remarks":"abcasadadasdsdsa","rent_period":"1","status_id":"1"}'),
(215, 1, '2017-06-16 10:51:03', 'salesorderline', 34, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"1","towerheight_id":"1","latitude":"321","longitude":"123","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/20\\/2017","baps":"06\\/23\\/2017","remarks":"aaaaaaaaa","rent_period":"1","status_id":"1"}');
INSERT INTO `systemlogtable` (`row_id`, `created_by`, `created_date`, `module`, `key_id`, `action`, `data`) VALUES
(216, 1, '2017-06-16 10:53:07', 'salesorderline', 35, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"3","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(217, 1, '2017-06-16 11:04:18', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"3","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(218, 1, '2017-06-16 11:05:19', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"3","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(219, 1, '2017-06-16 11:06:09', 'salesorderline', 34, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"1","towerheight_id":"1","latitude":"321","longitude":"123","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/20\\/2017","baps":"06\\/23\\/2017","remarks":"aaaaaaaaa","rent_period":"1","status_id":"1"}'),
(220, 1, '2017-06-16 11:06:46', 'salesorderline', 34, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"1","towerheight_id":"1","latitude":"321","longitude":"123","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/20\\/2017","baps":"06\\/23\\/2017","remarks":"aaaaaaaaa","rent_period":"1","status_id":"1"}'),
(221, 1, '2017-06-16 11:07:10', 'salesorderline', 34, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"1","towerheight_id":"1","latitude":"321","longitude":"123","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/20\\/2017","baps":"06\\/23\\/2017","remarks":"aaaaaaaaa","rent_period":"1","status_id":"1"}'),
(222, 1, '2017-06-16 11:09:31', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222 ccccc","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"3","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(223, 1, '2017-06-16 11:10:07', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"3","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(224, 1, '2017-06-16 11:12:36', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"3","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(225, 1, '2017-06-16 11:13:03', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"3","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(226, 1, '2017-06-16 11:16:44', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"1","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(227, 1, '2017-06-16 13:33:20', 'salesorderline', 36, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"1","towerheight_id":"1","latitude":"321","longitude":"123","area_id":"1","region_id":"1","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/23\\/2017","remarks":"","rent_period":"","status_id":""}'),
(228, 1, '2017-06-16 13:54:13', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"1","towerheight_id":"3","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(229, 1, '2017-06-16 13:55:10', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"2","latitude":"-21313","longitude":"31131","area_id":"2","region_id":"4","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(230, 1, '2017-06-16 13:55:50', 'salesorderline', 35, 'Update salesorderline', '{"salesorder_no":"SO26000010","product_id":"1","site_id_tenant":"t222222","site_name_tenant":"tebet raya","site_id_api":"api22222","site_name_api":"API_tebet raya","sitetype_id":"2","towerheight_id":"2","latitude":"-21313","longitude":"31131","area_id":"1","region_id":"2","baks":"06\\/17\\/2017","rfi":"06\\/19\\/2017","baps":"06\\/21\\/2017","remarks":"rermmmmmmmmmmmmmmmmm","rent_period":"1","status_id":"1"}'),
(231, 1, '2017-06-16 13:56:11', 'salesorderline', 35, 'Delete salesorderline', '""'),
(232, NULL, '2017-06-19 13:54:12', 'salesorderline', 37, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"1","towerheight_id":"1","latitude":"321","longitude":"123","area_id":"1","region_id":"1","baks":"06\\/20\\/2017","rfi":"06\\/22\\/2017","baps":"06\\/23\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(233, NULL, '2017-06-19 13:54:15', 'salesorderline', 38, 'Insert salesorderline', '{"salesorder_no":"SO26000010","product_id":"4","site_id_tenant":"1231","site_name_tenant":"api_bekasi","site_id_api":"210321","site_name_api":"bekasi","sitetype_id":"1","towerheight_id":"1","latitude":"321","longitude":"123","area_id":"1","region_id":"1","baks":"06\\/20\\/2017","rfi":"06\\/22\\/2017","baps":"06\\/23\\/2017","remarks":"rem","rent_period":"1","status_id":"1"}'),
(234, 1, '2017-06-19 14:28:03', 'salesorder', 4, 'Update salesordertable', '{"salesorder_no":"SO15000011","salesorder_date":"2017-03-15 00:00:00","drm_date":"2017-03-15 00:00:00","batch":"2","po_tenant_no":"PO023093","po_tenant_date":"2017-03-15 00:00:00","tenant_id":"2","status_id":"1"}'),
(235, 1, '2017-06-19 14:28:32', 'salesorder', 5, 'Delete salesordertable', '""'),
(236, 1, '2017-06-19 14:34:11', 'salesorderline', 39, 'Insert salesorderline', '{"salesorder_no":"SO15000006","product_id":"1","site_id_tenant":"TEN001","site_name_tenant":"TEBET TIMUR","site_id_api":"API001","site_name_api":"API_TEBET TIMUR","sitetype_id":"1","towerheight_id":"3","latitude":"-131.667","longitude":"234.7778","area_id":"1","region_id":"1","baks":"06\\/20\\/2017","rfi":"06\\/21\\/2017","baps":"06\\/23\\/2017","remarks":"REM","rent_period":"1","status_id":"1"}'),
(237, 1, '2017-06-19 16:25:10', 'site', 3, 'Insert sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"API001","site_name_api":"API_TEBET TIMUR","site_id_tenant":"TEN001","site_name_tenant":"TEBET TIMUR","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"234.7778","latitude_ori":"-131.667","address":"Jl Tebet Timur","sitetype_id":"1","towerheight_id":"3","transmissiontype_id":"2","status_id":"1"}'),
(238, 1, '2017-06-19 16:25:55', 'site', 4, 'Insert sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"MJU-MCP-0001","site_name_api":"API_Desa_Udik","site_id_tenant":"884938","site_name_tenant":"Desa Udik","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"-55.10909","latitude_ori":"15.017266","address":"Jl Tebet Timur Raya","sitetype_id":"1","towerheight_id":"1","transmissiontype_id":"2","status_id":"1"}'),
(239, 1, '2017-06-19 16:26:43', 'site', 5, 'Insert sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"MJU-MCP-0001","site_name_api":"API_Desa_Udik","site_id_tenant":"884938","site_name_tenant":"Desa Udik","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"-55.10909","latitude_ori":"15.017266","address":"Jl Tebet Timur Raya Jaya","sitetype_id":"1","towerheight_id":"1","transmissiontype_id":"2","status_id":"1"}'),
(240, 1, '2017-06-19 16:26:54', 'site', 5, 'Delete sitetable', '""'),
(241, 1, '2017-06-19 16:27:03', 'site', 4, 'Delete sitetable', '""'),
(242, 1, '2017-06-19 16:41:01', 'site', 3, 'Update sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"API001","site_name_api":"API_TEBET TIMUR","site_id_tenant":"TEN001","site_name_tenant":"TEBET TIMUR","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"234.7778","latitude_ori":"-131.667","address":"Jl Tebet Timur Raya","sitetype_id":"1","towerheight_id":"3","transmissiontype_id":"2","status_id":"1"}'),
(243, 1, '2017-06-19 16:41:34', 'site', 3, 'Update sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"API001","site_name_api":"API_TEBET TIMUR","site_id_tenant":"TEN001","site_name_tenant":"TEBET TIMUR","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"234.7778","latitude_ori":"-131.667","address":"Jl Tebet Timur Raya Jakarta Selatan","sitetype_id":"1","towerheight_id":"3","transmissiontype_id":"2","status_id":"1"}'),
(244, 1, '2017-06-19 16:47:37', 'site', 3, 'Update sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"API001","site_name_api":"API_TEBET TIMUR","site_id_tenant":"TEN001","site_name_tenant":"TEBET TIMUR","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"234.7778","latitude_ori":"-131.667","address":"Jl Tebet Timur Raya Jakarta Selatan","sitetype_id":"1","towerheight_id":"3","transmissiontype_id":"2","remark":"remark...................","status_id":"1"}'),
(245, 1, '2017-06-19 16:58:17', 'site', 3, 'Delete sitetable', '""'),
(246, 1, '2017-06-19 16:58:59', 'site', 6, 'Insert sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"API001","site_name_api":"API_TEBET TIMUR","site_id_tenant":"TEN001","site_name_tenant":"TEBET TIMUR","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"234.7778","latitude_ori":"-131.667","address":"Jl Tebet Timur Raya","sitetype_id":"1","towerheight_id":"3","transmissiontype_id":"2","remark":"remarking","status_id":"1"}'),
(247, 1, '2017-06-20 08:06:46', 'site', 6, 'Update sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"API001","site_name_api":"API_TEBET TIMUR","site_id_tenant":"TEN001","site_name_tenant":"TEBET TIMUR","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"234.7778","latitude_ori":"-131.667","address":"Jl Tebet Timur Raya","sitetype_id":"1","towerheight_id":"3","transmissiontype_id":"2","remark":"remarking ...............","status_id":"1"}'),
(248, 1, '2017-06-20 08:07:12', 'site', 6, 'Delete sitetable', '""'),
(249, 1, '2017-06-20 08:08:23', 'site', 7, 'Insert sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"API001","site_name_api":"API_TEBET TIMUR","site_id_tenant":"TEN001","site_name_tenant":"TEBET TIMUR","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"234.7778","latitude_ori":"-131.667","address":"Jl Tebet Timur No 255 Jakarta Selatan","sitetype_id":"1","towerheight_id":"3","transmissiontype_id":"1","remark":"Dalam proses finishing","status_id":"1"}'),
(250, 1, '2017-06-20 11:47:13', 'projectactivity', 5, 'Update projectactivitytable', '{"txt":"RFI","status_id":"1"}'),
(251, 1, '2017-06-20 11:47:27', 'projectactivity', 5, 'Update projectactivitytable', '{"txt":"RFI","status_id":"1"}'),
(252, 1, '2017-06-20 11:50:31', 'projectactivity', 5, 'Update projectactivitytable', '{"code":"050000","txt":"RFI","remark":"Ready for Installation ....","status_id":"1"}'),
(253, 1, '2017-06-20 11:50:46', 'projectactivity', 5, 'Update projectactivitytable', '{"code":"050000","txt":"RFI","remark":"Ready for Installation","status_id":"1"}'),
(254, 1, '2017-06-20 11:51:14', 'projectactivity', 6, 'Insert projectactivitytable', '{"code":"060000","txt":"Sign Off","remark":"End Of Project","status_id":"1"}'),
(255, 1, '2017-06-20 11:51:22', 'projectactivity', 6, 'Delete projectactivitytable', '""'),
(256, 1, '2017-06-20 13:43:58', 'projectactivity', 7, 'Insert projectactivitytable', '{"code":"020100","txt":"SOT","remark":"Soil Test ","status_id":"1"}'),
(257, 1, '2017-06-20 15:03:38', 'projectactivity', 1, 'Update projectactivitytable', '{"isscope":"1","code":"010000","txt":"SIS","remark":"Site Surveys...","status_id":"1"}'),
(258, 1, '2017-06-20 15:03:51', 'projectactivity', 1, 'Update projectactivitytable', '{"isscope":"1","code":"010000","txt":"SIS","remark":"Site Surveys","status_id":"1"}'),
(259, 1, '2017-06-20 15:06:16', 'projectactivity', 8, 'Insert projectactivitytable', '{"isscope":"0","code":"020200","txt":"IW","remark":"Izin Warga","status_id":"1"}'),
(260, 1, '2017-06-21 08:34:29', 'projectactivity', 9, 'Insert projectactivitytable', '{"isscope":"1","code":"020300","txt":"BAN BAK","remark":"Berita Acea Negoisasi \\/ Berita Acara Kesepakatan","status_id":"1"}'),
(261, 1, '2017-07-04 09:57:47', 'workorder', 4, 'Delete workordertable', '""'),
(262, 1, '2017-07-04 10:10:59', 'workorderline', 9, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"1","start_date":"07\\/05\\/2017","end_date":"07\\/05\\/2017","status_id":"1"}'),
(263, 1, '2017-07-04 10:26:08', 'workorderline', 10, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"2","start_date":"07\\/12\\/2017","end_date":"07\\/19\\/2017","status_id":"1"}'),
(264, 1, '2017-07-04 10:44:53', 'workorderline', 11, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"3","start_date":"1970-01-01","end_date":"1970-01-01","status_id":"1"}'),
(265, 1, '2017-07-04 10:48:22', 'workorderline', 12, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"4","start_date":"1970-01-01","end_date":"1970-01-01","status_id":"1"}'),
(266, 1, '2017-07-04 10:50:18', 'workorderline', 13, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"5","start_date":"07\\/26\\/2017","end_date":"07\\/30\\/2017","status_id":"1"}'),
(267, 1, '2017-07-04 10:51:40', 'workorderline', 14, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"7","start_date":"1970-01-01","end_date":"1970-01-01","status_id":"1"}'),
(268, 1, '2017-07-04 11:05:54', 'workorderline', 15, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"9","start_date":"07\\/19\\/2017","end_date":"07\\/18\\/2017","status_id":"1"}'),
(269, 1, '2017-07-04 11:15:44', 'workorderline', 1, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-07","status_id":"1"}'),
(270, 1, '2017-07-04 11:18:18', 'workorderline', 2, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"2","start_date":"2017-07-13","end_date":"2017-07-05","status_id":"1"}'),
(271, 1, '2017-07-04 11:30:09', 'workorderline', 3, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"3","start_date":"2017-07-13","end_date":"2017-07-19","status_id":"1"}'),
(272, 1, '2017-07-04 11:30:24', 'workorderline', 4, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"3","start_date":"2017-07-13","end_date":"2017-07-19","status_id":"1"}'),
(273, 1, '2017-07-04 13:16:05', 'workorderline', 4, 'Delete workorderline', '""'),
(274, 1, '2017-07-04 14:09:47', 'workorderline', 3, 'Delete workorderline', '""'),
(275, 1, '2017-07-04 14:17:14', 'workorderline', 5, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"3","start_date":"2017-07-05","end_date":"2017-07-19","status_id":"1"}'),
(276, 1, '2017-07-04 14:17:33', 'workorderline', 5, 'Delete workorderline', '""'),
(277, 1, '2017-07-04 15:08:43', 'workorderline', 1, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-05","status_id":"1"}'),
(278, 1, '2017-07-04 16:48:32', 'workorderline', 6, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"3","start_date":"2017-07-19","end_date":"2017-07-20","status_id":"1"}'),
(279, 1, '2017-07-04 16:52:12', 'workorderline', 6, 'Delete workorderline', '""'),
(280, 1, '2017-07-04 16:52:33', 'workorderline', 7, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"3","start_date":"2017-07-19","end_date":"2017-07-24","status_id":"1"}'),
(281, 1, '2017-07-04 16:53:48', 'workorderline', 8, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"4","start_date":"2017-07-27","end_date":"2017-07-28","status_id":"1"}'),
(282, 1, '2017-07-04 16:54:45', 'workorderline', 8, 'Delete workorderline', '""'),
(283, 1, '2017-07-04 16:54:52', 'workorderline', 7, 'Delete workorderline', '""'),
(284, 1, '2017-07-04 16:54:57', 'workorderline', 2, 'Delete workorderline', '""'),
(285, 1, '2017-07-04 16:55:04', 'workorderline', 1, 'Delete workorderline', '""'),
(286, 1, '2017-07-04 16:55:21', 'workorderline', 9, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-06","status_id":"1"}'),
(287, 1, '2017-07-04 17:19:44', 'workorderline', 10, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"2","start_date":"2017-07-05","end_date":"2017-07-06","status_id":"1"}'),
(288, 1, '2017-07-04 17:19:59', 'workorderline', 10, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"2","start_date":"2017-07-05","end_date":"2017-07-05","status_id":"1"}'),
(289, 1, '2017-07-04 17:20:17', 'workorderline', 10, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"2","start_date":"2017-07-03","end_date":"2017-07-05","status_id":"1"}'),
(290, 1, '2017-07-04 17:22:11', 'workorderline', 11, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"3","start_date":"2017-07-05","end_date":"2017-07-10","status_id":"1"}'),
(291, 1, '2017-07-04 17:22:29', 'workorderline', 12, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"4","start_date":"2017-07-07","end_date":"2017-07-13","status_id":"1"}'),
(292, 1, '2017-07-04 17:22:47', 'workorderline', 13, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"5","start_date":"2017-07-12","end_date":"2017-07-19","status_id":"1"}'),
(293, 1, '2017-07-04 17:23:05', 'workorderline', 14, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"7","start_date":"2017-07-19","end_date":"2017-07-22","status_id":"1"}'),
(294, 1, '2017-07-04 17:23:36', 'workorderline', 15, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"9","start_date":"2017-07-13","end_date":"2017-07-14","status_id":"1"}'),
(295, 1, '2017-07-04 17:24:07', 'workorderline', 9, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"1","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-05","status_id":"1"}'),
(296, 1, '2017-07-04 17:25:01', 'workorderline', 9, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-05","status_id":"1"}'),
(297, 1, '2017-07-04 17:25:24', 'workorderline', 9, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"1","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-05","status_id":"1"}'),
(298, 1, '2017-07-04 17:25:50', 'workorderline', 9, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-05","status_id":"1"}'),
(299, 1, '2017-07-04 17:26:02', 'workorderline', 15, 'Delete workorderline', '""'),
(300, 1, '2017-07-04 17:26:15', 'workorderline', 14, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"9","start_date":"2017-07-19","end_date":"2017-07-19","status_id":"1"}'),
(301, 1, '2017-07-04 17:26:33', 'workorderline', 14, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"7","start_date":"2017-07-19","end_date":"2017-07-19","status_id":"1"}'),
(302, 1, '2017-07-04 17:35:54', 'workorderline', 9, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"1","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-05","status_id":"1"}'),
(303, 1, '2017-07-05 10:34:42', 'workorderline', 9, 'Update workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"1","start_date":"2017-07-05","end_date":"2017-07-05","status_id":"1"}'),
(304, 1, '2017-07-05 16:32:26', 'workorderline', 15, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"9","start_date":"2017-07-26","end_date":"2017-07-28","status_id":"1"}'),
(305, 1, '2017-07-05 16:32:46', 'workorderline', 15, 'Delete workorderline', '""'),
(306, 1, '2017-07-06 10:01:44', 'workorderline', 15, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"9","start_date":"2017-07-07","end_date":"2017-07-11","status_id":"1"}'),
(307, 1, '2017-07-06 10:02:28', 'workorderline', 15, 'Delete workorderline', '""'),
(308, 1, '2017-07-06 10:26:52', 'projectactivity', 5, 'Update projectactivitytable', '{"isscope":"0","code":"050000","txt":"RFI - Ready for Installation","remark":"","status_id":"1"}'),
(309, 1, '2017-07-06 10:27:06', 'projectactivity', 4, 'Update projectactivitytable', '{"isscope":"0","code":"040000","txt":"RFC - Ready for Construction","remark":"","status_id":"1"}'),
(310, 1, '2017-07-06 10:27:20', 'projectactivity', 3, 'Update projectactivitytable', '{"isscope":"0","code":"030000","txt":"CME - Civil & Mechanical Engineering","remark":"","status_id":"1"}'),
(311, 1, '2017-07-06 10:27:39', 'projectactivity', 9, 'Update projectactivitytable', '{"isscope":"0","code":"020300","txt":"BAN \\/ BAK - Berita Acea Negoisasi \\/ Berita Acara Kesepakatan","remark":"","status_id":"1"}'),
(312, 1, '2017-07-06 10:28:15', 'projectactivity', 8, 'Update projectactivitytable', '{"isscope":"0","code":"020200","txt":"IW - Izin Warga","remark":"","status_id":"1"}'),
(313, 1, '2017-07-06 10:28:39', 'projectactivity', 7, 'Update projectactivitytable', '{"isscope":"0","code":"020100","txt":"Hammer \\/ Soil Test","remark":"","status_id":"1"}'),
(314, 1, '2017-07-06 10:28:53', 'projectactivity', 2, 'Update projectactivitytable', '{"isscope":"0","code":"020000","txt":"SITAC - Site Acqutition","remark":"","status_id":"1"}'),
(315, 1, '2017-07-06 10:29:05', 'projectactivity', 1, 'Update projectactivitytable', '{"isscope":"0","code":"010000","txt":"SIS - Site Surveys","remark":"","status_id":"1"}'),
(316, 1, '2017-07-06 10:41:51', 'workorderline', 16, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"2","projectactivity_id":false,"start_date":"2017-07-07","end_date":"2017-07-07","status_id":"1"}'),
(317, 1, '2017-07-06 10:43:21', 'projectactivity', 5, 'Update projectactivitytable', '{"isscope":"1","code":"050000","txt":"RFI - Ready for Installation","remark":"","status_id":"1"}'),
(318, 1, '2017-07-06 10:43:35', 'projectactivity', 5, 'Update projectactivitytable', '{"isscope":"0","code":"050000","txt":"RFI - Ready for Installation","remark":"","status_id":"1"}'),
(319, 1, '2017-07-06 10:43:47', 'projectactivity', 1, 'Update projectactivitytable', '{"isscope":"1","code":"010000","txt":"SIS - Site Surveys","remark":"","status_id":"1"}'),
(320, 1, '2017-07-06 10:44:04', 'projectactivity', 2, 'Update projectactivitytable', '{"isscope":"1","code":"020000","txt":"SITAC - Site Acqutition","remark":"","status_id":"1"}'),
(321, 1, '2017-07-06 10:44:17', 'projectactivity', 3, 'Update projectactivitytable', '{"isscope":"1","code":"030000","txt":"CME - Civil & Mechanical Engineering","remark":"","status_id":"1"}'),
(322, 1, '2017-07-06 10:44:34', 'projectactivity', 7, 'Update projectactivitytable', '{"isscope":"1","code":"020100","txt":"Hammer \\/ Soil Test","remark":"","status_id":"1"}'),
(323, 1, '2017-07-06 10:45:01', 'workorderline', 16, 'Delete workorderline', '""'),
(324, 1, '2017-07-06 10:50:02', 'workorderline', 17, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"1","start_date":"2017-07-07","end_date":"2017-07-08","status_id":"1"}'),
(325, 1, '2017-07-06 10:52:06', 'workorderline', 18, 'Insert workorderline', '{"workorder_no":"WO-001","site_id":"7","projectactivity_id":"2","start_date":"2017-07-08","end_date":"2017-07-09","status_id":"1"}'),
(326, 1, '2017-07-06 11:10:58', 'salesorder', 10, 'Update salesordertable', '{"salesorder_no":"SO26000011","salesorder_date":"2017-05-26","drm_date":"1970-01-01","batch":"1","po_tenant_no":"1","po_tenant_date":"1970-01-01","tenant_id":"1","status_id":"1"}'),
(327, 1, '2017-07-06 11:11:28', 'salesorder', 10, 'Update salesordertable', '{"salesorder_no":"SO26000011","salesorder_date":"2017-05-26","drm_date":"1970-01-01","batch":"1","po_tenant_no":"PO-001","po_tenant_date":"1970-01-01","tenant_id":"1","status_id":"1"}'),
(328, 1, '2017-07-06 11:12:19', 'salesorder', 11, 'Insert salesordertable', '{"salesorder_no":"SO17000011","salesorder_date":"2017-07-07","drm_date":"2017-07-07","batch":"1","po_tenant_no":"1","po_tenant_date":"2017-07-12","tenant_id":"1","status_id":"1"}'),
(329, 1, '2017-07-06 11:23:47', 'salesorder', 11, 'Update salesordertable', '{"salesorder_no":"SO07000012","salesorder_date":"2017-07-07","drm_date":"2017-07-07","batch":"1","po_tenant_no":"PO No 123","po_tenant_date":"2017-07-12","tenant_id":"1","status_id":"1"}'),
(330, 1, '2017-07-06 11:24:34', 'salesorder', 11, 'Update salesordertable', '{"salesorder_no":"SO07000012","salesorder_date":"2017-07-07","drm_date":"2017-07-07","batch":"1","po_tenant_no":"PO No 123 ......................","po_tenant_date":"2017-07-12","tenant_id":"1","status_id":"1"}'),
(331, 1, '2017-07-06 11:26:50', 'salesorder', 11, 'Update salesordertable', '{"salesorder_no":"SO07000012","salesorder_date":"2017-07-07","drm_date":"2017-07-07","batch":"1","po_tenant_no":"PO No 123 Api","po_tenant_date":"2017-07-12","tenant_id":"1","status_id":"1"}'),
(332, 1, '2017-07-06 14:28:34', 'salesorderline', 40, 'Insert salesorderline', '{"salesorder_no":"SO18000009","product_id":"1","site_id_tenant":"1","site_name_tenant":"A","site_id_api":"1","site_name_api":"API_A","sitetype_id":"2","towerheight_id":"1","latitude":"1","longitude":"1","area_id":"1","region_id":"1","baks":"2017-07-07","rfi":"2017-07-07","baps":"2017-07-07","remarks":"R","rent_period":"","status_id":"1"}'),
(333, 1, '2017-07-06 14:42:30', 'salesorderline', 41, 'Insert salesorderline', '{"salesorder_no":"SO18000009","product_id":"2","site_id_tenant":"2","site_name_tenant":"b","site_id_api":"2","site_name_api":"API_b","sitetype_id":"1","towerheight_id":"2","latitude":"1","longitude":"1","area_id":"1","region_id":"1","baks":"2017-07-12","rfi":"2017-07-07","baps":"2017-07-18","remarks":"1","rent_period":"1","status_id":"1"}'),
(334, 1, '2017-07-06 14:46:32', 'salesorderline', 40, 'Delete salesorderline', '""'),
(335, 1, '2017-07-06 14:46:39', 'salesorderline', 41, 'Delete salesorderline', '""'),
(336, 1, '2017-07-06 14:47:51', 'salesorderline', 42, 'Insert salesorderline', '{"salesorder_no":"SO18000009","product_id":"1","site_id_tenant":"SIT-001","site_name_tenant":"KEBAYORAN","site_id_api":"SIA-001","site_name_api":"API_KEBAYORAN","sitetype_id":"1","towerheight_id":"2","latitude":"13","longitude":"13","area_id":"1","region_id":"1","baks":"2017-07-07","rfi":"2017-07-05","baps":"2017-07-21","remarks":"R","rent_period":"1","status_id":"1"}'),
(337, 1, '2017-07-06 14:49:07', 'salesorderline', 42, 'Delete salesorderline', '""'),
(338, 1, '2017-07-06 14:50:15', 'salesorderline', 43, 'Insert salesorderline', '{"salesorder_no":"SO18000009","product_id":"1","site_id_tenant":"SIT-001","site_name_tenant":"TEBET","site_id_api":"SIA-001","site_name_api":"API_TEBET","sitetype_id":"1","towerheight_id":"2","latitude":"13","longitude":"15","area_id":"1","region_id":"2","baks":"2017-07-07","rfi":"2017-07-04","baps":"2017-07-21","remarks":"NA","rent_period":"1","status_id":"1"}'),
(339, 1, '2017-07-06 14:54:10', 'salesorderline', 43, 'Delete salesorderline', '""'),
(340, 1, '2017-07-06 15:09:07', 'salesorderline', 44, 'Insert salesorderline', '{"salesorder_no":"SO18000009","product_id":"2","site_id_tenant":"SIT-001","site_name_tenant":"KEBAYORAN","site_id_api":"SIA-001","site_name_api":"API_KEBAYORAN","sitetype_id":"1","towerheight_id":"2","latitude":"13","longitude":"13","area_id":"1","region_id":"1","baks":"2017-07-07","rfi":"2017-07-14","baps":"2017-07-21","remarks":"NA","rent_period":"1","status_id":"1"}'),
(341, 1, '2017-07-10 14:15:47', 'workorderline', 19, 'Insert workorderline', '{"workorder_no":"WO-002","site_id":"7","projectactivity_id":"7","start_date":"2017-07-12","end_date":"2017-07-19","status_id":"1"}'),
(342, 1, '2017-07-10 14:16:36', 'workorder', 4, 'Insert workordertable', '{"workorder_no":"WO-003","workorder_date":"2017-07-11 00:00:00","vendor_id":"1","vendorcontact_id":"2","product_group":"ABC","project_manager":"Asep","remark":"test","status_id":"1"}'),
(343, 1, '2017-07-12 14:18:34', 'salesorder', 12, 'Insert salesordertable', '{"salesorder_no":"SO12000012","salesorder_date":"2017-07-12","drm_date":"1970-01-01","batch":"1","po_tenant_no":"PO 001","po_tenant_date":"2017-07-12","tenant_id":"1","status_id":"1"}'),
(344, 1, '2017-07-12 14:21:18', 'salesorderline', 45, 'Insert salesorderline', '{"salesorder_no":"SO12000012","product_id":"1","site_id_tenant":"JKT002","site_name_tenant":"Tebet","site_id_api":"API002","site_name_api":"API_Tebet","sitetype_id":"1","towerheight_id":"3","latitude":"-13.6555","longitude":"12.000","area_id":"1","region_id":"1","baks":"2017-07-12","rfi":"2017-07-13","baps":"2017-07-14","remarks":"na","rent_period":"5","status_id":"1"}'),
(345, 1, '2017-07-12 14:30:01', 'site', 8, 'Insert sitetable', '{"product_id":"1","tenant_id":"1","site_id_api":"API002","site_name_api":"API_Tebet","site_id_tenant":"JKT002","site_name_tenant":"Tebet","province":"DKI Jakarta","area_id":"1","region_id":"1","longitude_ori":"12","latitude_ori":"-13.6555","address":"Tebet Raya","sitetype_id":"1","towerheight_id":"3","transmissiontype_id":"1","remark":"na","status_id":"1"}'),
(346, 1, '2017-07-13 15:55:26', 'vendorcontact', 8, 'Insert vendorcontacttable', '{"vendor_id":"4","name":"Sabri","position":"Manager","organization":"GA","email":"sabri@samsung.co.id","mobile_no":"081213831837","status_id":"1"}'),
(347, 1, '2017-07-13 16:18:11', 'workorder', 5, 'Insert workordertable', '{"workorder_no":"WO-005","workorder_date":"2017-07-13 00:00:00","vendor_id":"4","vendorcontact_id":"7","product_group":"abc","project_manager":"abde","remark":"na","status_id":"1"}'),
(348, 1, '2017-07-14 10:17:17', 'product', 5, 'Insert producttable', '{"short_txt":"x","txt":"xxxx","status_id":"1"}'),
(349, 1, '2017-07-14 10:17:39', 'product', 5, 'Update producttable', '{"short_txt":"a","txt":"aaa","status_id":"2"}'),
(350, 1, '2017-07-14 10:17:47', 'product', 5, 'Delete producttable', '""'),
(351, 1, '2017-07-14 10:38:54', 'area', 3, 'Insert areatable', '{"short_txt":"C","txt":"Area 3 (Jateng Jatim Balnus)","status_id":"1"}'),
(352, 1, '2017-07-14 10:39:32', 'area', 3, 'Update areatable', '{"short_txt":"D","txt":"Area 4 (Jateng Jatim Balnus)","status_id":"1"}'),
(353, 1, '2017-07-14 10:39:46', 'area', 3, 'Update areatable', '{"short_txt":"C","txt":"Area 3 (Jateng Jatim Balnus)","status_id":"1"}'),
(354, 1, '2017-07-14 10:39:55', 'area', 3, 'Update areatable', '{"short_txt":"C","txt":"Area 3 (Jateng Jatim Balnus)","status_id":"2"}'),
(355, 1, '2017-07-14 10:40:02', 'area', 3, 'Update areatable', '{"short_txt":"C","txt":"Area 3 (Jateng Jatim Balnus)","status_id":"1"}'),
(356, 1, '2017-07-14 10:43:29', 'area', 4, 'Insert areatable', '{"short_txt":"D","txt":"Area 4 (Kalimantan Sulawesi Puma)","status_id":"1"}'),
(357, 1, '2017-07-14 11:12:16', 'region', 5, 'Insert regiontable', '{"short_txt":"E","txt":"Jateng","area_id":"3","status_id":"1"}'),
(358, 1, '2017-07-14 11:12:47', 'region', 5, 'Update regiontable', '{"short_txt":"F","txt":"Jatim","area_id":"2","status_id":"2"}'),
(359, 1, '2017-07-14 11:13:16', 'region', 5, 'Update regiontable', '{"short_txt":"E","txt":"Jateng","area_id":"3","status_id":"1"}'),
(360, 1, '2017-07-14 11:13:58', 'region', 6, 'Insert regiontable', '{"short_txt":"F","txt":"Jatim","area_id":"3","status_id":"1"}'),
(361, 1, '2017-07-14 11:14:22', 'region', 7, 'Insert regiontable', '{"short_txt":"G","txt":"Balnus","area_id":"3","status_id":"1"}'),
(362, 1, '2017-07-14 11:14:47', 'region', 8, 'Insert regiontable', '{"short_txt":"H","txt":"Kalimantan","area_id":"4","status_id":"1"}'),
(363, 1, '2017-07-14 11:15:09', 'region', 9, 'Insert regiontable', '{"short_txt":"I","txt":"Sulawesi","area_id":"4","status_id":"1"}'),
(364, 1, '2017-07-14 11:15:35', 'region', 10, 'Insert regiontable', '{"short_txt":"J","txt":"Sumbagteng","area_id":"4","status_id":"1"}'),
(365, 1, '2017-07-14 11:15:54', 'region', 11, 'Insert regiontable', '{"short_txt":"K","txt":"Puma","area_id":"4","status_id":"1"}'),
(366, 1, '2017-07-14 15:53:40', 'province', 2, 'Insert provincetable', '{"short_txt":"PKU","txt":"Pekanbaru","region_id":"2","status_id":"1"}'),
(367, 1, '2017-07-14 15:54:41', 'province', 3, 'Insert provincetable', '{"short_txt":"JKT","txt":"Jakarta","region_id":"3","status_id":"1"}'),
(368, 1, '2017-07-14 15:55:43', 'province', 4, 'Insert provincetable', '{"short_txt":"BDG","txt":"Bandung","region_id":"4","status_id":"1"}'),
(369, 1, '2017-07-14 15:56:08', 'province', 5, 'Insert provincetable', '{"short_txt":"SRG","txt":"Semarang","region_id":"5","status_id":"1"}'),
(370, 1, '2017-07-14 15:56:46', 'province', 6, 'Insert provincetable', '{"short_txt":"SBY","txt":"Surabaya","region_id":"6","status_id":"1"}'),
(371, 1, '2017-07-14 16:02:18', 'province', 7, 'Insert provincetable', '{"short_txt":"DPS","txt":"Denpasar","region_id":"7","status_id":"1"}'),
(372, 1, '2017-07-14 16:02:40', 'province', 8, 'Insert provincetable', '{"short_txt":"BLK","txt":"Balikpapan","region_id":"8","status_id":"1"}'),
(373, 1, '2017-07-14 16:03:03', 'province', 9, 'Insert provincetable', '{"short_txt":"MKS","txt":"Makasar","region_id":"9","status_id":"1"}'),
(374, 1, '2017-07-14 16:03:39', 'province', 10, 'Insert provincetable', '{"short_txt":"PLM","txt":"Palembang","region_id":"2","status_id":"1"}'),
(375, 1, '2017-07-14 16:08:53', 'province', 11, 'Insert provincetable', '{"short_txt":"SOG","txt":"Sorong","region_id":"11","status_id":"1"}'),
(376, 1, '2017-07-17 09:04:06', 'role', 11, 'Insert roletable', '{"txt":"zzz"}'),
(377, 1, '2017-07-17 09:04:24', 'role', 11, 'Update roletable', '{"txt":"zzz ....."}'),
(378, 1, '2017-07-17 09:04:35', 'role', 11, 'Delete roletable', '""'),
(379, 1, '2017-07-17 09:52:07', 'approvalflowlist', 1, 'Insert approvalflowlisttable', '{"txt":"SO - Sales Order"}'),
(380, 1, '2017-07-17 09:52:21', 'approvalflowlist', 2, 'Insert approvalflowlisttable', '{"txt":"WO - Work Order"}'),
(381, 1, '2017-07-17 09:52:27', 'approvalflowlist', 3, 'Insert approvalflowlisttable', '{"txt":"xxxx"}'),
(382, 1, '2017-07-17 09:52:37', 'approvalflowlist', 3, 'Update approvalflowlisttable', '{"txt":"zzz"}'),
(383, 1, '2017-07-17 09:52:44', 'approvalflowlist', 3, 'Delete approvalflowlisttable', '""'),
(384, 1, '2017-07-17 10:22:57', 'approvalflowlist', 1, 'Update approvalflowlisttable', '{"txt":"SO - Sales Order ....."}'),
(385, 1, '2017-07-17 10:25:53', 'approvalflowlist', 1, 'Update approvalflowlisttable', '{"txt":"SO - Sales Order"}'),
(386, 1, '2017-07-17 10:26:12', 'approvalflowlist', 4, 'Insert approvalflowlisttable', '{"txt":"test"}'),
(387, 1, '2017-07-17 10:26:17', 'approvalflowlist', 4, 'Delete approvalflowlisttable', '""'),
(388, 1, '2017-07-17 10:49:30', 'salesorder', 13, 'Insert salesordertable', '{"salesorder_no":"SO17000013","salesorder_date":"2017-07-17","drm_date":"2017-07-16","batch":"1","po_tenant_no":"PO-001","po_tenant_date":"2017-07-13","tenant_id":"1","status_id":"1"}'),
(389, 1, '2017-07-17 11:20:36', 'role', 12, 'Insert roletable', '{"txt":"Sales Manager"}'),
(390, 1, '2017-07-17 11:21:57', 'user', 2, 'Insert usertable', '{"role_id":"2","name":"Abay","email":"abay@akses-prima.co.id","user_id":"sales1","pswrd":"123","photo":"","status_id":"1"}'),
(391, 1, '2017-07-17 11:22:24', 'user', 2, 'Update usertable', '{"role_id":"2","name":"Abay","email":"abay@akses-prima.co.id","user_id":"salesperson","pswrd":"123","photo":"","status_id":"1"}'),
(392, 1, '2017-07-17 11:23:00', 'user', 3, 'Insert usertable', '{"role_id":"12","name":"araf","email":"araf@akses-prima.co.id","user_id":"salesmanager","pswrd":"123","photo":"","status_id":"1"}'),
(393, 1, '2017-07-17 14:44:00', 'salesorderapprovalflow', 1, 'Update salesorderapprovalflowtable', '{"ordering":"1","role_id":"1","remark":"NA ...."}'),
(394, 1, '2017-07-17 14:46:59', 'salesorderapprovalflow', 1, 'Update salesorderapprovalflowtable', '{"ordering":"1","role_id":"2","remark":"NA ...."}'),
(395, 1, '2017-07-17 14:47:27', 'salesorderapprovalflow', 2, 'Insert salesorderapprovalflowtable', '{"ordering":"2","role_id":"12","remark":"Level 1 Approval"}'),
(396, 1, '2017-07-17 14:47:45', 'salesorderapprovalflow', 1, 'Update salesorderapprovalflowtable', '{"ordering":"1","role_id":"2","remark":"SO Creator"}'),
(397, 1, '2017-07-17 15:07:34', 'workorderapprovalflow', 1, 'Insert workorderapprovalflowtable', '{"ordering":"1","role_id":"4","remark":"Create WO"}'),
(398, 1, '2017-07-17 15:56:55', 'workorderapprovalflow', 2, 'Insert workorderapprovalflowtable', '{"ordering":"2","role_id":"2","remark":"approval"}'),
(399, 1, '2017-07-17 15:57:08', 'workorderapprovalflow', 3, 'Insert workorderapprovalflowtable', '{"ordering":"3","role_id":"12","remark":"dasd"}'),
(400, 1, '2017-07-17 15:57:13', 'workorderapprovalflow', 3, 'Delete workorderapprovalflowtable', '""'),
(401, 1, '2017-07-18 15:40:25', 'salesorder', 14, 'Insert salesordertable', '{"salesorder_no":"SO18000014","salesorder_date":"2017-07-18","drm_date":"2017-07-11","batch":"111111","po_tenant_no":"PO 123456789","po_tenant_date":"2017-07-12","tenant_id":"2","status_id":"1"}'),
(402, 1, '2017-07-18 15:40:29', 'salesorder', 15, 'Insert salesordertable', '{"salesorder_no":"SO18000015","salesorder_date":"2017-07-18","drm_date":"2017-07-11","batch":"111111","po_tenant_no":"PO 123456789","po_tenant_date":"2017-07-12","tenant_id":"2","status_id":"1"}'),
(403, 1, '2017-07-18 15:53:57', 'salesorder', 16, 'Insert salesordertable', '{"salesorder_no":null,"salesorder_date":"2017-07-18","drm_date":"2017-07-11","batch":"123213","po_tenant_no":"1213","po_tenant_date":"2017-07-19","tenant_id":"1","status_id":"1","locked":false}'),
(404, 1, '2017-07-18 15:55:16', 'salesorder', 16, 'Delete salesordertable', '""'),
(405, 1, '2017-07-18 15:55:44', 'salesorder', 17, 'Insert salesordertable', '{"salesorder_no":"SO18000016","salesorder_date":"2017-07-18","drm_date":"2017-07-17","batch":"12345","po_tenant_no":"123345","po_tenant_date":"2017-07-17","tenant_id":"1","status_id":"1","locked":false}'),
(406, 1, '2017-07-19 10:05:21', 'salesorder', 7, 'Delete salesordertable', '""'),
(407, 1, '2017-07-19 10:05:38', 'salesorder', 11, 'Delete salesordertable', '""'),
(408, 1, '2017-07-19 10:06:06', 'salesorder', 10, 'Delete salesordertable', '""'),
(409, 1, '2017-07-19 10:06:12', 'salesorder', 17, 'Delete salesordertable', '""'),
(410, 1, '2017-07-19 10:06:19', 'salesorder', 15, 'Delete salesordertable', '""'),
(411, 1, '2017-07-19 10:06:24', 'salesorder', 14, 'Delete salesordertable', '""'),
(412, 1, '2017-07-19 10:06:31', 'salesorder', 13, 'Delete salesordertable', '""'),
(413, 1, '2017-07-19 10:06:55', 'salesorder', 4, 'Delete salesordertable', '""'),
(414, 1, '2017-07-19 10:07:08', 'salesorderline', 39, 'Delete salesorderline', '""'),
(415, 1, '2017-07-19 10:07:32', 'salesorder', 6, 'Delete salesordertable', '""'),
(416, 1, '2017-07-19 10:08:05', 'salesorderline', 3, 'Delete salesorderline', '""'),
(417, 1, '2017-07-19 10:08:10', 'salesorderline', 6, 'Delete salesorderline', '""'),
(418, 1, '2017-07-19 10:08:59', 'salesorder', 8, 'Delete salesordertable', '""'),
(419, 1, '2017-07-19 10:09:13', 'salesorderline', 1, 'Delete salesorderline', '""'),
(420, 1, '2017-07-19 10:09:17', 'salesorderline', 44, 'Delete salesorderline', '""'),
(421, 1, '2017-07-19 10:09:30', 'salesorder', 9, 'Delete salesordertable', '""'),
(422, 1, '2017-07-19 10:09:45', 'salesorderline', 45, 'Delete salesorderline', '""'),
(423, 1, '2017-07-19 10:10:18', 'salesorder', 12, 'Delete salesordertable', '""'),
(424, NULL, '2017-07-19 13:13:35', 'salesorder', 21, 'Delete salesordertable', '""'),
(425, NULL, '2017-07-19 13:13:45', 'salesorder', 21, 'Delete salesordertable', '""'),
(426, 1, '2017-07-19 13:22:53', 'salesorder', 22, 'Delete salesordertable', '""'),
(427, 1, '2017-07-19 13:23:27', 'salesorder', 23, 'Insert salesordertable', '{"salesorder_no":"0001\\/SO\\/07\\/2017","salesorder_date":"2017-07-19","drm_date":"2017-07-18","batch":"1","po_tenant_no":"TLKM-0001","po_tenant_date":"2017-07-17","tenant_id":"1","status_id":"1","locked":0}'),
(428, 1, '2017-07-19 13:28:30', 'salesorder', 24, 'Insert salesordertable', '{"salesorder_no":"0002\\/SO\\/XII\\/2017","salesorder_date":"2017-07-19","drm_date":"2017-07-18","batch":"2","po_tenant_no":"TLKM-002","po_tenant_date":"2017-07-16","tenant_id":"1","status_id":"1","locked":0}'),
(429, 1, '2017-07-19 13:30:48', 'salesorder', 25, 'Insert salesordertable', '{"salesorder_no":"0003\\/SO\\/XII\\/2017","salesorder_date":"2017-07-19","drm_date":"2017-07-18","batch":"3","po_tenant_no":"TLKM-0003","po_tenant_date":"2017-07-16","tenant_id":"1","status_id":"1","locked":0}'),
(430, 1, '2017-07-19 13:33:52', 'salesorder', 26, 'Insert salesordertable', '{"salesorder_no":"0004\\/SO\\/VII\\/2017","salesorder_date":"2017-07-19","drm_date":"2017-07-17","batch":"4","po_tenant_no":"TLKM-0004","po_tenant_date":"2017-07-16","tenant_id":"1","status_id":"1","locked":0}'),
(431, 1, '2017-07-19 13:34:05', 'salesorder', 23, 'Delete salesordertable', '""'),
(432, 1, '2017-07-19 13:34:11', 'salesorder', 24, 'Delete salesordertable', '""'),
(433, 1, '2017-07-19 13:34:15', 'salesorder', 25, 'Delete salesordertable', '""'),
(434, 1, '2017-07-19 15:10:40', 'salesorder', 27, 'Insert salesordertable', '{"salesorder_no":"0005\\/SO\\/VII\\/2017","salesorder_date":"2017-07-20","drm_date":"2017-07-17","batch":"5","po_tenant_no":"ISAT","po_tenant_date":"2017-07-16","tenant_id":"2","status_id":"1","locked":0}'),
(435, 1, '2017-07-19 17:17:28', 'salesorder', 28, 'Insert salesordertable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","salesorder_date":"2017-07-19","drm_date":"2017-07-18","batch":"6","po_tenant_no":"ISAT-0006","po_tenant_date":"2017-07-17","tenant_id":"1","approval_milestone_id":"","status_id":"1"}'),
(436, 1, '2017-07-19 17:18:37', 'salesorder', 26, 'Update salesordertable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","salesorder_date":"2017-07-19","drm_date":"2017-07-17","batch":"4444","po_tenant_no":"TLKM-0004","po_tenant_date":"2017-07-16","tenant_id":"1","approval_milestone_id":"0","status_id":"1"}'),
(437, 1, '2017-07-19 17:19:02', 'salesorder', 26, 'Update salesordertable', '{"salesorder_no":"0008\\/SO\\/VII\\/2017","salesorder_date":"2017-07-18","drm_date":"2017-07-17","batch":"4444","po_tenant_no":"TLKM-0004","po_tenant_date":"2017-07-16","tenant_id":"1","approval_milestone_id":"0","status_id":"1"}'),
(438, 1, '2017-07-19 17:19:18', 'salesorder', 26, 'Update salesordertable', '{"salesorder_no":"0009\\/SO\\/VII\\/2017","salesorder_date":"2017-07-18","drm_date":"2017-07-16","batch":"4444","po_tenant_no":"TLKM-0004","po_tenant_date":"2017-07-16","tenant_id":"1","approval_milestone_id":"0","status_id":"1"}'),
(439, 1, '2017-07-19 17:19:35', 'salesorder', 26, 'Update salesordertable', '{"salesorder_no":"00010\\/SO\\/VII\\/2017","salesorder_date":"2017-07-18","drm_date":"2017-07-16","batch":"4444","po_tenant_no":"TLKM-00044444","po_tenant_date":"2017-07-16","tenant_id":"1","approval_milestone_id":"0","status_id":"1"}'),
(440, 1, '2017-07-19 17:19:50', 'salesorder', 26, 'Update salesordertable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","salesorder_date":"2017-07-18","drm_date":"2017-07-16","batch":"4444","po_tenant_no":"TLKM-00044444","po_tenant_date":"2017-07-15","tenant_id":"1","approval_milestone_id":"0","status_id":"1"}'),
(441, 1, '2017-07-19 17:20:03', 'salesorder', 26, 'Update salesordertable', '{"salesorder_no":"0008\\/SO\\/VII\\/2017","salesorder_date":"2017-07-18","drm_date":"2017-07-16","batch":"4444","po_tenant_no":"TLKM-00044444","po_tenant_date":"2017-07-15","tenant_id":"2","approval_milestone_id":"0","status_id":"1"}'),
(442, 1, '2017-07-19 17:20:18', 'salesorder', 26, 'Delete salesordertable', '""'),
(443, 1, '2017-07-20 09:54:39', 'role', 13, 'Insert roletable', '{"txt":"Sales Director"}'),
(444, 1, '2017-07-20 09:54:53', 'role', 14, 'Insert roletable', '{"txt":"Project Director"}'),
(445, 1, '2017-07-20 09:55:45', 'salesorderapprovalflow', 1, 'Update salesorderapprovalflowtable', '{"ordering":"1","role_id":"12","remark":"Approval Level 1"}'),
(446, 1, '2017-07-20 09:57:02', 'user', 4, 'Insert usertable', '{"role_id":"13","name":"Joko","email":"joko@akses-prima.co.id","user_id":"salesdirector","pswrd":"1234","photo":"","status_id":"1"}'),
(447, 1, '2017-07-20 09:57:38', 'salesorderapprovalflow', 2, 'Update salesorderapprovalflowtable', '{"ordering":"2","role_id":"13","remark":"Level 2 Approval"}'),
(448, 1, '2017-07-20 16:02:37', 'salesorderline', 1, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"TLKM-001","site_name_tenant":"TEBET","site_id_api":"Generated by System","site_name_api":"API_TEBET","sitetype_id":"1","towerheight_id":"3","latitude":"-136","longitude":"135","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"NA","rent_period":"3","status_id":"1"}'),
(449, 1, '2017-07-20 16:12:06', 'salesorderline', 2, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"TLKM-0001","site_name_tenant":"TEBET","site_id_api":"-","site_name_api":"API_TEBET","sitetype_id":"1","towerheight_id":"3","latitude":"136","longitude":"123","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"NA","rent_period":"5","status_id":"1"}'),
(450, 1, '2017-07-20 17:11:22', 'salesorderline', 3, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"TLKM-002","site_name_tenant":"TEBET","site_id_api":"Generated by System","site_name_api":"API_TEBET","sitetype_id":"1","towerheight_id":"3","latitude":"13","longitude":"13","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"NA","rent_period":"5","status_id":"1"}'),
(451, 1, '2017-07-20 17:20:53', 'salesorderline', 4, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"TLKM0002","site_name_tenant":"TEBET","site_id_api":"MAA","site_name_api":"API_TEBET","sitetype_id":"1","towerheight_id":"3","latitude":"13","longitude":"13","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"NA","rent_period":"5","status_id":"1"}');
INSERT INTO `systemlogtable` (`row_id`, `created_by`, `created_date`, `module`, `key_id`, `action`, `data`) VALUES
(452, 1, '2017-07-20 17:27:16', 'salesorderline', 5, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"AAAA","site_name_tenant":"AAAA","site_id_api":"site_id_api","site_name_api":"API_AAAA","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"DD","rent_period":"5","status_id":"1"}'),
(453, 1, '2017-07-20 17:35:48', 'salesorderline', 6, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"A","site_name_tenant":"A","site_id_api":"1","site_name_api":"API_A","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"A","rent_period":"5","status_id":"1"}'),
(454, 1, '2017-07-20 17:37:53', 'salesorderline', 7, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"P","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"3","latitude":"13","longitude":"13","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"a","rent_period":"5","status_id":"1"}'),
(455, 1, '2017-07-20 17:59:13', 'salesorderline', 8, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PB","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"a","rent_period":"4","status_id":"1"}'),
(456, 1, '2017-07-20 18:07:40', 'salesorderline', 9, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PB3","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"a","rent_period":"1","status_id":"1"}'),
(457, 1, '2017-07-20 18:11:33', 'salesorderline', 10, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PBC-JKT","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-22","rfi":"2017-07-21","baps":"2017-07-23","remarks":"a","rent_period":"1","status_id":"1"}'),
(458, 1, '2017-07-21 11:11:15', 'salesorderline', 11, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"A","site_name_tenant":"A","site_id_api":"PAA-MDN-","site_name_api":"API_A","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"1","region_id":"1","province_id":"1","baks":"2017-07-23","rfi":"2017-07-22","baps":"2017-07-24","remarks":"A","rent_period":"3","status_id":"1"}'),
(459, 1, '2017-07-21 11:14:23', 'salesorderline', 12, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PAA-MDN-002","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"1","region_id":"1","province_id":"1","baks":"2017-07-23","rfi":"2017-07-22","baps":"2017-07-24","remarks":"aa","rent_period":"4","status_id":"1"}'),
(460, 1, '2017-07-21 11:25:00', 'salesorderline', 13, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PAA-MDN-005","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"1","region_id":"1","province_id":"1","baks":"2017-07-23","rfi":"2017-07-22","baps":"2017-07-24","remarks":"a","rent_period":"1","status_id":"1"}'),
(461, 1, '2017-07-21 11:27:02', 'salesorderline', 14, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PAA-MDN-005","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"1","region_id":"1","province_id":"1","baks":"2017-07-23","rfi":"2017-07-22","baps":"2017-07-24","remarks":"a","rent_period":"1","status_id":"1"}'),
(462, 1, '2017-07-21 11:28:33', 'salesorderline', 15, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PAA-MDN-001","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"1","latitude":"1","longitude":"1","area_id":"1","region_id":"1","province_id":"1","baks":"2017-07-23","rfi":"2017-07-22","baps":"2017-07-24","remarks":"a","rent_period":"1","status_id":"1"}'),
(463, 1, '2017-07-21 11:31:37', 'salesorderline', 16, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PAA-MDN-006","site_name_api":"API_a","sitetype_id":"1","towerheight_id":"3","latitude":"1","longitude":"1","area_id":"1","region_id":"1","province_id":"1","baks":"2017-07-23","rfi":"2017-07-22","baps":"2017-07-24","remarks":"a","rent_period":"1","status_id":"1"}'),
(464, 1, '2017-07-21 13:25:50', 'salesorderline', 7, 'Delete salesorderline', '""'),
(465, 1, '2017-07-21 13:25:58', 'salesorderline', 6, 'Delete salesorderline', '""'),
(466, 1, '2017-07-21 13:26:08', 'salesorderline', 8, 'Delete salesorderline', '""'),
(467, 1, '2017-07-21 13:26:17', 'salesorderline', 9, 'Delete salesorderline', '""'),
(468, 1, '2017-07-21 13:26:25', 'salesorderline', 10, 'Delete salesorderline', '""'),
(469, 1, '2017-07-21 13:26:33', 'salesorderline', 11, 'Delete salesorderline', '""'),
(470, 1, '2017-07-21 13:26:41', 'salesorderline', 12, 'Delete salesorderline', '""'),
(471, 1, '2017-07-21 13:26:48', 'salesorderline', 13, 'Delete salesorderline', '""'),
(472, 1, '2017-07-21 13:26:54', 'salesorderline', 14, 'Delete salesorderline', '""'),
(473, 1, '2017-07-21 13:27:02', 'salesorderline', 15, 'Delete salesorderline', '""'),
(474, 1, '2017-07-21 13:27:09', 'salesorderline', 16, 'Delete salesorderline', '""'),
(475, 1, '2017-07-21 13:42:21', 'salesorderline', 17, 'Insert salesorderline', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"TLKM-JKT-0001","site_name_tenant":"TEBET","site_id_api":"PBC-JKT-001","site_name_api":"API_TEBET","sitetype_id":"1","towerheight_id":"3","latitude":"-13.6766","longitude":"256.990999","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-26","rfi":"2017-07-24","baps":"2017-07-28","remarks":"NA","rent_period":"5","status_id":"1"}'),
(476, 1, '2017-07-21 14:28:00', 'salesorder', 0, 'Update salesordertable', '{"salesorder_no":"0007\\/SO\\/I\\/1970","salesorder_date":"1970-01-01","drm_date":"1970-01-01","batch":false,"po_tenant_no":false,"po_tenant_date":"1970-01-01","tenant_id":false,"approval_milestone_id":false,"status_id":false}'),
(477, 1, '2017-07-21 14:30:17', 'salesorder', 0, 'Update salesordertable', '{"salesorder_no":"0007\\/SO\\/I\\/1970","salesorder_date":"1970-01-01","drm_date":"1970-01-01","batch":false,"po_tenant_no":false,"po_tenant_date":"1970-01-01","tenant_id":false,"approval_milestone_id":false,"status_id":false}'),
(478, 1, '2017-07-21 14:31:13', 'salesorder', 0, 'Update salesordertable', '{"salesorder_no":"0007\\/SO\\/I\\/1970","salesorder_date":"1970-01-01","drm_date":"1970-01-01","batch":false,"po_tenant_no":false,"po_tenant_date":"1970-01-01","tenant_id":false,"approval_milestone_id":false,"status_id":false}'),
(479, 1, '2017-07-21 14:31:36', 'salesorder', 0, 'Update salesordertable', '{"salesorder_no":"0007\\/SO\\/I\\/1970","salesorder_date":"1970-01-01","drm_date":"1970-01-01","batch":false,"po_tenant_no":false,"po_tenant_date":"1970-01-01","tenant_id":false,"approval_milestone_id":false,"status_id":false}'),
(480, 1, '2017-07-21 15:45:38', 'salesorder', NULL, 'Update salesordertable', '{"approval_milestone_id":1}'),
(481, 1, '2017-07-21 15:47:02', 'salesorder', 0, 'Update salesordertable', '{"approval_milestone_id":1}'),
(482, 1, '2017-07-21 16:00:01', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(483, 1, '2017-07-21 16:02:42', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(484, 1, '2017-07-21 16:02:58', 'salesorder', 0, 'Update salesordertable', '{"approval_milestone_id":1}'),
(485, 1, '2017-07-21 16:19:47', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(486, 1, '2017-07-21 16:22:44', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(487, 1, '2017-07-21 16:30:32', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(488, 1, '2017-07-25 14:13:39', 'salesorder', 29, 'Insert salesordertable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","salesorder_date":"2017-07-25","drm_date":"2017-07-26","batch":"3","po_tenant_no":"POTLKM-001","po_tenant_date":"2017-07-24","tenant_id":"1","approval_milestone_id":"","status_id":"1"}'),
(489, 1, '2017-07-25 14:16:00', 'salesorderline', 18, 'Insert salesorderline', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"TLK-TBT-001","site_name_tenant":"TEBET DALAM","site_id_api":"PBC-JKT-002","site_name_api":"API_TEBET DALAM","sitetype_id":"1","towerheight_id":"3","latitude":"-12.453","longitude":"15.987","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-29","rfi":"2017-07-28","baps":"2017-07-30","remarks":"na","rent_period":"5","status_id":"1"}'),
(490, 1, '2017-07-25 14:18:16', 'salesorderline', 19, 'Insert salesorderline', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"TLK-TBT-002","site_name_tenant":"TEBET TIMUR","site_id_api":"PBC-JKT-003","site_name_api":"API_TEBET TIMUR","sitetype_id":"1","towerheight_id":"3","latitude":"-13.655","longitude":"15.0098","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-29","rfi":"2017-07-27","baps":"2017-07-31","remarks":"na","rent_period":"5","status_id":"1"}'),
(491, 1, '2017-07-25 14:19:50', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":1}'),
(492, 1, '2017-07-25 14:30:50', 'salesorderapprovalflow', 1, 'Update salesorderapprovalflowtable', '{"ordering":"2","role_id":"12","remark":"Approval Level 1"}'),
(493, 1, '2017-07-25 14:31:16', 'salesorderapprovalflow', 1, 'Update salesorderapprovalflowtable', '{"ordering":"1","role_id":"12","remark":"Approval Level 1"}'),
(494, 1, '2017-07-27 14:03:34', 'salesorderline', 20, 'Insert salesorderline', '{"salesorder_no":"0005\\/SO\\/VII\\/2017","product_id":"1","site_id_tenant":"TLK-TBT-005","site_name_tenant":"TEBET TIMUR DALAM","site_id_api":"PBC-JKT-004","site_name_api":"API_TEBET TIMUR DALAM","sitetype_id":"1","towerheight_id":"3","latitude":"-11.988777","longitude":"15.09808","area_id":"2","region_id":"3","province_id":"3","baks":"2017-07-29","rfi":"2017-07-28","baps":"2017-07-30","remarks":"NA","rent_period":"5","status_id":"1"}'),
(495, 1, '2017-07-27 14:09:41', 'salesorder', 30, 'Insert salesordertable', '{"salesorder_no":"0008\\/SO\\/VII\\/2017","salesorder_date":"2017-07-27","drm_date":"2017-07-28","batch":"3","po_tenant_no":"TLKM-PO-001","po_tenant_date":"2017-07-25","tenant_id":"1","approval_milestone_id":"","status_id":"1"}'),
(496, 1, '2017-07-27 15:55:09', 'salesorder', 0, 'Update salesordertable', '{"approval_milestone_id":1}'),
(497, 1, '2017-07-27 16:05:12', 'salesorder', 27, 'Update salesordertable', '{"approval_milestone_id":0}'),
(498, 1, '2017-07-27 16:06:19', 'salesorder', 27, 'Update salesordertable', '{"approval_milestone_id":1}'),
(499, 1, '2017-07-27 16:06:36', 'salesorder', 27, 'Update salesordertable', '{"approval_milestone_id":0}'),
(500, 1, '2017-07-28 11:42:53', 'salesorder', 27, 'Update salesordertable', '{"approval_milestone_id":1}'),
(501, 1, '2017-07-28 11:43:17', 'salesorder', 27, 'Update salesordertable', '{"approval_milestone_id":0}'),
(502, 1, '2017-07-31 18:45:47', 'salesorderapproval', 2, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"","approved_date":"2017-07-31","approved_status":"1","remark":"approved","status_id":false}'),
(503, 1, '2017-07-31 19:09:59', 'salesorderapproval', 3, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"","approved_date":"2017-07-31","approved_status":"1","remark":"approved","status_id":1}'),
(504, 1, '2017-07-31 19:17:34', 'salesorderapproval', 4, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"","approved_date":"2017-07-31","approved_status":"1","remark":"approved","status_id":1}'),
(505, 1, '2017-07-31 19:18:30', 'salesorderapproval', 5, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"","approved_date":"2017-07-31","approved_status":"1","remark":"a","status_id":1}'),
(506, 1, '2017-07-31 19:20:10', 'salesorderapproval', 6, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"","approved_date":"2017-07-31","approved_status":"1","remark":"a","status_id":1}'),
(507, 1, '2017-07-31 19:24:01', 'salesorderapproval', 7, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":false,"approved_date":"2017-07-31","approved_status":"1","remark":"a","status_id":1}'),
(508, 1, '2017-08-01 08:48:49', 'salesorderapproval', 2, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":false,"approved_date":"2017-08-01","approved_status":"1","remark":"Approved","status_id":1}'),
(509, 1, '2017-08-01 08:50:57', 'salesorderapproval', 3, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"setuju lah","status_id":1}'),
(510, 1, '2017-08-01 08:56:13', 'salesorderapproval', 4, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"approooved","status_id":1}'),
(511, 1, '2017-08-01 10:37:42', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":9}'),
(512, 1, '2017-08-01 10:38:12', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(513, 1, '2017-08-01 10:45:49', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":9}'),
(514, 1, '2017-08-01 11:18:21', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(515, 1, '2017-08-01 11:21:17', 'salesorderapproval', 2, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"0","remark":"Pls, add 1 line for Telkom Site in Tebet","status_id":1}'),
(516, 1, '2017-08-01 11:21:17', 'salesorderapproval', 6, 'Update salesorderapprovaltable', '{"status_id":0}'),
(517, 1, '2017-08-01 11:24:06', 'salesorderapproval', 2, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"0","remark":"Pls add change site number TLKM-TEBET-01 to TLKM-TEBET-02","status_id":1}'),
(518, 1, '2017-08-01 11:24:06', 'salesorderapproval', 6, 'Update salesorderapprovaltable', '{"status_id":2}'),
(519, 1, '2017-08-01 11:47:43', 'salesorderapproval', 3, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"999","approved_date":"2017-08-01","approved_status":"1","remark":"Approved ya","status_id":1}'),
(520, 1, '2017-08-01 12:00:57', 'salesorderapproval', 4, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"","approved_date":"2017-08-01","approved_status":"1","remark":"aproved","status_id":1}'),
(521, 1, '2017-08-01 12:07:21', 'salesorderapproval', 5, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"a","status_id":1}'),
(522, 1, '2017-08-01 12:43:05', 'salesorderapproval', 6, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"approved","status_id":1}'),
(523, 1, '2017-08-01 14:36:36', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":9}'),
(524, 1, '2017-08-01 14:37:10', 'salesorderapproval', 7, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(525, 1, '2017-08-01 14:37:20', 'salesorderapproval', 8, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(526, 1, '2017-08-01 14:38:06', 'salesorderapproval', 8, 'Update salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"0","remark":"ok","status_id":false}'),
(527, 1, '2017-08-01 14:38:52', 'salesorderapproval', 9, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"0","remark":"kurang lengkap","status_id":1}'),
(528, 1, '2017-08-01 14:38:52', 'salesorderapproval', 7, 'Update salesorderapprovaltable', '{"status_id":2}'),
(529, 1, '2017-08-01 14:39:04', 'salesorderapproval', 10, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(530, 1, '2017-08-01 15:10:46', 'salesorderapproval', 11, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"okkkk","status_id":1}'),
(531, 1, '2017-08-01 15:12:06', 'salesorderapproval', 12, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok ok","status_id":1}'),
(532, 1, '2017-08-01 15:14:55', 'salesorderapproval', 13, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"ok ok ok","status_id":1}'),
(533, 1, '2017-08-01 15:17:13', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(534, 1, '2017-08-01 15:21:01', 'salesorderapproval', 2, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"OK ok","status_id":1}'),
(535, 1, '2017-08-01 15:21:13', 'salesorderapproval', 6, 'Update salesorderapprovaltable', '{"status_id":2}'),
(536, 1, '2017-08-01 15:21:13', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"0","remark":"OK ok","status_id":false}'),
(537, 1, '2017-08-01 15:21:57', 'salesorderapproval', 3, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok ok","status_id":1}'),
(538, 1, '2017-08-01 15:22:44', 'salesorderapproval', 4, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"ok ok","status_id":1}'),
(539, 1, '2017-08-01 15:22:59', 'salesorderapproval', 6, 'Update salesorderapprovaltable', '{"status_id":2}'),
(540, 1, '2017-08-01 15:22:59', 'salesorderapproval', 4, 'Update salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"0","remark":"ok ok","status_id":false}'),
(541, 1, '2017-08-01 15:26:48', 'salesorderapproval', 5, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"Ok Ok","status_id":1}'),
(542, 1, '2017-08-01 15:27:01', 'salesorderapproval', 6, 'Update salesorderapprovaltable', '{"status_id":2}'),
(543, 1, '2017-08-01 15:27:01', 'salesorderapproval', 5, 'Update salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"0","remark":"Ok Ok","status_id":"1"}'),
(544, 1, '2017-08-01 15:30:13', 'salesorderapproval', 6, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"Ok Dech","status_id":1}'),
(545, 1, '2017-08-01 15:30:36', 'salesorderapproval', 6, 'Update salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"0","remark":"Pls add on site","status_id":"1"}'),
(546, 1, '2017-08-01 15:30:37', 'salesorderapproval', 6, 'Update salesorderapprovaltable', '{"status_id":2}'),
(547, 1, '2017-08-01 15:32:04', 'salesorderapproval', 7, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(548, 1, '2017-08-01 15:32:18', 'salesorderapproval', 8, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok ok","status_id":1}'),
(549, 1, '2017-08-01 15:32:57', 'salesorderapproval', 8, 'Update salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"0","remark":"kurang 1 baris","status_id":"1"}'),
(550, 1, '2017-08-01 15:32:57', 'salesorderapproval', 7, 'Update salesorderapprovaltable', '{"status_id":2}'),
(551, 1, '2017-08-01 15:33:28', 'salesorderapproval', 9, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(552, 1, '2017-08-01 15:37:07', 'salesorderapproval', 10, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(553, 1, '2017-08-01 15:37:15', 'salesorderapproval', 11, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(554, 1, '2017-08-01 15:38:11', 'salesorderapproval', 12, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(555, 1, '2017-08-01 15:38:18', 'salesorderapproval', 13, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(556, 1, '2017-08-01 15:41:17', 'salesorderapproval', 14, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(557, 1, '2017-08-01 15:45:41', 'salesorderapproval', 15, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"ok ok","status_id":1}'),
(558, 1, '2017-08-01 15:48:50', 'salesorderapproval', 16, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(559, 1, '2017-08-01 15:50:33', 'salesorderapproval', 17, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"ok ook","status_id":1}'),
(560, 1, '2017-08-01 15:53:32', 'salesorderapproval', 18, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"oooooo","status_id":1}'),
(561, 1, '2017-08-01 15:55:46', 'salesorderapproval', 19, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"okok","status_id":1}'),
(562, 1, '2017-08-01 15:58:42', 'salesorderapproval', 20, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"okok","status_id":1}'),
(563, 1, '2017-08-01 15:58:42', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(564, 1, '2017-08-01 15:58:42', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(565, 1, '2017-08-01 15:58:42', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(566, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(567, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(568, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(569, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(570, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(571, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(572, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(573, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(574, 1, '2017-08-01 15:58:43', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(575, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(576, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(577, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(578, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(579, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(580, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(581, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(582, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(583, 1, '2017-08-01 15:58:44', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(584, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(585, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(586, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(587, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(588, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(589, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(590, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(591, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(592, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(593, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(594, 1, '2017-08-01 15:58:45', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(595, 1, '2017-08-01 15:58:46', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(596, 1, '2017-08-01 15:58:46', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(597, 1, '2017-08-01 15:58:46', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(598, 1, '2017-08-01 15:58:46', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(599, 1, '2017-08-01 15:58:46', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(600, 1, '2017-08-01 15:58:46', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(601, 1, '2017-08-01 15:58:46', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"salesorder_no":false,"approvalflow_id":false,"approved_date":false,"approved_status":false,"remark":false,"status_id":false}'),
(602, 1, '2017-08-01 15:58:46', 'salesorderapproval', 0, 'Update salesorderapprovaltable', '{"status_id":2}'),
(603, 1, '2017-08-01 16:01:26', 'salesorderapproval', 21, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"okok","status_id":1}'),
(604, 1, '2017-08-01 16:03:27', 'salesorderapproval', 22, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"okok","status_id":1}'),
(605, 1, '2017-08-01 16:04:50', 'salesorderapproval', 23, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"okok","status_id":1}'),
(606, 1, '2017-08-01 16:05:26', 'salesorderapproval', 23, 'Update salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"0","remark":"okok","status_id":"1"}'),
(607, 1, '2017-08-01 16:05:26', 'salesorderapproval', 6, 'Update salesorderapprovaltable', '{"status_id":2}'),
(608, 1, '2017-08-01 16:06:27', 'salesorderapproval', 24, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(609, 1, '2017-08-01 16:06:37', 'salesorderapproval', 25, 'Insert salesorderapprovaltable', '{"salesorder_no":"0006\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-01","approved_status":"1","remark":"okok","status_id":1}'),
(610, 1, '2017-08-01 16:07:34', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":9}'),
(611, 1, '2017-08-01 16:21:22', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":1}'),
(612, 1, '2017-08-01 16:21:37', 'salesorder', 28, 'Update salesordertable', '{"approval_milestone_id":9}'),
(613, 1, '2017-08-01 16:22:57', 'salesorderapproval', 26, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-01","approved_status":"1","remark":"ok","status_id":1}'),
(614, 1, '2017-08-01 17:50:04', 'user', 4, 'Update usertable', '{"role_id":"13","name":"Joko","email":"joko@akses-prima.co.id","user_id":"salesdirector","pswrd":"81dc9bdb52d04dc20036dbd8313ed055","photo":"","status_id":"1"}'),
(615, 1, '2017-08-01 17:50:57', 'user', 3, 'Update usertable', '{"role_id":"12","name":"araf","email":"araf@akses-prima.co.id","user_id":"salesmanager","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(616, 1, '2017-08-01 17:51:21', 'user', 2, 'Update usertable', '{"role_id":"2","name":"Abay","email":"abay@akses-prima.co.id","user_id":"salesperson","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(617, 1, '2017-08-03 16:42:40', 'user', 4, 'Update usertable', '{"role_id":"13","name":"Joko","email":"makpui@indonesiancloud.com","user_id":"salesdirector","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(618, 1, '2017-08-03 16:43:10', 'user', 3, 'Update usertable', '{"role_id":"12","name":"araf","email":"makpui@indonesiancloud.com","user_id":"salesmanager","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(619, 1, '2017-08-03 16:43:35', 'user', 1, 'Update usertable', '{"role_id":"1","name":"John Doe","email":"ysfbryn@yahoo.com","user_id":"administrator","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(620, 2, '2017-08-03 16:45:11', 'user', 2, 'Update usertable', '{"role_id":"2","name":"Abay","email":"abay@akses-prima.co.id","user_id":"salesperson","pswrd":"d9b1d7db4cd6e70935368a1efb10e377","photo":"","status_id":"1"}'),
(621, 2, '2017-08-03 16:45:25', 'user', 2, 'Update usertable', '{"role_id":"2","name":"Abay","email":"abay@akses-prima.co.id","user_id":"salesperson","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(622, 2, '2017-08-03 16:46:03', 'role', 2, 'Update roletable', '{"txt":"Sales Person"}'),
(623, 2, '2017-08-03 16:47:20', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":0}'),
(624, 2, '2017-08-03 16:47:41', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":1}'),
(625, 2, '2017-08-03 16:53:35', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":1}'),
(626, 2, '2017-08-03 17:00:40', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":0}'),
(627, 2, '2017-08-03 17:01:10', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":1}'),
(628, 2, '2017-08-03 17:05:24', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":0}'),
(629, 2, '2017-08-03 17:07:54', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":1}'),
(630, 3, '2017-08-03 19:12:41', 'salesorderapproval', 27, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-03","approved_status":"1","remark":"ok","status_id":1}'),
(631, 4, '2017-08-04 11:44:52', 'salesorderapproval', 28, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-04","approved_status":"1","remark":"ok","status_id":1}'),
(632, 4, '2017-08-04 11:49:07', 'salesorderapproval', 29, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-04","approved_status":"1","remark":"ok","status_id":1}'),
(633, 4, '2017-08-04 14:23:38', 'salesorderapproval', 30, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-04","approved_status":"1","remark":"ok oke","status_id":1}'),
(634, 4, '2017-08-04 14:28:15', 'salesorderapproval', 31, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-04","approved_status":"1","remark":"okeh","status_id":1}'),
(635, 4, '2017-08-04 14:29:29', 'salesorderapproval', 32, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-04","approved_status":"1","remark":"okeh","status_id":1}'),
(636, 4, '2017-08-04 14:42:20', 'salesorderapproval', 33, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-04","approved_status":"1","remark":"okeh","status_id":1}'),
(637, 4, '2017-08-04 14:45:47', 'salesorderapproval', 34, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-04","approved_status":"1","remark":"okeh","status_id":1}'),
(638, 4, '2017-08-04 15:08:53', 'salesorderapproval', 35, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"2","approved_date":"2017-08-04","approved_status":"1","remark":"okeh","status_id":1}'),
(639, 2, '2017-08-04 15:13:17', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":0}'),
(640, 2, '2017-08-04 15:14:39', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":1}'),
(641, 3, '2017-08-04 15:19:39', 'salesorderapproval', 36, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-04","approved_status":"1","remark":"ok","status_id":1}'),
(642, 2, '2017-08-07 14:10:23', 'site', 9, 'Insert sitetable', '{"product_id":"1  ","tenant_id":"1  ","site_id_api":"PBC-JKT-001","site_name_api":"API_TEBET","site_id_tenant":"TLKM-JKT-0001","site_name_tenant":"TEBET","area_id":"2  ","region_id":"3  ","province_id":"3  ","longitude_ori":"256.990999","latitude_ori":"-13.6766","address":"Jl Tebet Raya No 201111111","sitetype_id":"1  ","towerheight_id":"3  ","transmissiontype_id":"1","remark":"na","status_id":"1"}'),
(643, 2, '2017-08-07 14:18:05', 'role', 5, 'Update roletable', '{"txt":"Project Manager"}'),
(644, 2, '2017-08-07 14:19:46', 'user', 5, 'Insert usertable', '{"role_id":"6","name":"Agus","email":"makpui@indonesiancloud.com","user_id":"projectsupport","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(645, 2, '2017-08-07 14:20:27', 'user', 6, 'Insert usertable', '{"role_id":"5","name":"Dewa","email":"makpui@indonesiancloud.com","user_id":"projectmanager","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(646, 2, '2017-08-07 14:21:18', 'user', 7, 'Insert usertable', '{"role_id":"14","name":"Andi","email":"makpui@indonesiancloud.com","user_id":"projectdirector","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":"1"}'),
(647, 2, '2017-08-07 14:21:57', 'workorderapprovalflow', 1, 'Update workorderapprovalflowtable', '{"ordering":"1","role_id":"5","remark":"Approval Level 1"}'),
(648, 2, '2017-08-07 14:22:16', 'workorderapprovalflow', 2, 'Update workorderapprovalflowtable', '{"ordering":"2","role_id":"14","remark":"Approval Level 2"}'),
(649, 1, '2017-08-09 13:40:15', 'workorder', 5, 'Update workordertable', '{"workorder_no":"WO-005","workorder_date":"2017-07-13 00:00:00","vendor_id":"4","vendorcontact_id":"7","product_group":"abcefg","project_manager":"abde","remark":"na","workorder_milestone_id":"0","status_id":"1"}'),
(650, 5, '2017-08-10 13:39:48', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":0}'),
(651, 5, '2017-08-10 13:43:00', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":1}'),
(652, 5, '2017-08-10 13:44:18', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":0}'),
(653, 5, '2017-08-10 13:44:38', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":1}'),
(654, 6, '2017-08-10 13:47:32', 'workorderapproval', 2, 'Insert workorderapprovaltable', '{"workorder_no":"","approvalflow_id":"","approved_date":"2017-08-10","approved_status":"1","remark":"approved","status_id":1}'),
(655, 6, '2017-08-10 14:02:24', 'workorderapproval', 3, 'Insert workorderapprovaltable', '{"workorder_no":"","approvalflow_id":"","approved_date":"2017-08-10","approved_status":"1","remark":"approved","status_id":1}'),
(656, 6, '2017-08-10 14:13:33', 'workorderapproval', 4, 'Insert workorderapprovaltable', '{"workorder_no":"","approvalflow_id":"","approved_date":"2017-08-10","approved_status":"1","remark":"ok","status_id":1}'),
(657, 6, '2017-08-10 14:43:47', 'workorderapproval', 5, 'Insert workorderapprovaltable', '{"workorder_no":"WO-001","approvalflow_id":"1","approved_date":"2017-08-10","approved_status":"1","remark":"approved","status_id":1}'),
(658, 5, '2017-08-10 14:50:19', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":0}'),
(659, 5, '2017-08-10 14:50:39', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":1}'),
(660, 5, '2017-08-10 14:59:17', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":0}'),
(661, 5, '2017-08-10 14:59:33', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":1}'),
(662, 2, '2017-08-10 15:05:30', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":0}'),
(663, 2, '2017-08-10 15:06:16', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":1}'),
(664, 2, '2017-08-10 15:15:19', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":0}'),
(665, 2, '2017-08-10 15:16:10', 'salesorder', 29, 'Update salesordertable', '{"approval_milestone_id":1}'),
(666, 3, '2017-08-10 15:37:48', 'salesorderapproval', 37, 'Insert salesorderapprovaltable', '{"salesorder_no":"0007\\/SO\\/VII\\/2017","approvalflow_id":"1","approved_date":"2017-08-10","approved_status":"1","remark":"approved","status_id":1}'),
(667, 6, '2017-08-10 15:40:29', 'workorderapproval', 6, 'Insert workorderapprovaltable', '{"workorder_no":"WO-001","approvalflow_id":"1","approved_date":"2017-08-10","approved_status":"1","remark":"approved","status_id":1}'),
(668, 5, '2017-08-10 16:49:35', 'workorder', 6, 'Insert workordertable', '{"workorder_no":"Generated by system","workorder_date":"2017-08-10 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"TOWER","project_manager":"Asep","remark":"na","workorder_milestone_id":"","status_id":"1"}'),
(669, 5, '2017-08-10 16:50:11', 'workorder', 6, 'Delete workordertable', '""'),
(670, 5, '2017-08-10 16:50:55', 'workorder', 7, 'Insert workordertable', '{"workorder_no":"01\\/WO\\/VIII\\/2017","workorder_date":"2017-08-10 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"ABC","project_manager":"Asep","remark":"na","workorder_milestone_id":"","status_id":"1"}'),
(671, 5, '2017-08-10 16:52:45', 'workorder', 7, 'Delete workordertable', '""'),
(672, 5, '2017-08-10 16:52:59', 'workorder', 8, 'Insert workordertable', '{"workorder_no":"01\\/WO\\/VIII\\/2017","workorder_date":"2017-08-10 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"a","project_manager":"a","remark":"a","workorder_milestone_id":"","status_id":"1"}'),
(673, 5, '2017-08-10 16:57:56', 'workorder', 8, 'Delete workordertable', '""'),
(674, 5, '2017-08-10 17:04:25', 'workorder', 5, 'Delete workordertable', '""'),
(675, 5, '2017-08-10 17:04:33', 'workorder', 4, 'Delete workordertable', '""'),
(676, 5, '2017-08-10 17:05:36', 'workorderline', 19, 'Delete workorderline', '""'),
(677, 5, '2017-08-10 17:07:11', 'workorder', 3, 'Delete workordertable', '""'),
(678, 5, '2017-08-10 17:08:28', 'workorder', 1, 'Update workordertable', '{"workorder_milestone_id":0}'),
(679, 5, '2017-08-10 17:08:47', 'workorderline', 18, 'Delete workorderline', '""'),
(680, 5, '2017-08-10 17:09:26', 'workorderline', 17, 'Delete workorderline', '""'),
(681, 5, '2017-08-10 17:10:18', 'workorder', 1, 'Delete workordertable', '""'),
(682, 5, '2017-08-10 17:10:49', 'workorder', 9, 'Insert workordertable', '{"workorder_no":"0001\\/WO\\/VIII\\/2017","workorder_date":"2017-08-10 00:00:00","vendor_id":"1","vendorcontact_id":"1","product_group":"AAA","project_manager":"EDI","remark":"NA","workorder_milestone_id":"","status_id":"1"}'),
(683, 5, '2017-08-10 17:19:39', 'workorderline', 20, 'Insert workorderline', '{"workorder_no":"0001\\/WO\\/VIII\\/2017","site_id":"2","projectactivity_id":"1","start_date":"2017-08-10","end_date":"2017-08-10","status_id":"1"}'),
(684, 1, '2017-08-15 11:31:24', 'tenant', 1, 'Update tenanttable', '{"name":"Telkomsel","email":"info@telkomsel.com","phone_no":"1213212312","address":"Jalan Sudirman","status_id":"1"}'),
(685, 1, '2017-08-15 11:32:13', 'tenant', 1, 'Update tenanttable', '{"name":"Telkomsel","email":"info@telkomsel.com","phone_no":"021 345 6789","address":"Jalan Sudirman","status_id":"1"}'),
(686, 1, '2017-08-15 11:40:24', 'salesorder', 27, 'Update salesordertable', '{"salesorder_no":"0009\\/SO\\/VIII\\/2017","salesorder_date":"2017-08-15","drm_date":"2017-08-15","batch":"5","po_tenant_no":"TLKM-001","po_tenant_date":"2017-08-15","tenant_id":"1","approval_milestone_id":"0","status_id":"1"}'),
(687, 1, '2017-08-15 11:40:49', 'salesorder', 27, 'Update salesordertable', '{"salesorder_no":"00010\\/SO\\/VIII\\/2017","salesorder_date":"2017-08-15","drm_date":"2017-08-15","batch":"5","po_tenant_no":"TSEL-001","po_tenant_date":"2017-08-15","tenant_id":"1","approval_milestone_id":"0","status_id":"1"}'),
(688, 1, '2017-08-15 11:54:45', 'salesorderline', 21, 'Insert salesorderline', '{"salesorder_no":"00010\\/SO\\/VIII\\/2017","product_id":"1","site_id_tenant":"TSEL-0001","site_name_tenant":"Mampang Prapatan","site_id_api":"PAA-MDN-001","site_name_api":"API_Mampang Prapatan","sitetype_id":"1","towerheight_id":"3","latitude":"-12.34566","longitude":"10.9889","area_id":"1","region_id":"1","province_id":"1","baks":"2017-08-16","rfi":"2017-08-15","baps":"2017-08-17","remarks":"NA","rent_period":"5","status_id":"1"}'),
(689, 2, '2017-08-15 14:09:34', 'salesorder', 27, 'Update salesordertable', '{"approval_milestone_id":1}'),
(690, 3, '2017-08-15 14:14:11', 'salesorderapproval', 38, 'Insert salesorderapprovaltable', '{"salesorder_no":"00010\\/SO\\/VIII\\/2017","approvalflow_id":"1","approved_date":"2017-08-15","approved_status":"1","remark":"approved","status_id":1}'),
(691, 5, '2017-08-15 14:23:56', 'site', 10, 'Insert sitetable', '{"product_id":"1  ","tenant_id":"1  ","site_id_api":"PAA-MDN-001","site_name_api":"API_Mampang Prapatan","site_id_tenant":"TSEL-0001","site_name_tenant":"Mampang Prapatan","area_id":"1  ","region_id":"1  ","province_id":"1  ","longitude_ori":"10.9889","latitude_ori":"-12.34566","address":"Jl Mampang Prapatan Medan Baru Sumut","sitetype_id":"1  ","towerheight_id":"3  ","transmissiontype_id":"2","remark":"NA","status_id":"1"}'),
(692, 5, '2017-08-15 14:30:04', 'workorderline', 20, 'Delete workorderline', '""'),
(693, 5, '2017-08-15 14:33:38', 'workorderline', 21, 'Insert workorderline', '{"workorder_no":"0001\\/WO\\/VIII\\/2017","site_id":"10","projectactivity_id":"1","start_date":"2017-08-16","end_date":"2017-08-17","status_id":"1"}'),
(694, 5, '2017-08-15 14:33:45', 'workorder', 9, 'Update workordertable', '{"workorder_milestone_id":1}'),
(695, 6, '2017-08-15 14:35:25', 'workorderapproval', 1, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/VIII\\/2017","approvalflow_id":"1","approved_date":"2017-08-15","approved_status":"1","remark":"approved","status_id":1}'),
(696, 1, '2017-08-31 10:41:17', 'salesorder', 31, 'Insert salesordertable', '{"salesorder_no":"0009\\/SO\\/VIII\\/2017","salesorder_date":"2017-08-31","drm_date":"2017-08-31","batch":"1","po_tenant_no":"abc","po_tenant_date":"2017-08-31","tenant_id":"1","approval_milestone_id":"","status_id":"1"}'),
(697, 1, '2017-08-31 10:41:57', 'salesorder', 31, 'Delete salesordertable', '""'),
(698, 2, '2017-09-04 13:17:35', 'salesorder', 31, 'Insert salesordertable', '{"salesorder_no":"0009\\/SO\\/IX\\/2017","salesorder_date":"2017-09-04","drm_date":"2017-09-04","batch":"1","po_tenant_no":"abc","po_tenant_date":"2017-09-04","tenant_id":"1","approval_milestone_id":"","status_id":"1"}'),
(699, 2, '2017-09-04 13:27:20', 'salesorder', 31, 'Delete salesordertable', '""'),
(700, 2, '2017-09-04 17:22:42', 'salesorder', 32, 'Insert salesordertable', '{"salesorder_no":"0009\\/SO\\/IX\\/2017","salesorder_date":"2017-09-04","drm_date":"2017-09-04","batch":"1","po_tenant_no":"123","po_tenant_date":"2017-09-04","tenant_id":"1","approval_milestone_id":0,"status_id":1}');
INSERT INTO `systemlogtable` (`row_id`, `created_by`, `created_date`, `module`, `key_id`, `action`, `data`) VALUES
(701, 1, '2017-09-06 10:16:36', 'product', 5, 'Insert producttable', '{"short_txt":"ABC","txt":"abcs","status_id":1}'),
(702, 1, '2017-09-06 10:17:01', 'product', 5, 'Update producttable', '{"short_txt":"A","txt":"abcsasdadasdasdfas","status_id":1}'),
(703, 1, '2017-09-06 10:17:15', 'product', 5, 'Update producttable', '{"short_txt":"Z","txt":"abcsasdadasdasdfas","status_id":1}'),
(704, 1, '2017-09-06 10:17:31', 'product', 5, 'Update producttable', '{"short_txt":"Z","txt":"abcsasdadasdasdfas","status_id":2}'),
(705, 1, '2017-09-06 10:17:40', 'product', 5, 'Delete producttable', '""'),
(706, 1, '2017-09-06 10:22:51', 'workorder', 10, 'Insert workordertable', '{"workorder_no":"0002\\/WO\\/IX\\/2017","workorder_date":"2017-09-06 00:00:00","vendor_id":3,"vendorcontact_id":4,"product_group":"ABC","project_manager":"Adul","remark":"na","workorder_milestone_id":0,"status_id":1}'),
(707, 1, '2017-09-06 10:23:17', 'workorderline', 22, 'Insert workorderline', '{"workorder_no":"0002\\/WO\\/IX\\/2017","site_id":1,"projectactivity_id":1,"start_date":"2017-09-06","end_date":"2017-09-06","status_id":1}'),
(708, 1, '2017-09-06 11:09:00', 'workorder', 11, 'Insert workordertable', '{"workorder_no":"0003\\/WO\\/IX\\/2017","workorder_date":"2017-09-06 00:00:00","vendor_id":1,"vendorcontact_id":1,"product_group":"afaczcz","project_manager":"czczzc","remark":"czcxzc","workorder_milestone_id":0,"status_id":1}'),
(709, 2, '2017-09-06 13:50:51', 'salesorderline', 22, 'Insert salesorderline', '{"salesorder_no":"0009\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"TLKM-003","site_name_tenant":"DESA-GUE","site_id_api":"PAA-MDN-002","site_name_api":"API_DESA-GUE","sitetype_id":1,"towerheight_id":3,"latitude":-0,"longitude":12,"area_id":1,"region_id":1,"province_id":1,"baks":"2017-09-06","rfi":"2017-09-06","baps":"2017-09-06","remarks":"NA","rent_period":3,"status_id":1}'),
(710, 1, '2017-09-07 11:29:33', 'salesorder', 1, 'Insert salesordertable', '{"salesorder_no":"0001\\/SO\\/IX\\/2017","salesorder_date":"2017-09-07","drm_date":"2017-09-07","batch":"2","po_tenant_no":"111","po_tenant_date":"2017-09-07","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(711, 1, '2017-09-07 11:30:11', 'salesorder', 1, 'Delete salesordertable', '""'),
(712, 2, '2017-09-07 11:34:51', 'salesorder', 2, 'Insert salesordertable', '{"salesorder_no":"0001\\/SO\\/IX\\/2017","salesorder_date":"2017-09-07","drm_date":"2017-09-07","batch":"1","po_tenant_no":"TLKM-0001","po_tenant_date":"2017-09-07","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(713, 2, '2017-09-07 11:36:36', 'salesorderline', 1, 'Insert salesorderline', '{"salesorder_no":"0001\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"TLKM-0001","site_name_tenant":"ABC","site_id_api":"PAA-MDN-001","site_name_api":"API_ABC","sitetype_id":1,"towerheight_id":3,"latitude":-11.99,"longitude":11.44555,"area_id":1,"region_id":1,"province_id":1,"baks":"2017-09-07","rfi":"2017-09-07","baps":"2017-09-07","remarks":"PO TELKOM 0001","rent_period":5,"status_id":1}'),
(714, 2, '2017-09-07 11:36:49', 'salesorder', 2, 'Update salesordertable', '{"approval_milestone_id":1}'),
(715, 3, '2017-09-07 11:43:18', 'salesorderapproval', 39, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/IX\\/2017","approvalflow_id":1,"approved_date":"2017-09-07","approved_status":"1","remark":"Sales Manager Approval","status_id":1}'),
(716, 3, '2017-09-08 14:51:17', 'workorder', 12, 'Insert workordertable', '{"workorder_no":"0004\\/WO\\/IX\\/2017","workorder_date":"2017-09-08 00:00:00","vendor_id":1,"vendorcontact_id":1,"product_group":"abc","project_manager":"asepo","remark":"rem","workorder_milestone_id":0,"status_id":1}'),
(717, 3, '2017-09-08 14:52:06', 'workorder', 12, 'Delete workordertable', '""'),
(718, 5, '2017-09-08 17:24:19', 'workorderline', 23, 'Insert workorderline', '{"workorder_no":"0003\\/WO\\/IX\\/2017","site_id":2,"projectactivity_id":1,"start_date":"2017-09-08","end_date":"2017-09-09","status_id":1}'),
(719, 5, '2017-09-11 09:16:41', 'workorder', 11, 'Update workordertable', '{"workorder_milestone_id":1}'),
(720, 6, '2017-09-11 09:19:18', 'workorderapproval', 2, 'Insert workorderapprovaltable', '{"workorder_no":"0003\\/WO\\/IX\\/2017","approvalflow_id":1,"approved_date":"2017-09-11","approved_status":1,"remark":"Approved by Project Manager","status_id":1}'),
(721, 7, '2017-09-11 09:20:57', 'workorderapproval', 3, 'Insert workorderapprovaltable', '{"workorder_no":"0003\\/WO\\/IX\\/2017","approvalflow_id":2,"approved_date":"2017-09-11","approved_status":1,"remark":"Approved by Project Director","status_id":1}'),
(722, 7, '2017-09-11 09:25:36', 'workorder', 11, 'Update workordertable', '{"workorder_milestone_id":9}'),
(723, 7, '2017-09-11 09:55:27', 'workorder', 11, 'Update workordertable', '{"workorder_milestone_id":1}'),
(724, 7, '2017-09-11 09:55:47', 'workorder', 11, 'Update workordertable', '{"workorder_milestone_id":9}'),
(725, 4, '2017-09-11 09:58:10', 'salesorderapproval', 40, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/IX\\/2017","approvalflow_id":2,"approved_date":"2017-09-11","approved_status":"1","remark":"Approved by Sales Director","status_id":1}'),
(726, 4, '2017-09-11 09:58:22', 'salesorder', 2, 'Update salesordertable', '{"approval_milestone_id":9}'),
(727, 4, '2017-09-11 09:58:37', 'salesorder', 2, 'Update salesordertable', '{"approval_milestone_id":1}'),
(728, 4, '2017-09-11 09:58:55', 'salesorder', 2, 'Update salesordertable', '{"approval_milestone_id":9}'),
(729, 5, '2017-09-11 16:31:57', 'workorderline', 24, 'Insert workorderline', '{"workorder_no":"0002\\/WO\\/IX\\/2017","site_id":7,"projectactivity_id":1,"start_date":"2017-09-11","end_date":"2017-09-12","status_id":1}'),
(730, 5, '2017-09-11 16:32:13', 'workorderline', 22, 'Delete workorderline', '""'),
(731, 7, '2017-09-11 16:38:19', 'workorder', 11, 'Update workordertable', '{"workorder_milestone_id":1}'),
(732, 4, '2017-09-13 13:30:37', 'salesorder', 2, 'Update salesordertable', '{"approval_milestone_id":1}'),
(733, 2, '2017-09-22 10:54:50', 'salesorder', 3, 'Insert salesordertable', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","salesorder_date":"2017-09-22","drm_date":"1970-01-01","batch":"1","po_tenant_no":"TLKM002","po_tenant_date":"2017-09-20","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(734, 2, '2017-09-22 10:56:45', 'salesorderline', 2, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"ABC","site_id_api":"PAA-MDN-002","site_name_api":"API_ABC","sitetype_id":1,"towerheight_id":3,"latitude":-12.0988,"longitude":12.0099,"area_id":1,"region_id":1,"province_id":1,"baks":"22-09-2017","rfi":"21-09-2017","baps":"23-09-2017","remarks":"","rent_period":5,"status_id":1}'),
(735, 2, '2017-09-22 10:57:13', 'salesorder', 3, 'Update salesordertable', '{"approval_milestone_id":1}'),
(736, 2, '2017-09-22 10:58:09', 'salesorder', 3, 'Update salesordertable', '{"approval_milestone_id":0}'),
(737, 2, '2017-09-22 10:58:30', 'salesorderline', 2, 'Delete salesorderline', '""'),
(738, 2, '2017-09-22 11:00:28', 'salesorder', 3, 'Delete salesordertable', '""'),
(739, 2, '2017-09-22 11:01:18', 'salesorder', 4, 'Insert salesordertable', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","salesorder_date":"2017-09-22","drm_date":"1970-01-01","batch":"3","po_tenant_no":"TLKM-0012","po_tenant_date":"2017-09-10","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(740, 2, '2017-09-22 14:25:16', 'salesorderline', 3, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-002","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-12.3199,"longitude":11.299,"area_id":1,"region_id":1,"province_id":1,"baks":"24-09-2017","rfi":"23-09-2017","baps":"30-09-2017","remarks":"na","rent_period":2,"status_id":1}'),
(741, 2, '2017-09-22 14:28:04', 'salesorderline', 3, 'Delete salesorderline', '""'),
(742, 2, '2017-09-22 14:33:44', 'salesorderline', 4, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-002","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-11.22,"longitude":12.12,"area_id":1,"region_id":1,"province_id":1,"baks":"25-09-2017","rfi":"24-09-2017","baps":"26-09-2017","remarks":"","rent_period":5,"status_id":1}'),
(743, 2, '2017-09-22 14:34:51', 'salesorderline', 4, 'Delete salesorderline', '""'),
(744, 2, '2017-09-22 14:36:32', 'salesorderline', 5, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-002","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-12.12,"longitude":22.11,"area_id":1,"region_id":1,"province_id":1,"baks":"24-09-2017","rfi":"23-09-2017","baps":"25-09-2017","remarks":"","rent_period":2,"status_id":1}'),
(745, 2, '2017-09-22 14:39:08', 'salesorderline', 5, 'Delete salesorderline', '""'),
(746, 2, '2017-09-22 14:40:56', 'salesorderline', 6, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-002","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-2.22,"longitude":12.12,"area_id":1,"region_id":1,"province_id":1,"baks":"24-09-2017","rfi":"23-09-2017","baps":"25-09-2017","remarks":"","rent_period":2,"status_id":1}'),
(747, 2, '2017-09-22 14:42:26', 'salesorderline', 6, 'Delete salesorderline', '""'),
(748, 2, '2017-09-22 14:43:42', 'salesorderline', 7, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-002","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-12.22,"longitude":22.11,"area_id":1,"region_id":1,"province_id":1,"baks":"24-09-2017","rfi":"23-09-2017","baps":"25-09-2017","remarks":"","rent_period":3,"status_id":1}'),
(749, 2, '2017-09-22 14:47:40', 'salesorderline', 7, 'Delete salesorderline', '""'),
(750, 2, '2017-09-22 14:49:10', 'salesorderline', 8, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-002","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-12.23,"longitude":22.21,"area_id":1,"region_id":1,"province_id":1,"baks":"24-09-2017","rfi":"23-09-2017","baps":"25-09-2017","remarks":"","rent_period":2,"status_id":1}'),
(751, 2, '2017-09-22 14:49:38', 'salesorderline', 8, 'Delete salesorderline', '""'),
(752, 2, '2017-09-22 15:02:09', 'salesorderline', 2, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-002","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-12.22,"longitude":12.12,"area_id":1,"region_id":1,"province_id":1,"baks":"24-09-2017","rfi":"23-09-2017","baps":"25-09-2017","remarks":"","rent_period":2,"status_id":1}'),
(753, 2, '2017-09-22 15:02:53', 'salesorderline', 2, 'Delete salesorderline', '""'),
(754, 2, '2017-09-22 15:36:34', 'salesorderline', 3, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-002","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-12.22,"longitude":-21.22,"area_id":1,"region_id":1,"province_id":1,"baks":"24-09-2017","rfi":"23-09-2017","baps":"25-09-2017","remarks":"","rent_period":3,"status_id":1}'),
(755, 2, '2017-09-22 15:36:57', 'salesorder', 4, 'Update salesordertable', '{"approval_milestone_id":1}'),
(756, 3, '2017-09-22 15:39:31', 'salesorderapproval', 40, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","approvalflow_id":1,"approved_date":"22-09-2017","approved_status":"1","remark":"Approved by Sales Manager","status_id":1}'),
(757, 2, '2017-09-22 16:04:58', 'salesorder', 5, 'Insert salesordertable', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","salesorder_date":"2017-09-22","drm_date":"1970-01-01","batch":"1","po_tenant_no":"123","po_tenant_date":"2017-09-19","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(758, 2, '2017-09-22 16:05:55', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0004\\/SO\\/IX\\/2017","salesorder_date":"2017-09-22","drm_date":"1970-01-01","batch":"1","po_tenant_no":"123","po_tenant_date":"2017-09-19","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(759, 2, '2017-09-22 17:01:35', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0005\\/SO\\/IX\\/2017","salesorder_date":"2017-09-22","drm_date":"1970-01-01","batch":"1","po_tenant_no":"123","po_tenant_date":"2017-09-19","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(760, 2, '2017-09-22 17:07:09', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0006\\/SO\\/I\\/1970","salesorder_date":"222017-09-","drm_date":"192017-09-","batch":"1","po_tenant_no":"123","po_tenant_date":"192017-09-","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(761, 2, '2017-09-22 17:09:18', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0007\\/SO\\/I\\/1970","salesorder_date":"011970-01-","drm_date":"011970-01-","batch":"1","po_tenant_no":"123","po_tenant_date":"011970-01-","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(762, 2, '2017-09-22 17:10:24', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0008\\/SO\\/I\\/1970","salesorder_date":"011970-01-","drm_date":"011970-01-","batch":"1","po_tenant_no":"123","po_tenant_date":"011970-01-","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(763, 2, '2017-09-22 17:12:30', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0009\\/SO\\/I\\/1970","salesorder_date":"01-01-1970","drm_date":"","batch":"1","po_tenant_no":"123","po_tenant_date":"","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(764, 2, '2017-09-22 17:15:07', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"00010\\/SO\\/IX\\/2017","salesorder_date":"22-09-2017","drm_date":"12-09-2017","batch":"1","po_tenant_no":"123","po_tenant_date":"12-09-2017","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(765, 2, '2017-09-22 17:20:09', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","salesorder_date":"22-09-2017","drm_date":"","batch":"1","po_tenant_no":"123","po_tenant_date":"12-09-2017","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(766, 2, '2017-09-22 17:31:46', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0004\\/SO\\/IX\\/2017","salesorder_date":"22-09-2017","drm_date":"","batch":"1","po_tenant_no":"123","po_tenant_date":"12-09-2017","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(767, 2, '2017-09-22 17:50:39', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0005\\/SO\\/IX\\/2017","salesorder_date":"0000-00-00","drm_date":null,"batch":"1","po_tenant_no":"123","po_tenant_date":null,"tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(768, 2, '2017-09-22 17:52:49', 'salesorder', 5, 'Update salesordertable', '{"salesorder_no":"0006\\/SO\\/IX\\/2017","salesorder_date":"22-09-2017","drm_date":"0000-00-00","batch":"1","po_tenant_no":"123","po_tenant_date":"0000-00-00","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(769, 2, '2017-09-25 09:11:45', 'salesorder', 5, 'Delete salesordertable', '""'),
(770, 2, '2017-09-25 09:14:12', 'salesorder', 6, 'Insert salesordertable', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","salesorder_date":"25-09-2017","drm_date":"0000-00-00","batch":"1","po_tenant_no":"123","po_tenant_date":"20-09-2017","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(771, 2, '2017-09-25 09:20:12', 'salesorder', 6, 'Delete salesordertable', '""'),
(772, 2, '2017-09-25 09:21:05', 'salesorder', 7, 'Insert salesordertable', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","salesorder_date":"25-09-2017","drm_date":"0000-00-00","batch":"1","po_tenant_no":"123","po_tenant_date":"20-09-2017","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(773, 2, '2017-09-25 09:25:20', 'salesorder', 7, 'Delete salesordertable', '""'),
(774, 2, '2017-09-25 09:26:03', 'salesorder', 8, 'Insert salesordertable', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","salesorder_date":"2017-09-25","drm_date":"0000-00-00","batch":"1","po_tenant_no":"123","po_tenant_date":"2017-09-20","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(775, 2, '2017-09-25 09:51:31', 'salesorder', 8, 'Delete salesordertable', '""'),
(776, 2, '2017-09-25 09:52:03', 'salesorder', 9, 'Insert salesordertable', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","salesorder_date":"2017-09-22","drm_date":"0000-00-00","batch":"1","po_tenant_no":"123","po_tenant_date":"0000-00-00","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(777, 2, '2017-09-25 10:10:37', 'salesorder', 10, 'Insert salesordertable', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","salesorder_date":"2017-09-25","drm_date":"0000-00-00","batch":"1","po_tenant_no":"123","po_tenant_date":"0000-00-00","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(778, 2, '2017-09-25 10:54:24', 'salesorderline', 4, 'Insert salesorderline', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abcd","site_id_api":"PAA-MDN-003","site_name_api":"API_abcd","sitetype_id":1,"towerheight_id":3,"latitude":-12,"longitude":123,"area_id":1,"region_id":1,"province_id":1,"baks":"2017-09-27","rfi":"2017-09-26","baps":"2017-09-28","remarks":"","rent_period":5,"status_id":1}'),
(779, 2, '2017-09-25 10:58:17', 'salesorderline', 5, 'Insert salesorderline', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"345","site_name_tenant":"def","site_id_api":"PAA-MDN-004","site_name_api":"API_def","sitetype_id":1,"towerheight_id":3,"latitude":-11,"longitude":14,"area_id":1,"region_id":1,"province_id":1,"baks":"2017-09-28","rfi":"2017-09-27","baps":"2017-09-29","remarks":"","rent_period":5,"status_id":1}'),
(780, 2, '2017-09-25 11:04:32', 'salesorderline', 5, 'Delete salesorderline', '""'),
(781, 2, '2017-09-25 11:05:21', 'salesorderline', 4, 'Delete salesorderline', '""'),
(782, 2, '2017-09-25 11:08:09', 'salesorderline', 6, 'Insert salesorderline', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-003","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-11.2,"longitude":11.67,"area_id":1,"region_id":1,"province_id":1,"baks":"2017-09-27","rfi":"2017-09-26","baps":"2017-09-28","remarks":"atest","rent_period":2,"status_id":1}'),
(783, 2, '2017-09-25 11:08:27', 'salesorderline', 6, 'Delete salesorderline', '""'),
(784, 3, '2017-09-25 11:28:35', 'salesorderapproval', 40, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","approvalflow_id":1,"approved_date":"25-09-2017 12:00:00","approved_status":"1","remark":"Approved by Sales Manager","status_id":1}'),
(785, 3, '2017-09-25 11:31:18', 'salesorderapproval', 41, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","approvalflow_id":1,"approved_date":"25-09-2017 11:30:00","approved_status":"1","remark":"Approved by Sales Manager","status_id":1}'),
(786, 3, '2017-09-25 11:34:33', 'salesorderapproval', 42, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/IX\\/2017","approvalflow_id":1,"approved_date":"2017-09-25 12:00:00","approved_status":"1","remark":"Approved by Sales Manager","status_id":1}'),
(787, 2, '2017-09-25 13:43:39', 'salesorderline', 7, 'Insert salesorderline', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-003","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-12.22,"longitude":11.22,"area_id":1,"region_id":1,"province_id":1,"baks":"2017-09-27","rfi":"2017-09-26","baps":"2017-09-28","remarks":"na","rent_period":5,"status_id":1}'),
(788, 2, '2017-09-25 13:44:36', 'salesorderline', 7, 'Delete salesorderline', '""'),
(789, 2, '2017-09-25 13:45:55', 'salesorderline', 8, 'Insert salesorderline', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-003","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-11.233,"longitude":12.3454,"area_id":1,"region_id":1,"province_id":1,"baks":"2017-09-27","rfi":"2017-09-26","baps":"2017-09-28","remarks":"na","rent_period":2,"status_id":1}'),
(790, 2, '2017-09-25 13:46:52', 'salesorderline', 8, 'Delete salesorderline', '""'),
(791, 2, '2017-09-25 13:48:00', 'salesorderline', 9, 'Insert salesorderline', '{"salesorder_no":"0003\\/SO\\/IX\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"abc","site_id_api":"PAA-MDN-003","site_name_api":"API_abc","sitetype_id":1,"towerheight_id":3,"latitude":-11.22,"longitude":12.33,"area_id":1,"region_id":1,"province_id":1,"baks":"2017-09-27","rfi":"2017-09-26","baps":"2017-09-28","remarks":"na","rent_period":5,"status_id":1}'),
(792, 1, '2017-10-02 10:17:08', 'province', 1, 'Insert provincetable', '{"short_txt":"NAD","txt":"Nanggroe Aceh Darussalam","region_id":1,"status_id":1}'),
(793, 1, '2017-10-02 10:17:31', 'province', 2, 'Insert provincetable', '{"short_txt":"SUMUT","txt":"Sumatera Utara","region_id":1,"status_id":1}'),
(794, 1, '2017-10-02 10:24:55', 'province', 3, 'Insert provincetable', '{"short_txt":"SUMBAR","txt":"Sumatera Barat","region_id":10,"status_id":1}'),
(795, 1, '2017-10-02 10:25:44', 'province', 4, 'Insert provincetable', '{"short_txt":"RIAU","txt":"Riau","region_id":10,"status_id":1}'),
(796, 1, '2017-10-02 10:26:15', 'province', 5, 'Insert provincetable', '{"short_txt":"KEPRI","txt":"Kepulauan Riau","region_id":10,"status_id":1}'),
(797, 1, '2017-10-02 10:26:49', 'province', 6, 'Insert provincetable', '{"short_txt":"SUMSEL","txt":"Sumatera Selatan","region_id":2,"status_id":1}'),
(798, 1, '2017-10-02 10:27:13', 'province', 7, 'Insert provincetable', '{"short_txt":"BENGKU","txt":"Bengkulu","region_id":2,"status_id":1}'),
(799, 1, '2017-10-02 10:27:41', 'province', 8, 'Insert provincetable', '{"short_txt":"JAMB","txt":"Jambi","region_id":2,"status_id":1}'),
(800, 1, '2017-10-02 10:28:03', 'province', 9, 'Insert provincetable', '{"short_txt":"LAMP","txt":"Lampung","region_id":2,"status_id":1}'),
(801, 1, '2017-10-02 10:28:21', 'province', 10, 'Insert provincetable', '{"short_txt":"BABEL","txt":"Bangka Belitung","region_id":2,"status_id":1}'),
(802, 1, '2017-10-02 10:28:59', 'province', 11, 'Insert provincetable', '{"short_txt":"JKT","txt":"DKI Jakarta","region_id":3,"status_id":1}'),
(803, 1, '2017-10-02 10:29:50', 'province', 12, 'Insert provincetable', '{"short_txt":"BANTEN","txt":"Banten","region_id":4,"status_id":1}'),
(804, 1, '2017-10-02 10:30:12', 'province', 13, 'Insert provincetable', '{"short_txt":"JABAR","txt":"Jawa Barat","region_id":4,"status_id":1}'),
(805, 1, '2017-10-02 10:31:17', 'province', 14, 'Insert provincetable', '{"short_txt":"JATENG","txt":"Jawa Tengah","region_id":5,"status_id":1}'),
(806, 1, '2017-10-02 10:32:00', 'province', 15, 'Insert provincetable', '{"short_txt":"DIY","txt":"DI Yogyakarta","region_id":5,"status_id":1}'),
(807, 1, '2017-10-02 10:32:18', 'province', 16, 'Insert provincetable', '{"short_txt":"JATIM","txt":"Jawa Timur","region_id":6,"status_id":1}'),
(808, 1, '2017-10-02 10:32:55', 'province', 17, 'Insert provincetable', '{"short_txt":"BALI","txt":"Bali","region_id":7,"status_id":1}'),
(809, 1, '2017-10-02 10:33:22', 'province', 18, 'Insert provincetable', '{"short_txt":"NTT","txt":"Nusa Tenggara Timue","region_id":7,"status_id":1}'),
(810, 1, '2017-10-02 10:33:42', 'province', 19, 'Insert provincetable', '{"short_txt":"NTB","txt":"Nusa Tenggara Barat","region_id":7,"status_id":1}'),
(811, 1, '2017-10-02 10:34:16', 'province', 20, 'Insert provincetable', '{"short_txt":"GORON","txt":"Gorontalo","region_id":9,"status_id":1}'),
(812, 1, '2017-10-02 10:34:44', 'province', 21, 'Insert provincetable', '{"short_txt":"SULBAR","txt":"Sulawesi Barat","region_id":9,"status_id":1}'),
(813, 1, '2017-10-02 10:35:11', 'province', 22, 'Insert provincetable', '{"short_txt":"SULTENG","txt":"Sulawesi Tengah","region_id":9,"status_id":1}'),
(814, 1, '2017-10-02 10:35:54', 'province', 23, 'Insert provincetable', '{"short_txt":"SULUT","txt":"Sulawesi Utara","region_id":9,"status_id":1}'),
(815, 1, '2017-10-02 10:36:40', 'province', 24, 'Insert provincetable', '{"short_txt":"SULTENGGA","txt":"Sulawesi Tenggara","region_id":9,"status_id":1}'),
(816, 1, '2017-10-02 10:37:04', 'province', 25, 'Insert provincetable', '{"short_txt":"SULSEL","txt":"Sulawesi Selatan","region_id":9,"status_id":1}'),
(817, 1, '2017-10-02 10:42:37', 'province', 26, 'Insert provincetable', '{"short_txt":"MALUT","txt":"Maluku Utara","region_id":11,"status_id":1}'),
(818, 1, '2017-10-02 10:42:53', 'province', 27, 'Insert provincetable', '{"short_txt":"MALUKU","txt":"Maluku","region_id":11,"status_id":1}'),
(819, 1, '2017-10-02 10:43:17', 'province', 28, 'Insert provincetable', '{"short_txt":"PABAR","txt":"Papua Barat","region_id":11,"status_id":1}'),
(820, 1, '2017-10-02 10:43:37', 'province', 29, 'Insert provincetable', '{"short_txt":"PAPUA","txt":"Papua","region_id":11,"status_id":1}'),
(821, 1, '2017-10-02 10:43:58', 'province', 30, 'Insert provincetable', '{"short_txt":"KALBAR","txt":"Kalimantan Barat","region_id":8,"status_id":1}'),
(822, 1, '2017-10-02 10:44:21', 'province', 31, 'Insert provincetable', '{"short_txt":"KALTIM","txt":"Kalimantan Timur","region_id":8,"status_id":1}'),
(823, 1, '2017-10-02 10:44:40', 'province', 32, 'Insert provincetable', '{"short_txt":"KALSEL","txt":"Kalimantan Selatan","region_id":8,"status_id":1}'),
(824, 1, '2017-10-02 10:45:08', 'province', 33, 'Insert provincetable', '{"short_txt":"KALTENG","txt":"Kalimantan Tengah","region_id":8,"status_id":1}'),
(825, 1, '2017-10-02 10:45:35', 'province', 34, 'Insert provincetable', '{"short_txt":"KALUT","txt":"Kalimantan Utara","region_id":8,"status_id":1}'),
(826, 1, '2017-10-02 14:56:33', 'city', 1, 'Update citytable', '{"short_txt":"MDN","txt":"Medan","province_id":2,"status_id":1}'),
(827, 1, '2017-10-02 15:01:20', 'city', 2, 'Update citytable', '{"short_txt":"PKU","txt":"Pekanbaru","province_id":4,"status_id":1}'),
(828, 1, '2017-10-02 15:02:22', 'city', 2, 'Update citytable', '{"short_txt":"PBR","txt":"Pekanbaru","province_id":4,"status_id":1}'),
(829, 1, '2017-10-02 15:09:42', 'city', 3, 'Update citytable', '{"short_txt":"JKT","txt":"Jakarta","province_id":11,"status_id":1}'),
(830, 1, '2017-10-02 15:10:13', 'city', 4, 'Update citytable', '{"short_txt":"BDG","txt":"Bandung","province_id":13,"status_id":1}'),
(831, 1, '2017-10-02 15:11:06', 'city', 5, 'Update citytable', '{"short_txt":"SMG","txt":"Semarang","province_id":14,"status_id":1}'),
(832, 1, '2017-10-02 15:11:51', 'city', 6, 'Update citytable', '{"short_txt":"SBY","txt":"Surabaya","province_id":16,"status_id":1}'),
(833, 1, '2017-10-02 15:12:30', 'city', 7, 'Update citytable', '{"short_txt":"DPR","txt":"Denpasar","province_id":17,"status_id":1}'),
(834, 1, '2017-10-02 15:13:32', 'city', 8, 'Update citytable', '{"short_txt":"BPP","txt":"Balikpapan","province_id":31,"status_id":1}'),
(835, 1, '2017-10-02 15:14:35', 'city', 9, 'Update citytable', '{"short_txt":"MKS","txt":"Makasar","province_id":25,"status_id":1}'),
(836, 1, '2017-10-02 15:15:31', 'city', 10, 'Update citytable', '{"short_txt":"PLG","txt":"Palembang","province_id":6,"status_id":1}'),
(837, 1, '2017-10-02 15:16:25', 'city', 11, 'Update citytable', '{"short_txt":"SON","txt":"Sorong","province_id":28,"status_id":1}'),
(838, 1, '2017-10-02 15:24:59', 'city', 12, 'Insert citytable', '{"short_txt":"BNA","txt":"Banda Aceh","province_id":1,"status_id":1}'),
(839, 1, '2017-10-02 19:03:47', 'salesorder', 11, 'Insert salesordertable', '{"salesorder_no":"0004\\/SO\\/XI\\/2017","salesorder_date":"2017-10-02","drm_date":"0000-00-00","batch":"1","po_tenant_no":"123","po_tenant_date":"1970-01-01","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(840, 2, '2017-10-03 10:21:11', 'salesorderline', 10, 'Insert salesorderline', '{"salesorder_no":"0004\\/SO\\/XI\\/2017","product_id":1,"site_id_tenant":"123","site_name_tenant":"ABC","site_id_api":"PAA-MDN-004","site_name_api":"API_ABC","sitetype_id":1,"towerheight_id":3,"latitude":-11.989898,"longitude":12.98989,"area_id":1,"region_id":1,"province_id":1,"city_id":12,"baks":"2017-10-05","rfi":"2017-10-04","baps":"2017-10-06","remarks":"na","rent_period":2,"status_id":1}'),
(841, 1, '2017-10-03 10:38:19', 'workorder', 11, 'Update workordertable', '{"workorder_milestone_id":0}'),
(842, 1, '2017-10-03 15:23:33', 'site', 11, 'Insert sitetable', '{"product_id":1,"tenant_id":1,"site_id_api":"PAA-MDN-001","site_name_api":"API_ABC","site_id_tenant":"TLKM-0001","site_name_tenant":"ABC","area_id":1,"region_id":1,"province_id":1,"city_id":1,"longitude_ori":11.44555,"latitude_ori":-11.99,"address":"jl haha hihi","sitetype_id":1,"towerheight_id":3,"transmissiontype_id":1,"remark":"remarks","status_id":1}'),
(843, 2, '2017-10-05 13:25:41', 'password', 2, 'Update usertable', '{"pswrd":"698d51a19d8a121ce581499d7b701668"}'),
(844, 2, '2017-10-05 13:26:56', 'password', 2, 'Update usertable', '{"pswrd":"202cb962ac59075b964b07152d234b70"}'),
(845, 1, '2017-10-05 14:08:04', 'user', 8, 'Insert usertable', '{"role_id":8,"name":"Operation Director","email":"operationdirector@akses-prima.co.id","user_id":"operationdirector","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":1}'),
(846, 1, '2017-10-05 14:09:00', 'user', 9, 'Insert usertable', '{"role_id":9,"name":"President Director","email":"presdir@akses-prima.co.id","user_id":"presidentdirector","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":1}'),
(847, 1, '2017-10-05 14:23:12', 'user', 10, 'Insert usertable', '{"role_id":10,"name":"President Director","email":"presdir@akses-prima.co.id","user_id":"presdir","pswrd":"202cb962ac59075b964b07152d234b70","photo":"","status_id":1}'),
(848, 1, '2017-10-05 14:24:05', 'user', 10, 'Update usertable', '{"role_id":10,"name":"President Director","email":"presdir@akses-prima.co.id","user_id":"presidentdirector","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(849, 1, '2017-10-05 14:43:27', 'salesorderapprovalflow', 2, 'Update salesorderapprovalflowtable', '{"ordering":2,"role_id":6,"remark":"Project Manager"}'),
(850, 1, '2017-10-05 14:43:55', 'salesorderapprovalflow', 3, 'Insert salesorderapprovalflowtable', '{"ordering":3,"role_id":4,"remark":"GM Sales"}'),
(851, 1, '2017-10-05 14:44:21', 'salesorderapprovalflow', 4, 'Insert salesorderapprovalflowtable', '{"ordering":4,"role_id":9,"remark":"Operation Director"}'),
(852, 1, '2017-10-05 14:44:51', 'salesorderapprovalflow', 5, 'Insert salesorderapprovalflowtable', '{"ordering":5,"role_id":10,"remark":"President Director"}'),
(853, 1, '2017-10-05 14:57:43', 'city', 1, 'Update citytable', '{"short_txt":"BNA","txt":"Banda Aceh","province_id":1,"status_id":1}'),
(854, 1, '2017-10-05 14:58:15', 'city', 2, 'Insert citytable', '{"short_txt":"BIR","txt":"Bireuen","province_id":1,"status_id":1}'),
(855, 1, '2017-10-05 14:58:47', 'city', 3, 'Insert citytable', '{"short_txt":"BKJ","txt":"Blangkejeren","province_id":1,"status_id":1}'),
(856, 1, '2017-10-05 15:01:54', 'city', 4, 'Insert citytable', '{"short_txt":"BPD","txt":"Blangpidie","province_id":1,"status_id":1}'),
(857, 1, '2017-10-05 15:03:05', 'city', 5, 'Insert citytable', '{"short_txt":"CAG","txt":"Calang","province_id":1,"status_id":1}'),
(858, 1, '2017-10-05 15:50:47', 'city', 6, 'Insert citytable', '{"short_txt":"JTH","txt":"Jantho","province_id":1,"status_id":1}'),
(859, 1, '2017-10-05 15:51:23', 'city', 7, 'Insert citytable', '{"short_txt":"KRB","txt":"Karang Baru","province_id":1,"status_id":1}'),
(860, 1, '2017-10-05 15:51:49', 'city', 8, 'Insert citytable', '{"short_txt":"KTN","txt":"Kutacane","province_id":1,"status_id":1}'),
(861, 1, '2017-10-05 15:52:14', 'city', 9, 'Insert citytable', '{"short_txt":"LGS","txt":"Langsa","province_id":1,"status_id":1}'),
(862, 1, '2017-10-09 11:01:18', 'towerheight', 3, 'Update towerheighttable', '{"product_id":1,"txt":"12 Meter m","status_id":1}'),
(863, 1, '2017-10-09 11:04:00', 'towerheight', 3, 'Update towerheighttable', '{"product_id":1,"txt":"12 Meter","status_id":1}'),
(864, 1, '2017-10-09 11:07:32', 'towerheight', 4, 'Insert towerheighttable', '{"product_id":1,"txt":"15 Meter","status_id":1}'),
(865, 1, '2017-10-09 11:11:26', 'towerheight', 5, 'Insert towerheighttable', '{"product_id":1,"txt":"17 Meter","status_id":1}'),
(866, 1, '2017-10-09 11:11:47', 'towerheight', 6, 'Insert towerheighttable', '{"product_id":1,"txt":"21 Meter","status_id":1}'),
(867, 1, '2017-10-09 11:29:48', 'product', 2, 'Update producttable', '{"short_txt":"N","txt":"Mini Macro","status_id":1}'),
(868, 1, '2017-10-09 11:30:12', 'product', 5, 'Insert producttable', '{"short_txt":"C","txt":"Collo","status_id":1}'),
(869, 1, '2017-10-09 11:32:15', 'product', 1, 'Update producttable', '{"short_txt":"P","txt":"MCP","status_id":1}'),
(870, 1, '2017-10-09 11:32:29', 'product', 3, 'Update producttable', '{"short_txt":"M","txt":"Macro","status_id":1}'),
(871, 1, '2017-10-09 11:33:06', 'towerheight', 7, 'Insert towerheighttable', '{"product_id":2,"txt":"27 Meter","status_id":1}'),
(872, 1, '2017-10-09 11:33:31', 'towerheight', 8, 'Insert towerheighttable', '{"product_id":2,"txt":"30 Meter","status_id":1}'),
(873, 1, '2017-10-09 11:34:06', 'towerheight', 9, 'Insert towerheighttable', '{"product_id":3,"txt":"42 Meter","status_id":1}'),
(874, 1, '2017-10-09 11:34:31', 'towerheight', 10, 'Insert towerheighttable', '{"product_id":3,"txt":"52 Meter","status_id":1}'),
(875, 1, '2017-10-09 11:35:04', 'towerheight', 11, 'Insert towerheighttable', '{"product_id":3,"txt":"62 Meter","status_id":1}'),
(876, 1, '2017-10-09 11:35:27', 'towerheight', 12, 'Insert towerheighttable', '{"product_id":3,"txt":"72 Meter","status_id":1}'),
(877, 1, '2017-10-09 11:35:43', 'towerheight', 13, 'Insert towerheighttable', '{"product_id":3,"txt":"82 Meter","status_id":1}'),
(878, 1, '2017-10-11 14:36:57', 'vendor', 1, 'Insert vendortable', '{"vendortype_id":2,"id":"ORLIE","name":"Orlie Indonesia","email":"na","phone_no":"021-29461743","address":"Jl RS Fatmawati Raya No 30E Cilandak Barat Cilandak Jakarta","status_id":1}'),
(879, 1, '2017-10-11 14:39:24', 'vendorcontact', 1, 'Insert vendorcontacttable', '{"vendor_id":1,"name":"Yesiskha Soeryo","position":"Direktur Utama","organization":"Direksi","email":"ysoeryo@orlie.co.id","mobile_no":"0811-885-2582 \\/ 0816-485-2585","status_id":1}'),
(880, 1, '2017-10-11 14:42:04', 'vendor', 2, 'Insert vendortable', '{"vendortype_id":2,"id":"BMG","name":"Bach Multi Global","email":"na","phone_no":"021-386-2451","address":"Jl Cideng Barat No 72 Jakarta Pusat","status_id":1}'),
(881, 1, '2017-10-11 14:43:46', 'vendorcontact', 2, 'Insert vendorcontacttable', '{"vendor_id":2,"name":"Indra Kurnia","position":"SACME Manager","organization":"SACME","email":"indra.kurnia@bachmultiglobal.co.id","mobile_no":"na","status_id":1}'),
(882, 1, '2017-10-11 14:44:23', 'vendor', 2, 'Update vendortable', '{"vendortype_id":2,"id":"BACH","name":"Bach Multi Global","email":"na","phone_no":"021-386-2451","address":"Jl Cideng Barat No 72 Jakarta Pusat","status_id":1}'),
(883, 1, '2017-10-11 14:46:41', 'vendor', 3, 'Insert vendortable', '{"vendortype_id":2,"id":"GLOBAL","name":"Global Comtech Sejahtera","email":"na","phone_no":"061-456-1620","address":"Jl Labu I No 13\\/35 Kelurahan Petisah Hulu Medan Sumatera Utara","status_id":1}'),
(884, 1, '2017-10-11 14:48:24', 'vendorcontact', 3, 'Insert vendorcontacttable', '{"vendor_id":3,"name":"Chyang","position":"Direktur Operasional","organization":"Operasional","email":"chyang@globalcomtech.co.id","mobile_no":"0812-606-1777","status_id":1}'),
(885, 1, '2017-10-11 15:04:53', 'vendor', 4, 'Insert vendortable', '{"vendortype_id":2,"id":"BANGKIT","name":"Bangkit Selaras Utama","email":"na","phone_no":"021-8265-0259","address":"Jl Aneka Elok No 6 RT 011\\/004 Kel Cipinang Muara Jatinegara Jakarta Timur","status_id":1}'),
(886, 1, '2017-10-11 15:06:09', 'vendorcontact', 4, 'Insert vendorcontacttable', '{"vendor_id":4,"name":"Fahmi Sukriatna","position":"Direktur Utama","organization":"Direksi","email":"fahmi@bsutama.com","mobile_no":"0811-888-2781 \\/ 0878-816-7808","status_id":1}'),
(887, 1, '2017-10-11 15:07:34', 'vendorcontact', 4, 'Update vendorcontacttable', '{"vendor_id":4,"name":"Fahmi Sukriatna","position":"Direktur Utama","organization":"Direksi","email":"fahmi@bsutama.com \\/ fahmi_787@yahoo.co.id","mobile_no":"0811-888-2781 \\/ 0878-816-7808","status_id":1}'),
(888, 1, '2017-10-11 15:09:57', 'vendor', 5, 'Insert vendortable', '{"vendortype_id":2,"id":"MULTI","name":"Multi Kreasi Utama","email":"na","phone_no":"021-8378-9208","address":"Gedung Menara MTH GF Jl MT Haryono Kav 23 Jakarta Selatan","status_id":1}'),
(889, 1, '2017-10-11 15:11:09', 'vendorcontact', 5, 'Insert vendorcontacttable', '{"vendor_id":5,"name":"Rizal Pahlevi","position":"General Manager","organization":"General","email":"rizal@mkinvestama.co.id","mobile_no":"na","status_id":1}'),
(890, 1, '2017-10-11 15:13:13', 'vendor', 6, 'Insert vendortable', '{"vendortype_id":2,"id":"ADYAWINSA","name":"Adyawinsa Telecommunication & Electrical","email":"na","phone_no":"021-460-3353","address":"Jl Pegangsaan Dua KM No 64 RT 005 RW 002","status_id":1}'),
(891, 1, '2017-10-11 15:15:19', 'vendorcontact', 6, 'Insert vendorcontacttable', '{"vendor_id":6,"name":"Rony Dosonugroho","position":"President Director","organization":"Direksi","email":"rony@adyawinsa.com","mobile_no":"0855-102-0320","status_id":1}'),
(892, 1, '2017-10-11 15:16:20', 'vendorcontact', 7, 'Insert vendorcontacttable', '{"vendor_id":6,"name":"Adi Prasetyo","position":"Account Manager","organization":"na","email":"adi@adyawinsa.com","mobile_no":"0811-175-5711","status_id":1}'),
(893, 1, '2017-10-11 15:53:40', 'vendor', 7, 'Insert vendortable', '{"vendortype_id":2,"id":"BINOTEL","name":"Binotel Arlintang Pratama","email":"na","phone_no":"0411-459-513","address":"Komplek BTN CV Dewi Blok B 5 No 13 Makasar","status_id":1}'),
(894, 1, '2017-10-11 15:55:11', 'vendorcontact', 8, 'Insert vendorcontacttable', '{"vendor_id":7,"name":"Abdul Rahim, ST","position":"na","organization":"na","email":"baimji@gmail.com","mobile_no":"0813-4385-8055 \\/ 0811-412-148","status_id":1}'),
(895, 1, '2017-10-11 15:56:55', 'vendorcontact', 9, 'Insert vendorcontacttable', '{"vendor_id":7,"name":"Muh Arham N, SE","position":"Project Manager","organization":"Project","email":"arhammuh21@gmail.com","mobile_no":"0852-3141-3512","status_id":1}'),
(896, 1, '2017-10-11 16:02:00', 'vendor', 8, 'Insert vendortable', '{"vendortype_id":2,"id":"ROBERTO","name":"Roberto Pratama Karya","email":"na","phone_no":"0751-461-668","address":"Maransi Indah No 129 RT 003\\/012 Kel Dadok Tunggul Hitam Kec Koto Tangah Padang","status_id":1}'),
(897, 1, '2017-10-11 16:03:03', 'vendorcontact', 10, 'Insert vendorcontacttable', '{"vendor_id":8,"name":"Surya Darma","position":"Direktur","organization":"Direksi","email":"surya.roberto@yahoo.com","mobile_no":"0812-6607-0558","status_id":1}'),
(898, 1, '2017-10-11 16:04:22', 'vendorcontact', 11, 'Insert vendorcontacttable', '{"vendor_id":8,"name":"Luki Al Hardi","position":"Project Manager","organization":"Project","email":"alhardiluki@gmail.com","mobile_no":"0811-660-0022","status_id":1}'),
(899, 1, '2017-10-11 16:06:35', 'vendor', 9, 'Insert vendortable', '{"vendortype_id":2,"id":"MANOLO","name":"Manolo Putra","email":"na","phone_no":"021-601-6317","address":"Sumber Endah No 20-12 Bandung","status_id":1}'),
(900, 1, '2017-10-11 16:07:58', 'vendorcontact', 12, 'Insert vendorcontacttable', '{"vendor_id":9,"name":"Hamonangan Pasaribu, Ir.","position":"General Manager","organization":"General","email":"monank_p1000@yahoo.com","mobile_no":"0813-1730-1965","status_id":1}'),
(901, 1, '2017-10-11 16:09:30', 'vendorcontact', 13, 'Insert vendorcontacttable', '{"vendor_id":9,"name":"Jhon Ramadhona Silitonga, Ir.","position":"Project Manager","organization":"Project","email":"jhon.r.silitonga@gmail.com","mobile_no":"0813-7563-9689","status_id":1}'),
(902, 1, '2017-10-11 16:22:56', 'vendor', 10, 'Insert vendortable', '{"vendortype_id":1,"id":"CELEBES","name":"Celebes Konstruksindo","email":"na","phone_no":"0451-413-4078 \\/ 021-569-54516","address":"Jl Tupai B\\/6 Tatura Selatan Palu Selatan Palu","status_id":1}'),
(903, 1, '2017-10-11 16:24:33', 'vendorcontact', 14, 'Insert vendorcontacttable', '{"vendor_id":10,"name":"R Darmawan","position":"na","organization":"na","email":"darmawan@cel-kon.net","mobile_no":"0811-189-6072 \\/ 021-29490587","status_id":1}'),
(904, 1, '2017-10-11 16:25:40', 'vendorcontact', 15, 'Insert vendorcontacttable', '{"vendor_id":10,"name":"Ibrahim Adjie","position":"SACME Project Manager","organization":"Project","email":"adjie.ibrahim@cel-kon.net","mobile_no":"0853-1761-0616","status_id":1}'),
(905, 1, '2017-10-11 16:27:52', 'vendor', 11, 'Insert vendortable', '{"vendortype_id":2,"id":"QUADRATEL","name":"Quadratel Persada","email":"na","phone_no":"021-7919-1955 \\/ 021-798-9011","address":"Gedung Fortune Jl Mampang Prapatan Raya No 96 Jakarta Selatan","status_id":1}'),
(906, 1, '2017-10-11 16:28:57', 'vendorcontact', 16, 'Insert vendorcontacttable', '{"vendor_id":11,"name":"Sigit Priawanto","position":"Direktur","organization":"Direksi","email":"sigit@quardratel.co.id","mobile_no":"0812-107-6600","status_id":1}'),
(907, 1, '2017-10-11 16:30:00', 'vendorcontact', 17, 'Insert vendorcontacttable', '{"vendor_id":11,"name":"Ida Nurviani","position":"Account","organization":"na","email":"ida@quadratel.co.id","mobile_no":"0813-1641-6239","status_id":1}'),
(908, 1, '2017-10-11 16:39:14', 'vendor', 12, 'Insert vendortable', '{"vendortype_id":2,"id":"AGCIA","name":"Agcia Pertiwi","email":"na","phone_no":"021-385-0965","address":"Jl Tanah Abang III Jakarta Pusat","status_id":1}'),
(909, 1, '2017-10-11 16:40:27', 'vendorcontact', 18, 'Insert vendorcontacttable', '{"vendor_id":12,"name":"Risdiyanto","position":"Direktur Proyek","organization":"na","email":"risdiyanto@agcia.co.id","mobile_no":"0812-5451-3197","status_id":1}'),
(910, 1, '2017-10-11 16:41:39', 'vendorcontact', 19, 'Insert vendorcontacttable', '{"vendor_id":12,"name":"Lutfi Abdi","position":"Wakil Direktur Proyek","organization":"na","email":"lutfi.abdi@agcia.co.id","mobile_no":"0811-583-3750","status_id":1}'),
(911, 1, '2017-10-11 17:34:51', 'vendor', 13, 'Insert vendortable', '{"vendortype_id":2,"id":"CIPTA","name":"Cipta Daya Selaras","email":"na","phone_no":"021-8661-5874","address":"Komplek PU P4 S No A5 Pondok Bambu Jakarta Timur","status_id":1}'),
(912, 1, '2017-10-11 17:36:22', 'vendorcontact', 20, 'Insert vendorcontacttable', '{"vendor_id":13,"name":"M Zulkarnain PP","position":"Direktur Utama","organization":"Direksi","email":"mz.pancaputra@yahoo.com","mobile_no":"0812-8612-6085 \\/ 0812-8613-30398","status_id":1}'),
(913, 1, '2017-10-11 17:37:35', 'vendorcontact', 21, 'Insert vendorcontacttable', '{"vendor_id":13,"name":"Nirwan","position":"General Manager","organization":"na","email":"nirwanb@yahoo.com","mobile_no":"0815-1112-2937","status_id":1}'),
(914, 1, '2017-10-11 17:39:43', 'vendor', 14, 'Insert vendortable', '{"vendortype_id":2,"id":"NAYAKA","name":"Nayaka Pratama","email":"na","phone_no":"021-8990-8743","address":"Jl Raya Pondok Kelapa Blok F No 1 Duren Sawit Jakarta Timur","status_id":1}'),
(915, 1, '2017-10-11 17:40:55', 'vendorcontact', 22, 'Insert vendorcontacttable', '{"vendor_id":14,"name":"Andrian Eka Putra","position":"Direktur Utama","organization":"Direksi","email":"andrian.putra@nayakapratama.co.id","mobile_no":"0813-1515-1424","status_id":1}'),
(916, 1, '2017-10-11 17:42:05', 'vendorcontact', 23, 'Insert vendorcontacttable', '{"vendor_id":14,"name":"Kurniawan Irianto","position":"Direktur","organization":"na","email":"kurniawan.irianto@nayakapratama.co.id","mobile_no":"0812-8933-4750","status_id":1}'),
(917, 1, '2017-10-11 17:44:31', 'vendor', 15, 'Insert vendortable', '{"vendortype_id":1,"id":"DAYA","name":"Daya Mitra Kasuar","email":"na","phone_no":"031-847-1365 \\/ 0815-5970-0033","address":"Bendul Merisi 134D Surabaya","status_id":1}'),
(918, 1, '2017-10-11 17:46:03', 'vendorcontact', 24, 'Insert vendorcontacttable', '{"vendor_id":15,"name":"Roedy Susanto","position":"Direktur utama","organization":"Direksi","email":"roedy@dmk-project.com \\/ roedy_lwg@yahoo.com","mobile_no":"0812-317-9581","status_id":1}'),
(919, 1, '2017-10-11 17:47:18', 'vendorcontact', 25, 'Insert vendorcontacttable', '{"vendor_id":15,"name":"MHA Nidom","position":"Direktur Operasional","organization":"na","email":"nidom@dmk-project.com \\/  nidomstrange@gmail.com","mobile_no":"0813-3018-5440","status_id":1}'),
(920, 1, '2017-10-11 17:49:13', 'vendor', 16, 'Insert vendortable', '{"vendortype_id":2,"id":"TRITAMA","name":"Tritama Aji Laksana","email":"na","phone_no":"021-290-47301","address":"Jl Bhayangkara Pergudangan T8 No 46-48 Alam Sutera Pakulon Serpong Utama Tangerang Selatan","status_id":1}'),
(921, 1, '2017-10-11 17:50:28', 'vendorcontact', 26, 'Insert vendorcontacttable', '{"vendor_id":16,"name":"Rob Ing Suryo Indarto","position":"Direktur Utama","organization":"na","email":"suryo.indarto@yahoo.com","mobile_no":"0811-267-188","status_id":1}'),
(922, 1, '2017-10-11 17:51:27', 'vendorcontact', 27, 'Insert vendorcontacttable', '{"vendor_id":16,"name":"Budhi Santoso","position":"Direktur","organization":"na","email":"0811-153-341","mobile_no":"taejkt@indosat.net.id","status_id":1}'),
(923, 1, '2017-10-11 17:53:13', 'vendor', 17, 'Insert vendortable', '{"vendortype_id":2,"id":"LIMA","name":"Lima Pilar Sukses","email":"na","phone_no":"061-844-8585","address":"Taman Tomang Elok #B 86 Jl Murai 1 Medan","status_id":1}'),
(924, 1, '2017-10-11 17:54:52', 'vendorcontact', 28, 'Insert vendorcontacttable', '{"vendor_id":17,"name":"William Soedibyo","position":"Direktur Utama","organization":"na","email":"soedibyo.william@limapilarsukses.co.id \\/ soedibyo.william@gmail.com","mobile_no":"0811-613-0555","status_id":1}'),
(925, 1, '2017-10-11 17:56:35', 'vendorcontact', 29, 'Insert vendorcontacttable', '{"vendor_id":17,"name":"Oky Irawan","position":"Account Manager","organization":"na","email":"oky.irawan@limapilarsukses.co.id","mobile_no":"0811-602-9999","status_id":1}'),
(926, 1, '2017-10-11 18:34:41', 'vendor', 18, 'Insert vendortable', '{"vendortype_id":2,"id":"TELEHOUSE","name":"Telehouse Engineering","email":"na","phone_no":"021-7280-0925","address":"Simprug Gallery Jl Teuku Nyak Arif No 10 Jakarta","status_id":1}'),
(927, 1, '2017-10-11 18:35:58', 'vendorcontact', 30, 'Insert vendorcontacttable', '{"vendor_id":18,"name":"Boy Aditya","position":"General Manager Marketing","organization":"Marketing","email":"boy.aditya@telehouse-eng.com","mobile_no":"0812-234-243","status_id":1}'),
(928, 1, '2017-10-11 18:37:07', 'vendorcontact', 31, 'Insert vendorcontacttable', '{"vendor_id":18,"name":"Donny Suprapto","position":"Manager Marketing","organization":"Marketing","email":"donny.s@telehouse-eng.com","mobile_no":"0812-239-7869","status_id":1}'),
(929, 1, '2017-10-11 18:40:12', 'vendor', 19, 'Insert vendortable', '{"vendortype_id":2,"id":"KAIZEN","name":"Kaizen Konsultan","email":"na","phone_no":"021-5890-6419","address":"Jl Meruya Selatan Komplek Ruko Puri Botanical Jakarta Barat","status_id":1}'),
(930, 1, '2017-10-11 18:41:52', 'vendorcontact', 32, 'Insert vendorcontacttable', '{"vendor_id":19,"name":"Nuh Akbar","position":"Direktur Utama","organization":"na","email":"nuh.akbar@kaizenkonsultan.co.id","mobile_no":"0858-1731-2426","status_id":1}'),
(931, 1, '2017-10-11 18:43:40', 'vendorcontact', 33, 'Insert vendorcontacttable', '{"vendor_id":19,"name":"Agus Supriadi","position":"Direktur","organization":"na","email":"agus.supriadi@kaizenkonsultan.co.id","mobile_no":"0811-1774-950","status_id":1}'),
(932, 1, '2017-10-11 18:46:11', 'vendor', 20, 'Insert vendortable', '{"vendortype_id":2,"id":"JAYA","name":"Jaya Engineering Technology","email":"na","phone_no":"021-8911-7341","address":"Jl Gaharu Blok F2 No 2D Delta Silocon II Lippo Cikarang Industri Park Bekasi","status_id":1}'),
(933, 1, '2017-10-11 18:47:58', 'vendorcontact', 34, 'Insert vendorcontacttable', '{"vendor_id":20,"name":"Harisman Adi Susilo","position":"Direktur Marketing & Busdev","organization":"na","email":"harisman.adi@jet.fortace-indonesia.com","mobile_no":"0813-1864-5645","status_id":1}'),
(934, 1, '2017-10-11 18:48:59', 'vendorcontact', 35, 'Insert vendorcontacttable', '{"vendor_id":20,"name":"Beti Anggraeni","position":"Marketing Manager","organization":"na","email":"beti@jet.fortace-indonesia.com","mobile_no":"0811-168-2706","status_id":1}'),
(935, 1, '2017-10-11 18:51:14', 'vendor', 21, 'Insert vendortable', '{"vendortype_id":2,"id":"UNIVERSE","name":"Universe Transportindo","email":"na","phone_no":"na","address":"Jl Cipinang Cempedak I No 46 A Cipinang Cempedak Jatinegara Jakarta Timur","status_id":1}'),
(936, 1, '2017-10-11 18:52:40', 'vendorcontact', 36, 'Insert vendorcontacttable', '{"vendor_id":21,"name":"Ugi Sismarendra","position":"President Director","organization":"na","email":"ugi.sismarendra@universet.co.id","mobile_no":"0811-998-8091","status_id":1}'),
(937, 1, '2017-10-11 18:53:57', 'vendorcontact', 37, 'Insert vendorcontacttable', '{"vendor_id":21,"name":"Junior Troosnabawa","position":"Business Development Director","organization":"Business Development","email":"j.troosnabawa@universet.co.id","mobile_no":"0813-10160215","status_id":1}'),
(938, 1, '2017-10-11 18:55:41', 'vendor', 22, 'Insert vendortable', '{"vendortype_id":2,"id":"INTI","name":"Inti Pindad Mitra Sejati","email":"na","phone_no":"022-520-1693","address":"Jl Moh Toha No 77 Gd GKP Lt 4 Bandung","status_id":1}');
INSERT INTO `systemlogtable` (`row_id`, `created_by`, `created_date`, `module`, `key_id`, `action`, `data`) VALUES
(939, 1, '2017-10-11 18:56:38', 'vendorcontact', 38, 'Insert vendorcontacttable', '{"vendor_id":22,"name":"Widya Arianti","position":"Manajer SE & Marketing","organization":"Marketing","email":"widya@ipms.co.id","mobile_no":"0812-3302-0200","status_id":1}'),
(940, 1, '2017-10-11 18:57:31', 'vendorcontact', 39, 'Insert vendorcontacttable', '{"vendor_id":22,"name":"Priyatno","position":"Manajer Operasional","organization":"na","email":"0813-2011-7116","mobile_no":"priatno@ipms.co.id","status_id":1}'),
(941, 1, '2017-10-11 18:58:58', 'vendor', 23, 'Insert vendortable', '{"vendortype_id":2,"id":"INDOTUNAS","name":"Indotunas Makmur, PT.","email":"na","phone_no":"na","address":"Mega Plaza Building 4 Floor Jl HR Rasuna Said Kav C3 Kuningan Jakarta Selatan","status_id":1}'),
(942, 1, '2017-10-11 18:59:41', 'vendorcontact', 40, 'Insert vendorcontacttable', '{"vendor_id":23,"name":"Nelson","position":"na","organization":"na","email":"nelson@itm-corps.com","mobile_no":"0811-910-757","status_id":1}'),
(943, 1, '2017-10-11 19:00:39', 'vendorcontact', 41, 'Insert vendorcontacttable', '{"vendor_id":23,"name":"Hariara","position":"na","organization":"na","email":"hariara@itm-corps.com","mobile_no":"0811-189-8242","status_id":1}'),
(944, 1, '2017-10-11 19:02:21', 'vendor', 24, 'Insert vendortable', '{"vendortype_id":2,"id":"DWIPUTRA","name":"Dwi Putra Nugraha","email":"na","phone_no":"024-670-6617","address":"Jl Mahameru Timur I \\/ 458 Semarang","status_id":1}'),
(945, 1, '2017-10-11 19:03:18', 'vendorcontact', 42, 'Insert vendorcontacttable', '{"vendor_id":24,"name":"Aji Putranto","position":"na","organization":"na","email":"ajiputranto@yahoo.com","mobile_no":"0818-0677-5284","status_id":1}'),
(946, 1, '2017-10-11 19:05:06', 'vendor', 25, 'Insert vendortable', '{"vendortype_id":2,"id":"MITRASETIA","name":"Mitra Setia Indonesia","email":"na","phone_no":"024-6706619","address":"Komp Ruko Fransisko Blok A2 No 6 Kel Kibing Kec Batu Aji Batam","status_id":1}'),
(947, 1, '2017-10-11 19:06:08', 'vendorcontact', 43, 'Insert vendorcontacttable', '{"vendor_id":25,"name":"Suvinarto Marpaung","position":"Direktur Utama","organization":"na","email":"narto.m@yahoo.com","mobile_no":"0812-7781-3232","status_id":1}'),
(948, 1, '2017-10-11 19:06:51', 'vendorcontact', 44, 'Insert vendorcontacttable', '{"vendor_id":25,"name":"Lukman Nadeak","position":"Wakil Direktur","organization":"na","email":"na","mobile_no":"0812-7708-293","status_id":1}'),
(949, 1, '2017-10-12 09:11:41', 'tenant', 1, 'Insert tenanttable', '{"name":"Telkomsel","email":"na","phone_no":"na","address":"na","status_id":1}'),
(950, 1, '2017-10-12 09:12:21', 'tenant', 2, 'Insert tenanttable', '{"name":"XL","email":"na","phone_no":"na","address":"na","status_id":1}'),
(951, 1, '2017-10-12 09:12:46', 'tenant', 3, 'Insert tenanttable', '{"name":"Indosat","email":"na","phone_no":"na","address":"na","status_id":1}'),
(952, 1, '2017-10-12 09:13:15', 'tenant', 4, 'Insert tenanttable', '{"name":"H3I","email":"na","phone_no":"na","address":"na","status_id":1}'),
(953, 1, '2017-10-12 09:13:54', 'tenant', 5, 'Insert tenanttable', '{"name":"Smartfren","email":"na","phone_no":"na","address":"na","status_id":1}'),
(954, 1, '2017-10-12 09:15:18', 'tenant', 5, 'Update tenanttable', '{"name":"Smartfren","email":"na","phone_no":"na","address":"...\\r\\nDKI Jakarta","status_id":1}'),
(955, 1, '2017-10-12 09:15:36', 'tenant', 4, 'Update tenanttable', '{"name":"H3I","email":"na","phone_no":"na","address":"...\\r\\nDKI Jakarta","status_id":1}'),
(956, 1, '2017-10-12 09:15:55', 'tenant', 3, 'Update tenanttable', '{"name":"Indosat","email":"na","phone_no":"na","address":"...\\r\\nDKI Jakarta","status_id":1}'),
(957, 1, '2017-10-12 09:16:11', 'tenant', 2, 'Update tenanttable', '{"name":"XL","email":"na","phone_no":"na","address":"...\\r\\nDKI Jakarta","status_id":1}'),
(958, 1, '2017-10-12 09:16:28', 'tenant', 1, 'Update tenanttable', '{"name":"Telkomsel","email":"na","phone_no":"na","address":"...\\r\\nDKI Jakarta","status_id":1}'),
(959, 1, '2017-10-16 09:30:27', 'salesorder', 1, 'Insert salesordertable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","salesorder_date":"2017-10-16","drm_date":"0000-00-00","batch":"1","po_tenant_no":"123","po_tenant_date":"2017-10-15","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(960, 1, '2017-10-16 09:51:25', 'salesorderline', 1, 'Insert salesorderline', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","product_id":1,"site_id_tenant":"TSEL-001","site_name_tenant":"TEBET","site_id_api":"PBC-LSK-001","site_name_api":"API_TEBET","sitetype_id":1,"towerheight_id":6,"latitude":-12.8998989,"longitude":15.87766,"area_id":2,"region_id":3,"province_id":11,"city_id":147,"baks":"2017-10-18","rfi":"2017-10-17","baps":"2017-10-19","remarks":"na","rent_period":5,"status_id":1}'),
(961, 1, '2017-10-16 09:52:54', 'user', 10, 'Update usertable', '{"role_id":10,"name":"President Director","email":"makpui@indonesiancloud.com","user_id":"presdir","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(962, 1, '2017-10-16 09:53:16', 'user', 9, 'Update usertable', '{"role_id":9,"name":"Operation Director","email":"makpui@indonesiancloud.com","user_id":"opdir","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(963, 1, '2017-10-16 09:53:41', 'user', 8, 'Update usertable', '{"role_id":8,"name":"Solution Director","email":"makpui@indonesiancloud.com","user_id":"soldir","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(964, 1, '2017-10-16 09:53:54', 'user', 7, 'Update usertable', '{"role_id":7,"name":"General Manager Project","email":"makpui@indonesiancloud.com","user_id":"gmproject","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(965, 1, '2017-10-16 09:54:12', 'user', 6, 'Update usertable', '{"role_id":6,"name":"Project Manager","email":"makpui@indonesiancloud.com","user_id":"pm","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(966, 1, '2017-10-16 09:54:26', 'user', 5, 'Update usertable', '{"role_id":5,"name":"Project Support","email":"makpui@indonesiancloud.com","user_id":"projectsupport","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(967, 1, '2017-10-16 09:54:39', 'user', 4, 'Update usertable', '{"role_id":4,"name":"General Manager","email":"makpui@indonesiancloud.com","user_id":"gmsales","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(968, 1, '2017-10-16 09:54:54', 'user', 3, 'Update usertable', '{"role_id":3,"name":"Account Manager","email":"makpui@indonesiancloud.com","user_id":"am","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(969, 1, '2017-10-16 09:55:08', 'user', 2, 'Update usertable', '{"role_id":2,"name":"Sales Admin","email":"makpui@indonesiancloud.com","user_id":"salesadm","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(970, 1, '2017-10-16 09:55:22', 'user', 1, 'Update usertable', '{"role_id":1,"name":"Administrator","email":"makpui@indonesiancloud.com","user_id":"admin","pswrd":"d41d8cd98f00b204e9800998ecf8427e","photo":"","status_id":1}'),
(971, 1, '2017-10-16 10:17:43', 'workorder', 1, 'Insert workordertable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","workorder_date":"2017-10-16","vendor_id":1,"vendorcontact_id":1,"product_group":"ABC","project_manager":"ABC","remark":"NA","workorder_milestone_id":0,"status_id":1}'),
(972, 1, '2017-10-16 10:47:22', 'password', 10, 'Update usertable', '{"pswrd":"202cb962ac59075b964b07152d234b70"}'),
(973, 1, '2017-10-16 10:48:04', 'password', 1, 'Update usertable', '{"pswrd":"202cb962ac59075b964b07152d234b70"}'),
(974, 1, '2017-10-16 10:50:11', 'user', 1, 'Update usertable', '{"role_id":1,"name":"AdministratorAdmin","email":"makpui@indonesiancloud.com","user_id":"admin","photo":"","status_id":1}'),
(975, 1, '2017-10-16 10:50:50', 'user', 1, 'Update usertable', '{"role_id":1,"name":"Administrator","email":"makpui@indonesiancloud.com","user_id":"admin","photo":"","status_id":1}'),
(976, 1, '2017-10-16 10:58:10', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(977, 1, '2017-10-16 11:06:14', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(978, 1, '2017-10-16 11:06:53', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(979, 1, '2017-10-16 11:33:06', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(980, 1, '2017-10-16 11:33:42', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(981, 1, '2017-10-16 11:36:04', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(982, 1, '2017-10-16 11:36:20', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(983, 1, '2017-10-16 11:37:15', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(984, 1, '2017-10-16 11:37:33', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(985, 1, '2017-10-16 13:34:21', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(986, 1, '2017-10-16 13:36:02', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(987, 1, '2017-10-16 13:38:28', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(988, 1, '2017-10-16 13:40:33', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(989, 1, '2017-10-16 13:41:52', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(990, 1, '2017-10-16 13:44:50', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(991, 1, '2017-10-16 13:45:19', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(992, 1, '2017-10-16 13:48:44', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(993, 1, '2017-10-16 13:49:04', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(994, 1, '2017-10-16 14:03:35', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(995, 1, '2017-10-16 14:04:22', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(996, 1, '2017-10-16 14:06:09', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(997, 1, '2017-10-16 14:06:53', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(998, 1, '2017-10-16 14:23:54', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(999, 1, '2017-10-16 14:24:24', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1000, 1, '2017-10-16 14:36:40', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1001, 1, '2017-10-16 14:37:23', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1002, 1, '2017-10-16 14:49:34', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1003, 1, '2017-10-16 14:51:19', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1004, 1, '2017-10-16 14:55:25', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1005, 1, '2017-10-16 14:56:08', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1006, 1, '2017-10-16 14:57:56', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1007, 1, '2017-10-16 14:59:08', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1008, 1, '2017-10-16 15:14:27', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1009, 1, '2017-10-16 15:14:54', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1010, 1, '2017-10-16 15:20:17', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1011, 1, '2017-10-16 15:23:42', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1012, 1, '2017-10-16 15:28:44', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1013, 1, '2017-10-16 15:29:12', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1014, 1, '2017-10-16 16:35:14', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1015, 1, '2017-10-16 16:35:17', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1016, 1, '2017-10-16 16:37:01', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1017, 1, '2017-10-16 16:42:55', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1018, 1, '2017-10-16 16:43:44', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1019, 1, '2017-10-16 16:44:44', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1020, 1, '2017-10-16 16:44:47', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1021, 1, '2017-10-16 16:45:08', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1022, 1, '2017-10-17 16:33:46', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1023, 1, '2017-10-17 16:33:59', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1024, 1, '2017-10-17 16:36:59', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1025, 1, '2017-10-17 16:37:00', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1026, 1, '2017-10-17 16:37:35', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1027, 1, '2017-10-17 16:46:22', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1028, 1, '2017-10-17 16:54:36', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1029, 1, '2017-10-17 17:00:36', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1030, 1, '2017-10-17 17:04:07', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1031, 1, '2017-10-18 09:06:00', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1032, 1, '2017-10-18 09:07:26', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1033, 1, '2017-10-18 09:10:25', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1034, 1, '2017-10-18 09:11:18', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1035, 1, '2017-10-18 09:16:51', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1036, 1, '2017-10-18 09:17:03', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1037, 1, '2017-10-18 09:17:59', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":0}'),
(1038, 1, '2017-10-18 09:18:13', 'salesorder', 1, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1039, 3, '2017-10-19 11:27:09', 'salesorderapproval', 1, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"am approved","status_id":1}'),
(1040, 3, '2017-10-19 14:25:10', 'salesorderapproval', 2, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"am approve","status_id":1}'),
(1041, 3, '2017-10-19 14:29:21', 'salesorderapproval', 3, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"am approved","status_id":1}'),
(1042, 3, '2017-10-19 14:34:58', 'salesorderapproval', 4, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"aaaa","status_id":1}'),
(1043, 3, '2017-10-19 14:46:41', 'salesorderapproval', 5, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"a","status_id":1}'),
(1044, 3, '2017-10-19 14:51:39', 'salesorderapproval', 6, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"a","status_id":1}'),
(1045, 3, '2017-10-19 14:52:46', 'salesorderapproval', 7, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"aaaa","status_id":1}'),
(1046, 3, '2017-10-19 15:01:22', 'salesorderapproval', 8, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"ok","status_id":1}'),
(1047, 3, '2017-10-19 15:01:25', 'salesorderapproval', 9, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"ok","status_id":1}'),
(1048, 3, '2017-10-19 16:43:28', 'salesorderapproval', 10, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"aaaa","status_id":1}'),
(1049, 3, '2017-10-19 16:49:56', 'salesorderapproval', 11, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"aaaaa","status_id":1}'),
(1050, 3, '2017-10-19 16:56:41', 'salesorderapproval', 12, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"aaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1051, 3, '2017-10-19 17:00:43', 'salesorderapproval', 13, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"aaaaaa","status_id":1}'),
(1052, 3, '2017-10-19 17:07:12', 'salesorderapproval', 14, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1053, 3, '2017-10-19 17:10:32', 'salesorderapproval', 15, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-19 12:00:00","approved_status":"1","remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1054, 6, '2017-10-20 09:28:36', 'salesorderapproval', 16, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-20 12:00:00","approved_status":"1","remark":"bbbbbbbbbbbbbbbbbbb","status_id":1}'),
(1055, 4, '2017-10-20 09:33:04', 'salesorderapproval', 17, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-10-20 12:00:00","approved_status":"1","remark":"gmsales approve","status_id":1}'),
(1056, 6, '2017-10-23 10:48:06', 'salesorderapproval', 6, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":"1","remark":"pm approve","status_id":1}'),
(1057, 4, '2017-10-23 10:57:47', 'salesorderapproval', 7, 'Insert salesorderapprovaltable', '{"salesorder_no":"0001\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-10-23 12:00:00","approved_status":"0","remark":"reject by gm sales","status_id":1}'),
(1058, 4, '2017-10-23 10:57:47', 'salesorderapproval', 1, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1059, 2, '2017-10-23 14:08:45', 'site', 1, 'Insert sitetable', '{"product_id":1,"tenant_id":1,"site_id_api":"PBC-LSK-001","site_name_api":"API_TEBET","site_id_tenant":"TSEL-001","site_name_tenant":"TEBET","area_id":2,"region_id":3,"province_id":11,"city_id":147,"longitude_ori":15.87766,"latitude_ori":-12.8998989,"address":"Jl Tebet Timur Raya No 23","sitetype_id":1,"towerheight_id":6,"transmissiontype_id":2,"remark":"Tebet ","status_id":1}'),
(1060, 1, '2017-10-23 14:20:31', 'workorder', 2, 'Insert workordertable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","workorder_date":"2017-10-23","vendor_id":1,"vendorcontact_id":1,"product_group":"AAA","project_manager":"Asep Sopyan","remark":"WO","workorder_milestone_id":0,"status_id":1}'),
(1061, 1, '2017-10-23 14:21:38', 'workorderline', 1, 'Insert workorderline', '{"workorder_no":"0001\\/WO\\/XI\\/2017","site_id":1,"projectactivity_id":1,"start_date":"2017-10-23","end_date":"2017-10-25","status_id":1}'),
(1062, 1, '2017-10-23 14:28:28', 'workorder', 2, 'Update workordertable', '{"workorder_milestone_id":1}'),
(1063, 1, '2017-10-23 14:29:31', 'workorder', 2, 'Update workordertable', '{"workorder_milestone_id":0}'),
(1064, 1, '2017-10-23 14:30:03', 'workorder', 2, 'Update workordertable', '{"workorder_milestone_id":1}'),
(1065, 6, '2017-10-23 14:31:49', 'workorderapproval', 1, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"Approved by Project Manager","status_id":1}'),
(1066, 6, '2017-10-23 15:00:14', 'workorderapproval', 9, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"Approved by Project Manager","status_id":1}'),
(1067, 9, '2017-10-23 15:01:54', 'workorderapproval', 10, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"Approved by Operation Director","status_id":1}'),
(1068, 6, '2017-10-23 15:24:34', 'workorderapproval', 11, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"Approved by Project Manager","status_id":1}'),
(1069, 9, '2017-10-23 15:26:20', 'workorderapproval', 12, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"Approved by Operation Director","status_id":1}'),
(1070, 9, '2017-10-23 15:33:54', 'workorderapproval', 14, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"app op dir","status_id":1}'),
(1071, 9, '2017-10-23 15:35:23', 'workorderapproval', 15, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"Approved by Operation Director","status_id":1}'),
(1072, 9, '2017-10-23 15:39:00', 'workorderapproval', 16, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"adadadfa,famf,m,m","status_id":1}'),
(1073, 9, '2017-10-23 15:41:40', 'workorderapproval', 17, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"gggggggggggggggggggggg","status_id":1}'),
(1074, 9, '2017-10-23 15:44:31', 'workorderapproval', 18, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1075, 9, '2017-10-23 15:47:33', 'workorderapproval', 19, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1076, 9, '2017-10-23 15:49:16', 'workorderapproval', 20, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1077, 9, '2017-10-23 16:45:24', 'workorderapproval', 21, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1078, 9, '2017-10-23 17:22:12', 'workorderapproval', 22, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1079, 9, '2017-10-23 17:23:47', 'workorderapproval', 23, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1080, 9, '2017-10-23 17:29:34', 'workorderapproval', 24, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1081, 9, '2017-10-23 18:24:14', 'workorderapproval', 25, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"a","status_id":1}'),
(1082, 9, '2017-10-23 18:26:30', 'workorderapproval', 26, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1083, 9, '2017-10-23 18:28:57', 'workorderapproval', 27, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-23 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1084, 9, '2017-10-24 08:40:22', 'workorderapproval', 28, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1085, 9, '2017-10-24 08:42:06', 'workorderapproval', 29, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"aaaaaaaaaaaaaaaaaaaaaaa","status_id":1}'),
(1086, 6, '2017-10-24 08:45:54', 'workorderapproval', 30, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"Approved by PM","status_id":1}'),
(1087, 9, '2017-10-24 08:53:09', 'workorderapproval', 31, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"Approved by Operation Dir.","status_id":1}'),
(1088, 9, '2017-10-24 09:01:04', 'workorderapproval', 32, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"Approved by Operation Dir.","status_id":1}'),
(1089, 9, '2017-10-24 09:03:22', 'workorderapproval', 33, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"Approved by Operation Direction","status_id":1}'),
(1090, 9, '2017-10-24 09:05:21', 'workorderapproval', 34, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"Approved by Op dir","status_id":1}'),
(1091, 9, '2017-10-24 09:06:59', 'workorderapproval', 35, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"approved by opdir","status_id":1}'),
(1092, 9, '2017-10-24 09:09:47', 'workorderapproval', 36, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"Approved  by Operation Director","status_id":1}'),
(1093, 9, '2017-10-24 09:12:24', 'workorderapproval', 37, 'Insert workorderapprovaltable', '{"workorder_no":"0001\\/WO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-24 12:00:00","approved_status":1,"remark":"Approved by Op dir","status_id":1}'),
(1094, 2, '2017-10-24 10:37:42', 'salesorder', 2, 'Insert salesordertable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","salesorder_date":"2017-10-24","drm_date":"0000-00-00","batch":"1","po_tenant_no":"adsa","po_tenant_date":"2017-10-25","tenant_id":1,"approval_milestone_id":0,"status_id":1}'),
(1095, 2, '2017-10-24 10:38:53', 'salesorderline', 2, 'Insert salesorderline', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","product_id":1,"site_id_tenant":"a","site_name_tenant":"a","site_id_api":"PBC-LSK-002","site_name_api":"API_a","sitetype_id":1,"towerheight_id":4,"latitude":-1,"longitude":122,"area_id":2,"region_id":3,"province_id":11,"city_id":147,"baks":"2017-10-25","rfi":"2017-10-24","baps":"2017-10-26","remarks":"a","rent_period":5,"status_id":1}'),
(1096, 2, '2017-10-24 10:43:21', 'salesorder', 2, 'Update salesordertable', '{"approval_milestone_id":1}'),
(1097, 2, '2017-10-25 10:50:30', 'site', 1, 'Delete sitetable', '""'),
(1098, 4, '2017-10-25 17:04:38', 'salesorderapproval', 21, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-10-25 12:00:00","approved_status":"0","remark":"Rejected by GM Sales","status_id":1}'),
(1099, 4, '2017-10-25 17:04:38', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1100, 3, '2017-10-26 18:19:42', 'salesorderapproval', 22, 'Insert salesorderapprovaltable', '{"salesorder_no":"","approvalflow_id":0,"approved_date":"2017-10-26 12:00:00","approved_status":"1","remark":"Approved by Account Manager","status_id":1}'),
(1101, 3, '2017-10-26 18:21:30', 'salesorderapproval', 23, 'Insert salesorderapprovaltable', '{"salesorder_no":"","approvalflow_id":0,"approved_date":"2017-10-26 12:00:00","approved_status":"1","remark":"Approved by Account Manager","status_id":1}'),
(1102, 6, '2017-10-31 13:05:39', 'salesorderapproval', 25, 'Insert salesorderapprovaltable', '{"salesorder_no":"","approvalflow_id":0,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1103, 6, '2017-10-31 13:52:02', 'salesorderapproval', 26, 'Insert salesorderapprovaltable', '{"salesorder_no":"","approvalflow_id":0,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1104, 6, '2017-10-31 13:52:58', 'salesorderapproval', 27, 'Insert salesorderapprovaltable', '{"salesorder_no":"","approvalflow_id":0,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Account Manager","status_id":1}'),
(1105, 6, '2017-10-31 14:04:30', 'salesorderapproval', 28, 'Insert salesorderapprovaltable', '{"salesorder_no":"","approvalflow_id":0,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1106, 6, '2017-10-31 14:07:42', 'salesorderapproval', 29, 'Insert salesorderapprovaltable', '{"salesorder_no":"","approvalflow_id":0,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1107, 6, '2017-10-31 14:21:06', 'salesorderapproval', 30, 'Insert salesorderapprovaltable', '{"salesorder_no":"","approvalflow_id":0,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1108, 6, '2017-10-31 15:51:46', 'salesorderapproval', 31, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1109, 6, '2017-10-31 16:16:07', 'salesorderapproval', 32, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1110, 6, '2017-10-31 16:21:27', 'salesorderapproval', 33, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved By Project Manager","status_id":1}'),
(1111, 6, '2017-10-31 16:26:18', 'salesorderapproval', 34, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-10-31 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1112, 4, '2017-11-01 13:53:05', 'salesorderapproval', 35, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1113, 4, '2017-11-01 13:55:30', 'salesorderapproval', 36, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1114, 6, '2017-11-01 14:07:19', 'salesorderapproval', 37, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-11-01 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1115, 4, '2017-11-01 14:09:15', 'salesorderapproval', 38, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1116, 4, '2017-11-01 14:09:15', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1117, 4, '2017-11-01 14:11:58', 'salesorderapproval', 39, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"1","remark":"Reject by GM Sales","status_id":1}'),
(1118, 4, '2017-11-01 14:13:12', 'salesorderapproval', 40, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1119, 4, '2017-11-01 14:13:12', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1120, 4, '2017-11-01 14:16:05', 'salesorderapproval', 41, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1121, 4, '2017-11-01 14:16:05', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1122, 4, '2017-11-01 14:19:50', 'salesorderapproval', 42, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GMSales","status_id":1}'),
(1123, 4, '2017-11-01 14:19:50', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1124, 4, '2017-11-01 14:39:11', 'salesorderapproval', 43, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1125, 4, '2017-11-01 14:39:11', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1126, 4, '2017-11-01 14:45:24', 'salesorderapproval', 44, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject ","status_id":1}'),
(1127, 4, '2017-11-01 14:45:24', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1128, 4, '2017-11-01 16:32:53', 'salesorderapproval', 45, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1129, 4, '2017-11-01 16:32:53', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1130, 4, '2017-11-01 16:34:35', 'salesorderapproval', 46, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1131, 4, '2017-11-01 16:34:35', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1132, 4, '2017-11-01 16:38:57', 'salesorderapproval', 47, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject by GM Sales","status_id":1}'),
(1133, 4, '2017-11-01 16:38:57', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1134, 4, '2017-11-01 16:41:40', 'salesorderapproval', 48, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject","status_id":1}'),
(1135, 4, '2017-11-01 16:41:40', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1136, 4, '2017-11-01 16:49:50', 'salesorderapproval', 49, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject","status_id":1}'),
(1137, 4, '2017-11-01 16:49:50', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1138, 4, '2017-11-01 16:53:03', 'salesorderapproval', 50, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject","status_id":1}'),
(1139, 4, '2017-11-01 16:53:03', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1140, 4, '2017-11-01 16:57:17', 'salesorderapproval', 51, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":3,"approved_date":"2017-11-01 12:00:00","approved_status":"0","remark":"Reject","status_id":1}'),
(1141, 4, '2017-11-01 16:57:18', 'salesorderapproval', 2, 'Update salesorderapprovaltable', '{"status_id":2}'),
(1142, 3, '2017-11-02 09:30:03', 'salesorderapproval', 52, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-11-02 12:00:00","approved_status":"0","remark":"Approved by Account Manager","status_id":1}'),
(1143, 3, '2017-11-02 09:30:56', 'salesorderapproval', 53, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-11-02 12:00:00","approved_status":"1","remark":"Approved","status_id":1}'),
(1144, 3, '2017-11-02 09:32:11', 'salesorderapproval', 54, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-11-02 12:00:00","approved_status":"1","remark":"Approved by Account Manager","status_id":1}'),
(1145, 3, '2017-11-02 09:33:38', 'salesorderapproval', 55, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":1,"approved_date":"2017-11-02 12:00:00","approved_status":"1","remark":"Approved by Account Manager","status_id":1}'),
(1146, 6, '2017-11-02 09:36:19', 'salesorderapproval', 56, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-11-02 12:00:00","approved_status":"0","remark":"Reject by Project Manager","status_id":1}'),
(1147, 6, '2017-11-02 09:37:37', 'salesorderapproval', 57, 'Insert salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-11-02 12:00:00","approved_status":"1","remark":"Approved by Project Manager","status_id":1}'),
(1148, 6, '2017-11-02 09:38:39', 'salesorderapproval', 57, 'Update salesorderapprovaltable', '{"salesorder_no":"0002\\/SO\\/XI\\/2017","approvalflow_id":2,"approved_date":"2017-11-02 12:00:00","approved_status":"0","remark":"Reject By Project Manager","status_id":1}'),
(1149, 1, '2017-12-06 10:29:01', 'sales_order_line', 3, 'Delete ', '""');

-- --------------------------------------------------------

--
-- Table structure for table `tenanttable`
--

CREATE TABLE IF NOT EXISTS `tenanttable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `address` text,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tenanttable`
--

INSERT INTO `tenanttable` (`row_id`, `name`, `email`, `phone_no`, `address`, `status_id`) VALUES
(1, 'Telkomsel', 'na', 'na', '...\r\nDKI Jakarta', 1),
(2, 'XL', 'na', 'na', '...\r\nDKI Jakarta', 1),
(3, 'Indosat', 'na', 'na', '...\r\nDKI Jakarta', 1),
(4, 'H3I', 'na', 'na', '...\r\nDKI Jakarta', 1),
(5, 'Smartfren', 'na', 'na', '...\r\nDKI Jakarta', 1);

-- --------------------------------------------------------

--
-- Table structure for table `towerheighttable`
--

CREATE TABLE IF NOT EXISTS `towerheighttable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt` varchar(50) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `towerheighttable`
--

INSERT INTO `towerheighttable` (`row_id`, `txt`, `product_id`, `status_id`) VALUES
(1, '6 Meter', 1, 1),
(2, '9 Meter', 1, 1),
(3, '12 Meter', 1, 1),
(4, '15 Meter', 1, 1),
(5, '17 Meter', 1, 1),
(6, '21 Meter', 1, 1),
(7, '27 Meter', 2, 1),
(8, '30 Meter', 2, 1),
(9, '42 Meter', 3, 1),
(10, '52 Meter', 3, 1),
(11, '62 Meter', 3, 1),
(12, '72 Meter', 3, 1),
(13, '82 Meter', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transmissiontypetable`
--

CREATE TABLE IF NOT EXISTS `transmissiontypetable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `transmissiontypetable`
--

INSERT INTO `transmissiontypetable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'FSO', 1),
(2, 'FO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `usertable`
--

CREATE TABLE IF NOT EXISTS `usertable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `pswrd` varchar(100) DEFAULT NULL,
  `photo` varchar(20) DEFAULT NULL,
  `status_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`user_id`,`pswrd`,`role_id`,`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `usertable`
--

INSERT INTO `usertable` (`row_id`, `role_id`, `name`, `email`, `user_id`, `pswrd`, `photo`, `status_id`) VALUES
(1, 1, 'Administrator', 'makpui@indonesiancloud.com', 'admin', '202cb962ac59075b964b07152d234b70', '', '1'),
(2, 2, 'Sales Admin', 'makpui@indonesiancloud.com', 'salesadm', '202cb962ac59075b964b07152d234b70', '', '1'),
(3, 3, 'Account Manager - 1', 'makpui@indonesiancloud.com', 'am_1', '202cb962ac59075b964b07152d234b70', '', '1'),
(4, 4, 'General Manager', 'makpui@indonesiancloud.com', 'gmsales', '202cb962ac59075b964b07152d234b70', '', '1'),
(5, 5, 'Project Support', 'makpui@indonesiancloud.com', 'prosup', '202cb962ac59075b964b07152d234b70', '', '1'),
(6, 6, 'Project Manager', 'makpui@indonesiancloud.com', 'pm', '202cb962ac59075b964b07152d234b70', '', '1'),
(7, 7, 'General Manager Project', 'makpui@indonesiancloud.com', 'gmproject', '202cb962ac59075b964b07152d234b70', '', '1'),
(8, 8, 'Solution Director', 'makpui@indonesiancloud.com', 'soldir', '202cb962ac59075b964b07152d234b70', '', '1'),
(9, 9, 'Operation Director', 'makpui@indonesiancloud.com', 'opdir', '202cb962ac59075b964b07152d234b70', '', '1'),
(10, 10, 'President Director', 'makpui@indonesiancloud.com', 'presdir', '202cb962ac59075b964b07152d234b70', '', '1'),
(13, 3, 'Accoun Manager - 2', 'makpui@indonesiancloud.com', 'am_2', '202cb962ac59075b964b07152d234b70', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `userterritorytable`
--

CREATE TABLE IF NOT EXISTS `userterritorytable` (
  `user_row_id` int(11) NOT NULL,
  `territory_id` int(11) NOT NULL,
  PRIMARY KEY (`user_row_id`,`territory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userterritorytable`
--

INSERT INTO `userterritorytable` (`user_row_id`, `territory_id`) VALUES
(3, 1),
(3, 2),
(13, 3);

-- --------------------------------------------------------

--
-- Table structure for table `vendorcontacttable`
--

CREATE TABLE IF NOT EXISTS `vendorcontacttable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `organization` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`vendor_id`,`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `vendorcontacttable`
--

INSERT INTO `vendorcontacttable` (`row_id`, `vendor_id`, `name`, `position`, `organization`, `email`, `mobile_no`, `status_id`) VALUES
(1, 1, 'Yesiskha Soeryo', 'Direktur Utama', 'Direksi', 'ysoeryo@orlie.co.id', '0811-885-2582 / 0816-485-2585', 1),
(2, 2, 'Indra Kurnia', 'SACME Manager', 'SACME', 'indra.kurnia@bachmultiglobal.co.id', 'na', 1),
(3, 3, 'Chyang', 'Direktur Operasional', 'Operasional', 'chyang@globalcomtech.co.id', '0812-606-1777', 1),
(4, 4, 'Fahmi Sukriatna', 'Direktur Utama', 'Direksi', 'fahmi@bsutama.com / fahmi_787@yahoo.co.id', '0811-888-2781 / 0878-816-7808', 1),
(5, 5, 'Rizal Pahlevi', 'General Manager', 'General', 'rizal@mkinvestama.co.id', 'na', 1),
(6, 6, 'Rony Dosonugroho', 'President Director', 'Direksi', 'rony@adyawinsa.com', '0855-102-0320', 1),
(7, 6, 'Adi Prasetyo', 'Account Manager', 'na', 'adi@adyawinsa.com', '0811-175-5711', 1),
(8, 7, 'Abdul Rahim, ST', 'na', 'na', 'baimji@gmail.com', '0813-4385-8055 / 0811-412-148', 1),
(9, 7, 'Muh Arham N, SE', 'Project Manager', 'Project', 'arhammuh21@gmail.com', '0852-3141-3512', 1),
(10, 8, 'Surya Darma', 'Direktur', 'Direksi', 'surya.roberto@yahoo.com', '0812-6607-0558', 1),
(11, 8, 'Luki Al Hardi', 'Project Manager', 'Project', 'alhardiluki@gmail.com', '0811-660-0022', 1),
(12, 9, 'Hamonangan Pasaribu, Ir.', 'General Manager', 'General', 'monank_p1000@yahoo.com', '0813-1730-1965', 1),
(13, 9, 'Jhon Ramadhona Silitonga, Ir.', 'Project Manager', 'Project', 'jhon.r.silitonga@gmail.com', '0813-7563-9689', 1),
(14, 10, 'R Darmawan', 'na', 'na', 'darmawan@cel-kon.net', '0811-189-6072 / 021-29490587', 1),
(15, 10, 'Ibrahim Adjie', 'SACME Project Manager', 'Project', 'adjie.ibrahim@cel-kon.net', '0853-1761-0616', 1),
(16, 11, 'Sigit Priawanto', 'Direktur', 'Direksi', 'sigit@quardratel.co.id', '0812-107-6600', 1),
(17, 11, 'Ida Nurviani', 'Account', 'na', 'ida@quadratel.co.id', '0813-1641-6239', 1),
(18, 12, 'Risdiyanto', 'Direktur Proyek', 'na', 'risdiyanto@agcia.co.id', '0812-5451-3197', 1),
(19, 12, 'Lutfi Abdi', 'Wakil Direktur Proyek', 'na', 'lutfi.abdi@agcia.co.id', '0811-583-3750', 1),
(20, 13, 'M Zulkarnain PP', 'Direktur Utama', 'Direksi', 'mz.pancaputra@yahoo.com', '0812-8612-6085 / 0812-8613-30398', 1),
(21, 13, 'Nirwan', 'General Manager', 'na', 'nirwanb@yahoo.com', '0815-1112-2937', 1),
(22, 14, 'Andrian Eka Putra', 'Direktur Utama', 'Direksi', 'andrian.putra@nayakapratama.co.id', '0813-1515-1424', 1),
(23, 14, 'Kurniawan Irianto', 'Direktur', 'na', 'kurniawan.irianto@nayakapratama.co.id', '0812-8933-4750', 1),
(24, 15, 'Roedy Susanto', 'Direktur utama', 'Direksi', 'roedy@dmk-project.com / roedy_lwg@yahoo.com', '0812-317-9581', 1),
(25, 15, 'MHA Nidom', 'Direktur Operasional', 'na', 'nidom@dmk-project.com /  nidomstrange@gmail.com', '0813-3018-5440', 1),
(26, 16, 'Rob Ing Suryo Indarto', 'Direktur Utama', 'na', 'suryo.indarto@yahoo.com', '0811-267-188', 1),
(27, 16, 'Budhi Santoso', 'Direktur', 'na', '0811-153-341', 'taejkt@indosat.net.id', 1),
(28, 17, 'William Soedibyo', 'Direktur Utama', 'na', 'soedibyo.william@limapilarsukses.co.id / soedibyo.william@gmail.com', '0811-613-0555', 1),
(29, 17, 'Oky Irawan', 'Account Manager', 'na', 'oky.irawan@limapilarsukses.co.id', '0811-602-9999', 1),
(30, 18, 'Boy Aditya', 'General Manager Marketing', 'Marketing', 'boy.aditya@telehouse-eng.com', '0812-234-243', 1),
(31, 18, 'Donny Suprapto', 'Manager Marketing', 'Marketing', 'donny.s@telehouse-eng.com', '0812-239-7869', 1),
(32, 19, 'Nuh Akbar', 'Direktur Utama', 'na', 'nuh.akbar@kaizenkonsultan.co.id', '0858-1731-2426', 1),
(33, 19, 'Agus Supriadi', 'Direktur', 'na', 'agus.supriadi@kaizenkonsultan.co.id', '0811-1774-950', 1),
(34, 20, 'Harisman Adi Susilo', 'Direktur Marketing & Busdev', 'na', 'harisman.adi@jet.fortace-indonesia.com', '0813-1864-5645', 1),
(35, 20, 'Beti Anggraeni', 'Marketing Manager', 'na', 'beti@jet.fortace-indonesia.com', '0811-168-2706', 1),
(36, 21, 'Ugi Sismarendra', 'President Director', 'na', 'ugi.sismarendra@universet.co.id', '0811-998-8091', 1),
(37, 21, 'Junior Troosnabawa', 'Business Development Director', 'Business Development', 'j.troosnabawa@universet.co.id', '0813-10160215', 1),
(38, 22, 'Widya Arianti', 'Manajer SE & Marketing', 'Marketing', 'widya@ipms.co.id', '0812-3302-0200', 1),
(39, 22, 'Priyatno', 'Manajer Operasional', 'na', '0813-2011-7116', 'priatno@ipms.co.id', 1),
(40, 23, 'Nelson', 'na', 'na', 'nelson@itm-corps.com', '0811-910-757', 1),
(41, 23, 'Hariara', 'na', 'na', 'hariara@itm-corps.com', '0811-189-8242', 1),
(42, 24, 'Aji Putranto', 'na', 'na', 'ajiputranto@yahoo.com', '0818-0677-5284', 1),
(43, 25, 'Suvinarto Marpaung', 'Direktur Utama', 'na', 'narto.m@yahoo.com', '0812-7781-3232', 1),
(44, 25, 'Lukman Nadeak', 'Wakil Direktur', 'na', 'na', '0812-7708-293', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendortable`
--

CREATE TABLE IF NOT EXISTS `vendortable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendortype_id` int(11) DEFAULT NULL,
  `id` varchar(15) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `address` text,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`vendortype_id`,`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `vendortable`
--

INSERT INTO `vendortable` (`row_id`, `vendortype_id`, `id`, `name`, `email`, `phone_no`, `address`, `status_id`) VALUES
(1, 2, 'ORLIE', 'Orlie Indonesia', 'na', '021-29461743', 'Jl RS Fatmawati Raya No 30E Cilandak Barat Cilandak Jakarta', 1),
(2, 2, 'BACH', 'Bach Multi Global', 'na', '021-386-2451', 'Jl Cideng Barat No 72 Jakarta Pusat', 1),
(3, 2, 'GLOBAL', 'Global Comtech Sejahtera', 'na', '061-456-1620', 'Jl Labu I No 13/35 Kelurahan Petisah Hulu Medan Sumatera Utara', 1),
(4, 2, 'BANGKIT', 'Bangkit Selaras Utama', 'na', '021-8265-0259', 'Jl Aneka Elok No 6 RT 011/004 Kel Cipinang Muara Jatinegara Jakarta Timur', 1),
(5, 2, 'MULTI', 'Multi Kreasi Utama', 'na', '021-8378-9208', 'Gedung Menara MTH GF Jl MT Haryono Kav 23 Jakarta Selatan', 1),
(6, 2, 'ADYAWINSA', 'Adyawinsa Telecommunication & Electrical', 'na', '021-460-3353', 'Jl Pegangsaan Dua KM No 64 RT 005 RW 002', 1),
(7, 2, 'BINOTEL', 'Binotel Arlintang Pratama', 'na', '0411-459-513', 'Komplek BTN CV Dewi Blok B 5 No 13 Makasar', 1),
(8, 2, 'ROBERTO', 'Roberto Pratama Karya', 'na', '0751-461-668', 'Maransi Indah No 129 RT 003/012 Kel Dadok Tunggul Hitam Kec Koto Tangah Padang', 1),
(9, 2, 'MANOLO', 'Manolo Putra', 'na', '021-601-6317', 'Sumber Endah No 20-12 Bandung', 1),
(10, 1, 'CELEBES', 'Celebes Konstruksindo', 'na', '0451-413-4078 / 021-569-54516', 'Jl Tupai B/6 Tatura Selatan Palu Selatan Palu', 1),
(11, 2, 'QUADRATEL', 'Quadratel Persada', 'na', '021-7919-1955 / 021-798-9011', 'Gedung Fortune Jl Mampang Prapatan Raya No 96 Jakarta Selatan', 1),
(12, 2, 'AGCIA', 'Agcia Pertiwi', 'na', '021-385-0965', 'Jl Tanah Abang III Jakarta Pusat', 1),
(13, 2, 'CIPTA', 'Cipta Daya Selaras', 'na', '021-8661-5874', 'Komplek PU P4 S No A5 Pondok Bambu Jakarta Timur', 1),
(14, 2, 'NAYAKA', 'Nayaka Pratama', 'na', '021-8990-8743', 'Jl Raya Pondok Kelapa Blok F No 1 Duren Sawit Jakarta Timur', 1),
(15, 1, 'DAYA', 'Daya Mitra Kasuar', 'na', '031-847-1365 / 0815-5970-0033', 'Bendul Merisi 134D Surabaya', 1),
(16, 2, 'TRITAMA', 'Tritama Aji Laksana', 'na', '021-290-47301', 'Jl Bhayangkara Pergudangan T8 No 46-48 Alam Sutera Pakulon Serpong Utama Tangerang Selatan', 1),
(17, 2, 'LIMA', 'Lima Pilar Sukses', 'na', '061-844-8585', 'Taman Tomang Elok #B 86 Jl Murai 1 Medan', 1),
(18, 2, 'TELEHOUSE', 'Telehouse Engineering', 'na', '021-7280-0925', 'Simprug Gallery Jl Teuku Nyak Arif No 10 Jakarta', 1),
(19, 2, 'KAIZEN', 'Kaizen Konsultan', 'na', '021-5890-6419', 'Jl Meruya Selatan Komplek Ruko Puri Botanical Jakarta Barat', 1),
(20, 2, 'JAYA', 'Jaya Engineering Technology', 'na', '021-8911-7341', 'Jl Gaharu Blok F2 No 2D Delta Silocon II Lippo Cikarang Industri Park Bekasi', 1),
(21, 2, 'UNIVERSE', 'Universe Transportindo', 'na', 'na', 'Jl Cipinang Cempedak I No 46 A Cipinang Cempedak Jatinegara Jakarta Timur', 1),
(22, 2, 'INTI', 'Inti Pindad Mitra Sejati', 'na', '022-520-1693', 'Jl Moh Toha No 77 Gd GKP Lt 4 Bandung', 1),
(23, 2, 'INDOTUNAS', 'Indotunas Makmur, PT.', 'na', 'na', 'Mega Plaza Building 4 Floor Jl HR Rasuna Said Kav C3 Kuningan Jakarta Selatan', 1),
(24, 2, 'DWIPUTRA', 'Dwi Putra Nugraha', 'na', '024-670-6617', 'Jl Mahameru Timur I / 458 Semarang', 1),
(25, 2, 'MITRASETIA', 'Mitra Setia Indonesia', 'na', '024-6706619', 'Komp Ruko Fransisko Blok A2 No 6 Kel Kibing Kec Batu Aji Batam', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendortypetable`
--

CREATE TABLE IF NOT EXISTS `vendortypetable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `txt` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `IDX` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vendortypetable`
--

INSERT INTO `vendortypetable` (`row_id`, `txt`, `status_id`) VALUES
(1, 'Construction', 1),
(2, 'Fabrication', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_usertablelist`
--
CREATE TABLE IF NOT EXISTS `vw_usertablelist` (
`row_id` int(11)
,`role_id` int(11)
,`roletable_txt` varchar(45)
,`name` varchar(100)
,`email` varchar(100)
,`user_id` varchar(100)
,`pswrd` varchar(100)
,`photo` varchar(20)
,`status_id` varchar(45)
,`statustable_name` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `workflowmilestonetable`
--

CREATE TABLE IF NOT EXISTS `workflowmilestonetable` (
  `id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workflowmilestonetable`
--

INSERT INTO `workflowmilestonetable` (`id`, `name`) VALUES
(1, 'Creation'),
(2, 'Approval'),
(9, 'Complete');

-- --------------------------------------------------------

--
-- Table structure for table `workorderapprovalflowtable`
--

CREATE TABLE IF NOT EXISTS `workorderapprovalflowtable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `ordering` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `workorderapprovalflowtable`
--

INSERT INTO `workorderapprovalflowtable` (`row_id`, `ordering`, `role_id`, `remark`) VALUES
(1, 1, 6, 'Project Manager'),
(2, 2, 9, 'Operation Director');

-- --------------------------------------------------------

--
-- Table structure for table `workorderapprovaltable`
--

CREATE TABLE IF NOT EXISTS `workorderapprovaltable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `workorder_no` varchar(125) NOT NULL,
  `approvalflow_id` int(11) NOT NULL,
  `approved_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `approved_status` int(11) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `workorderapprovaltable`
--

INSERT INTO `workorderapprovaltable` (`row_id`, `workorder_no`, `approvalflow_id`, `approved_date`, `approved_status`, `remark`, `status_id`) VALUES
(7, '0001/WO/XII/2017', 1, '2018-01-02 03:39:51', 1, 'Approved by Project Manager', 1),
(9, '0001/WO/XII/2017', 2, '2018-01-05 03:05:44', 1, 'Approved by Operation Director', 1);

-- --------------------------------------------------------

--
-- Table structure for table `workorderlinetable`
--

CREATE TABLE IF NOT EXISTS `workorderlinetable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `workorder_no` varchar(255) NOT NULL,
  `site_id` int(11) NOT NULL,
  `projectactivity_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `workorderlinetable`
--

INSERT INTO `workorderlinetable` (`row_id`, `workorder_no`, `site_id`, `projectactivity_id`, `start_date`, `end_date`, `status_id`) VALUES
(1, '0001/WO/XII/2017', 2, 1, '2017-12-19', '2017-12-20', 1),
(2, '0001/WO/XII/2017', 2, 2, '2017-12-21', '2017-12-21', 1),
(5, '0001/WO/XII/2017', 2, 3, '2017-12-21', '2017-12-23', 1),
(6, '0002/WO/XII/2017', 2, 7, '2018-01-09', '2018-01-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `workordertable`
--

CREATE TABLE IF NOT EXISTS `workordertable` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `workorder_no` varchar(255) NOT NULL,
  `workorder_date` date NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `vendorcontact_id` int(11) NOT NULL,
  `product_group` varchar(125) NOT NULL,
  `project_manager` varchar(125) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `work_flow_milestone_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `workordertable`
--

INSERT INTO `workordertable` (`row_id`, `workorder_no`, `workorder_date`, `vendor_id`, `vendorcontact_id`, `product_group`, `project_manager`, `remark`, `work_flow_milestone_id`, `status_id`) VALUES
(1, '0001/WO/XII/2017', '2017-12-18', 1, 1, 'ABC', 'Asep', 'test', 9, 1),
(2, '0002/WO/XII/2017', '2017-12-18', 1, 1, 'XYZ', 'Encep', 'test', 2, 1),
(3, '0003/WO/I/2018', '2018-01-16', 2, 2, 'ABC', 'Abai', 'na', 1, 1);

-- --------------------------------------------------------

--
-- Structure for view `vw_usertablelist`
--
DROP TABLE IF EXISTS `vw_usertablelist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_usertablelist` AS select `usertable`.`row_id` AS `row_id`,`usertable`.`role_id` AS `role_id`,`roletable`.`txt` AS `roletable_txt`,`usertable`.`name` AS `name`,`usertable`.`email` AS `email`,`usertable`.`user_id` AS `user_id`,`usertable`.`pswrd` AS `pswrd`,`usertable`.`photo` AS `photo`,`usertable`.`status_id` AS `status_id`,`statustable`.`name` AS `statustable_name` from ((`usertable` join `roletable` on((`roletable`.`row_id` = `usertable`.`role_id`))) join `statustable` on((`statustable`.`row_id` = `usertable`.`status_id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
