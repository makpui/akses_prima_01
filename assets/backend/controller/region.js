$(document).ready(function() {
  generateTable();

  $("#btnSave").click(function(){
    doSave();
  });
  
  $("#txtstatusidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusidfilter').html('');
				$('#txtstatusidfilter').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusidfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtareaidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getAreaList/',
          success: function(ret)
          {
				$('#txtareaidfilter').html('');
				$('#txtareaidfilter').append($('<option>').text('View All Area').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtareaidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  
});

function generateTable()
{   
	var where = [];
  
	if($('#txtstatusidfilter').val() != '')
	{
		where.push({key : 'status_id', value : $('#txtstatusidfilter').val()});
	}

	if($('#txtareaidfilter').val() != '')
	{
		where.push({key : 'area_id', value : $('#txtareaidfilter').val()});
	}

  var table = $('#tablelist').DataTable( {
    ajax: {
      "url": base_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 1, "asc" ],[ 2, "asc" ]],
    dom: 'Bfrtip',
    buttons: [
        {
            text: '<i class="fa fa-plus"></i> Add New',
            action: function ( e, dt, node, config ) {
                generateModalForm('add', '');
            }
        }, 
        {
            extend: 'copy',
            text: '<i class="fa fa-copy"></i> Copy',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'excel',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-excel-o"></i> Excel',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'pdf',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-pdf-o"></i> PDF',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'print',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-print"></i> Print',
            exportOptions: {
                columns: []
            }
        }
    ]
  });
}



function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}

function generateModalForm(state, row_id) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });
  
  $("#txtstatusid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusid').html('');
				//$('#txtstatusid').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  

  $("#txtareaid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getAreaList/',
          success: function(ret)
          {
				$('#txtareaid').html('');
				$('#txtareaid').append($('<option>').text('--Select an Area--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtareaid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
}

function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
		  $('#txtareaid').select2();
          $('#txtstatusid').select2();
      }
  });
}

function doSave(){

  var formData = new FormData($('.formInput')[0]);

  var txtareaid = $('#txtareaid').val(); 
  var txtshorttxt = $('#txtshorttxt').val(); 
  var txttxt = $('#txttxt').val(); 
  var txtstatusid = $('#txtstatusid').val(); 

  if(txtareaid == '') {infoStatus('Area must be filled', 0); exit();}
  if(txtshorttxt == '') {infoStatus('Region Code must be filled', 0); exit();}
  if(txttxt == '') {infoStatus('Region Name must be filled', 0); exit();}
  if(txtstatusid == '') {infoStatus('Status must be filled', 0); exit();}

  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: base_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: base_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}