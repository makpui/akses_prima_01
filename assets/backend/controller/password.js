$(document).ready(function() {
  generateTable();

  $("#btnSave").click(function(){
    doSave();
  });
  
  $("#txtstatusidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusidfilter').html('');
				$('#txtstatusidfilter').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusidfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtroleidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getRoleList/',
          success: function(ret)
          {
				$('#txtroleidfilter').html('');
				$('#txtroleidfilter').append($('<option>').text('View All Role').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtroleidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });  
  
});

function generateTable()
{   
	var where = [];
  
	if($('#txtstatusidfilter').val() != '')
	{
		where.push({key : 'status_id', value : $('#txtstatusidfilter').val()});
	}

	if($('#txtroleidfilter').val() != '')
	{
		where.push({key : 'role_id', value : $('#txtroleidfilter').val()});
	}
	 
                
  var table = $('#tablelist').DataTable( {
    ajax: {
      "url": base_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 0, "desc" ]],
    dom: 'Bfrtip',
    buttons: [

        {
            extend: 'copy',
            text: '<i class="fa fa-copy"></i> Copy',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'excel',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-excel-o"></i> Excel',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'pdf',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-pdf-o"></i> PDF',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'print',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-print"></i> Print',
            exportOptions: {
                columns: []
            }
        }
    ]
  });
}



function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}

function generateModalForm(state, row_id) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });  
  
}
 
function checkPassword()
{
	var txtrowid = $('#txtrowid').val();
	var txtpswrd = $('#txtpswrd').val(); 
	$.ajax({
      type: 'post',
      async: false,
      url: base_url + 'isPasswordExist/',
	  data: {'txtrowid': txtrowid, 'txtpswrd': txtpswrd},
      success: function(ret) {	  
		  var data = JSON.parse(ret);
		  if(data['isexist'] == 0) {
			alert("Incorrect current password");
			setTimeout(function() {
				  $('#txtpswrd').val('').focus();
				}, 0);
		  }
      }
	});	
}  


function checkNewPassword()
{
	var txtpswrdnewa = $('#txtpswrdnewa').val(); 
	var txtpswrdnewb = $('#txtpswrdnewb').val(); 
	if(txtpswrdnewa != txtpswrdnewb) {
		alert("Incorrect confirm password");
		setTimeout(function() {
			  $('#txtpswrdnewb').val('').focus();
			}, 0);
	}		
}  


function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          $('#txtroleid').select2();$('#txtstatusid').select2();
      }
  });
}

function doSave(){

  var formData = new FormData($('.formInput')[0]);

  var txtpswrd = $('#txtpswrd').val(); 
  var txtpswrdnewa = $('#txtpswrdnewa').val(); 
  var txtpswrdnewb = $('#txtpswrdnewb').val(); 

  if(txtpswrd == '') {infoStatus('Current password empty', 0); exit();}
  if(txtpswrdnewb == '') {infoStatus('Confirm password empty', 0); exit();}
  if(txtpswrdnewa != txtpswrdnewb) {infoStatus('Confirm password not match', 0); exit();}
  
  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: base_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
}
