$(document).ready(function() {
  generateTable();

  $("#btnSave").click(function(){
    doSave();
  });
  
  $("#txtstatusidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusidfilter').html('');
				$('#txtstatusidfilter').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusidfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtroleidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getRoleList/',
          success: function(ret)
          {
				$('#txtroleidfilter').html('');
				$('#txtroleidfilter').append($('<option>').text('View All Role').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtroleidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  
});

function generateTable()
{   
	var where = [];
  
	if($('#txtstatusidfilter').val() != '')
	{
		where.push({key : 'status_id', value : $('#txtstatusidfilter').val()});
	}

	if($('#txtroleidfilter').val() != '')
	{
		where.push({key : 'role_id', value : $('#txtroleidfilter').val()});
	}
	                
  var table = $('#tablelist').DataTable( {
    ajax: {
      "url": base_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 1, "asc" ] ],
    dom: 'Bfrtip',
    buttons: [
        {
            text: '<i class="fa fa-plus"></i> Add New',
            action: function ( e, dt, node, config ) {
                generateModalForm('add', '');
            }
        }, 
        {
            extend: 'copy',
            text: '<i class="fa fa-copy"></i> Copy',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'excel',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-excel-o"></i> Excel',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'pdf',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-pdf-o"></i> PDF',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'print',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-print"></i> Print',
            exportOptions: {
                columns: []
            }
        }
    ]
  });
}



function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}

function generateModalForm(state, row_id) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });
  
  
  $("#txtstatusid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusid').html('');
				//$('#txtstatusid').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtroleid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getRoleList/',
          success: function(ret)
          {
				$('#txtroleid').html('');
				$('#txtroleid').append($('<option>').text('--Select a Role--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtroleid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
  
}

function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          $('#txtroleid').select2();$('#txtstatusid').select2();
      }
  });
}

function doSave(){

  var formData = new FormData($('.formInput')[0]);

  var txtroleid = $('#txtroleid').val(); 
var txtname = $('#txtname').val(); 
var txtemail = $('#txtemail').val(); 
var txtuserid = $('#txtuserid').val(); 
var txtpswrd = $('#txtpswrd').val(); 
var txtphoto = $('#txtphoto').val(); 
var txtphoto_hdn = $('#txtphoto_hdn').val(); 
var txtstatusid = $('#txtstatusid').val(); 

  if(txtroleid == '') {infoStatus('Role must be filled', 0); exit();}
  if(txtname == '') {infoStatus('Name must be filled', 0); exit();}
  if(txtemail == '') {infoStatus('Email must be filled', 0); exit();}
  if(txtuserid == '') {infoStatus('User Id must be filled', 0); exit();}
  if(txtstatusid == '') {infoStatus('Status must be filled', 0); exit();}

  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: base_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: base_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}