if($(location).attr('origin').split('/').pop() == 'localhost')
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' + $(location).attr('pathname').split('/')[2] + '/';
}
else
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' ;
}

$(document).ready(function() {
  generateTable();
  
  $("#btnSave").click(function(){
    doSave();
  });  
         
});
 
 
function generateTable()
{   
	var where = [];

	if($('#txtfilter_workordertable_workorder_no').val() != '')
	{
		where.push({key : 'workorder_no', value : $('#txtfilter_workordertable_workorder_no').val()});
	}
    
  var table = $('#tablelist').DataTable( {
    ajax: {
	  "url": basename_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
	scrollX: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 0, "asc" ]],
    dom: 'Bfrtip',
    buttons: [
				{
					text: '<i class="fa fa-plus"></i> Add New Line',
					className: 'dt-button-add',					
					action: function ( e, dt, node, config ) {
						generateModalForm('add', '');
					}
				},
				{
					text: '<i class="fa fa-stop"></i> Complete',
					className: 'dt-button-stop',					
					action: function ( e, dt, node, config ) {	
						window.location = basename_url  + 'completeCreation/' + $('#txtfilter_workordertable_row_id').val();
					}
				},
				{
					text: '<i class="fa fa-play"></i> Un Complete' ,
					className: 'dt-button-play',					
					action: function ( e, dt, node, config ) {	
						window.location = basename_url + 'unCompleteCreation/' + $('#txtfilter_workordertable_row_id').val();
					}
				}
				
			]
  });

  /**
   * Controlling visibility button
   * 1. Add New Line is visible at creation state only
   * 2. Complete is visible at creation state && salesorder_no is already exist in salesorderlinetable
   * 3. Uncomplete is available in approval state && still no one allready approve 
   */
   
  $('.dt-button-add').hide(); 
  $('.dt-button-stop').hide(); 
  $('.dt-button-play').hide();

  if($('#is_so_maker').val() == 1)
  {  
  
	  if($('#workordertable_work_flow_milestone_id').val() ==  $('#workflowmilestonetable_creation_state').val())
	  {
		$('.dt-button-add').show(); 
		
		if($('#salesorderlinetable_work_order_isexist').val())
		{
			$('.dt-button-stop').show(); 	
		}
	  }
	  else if($('#workordertable_work_flow_milestone_id').val() !=  $('#workflowmilestonetable_ending_state').val() && !$('#workorderapprovaltable_work_order_isexist').val())
	  {
		$('.dt-button-play').show();
	  }  

  }
  
}

function reloadPage()
{
  location.reload(true);
}

function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();

}

function generateModalForm(state, row_id) {
  //$('#modalForm').modal();
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true,
	
  });
  
  $("#modalForm").on('shown.bs.modal', function () {
        $("#txtstartdate").datepicker({dateFormat: 'dd-mm-yy'});
        $("#txtenddate").datepicker({dateFormat: 'dd-mm-yy'});				
  });
    
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id, 'txtheaderworkorder': $('#txtfilter_workordertable_workorder_no').val()},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });

  /** SITE ID
	  auto complete version
    */
  $("#txtsiteidapi").autocomplete({
	minLength: 1,
	source: function(request, response)
	{
		$.ajax({
			type: 'POST',
			url: basename_url + 'getAvailableSiteList',
			data : {searched_site: request.term},
			dataType: 'json',
			
			success : function(ret) {
				if(ret.length < 1)
				{
					infoStatus('Data not found');
					$("#txtsiteidapi").focus().val('');
				}
				else
				{
					var siteidapis = [];
					$.each(ret, function(key, value)
					{
						siteidapis.push({label : value['site_id_api'], value : value['row_id'], towertype_id : value['towertype_id']});
					})
					response(siteidapis);				
				}				
			}		 
		})
	},
	
	select: function(event, ui) { 
		$('#txtsiteidapi').val(ui.item.label);
		$('#txttowertypeid').val(ui.item.towertype_id);
		$('#txtsiteid').val(ui.item.value);
		return false;	
	}	
	
  });

  
  $("#txtsiteidapi").blur(function(){
	$.ajax({
	  type: 'post',
	  async: false,
	  url: basename_url + 'getSiteIdApiFromSiteTable',
	  data: {site_id_api: $("#txtsiteidapi").val()},
	  success: function(ret)
	  {	  
		if(ret == 0)
		{
			infoStatus('Site Not Exist');
			$("#txtsiteidapi").focus();
		}
	  }
	});
  
  });
  
  
  /** SITE ID
      dropdown list version --- obsoleted replace with above auto complete
	  
  $("#txtsiteid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getAvailableSiteList/',
          success: function(ret)
          {
				$('#txtsiteid').html('');
				$('#txtsiteid').append($('<option>').text('--Select an Site--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtsiteid').append($('<option>').text(value['site_id_api'] +' : '+ value['site_name_api'] +' : '+ value['address']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  */
  
  var txtprojectactivityid = $("#txtprojectactivityid").val();  
  $("#txtprojectactivityid").focus(function()
  {		
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getAvailableProjectActivityList/' +  $("#txtsiteid").val(),
		  data: {'txtstate' : $("#txtstate").val(), 'txtprojectactivityid': txtprojectactivityid},		  
          success: function(ret)
          {
				$('#txtprojectactivityid').html('');
				$('#txtprojectactivityid').append($('<option>').text('--Select an Site--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtprojectactivityid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
  /** START & FINISH DATE (OBSOLETE)
   *  ==========================================================================================  
   *  TOWER TYPE  SOW            DURATION    PREDECESSOR    START     FINISH
   *  ------------------------------------------------------------------------------------------
   *  A. Macro
   *              Site Survey        7 d                   15-Mar      22-Mar   
   *              SITAC             45 d     FS+1          23-Mar       7-May
   *                 Soil Test       7 d     SF-7          30-Apr       7-May
   *                 IMB             3 d     SF-3           4-May       7-May
   *              CME               40 d     FS+1           8-May      17-Jun
   *
   *  B. Non Macro
   *              Site Survey        7 d                   15-Mar      22-Mar   
   *              SITAC             45 d     FS+1          23-Mar       7-May
   *                 Soil Test       7 d     SF-7          30-Apr       7-May
   *                 IMB             3 d     SF-3           4-May       7-May
   *              CME               30 d     FS+1           8-May       7-Jun
   *
   */

   // auto fill date
	var workorderdate =  $("#workordertable_work_order_date").val().split("-");

  /** START & FINISH DATE
   *  ==========================================================================================  
   *  TOWER TYPE  SOW            DURATION    PREDECESSOR    START     FINISH
   *  ------------------------------------------------------------------------------------------
   *  A. Macro
   *              Work Order         1 d                    2-Apr       2-Apr   
   *              Site Survey       14 d     FS+1           3-Apr      17-Apr   
   *              SITAC             45 d     FS+1          18-Apr       2-Jun
   *                 Soil Test       7 d     SF-7          26-May       2-Jun
   *                 IMB             1 m     SF-3           2-Jun       6-Jul
   *              CME               33 d     FS+1           3-Jun       6-Jul
   *
   */
	
	var macro_site_survey_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1);
	var macro_site_survey_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 14);
	
	var macro_sitac_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 14 + 1);
	var macro_sitac_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 14 + 1 + 45);
	
	var macro_soil_test_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 14 + 1 + 45 - 7);
	var macro_soil_test_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 14 + 1 + 45);
	
	var macro_imb_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 14 + 1 + 45 - 3);
	var macro_imb_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 14 + 1 + 45 + 1 + 33);
	
	var macro_cme_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 14 + 1 + 45 + 1);
	var macro_cme_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 14 + 1 + 45 + 1 + 33);

	
  /** NON  MACRO SITE START & FINISH DATE
   *  ==========================================================================================  
   *  TOWER TYPE  SOW            DURATION    PREDECESSOR    START     FINISH
   *  ------------------------------------------------------------------------------------------
   *  B. Non Macro
   *              Work Order         1 d                    2-Apr       2-Apr   
   *              Site Survey        7 d     FS+1           3-Apr      10-Apr   
   *              SITAC             30 d     FS+1          11-Apr      11-Jun
   *                 Soil Test       7 d     SF-7           4-May      11-Jun
   *                 IMB             1 m     SF-3          11-May       2-Jun
   *              CME               21 d     FS+1          12-May       2-Jun
   *
   */
	var non_macro_site_survey_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1);
	var non_macro_site_survey_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 7);
	
	var non_macro_sitac_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 7 + 1);
	var non_macro_sitac_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 7 + 1 + 30);
	
	var non_macro_soil_test_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 7 + 1 + 30 - 7);
	var non_macro_soil_test_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 7 + 1 + 30);
	
	var non_macro_imb_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2])  + 1 + 7 + 1 + 30 - 3);
	var non_macro_imb_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 7 + 1 + 30 + 1 + 21);
	
	var non_macro_cme_start_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 7 + 1 + 30 + 1);
	var non_macro_cme_finish_date  = new Date(parseInt(workorderdate[0]), parseInt(workorderdate[1]) - 1, parseInt(workorderdate[2]) + 1 + 7 + 1 + 30 + 1 + 21);
	
	$("#txtprojectactivityid").change(function()
	{
		if($("#txtprojectactivityid").val() == 1) // site survey
		{
			var txttowertypeid = $("#txttowertypeid").val();
			if(txttowertypeid == 3) // macro
			{
				$("#txtstartdate").val((macro_site_survey_start_date.getDate().toString().length == 2 ? macro_site_survey_start_date.getDate().toString() : 0+macro_site_survey_start_date.getDate().toString()) +'-'+ ((parseInt(macro_site_survey_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_site_survey_start_date.getMonth())+1).toString() : 0+(parseInt(macro_site_survey_start_date.getMonth())+1).toString()) +'-'+ macro_site_survey_start_date.getFullYear());			
				$("#txtenddate").val((macro_site_survey_finish_date.getDate().toString().length == 2 ? macro_site_survey_finish_date.getDate().toString() : 0+macro_site_survey_finish_date.getDate().toString()) +'-'+ ((parseInt(macro_site_survey_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_site_survey_finish_date.getMonth())+1).toString() : 0+(parseInt(macro_site_survey_finish_date.getMonth())+1).toString()) +'-'+ macro_site_survey_finish_date.getFullYear());
			}
			else
			{
				$("#txtstartdate").val((non_macro_site_survey_start_date.getDate().toString().length == 2 ? non_macro_site_survey_start_date.getDate().toString() : 0+non_macro_site_survey_start_date.getDate().toString()) +'-'+ ((parseInt(non_macro_site_survey_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_site_survey_start_date.getMonth())+1).toString() : 0+(parseInt(non_macro_site_survey_start_date.getMonth())+1).toString()) +'-'+ non_macro_site_survey_start_date.getFullYear());			
				$("#txtenddate").val((non_macro_site_survey_finish_date.getDate().toString().length == 2 ? non_macro_site_survey_finish_date.getDate().toString() : 0+non_macro_site_survey_finish_date.getDate().toString()) +'-'+ ((parseInt(non_macro_site_survey_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_site_survey_finish_date.getMonth())+1).toString() : 0+(parseInt(non_macro_site_survey_finish_date.getMonth())+1).toString()) +'-'+ non_macro_site_survey_finish_date.getFullYear());			
			}			
		}
		
		if($("#txtprojectactivityid").val() == 2) // sitac
		{
			var txttowertypeid = $("#txttowertypeid").val();
			if(txttowertypeid == 3) // macro
			{
				$("#txtstartdate").val((macro_sitac_start_date.getDate().toString().length == 2 ? macro_sitac_start_date.getDate().toString() : 0+macro_sitac_start_date.getDate().toString()) +'-'+ ((parseInt(macro_sitac_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_sitac_start_date.getMonth())+1).toString() : 0+(parseInt(macro_sitac_start_date.getMonth())+1).toString()) +'-'+ macro_sitac_start_date.getFullYear());			
				$("#txtenddate").val((macro_sitac_finish_date.getDate().toString().length == 2 ? macro_sitac_finish_date.getDate().toString() : 0+macro_sitac_finish_date.getDate().toString()) +'-'+ ((parseInt(macro_sitac_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_sitac_finish_date.getMonth())+1).toString() : 0+(parseInt(macro_sitac_finish_date.getMonth())+1).toString()) +'-'+ macro_sitac_finish_date.getFullYear());		
			}
			else
			{
				$("#txtstartdate").val((non_macro_sitac_start_date.getDate().toString().length == 2 ? non_macro_sitac_start_date.getDate().toString() : 0+non_macro_sitac_start_date.getDate().toString()) +'-'+ ((parseInt(non_macro_sitac_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_sitac_start_date.getMonth())+1).toString() : 0+(parseInt(non_macro_sitac_start_date.getMonth())+1).toString()) +'-'+ non_macro_sitac_start_date.getFullYear());			
				$("#txtenddate").val((non_macro_sitac_finish_date.getDate().toString().length == 2 ? non_macro_sitac_finish_date.getDate().toString() : 0+non_macro_sitac_finish_date.getDate().toString()) +'-'+ ((parseInt(non_macro_sitac_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_sitac_finish_date.getMonth())+1).toString() : 0+(parseInt(non_macro_sitac_finish_date.getMonth())+1).toString()) +'-'+ non_macro_sitac_finish_date.getFullYear());					
			}
		}

		if($("#txtprojectactivityid").val() == 5) // soil test
		{
			var txttowertypeid = $("#txttowertypeid").val();
			if(txttowertypeid == 3) // macro
			{
				$("#txtstartdate").val((macro_soil_test_start_date.getDate().toString().length == 2 ? macro_soil_test_start_date.getDate().toString() : 0+macro_soil_test_start_date.getDate().toString()) +'-'+ ((parseInt(macro_soil_test_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_soil_test_start_date.getMonth())+1).toString() : 0+(parseInt(macro_soil_test_start_date.getMonth())+1).toString()) +'-'+ macro_soil_test_start_date.getFullYear());			
				$("#txtenddate").val((macro_soil_test_finish_date.getDate().toString().length == 2 ? macro_soil_test_finish_date.getDate().toString() : 0+macro_soil_test_finish_date.getDate().toString()) +'-'+ ((parseInt(macro_soil_test_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_soil_test_finish_date.getMonth())+1).toString() : 0+(parseInt(macro_soil_test_finish_date.getMonth())+1).toString()) +'-'+ macro_soil_test_finish_date.getFullYear());		
			}
			else
			{
				$("#txtstartdate").val((non_macro_soil_test_start_date.getDate().toString().length == 2 ? non_macro_soil_test_start_date.getDate().toString() : 0+non_macro_soil_test_start_date.getDate().toString()) +'-'+ ((parseInt(non_macro_soil_test_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_soil_test_start_date.getMonth())+1).toString() : 0+(parseInt(non_macro_soil_test_start_date.getMonth())+1).toString()) +'-'+ non_macro_soil_test_start_date.getFullYear());			
				$("#txtenddate").val((non_macro_soil_test_finish_date.getDate().toString().length == 2 ? non_macro_soil_test_finish_date.getDate().toString() : 0+non_macro_soil_test_finish_date.getDate().toString()) +'-'+ ((parseInt(non_macro_soil_test_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_soil_test_finish_date.getMonth())+1).toString() : 0+(parseInt(non_macro_soil_test_finish_date.getMonth())+1).toString()) +'-'+ non_macro_soil_test_finish_date.getFullYear());			
			}
		}
		
		if($("#txtprojectactivityid").val() == 4) // imb
		{
			var txttowertypeid = $("#txttowertypeid").val();
			if(txttowertypeid == 3) // macro
			{
				$("#txtstartdate").val((macro_imb_start_date.getDate().toString().length == 2 ? macro_imb_start_date.getDate().toString() : 0+macro_imb_start_date.getDate().toString()) +'-'+ ((parseInt(macro_imb_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_imb_start_date.getMonth())+1).toString() : 0+(parseInt(macro_imb_start_date.getMonth())+1).toString()) +'-'+ macro_imb_start_date.getFullYear());			
				$("#txtenddate").val((macro_imb_finish_date.getDate().toString().length == 2 ? macro_imb_finish_date.getDate().toString() : 0+macro_imb_finish_date.getDate().toString()) +'-'+ ((parseInt(macro_imb_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_imb_finish_date.getMonth())+1).toString() : 0+(parseInt(macro_imb_finish_date.getMonth())+1).toString()) +'-'+ macro_imb_finish_date.getFullYear());		
			}
			else
			{
				$("#txtstartdate").val((non_macro_imb_start_date.getDate().toString().length == 2 ? non_macro_imb_start_date.getDate().toString() : 0+non_macro_imb_start_date.getDate().toString()) +'-'+ ((parseInt(non_macro_imb_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_imb_start_date.getMonth())+1).toString() : 0+(parseInt(non_macro_imb_start_date.getMonth())+1).toString()) +'-'+ non_macro_imb_start_date.getFullYear());			
				$("#txtenddate").val((non_macro_imb_finish_date.getDate().toString().length == 2 ? non_macro_imb_finish_date.getDate().toString() : 0+non_macro_imb_finish_date.getDate().toString()) +'-'+ ((parseInt(non_macro_imb_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_imb_finish_date.getMonth())+1).toString() : 0+(parseInt(non_macro_imb_finish_date.getMonth())+1).toString()) +'-'+ non_macro_imb_finish_date.getFullYear());					
			}
		}
		
		if($("#txtprojectactivityid").val() == 7) // CME
		{
			var txttowertypeid = $("#txttowertypeid").val();
			if(txttowertypeid == 3) // macro
			{
				$("#txtstartdate").val((macro_cme_start_date.getDate().toString().length == 2 ? macro_cme_start_date.getDate().toString() : 0+macro_cme_start_date.getDate().toString()) +'-'+ ((parseInt(macro_cme_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_cme_start_date.getMonth())+1).toString() : 0+(parseInt(macro_cme_start_date.getMonth())+1).toString()) +'-'+ macro_cme_start_date.getFullYear());			
				$("#txtenddate").val((macro_cme_finish_date.getDate().toString().length == 2 ? macro_cme_finish_date.getDate().toString() : 0+macro_cme_finish_date.getDate().toString()) +'-'+ ((parseInt(macro_cme_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(macro_cme_finish_date.getMonth())+1).toString() : 0+(parseInt(macro_cme_finish_date.getMonth())+1).toString()) +'-'+ macro_cme_finish_date.getFullYear());				
			}
			else // non macro
			{
				$("#txtstartdate").val((non_macro_cme_start_date.getDate().toString().length == 2 ? non_macro_cme_start_date.getDate().toString() : 0+non_macro_cme_start_date.getDate().toString()) +'-'+ ((parseInt(non_macro_cme_start_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_cme_start_date.getMonth())+1).toString() : 0+(parseInt(non_macro_cme_start_date.getMonth())+1).toString()) +'-'+ non_macro_cme_start_date.getFullYear());			
				$("#txtenddate").val((non_macro_cme_finish_date.getDate().toString().length == 2 ? non_macro_cme_finish_date.getDate().toString() : 0+non_macro_cme_finish_date.getDate().toString()) +'-'+ ((parseInt(non_macro_cme_finish_date.getMonth())+1).toString().legnth == 2 ? (parseInt(non_macro_cme_finish_date.getMonth())+1).toString() : 0+(parseInt(non_macro_cme_finish_date.getMonth())+1).toString()) +'-'+ non_macro_cme_finish_date.getFullYear());							
			}		
		}
	
    });
  
  
  $("#txtstatusid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusid').html('');
				//$('#txtstatusid').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
  
}

function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          $('#txtsiteid').select2();
      }
  });
}

function doSave(){

	var formData = new FormData($('.formInput')[0]);

	var txtworkorderno = $('#txtworkorderno').val(); 
	var txtsiteid = $('#txtsiteid').val(); 
	var txtprojectactivityid = $('#txtprojectactivityid').val();
	
	var txtstartdate = new Date(parseInt($('#txtstartdate').val().substring(7)), parseInt($('#txtstartdate').val().substring(3,6)) -1, parseInt($('#txtstartdate').val().substring(0,2)));	
	var txtenddate = new Date(parseInt($('#txtenddate').val().substring(7)), parseInt($('#txtenddate').val().substring(3,6)) -1, parseInt($('#txtenddate').val().substring(0,2)));	

	var txtstatusid = $('#txtstatusid').val();

	if(txtworkorderno == '') {infoStatus('Work Order No. must be filled', 0); exit();}
	if(txtsiteid == '') {infoStatus('Site must be filled', 0); exit();}
	if(txtprojectactivityid == '') {infoStatus('Milestone Tenant must be filled', 0); exit();}
	if($('#txtstartdate').val() == '') {infoStatus('Start date must be filled', 0); exit();}
	if($('#txtenddate').val() == '') {infoStatus('End date must be filled', 0); exit();}
	if(txtstatusid == '') {infoStatus('Status must be filled', 0); exit();}
	
	if(txtenddate < txtstartdate) {infoStatus('Target Finished later than Target Started', 0); exit();}	
	
  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: basename_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      //reloadDatatable();
	  reloadPage();
      infoStatus(data['msg'], data['type']);
    }
  });
}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: basename_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
			  reloadPage();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}