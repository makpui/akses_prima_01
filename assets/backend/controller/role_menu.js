if($(location).attr('origin').split('/').pop() == 'localhost')
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' + $(location).attr('pathname').split('/')[2] + '/';
}
else
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' ;
}

$(document).ready(function() {
  generateTable();

  $("#btnSave").click(function(){
    doSave();
  });
});

function generateTable()
{
	var where = [];
		
	if($('#txtfilter_role_id').val() != '')
	{
		where.push({key : 'role_id', value : $('#txtfilter_role_id').val()});
	}
   
  var table = $('#tablelist').DataTable( {
    ajax: {
	  "url": basename_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 0, "desc" ]],
    dom: 'Bfrtip',
    buttons: [
        {
            text: '<i class="fa fa-plus"></i> Add New',
            action: function ( e, dt, node, config ) {
                generateModalForm('add', $('#txtfilter_role_id').val());
            }
        },
        {
            extend: 'copy',
            text: '<i class="fa fa-copy"></i> Copy',
            exportOptions: {
                columns: []
            }
        },
        {
            extend: 'excel',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-excel-o"></i> Excel',
            exportOptions: {
                columns: []
            }
        },
        {
            extend: 'pdf',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-pdf-o"></i> PDF',
            exportOptions: {
                columns: []
            }
        },
        {
            extend: 'print',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-print"></i> Print',
            exportOptions: {
                columns: []
            }
        }
    ]
  });
}



function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}

function generateModalForm(state, role_id, menu_id) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'form',
      data: {'txtstate': state, 'txtroleid': role_id, 'txtmenuid': menu_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });
 
  $("#txtmenuid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getMenuList/',
		  data: {'txtroleid': $('#txtroleid').val()},
          success: function(ret)
          {
				$('#txtmenuid').html('');
				$('#txtmenuid').append($('<option>').text('--Select a Menu--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtmenuid').append($('<option>').text(value['code']+' - '+value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
}

function generateModalView(role_id, menu_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'view',
      data: {'txtroleid': role_id, 'txtmenuid': menu_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          //$('#txtproductid').select2();
      }
  });
}

function doSave(){

	var formData = new FormData($('.formInput')[0]);

	var txtroleid = $('#txtroleid').val(); 
	var txtmenuid = $('#txtmenuid').val(); 

    if(txtroleid == '') {infoStatus('Role must be filled', 0); exit();}
    if(txtmenuid == '') {infoStatus('Menu must be filled', 0); exit();}

  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: basename_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
}

function doDelete(role_id, menu_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: basename_url + 'delete',
            data: {'txtroleid': role_id, 'txtmenuid': menu_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}