$(document).ready(function() {
  generateTable();

  $("#btnSave").click(function(){
    doSave();
  });
  
  $("#txtstatusidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusidfilter').html('');
				$('#txtstatusidfilter').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusidfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
    
  $("#txtproductidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getProductList/',
          success: function(ret)
          {
				$('#txtproductidfilter').html('');
				$('#txtproductidfilter').append($('<option>').text('View All Product').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtproductidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txttenantidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getTenantList/',
          success: function(ret)
          {
				$('#txttenantidfilter').html('');
				$('#txttenantidfilter').append($('<option>').text('View All Tenant').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txttenantidfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });  
  
  $("#txtareaidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getAreaList/',
          success: function(ret)
          {
				$('#txtareaidfilter').html('');
				$('#txtareaidfilter').append($('<option>').text('View All Area').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtareaidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
 
  $("#txtregionidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getRegionList/'+ $("#txtareaidfilter").val(),
          success: function(ret)
          {
				$('#txtregionidfilter').html('');
				$('#txtregionidfilter').append($('<option>').text('View All Region').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtregionidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtprovinceidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getProvinceList/'+ $("#txtregionidfilter").val(),
          success: function(ret)
          {
				$('#txtprovinceidfilter').html('');
				$('#txtprovinceidfilter').append($('<option>').text('View All Province').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtprovinceidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
  $("#txtcityidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getCityList/'+ $("#txtprovinceidfilter").val(),
          success: function(ret)
          {
				$('#txtcityidfilter').html('');
				$('#txtcityidfilter').append($('<option>').text('View All Province').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtcityidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
  $("#txtsitetypeidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getSiteTypeList/',
          success: function(ret)
          {
				$('#txtsitetypeidfilter').html('');
				$('#txtsitetypeidfilter').append($('<option>').text('View All Site Type').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtsitetypeidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
  $("#txttowerheightidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getTowerHeightList/'+ $("#txtsitetypeidfilter").val(),
          success: function(ret)
          {
				$('#txttowerheightidfilter').html('');
				$('#txttowerheightidfilter').append($('<option>').text('View All Tower Height').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txttowerheightidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });  
  
  $("#txttransmissiontypeidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getTransmissionTypeList/',
          success: function(ret)
          {
				$('#txttransmissiontypeidfilter').html('');
				$('#txttransmissiontypeidfilter').append($('<option>').text('View All Transmission Type').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txttransmissiontypeidfilter').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });  
  
});

function generateTable()
{   
	var where = [];

	if($('#txtstatusidfilter').val() != '')
	{
		where.push({key : 'status_id', value : $('#txtstatusidfilter').val()});
	}
  
	if($('#txtproductidfilter').val() != '')
	{
		where.push({key : 'product_id', value : $('#txtproductidfilter').val()});
	}

	if($('#txttenantidfilter').val() != '')
	{
		where.push({key : 'tenant_id', value : $('#txttenantidfilter').val()});
	}

	if($('#txtareaidfilter').val() != '')
	{
		where.push({key : 'area_id', value : $('#txtareaidfilter').val()});
	}
	
	if($('#txtregionidfilter').val() != '')
	{
		where.push({key : 'region_id', value : $('#txtregionidfilter').val()});
	}	
	
	if($('#txtprovinceidfilter').val() != '')
	{
		where.push({key : 'province_id', value : $('#txtprovinceidfilter').val()});
	}	
	
	if($('#txtcityidfilter').val() != '')
	{
		where.push({key : 'city_id', value : $('#txtcityidfilter').val()});
	}	
	
	if($('#txtsitetypeidfilter').val() != '')
	{
		where.push({key : 'sitetype_id', value : $('#txtsitetypeidfilter').val()});
	}	
	
	if($('#txttowerheightidfilter').val() != '')
	{
		where.push({key : 'towerheight_id', value : $('#txttowerheightidfilter').val()});
	}	
	
	if($('#txttransmissiontypeidfilter').val() != '')
	{
		where.push({key : 'transmissiontype_id', value : $('#txttransmissiontypeidfilter').val()});
	}	

  
                
  var table = $('#tablelist').DataTable( {
	
    ajax: {
      "url": base_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
    destroy: true,
	scrollX: true,
    iDisplayLength: 10,
    order: [[ 0, "desc" ]],
    dom: 'Bfrtip',
    buttons: [
	
		/**
		 * Tombol add tetap ada untuk mengantisipasi jika proses copy di sales order approval
		 
        {
            text: '<i class="fa fa-plus"></i> Add New',
            action: function ( e, dt, node, config ) {
                generateModalForm('add', '');
            }
        },
		
		*/
		
        {
            extend: 'copy',
            text: '<i class="fa fa-copy"></i> Copy',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'excel',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-excel-o"></i> Excel',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'pdf',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-pdf-o"></i> PDF',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'print',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-print"></i> Print',
            exportOptions: {
                columns: []
            }
        }
    ]
  });
  
  
}



function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
  
}

function generateModalForm(state, row_id) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });
  
  $("#txtsalesorderlinetable").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getSalesOrderLineTableNotInSiteTable/',
          success: function(ret)
          {
				$('#txtsalesorderlinetable').html('');
				$('#txtsalesorderlinetable').append($('<option>').text('--Select a Data From Sales Order Line Table--').attr('value', ''));
        		  var data = JSON.parse(ret);
				  
        		  $.each(data, function(key, value)
					{
        				$('#txtsalesorderlinetable').append($('<option>').text(value['site_id_tenant']+ ' - ' +value['site_name_tenant']).attr('value', value['row_id']));
        		  });
				  
          }
    	});
  });  
  
  // feed to site table from sales order line table
  $("#txtsalesorderlinetable").change(function()
  {
	if($("#txtsalesorderlinetable").val() != '')
	{
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getSelectedSalesOrderLineTable/' + $("#txtsalesorderlinetable").val(),
          success: function(ret)
          {
     		var data = JSON.parse(ret);
				  
        	$.each(data, function(key, value)
			{			
				$('#txtproductid').html('');
        		$('#txtproductid').append($('<option>').text(value['producttable_txt']).attr('value', value['product_id']));
				
				$('#txttenantid').html('');
        		$('#txttenantid').append($('<option>').text(value['tenanttable_name']).attr('value', value['salesordertable_tenant_id']));
				
				$('#txtsiteidtenant').html('');
        		$('#txtsiteidtenant').val(value['site_id_tenant']);
				
				$('#txtsitenametenant').html('');
        		$('#txtsitenametenant').val(value['site_name_tenant']);

				$('#txtsiteidapi').html('');
        		$('#txtsiteidapi').val(value['site_id_api']);
				
				$('#txtsitenameapi').html('');
        		$('#txtsitenameapi').val(value['site_name_api']);

				$('#txtareaid').html('');
        		$('#txtareaid').append($('<option>').text(value['areatable_txt']).attr('value', value['area_id']));

				$('#txtregionid').html('');
        		$('#txtregionid').append($('<option>').text(value['regiontable_txt']).attr('value', value['region_id']));

				$('#txtprovinceid').html('');
        		$('#txtprovinceid').append($('<option>').text(value['provincetable_txt']).attr('value', value['province_id']));

				$('#txtcityid').html('');
        		$('#txtcityid').append($('<option>').text(value['citytable_txt']).attr('value', value['city_id']));
				
				$('#txtaddress').html('');
        		$('#txtaddress').val(value['address']).focus();
				
				$('#txtaddress').html('');
        		$('#txtaddress').val(value['address']);

				$('#txtlongitudeori').html('');
				$('#txtlongitudeori').val(value['longitude']);	
				
				$('#txtlatitudeori').html('');				
        		$('#txtlatitudeori').val(value['latitude']);
				
				
				$('#txtsitetypeid').html('');
        		$('#txtsitetypeid').append($('<option>').text(value['sitetypetable_txt']).attr('value', value['sitetype_id']));
				
				$('#txttowerheightid').html('');
        		$('#txttowerheightid').append($('<option>').text(value['towerheighttable_txt']).attr('value', value['towerheight_id']));

				$('#txttransmissiontypeid').html('');		
				$('#txttransmissiontypeid').append($('<option>').text('--Select a Transimission Type--').attr('value', ''));
				$.each(value['transmissiontypetable'], function(subkey, subvalue)
				{			
					$('#txttransmissiontypeid').append($('<option>').text(subvalue['txt']).attr('value', subvalue['row_id']));
				
				});
				
				$('#txtremark').html('');
        		$('#txtremark').val(value['remarks']);
				
				$('#txtstatusid').html('');
        		$('#txtstatusid').append($('<option>').text(value['statustable_name']).attr('value', value['status_id']));
				
			});
				  
          }
    	});
	}
  });  
  
  // drop down list
  $("#txtproductid").focus(function()
  {
    if($("#txtstate").val() == 'edit')
	{
		$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getProductList/',
          success: function(ret)
          {
				$('#txtproductid').html('');
				$('#txtproductid').append($('<option>').text('--Select a Product--').attr('value', ''));
        		  var data = JSON.parse(ret);
				  
        		  $.each(data, function(key, value)
					{
        				$('#txtproductid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
				  
          }
    	});
	}
  });  

  $("#txttenantid").focus(function()
  {
    if($("#txtstate").val() == 'edit')
	{
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getTenantList/',
          success: function(ret)
          {
				$('#txttenantid').html('');
				$('#txttenantid').append($('<option>').text('--Select a Tenant--').attr('value', ''));
        		  var data = JSON.parse(ret);
				  
        		  $.each(data, function(key, value)
					{
        				$('#txttenantid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
				  
          }
    	});
	}
  });  

  $("#txtareaid").focus(function()
  {
    if($("#txtstate").val() == 'edit')
	{
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getAreaList/',
          success: function(ret)
          {
				$('#txtareaid').html('');
				$('#txtareaid').append($('<option>').text('--Select a Area--').attr('value', ''));
        		  var data = JSON.parse(ret);
				  
        		  $.each(data, function(key, value)
					{
        				$('#txtareaid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
				  
          }
    	});
	}
  });  


  $("#txtregionid").focus(function()
  {
    if($("#txtstate").val() == 'edit')
	{
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getRegionList/'+$("#txtareaid").val(),
          success: function(ret)
          {
				$('#txtregionid').html('');
				$('#txtregionid').append($('<option>').text('--Select a Region--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtregionid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
	}	
  });
  
  $("#txtprovinceid").focus(function()
  {
    if($("#txtstate").val() == 'edit')
	{  
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getProvinceList/'+$("#txtregionid").val(),
          success: function(ret)
          {
				$('#txtprovinceid').html('');
				$('#txtprovinceid').append($('<option>').text('--Select a Province--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtprovinceid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
	}	
  });  
  
  $("#txtcityid").focus(function()
  {
    if($("#txtstate").val() == 'edit')
	{
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getCityList/'+$("#txtprovinceid").val(),
          success: function(ret)
          {
				$('#txtcityid').html('');
				$('#txtcityid').append($('<option>').text('--Select a City--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtcityid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
	}
  });    

  $("#txtsitetypeid").focus(function() 
  {
    if($("#txtstate").val() == 'edit')
	{
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getSiteTypeList/',
          success: function(ret)
          {
				$('#txtsitetypeid').html('');
				$('#txtsitetypeid').append($('<option>').text('--Select an Site Type--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtsitetypeid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
	}
  });  
   
  $("#txttowerheightid").focus(function()
  {
    if($("#txtstate").val() == 'edit')
	{
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getTowerHeightList/'+$("#txtsitetypeid").val(),
          success: function(ret)
          {
				$('#txttowerheightid').html('');
				$('#txttowerheightid').append($('<option>').text('--Select an Site Type--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txttowerheightid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
	}	
  });  
  
  $("#txttransmissiontypeid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getTransmissionTypeList/',
          success: function(ret)
          {
				$('#txttransmissiontypeid').html('');
				$('#txttransmissiontypeid').append($('<option>').text('--Select an Transmission Type--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txttransmissiontypeid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });  
  
  $("#txtstatusid").focus(function()
  {
    if($("#txtstate").val() == 'edit')
	{
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusid').html('');
				//$('#txtstatusid').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
	}	
  });  
  
}

function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
		  
		  $('#txtproductid').select2();
          $('#txttenantid').select2();
		  $('#txtareaid').select2();
		  $('#txtregionid').select2();
		  $('#txtprovinceid').select2();
		  $('#txtcityid').select2();
          $('#txtsitetypeid').select2();
		  $('#txttowerheightid').select2();
		  $('#txttransmissiontypeid').select2();
		  $('#txtmilestoneid').select2();
		  $('#txtinchstoneid').select2();
		  $('#txtstatusid').select2();          

      }
  });
  
  
}

function doSave(){

	var formData = new FormData($('.formInput')[0]);

	var txtproductid = $('#txtproductid').val(); 
	var txttenantid = $('#txttenantid').val();
	var txtsiteidtenant = $('#txtsiteidtenant').val(); 
	var txtsitenametenant = $('#txtsitenametenant').val(); 
	var txtsiteidapi = $('#txtsiteidapi').val(); 
	var txtsitenameapi = $('#txtsitenameapi').val(); 
	var txtprovince = $('#txtprovince').val(); 
	var txtareaid = $('#txtareaid').val(); 
	var txtregionid = $('#txtregionid').val(); 
	var txtprovinceid = $('#txtprovinceid').val();
	var txtcityid = $('#txtcityid').val();	
	var txtlatitudeori = $('#txtlatitudeori').val(); 
	var txtlongitudeoori = $('#txtlongitudeori').val(); 
	var txtaddress = $('#txtaddress').val(); 
	var txtsitetypeid = $('#txtsitetypeid').val(); 
	var txttowerheightid = $('#txttowerheightid').val(); 
	var txttransmissiontypeid = $('#txttransmissiontypeid').val(); 
	var txtstatusid = $('#txtstatusid').val(); 

   if(txtproductid == '') {infoStatus('Product must be filled', 0); exit();}
   if(txttenantid == '') {infoStatus('Tenant must be filled', 0); exit();}
   if(txtsiteidapi == '') {infoStatus('Site Id API must be filled', 0); exit();}
   if(txtsitenameapi == '') {infoStatus('Site Name API must be filled', 0); exit();}
   if(txtsiteidtenant == '') {infoStatus('Site Id Tenant must be filled', 0); exit();}
   if(txtsitenametenant == '') {infoStatus('Site Name Tenant must be filled', 0); exit();}
   if(txtprovince == '') {infoStatus('Province must be filled', 0); exit();}
   if(txtareaid == '') {infoStatus('Area must be filled', 0); exit();}
   if(txtregionid == '') {infoStatus('Region must be filled', 0); exit();}
   if(txtprovinceid == '') {infoStatus('Region must be filled', 0); exit();}
   if(txtcityid == '') {infoStatus('Region must be filled', 0); exit();}
   if(txtlongitudeori == '') {infoStatus('Longitude must be filled', 0); exit();}
   if(txtlatitudeori == '') {infoStatus('Latitude must be filled', 0); exit();}
   if(txtaddress == '') {infoStatus('Address must be filled', 0); exit();}  
   if(txtsitetypeid == '') {infoStatus('Site Type must be filled', 0); exit();}
   if(txttowerheightid == '') {infoStatus('Tower Height must be filled', 0); exit();}
   if(txttransmissiontypeid == '') {infoStatus('Transmission Type must be filled', 0); exit();}
   if(txtstatusid == '') {infoStatus('Status must be filled', 0); exit();}

  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: base_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: base_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}

function genSiteNameApi(){
	var nameTenant = $("#txtsitenametenant").val();
	$("#txtsitenameapi").val("API_" + nameTenant);
}

