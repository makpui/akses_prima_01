$(document).ready(function() {
  generateTable();

  $("#btnSave").click(function(){
    doSave();
  });
  
  $("#txtstatusidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusidfilter').html('');
				$('#txtstatusidfilter').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
				  
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusidfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtvendoridfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getVendorList/',
          success: function(ret)
          {
				$('#txtvendoridfilter').html('');
				$('#txtvendoridfilter').append($('<option>').text('View All Vendor').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
				{
        				    $('#txtvendoridfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtworkflowmilestoneidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getWorkFlowMilestoneList/',
          success: function(ret)
          {
				$('#txtworkflowmilestoneidfilter').html('');
				$('#txtworkflowmilestoneidfilter').append($('<option>').text('View All Work Flow Milestone').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
				{
        				    $('#txtworkflowmilestoneidfilter').append($('<option>').text(value['name']).attr('value', value['id']));
        		  });
          }
    	});
  });  
  
});

function generateTable()
{
	var where = [];

	if($('#txtstatusidfilter').val() != '')
	{
		where.push({key : 'status_id', value : $('#txtstatusidfilter').val()});
	}
	
	if($('#txtvendoridfilter').val() != '')
	{
		where.push({key : 'vendor_id', value : $('#txtvendoridfilter').val()});
	}
	
	if($('#txtworkflowmilestoneidfilter').val() != '')
	{
		where.push({key : 'work_flow_milestone_id', value : $('#txtworkflowmilestoneidfilter').val()});
	}
   
  var table = $('#tablelist').DataTable( {
    ajax: {
      "url": base_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
	scrollX: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 0, "desc" ]],
    dom: 'Bfrtip',
    buttons: [
        {
            text: '<i class="fa fa-plus"></i> Add New',
			className: 'dt-button-add',					
            action: function ( e, dt, node, config ) {
                generateModalForm('add', '');
            }
        }, 
        {
            extend: 'copy',
            text: '<i class="fa fa-copy"></i> Copy',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'excel',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-excel-o"></i> Excel',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'pdf',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-pdf-o"></i> PDF',
            exportOptions: {
                columns: []
            }
        }, 
        {
            extend: 'print',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-print"></i> Print',
            exportOptions: {
                columns: []
            }
        }
    ]
  });
  
  /**
   * Controlling visibility add button
   */

   $('.dt-button-add').hide();
  if($('#is_so_maker').val() == 1)
  {  
	$('.dt-button-add').show();
  }    
}

function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}

function generateModalForm(state, row_id) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  $("#modalForm").on('shown.bs.modal', function () {
        $("#txtworkorderdate").datepicker({dateFormat: 'dd-mm-yy'});
  });
  
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });
  
  $("#txtvendorid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getVendorList/',
          success: function(ret)
          {
				$('#txtvendorid').html('');
				$('#txtvendorid').append($('<option>').text('--Select a Vendor--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtvendorid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  }); 
  
  $("#txtvendorcontactid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getVendorContactList/'+$("#txtvendorid").val(),
          success: function(ret)
          {
				$('#txtvendorcontactid').html('');
				$('#txtvendorcontactid').append($('<option>').text('--Select a Vendor Contact--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtvendorcontactid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });     
  
  $("#txtstatusid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusid').html('');
				//$('#txtstatusid').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });  
  
}

function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          $('#txtvendorid').select2();
          $('#txtstatusid').select2();
      }
  });
}

function doSave(){

  var formData = new FormData($('.formInput')[0]);

  var txtworkorderno = $('#txtworkorderno').val(); 
  var txtworkorderdate = $('#txtworkorderdate').val(); 
  var txtvendorid = $('#txtvendor').val(); 
  var txtvendorcontactid = $('#txtvendorcontactid').val(); 
  var txtproductgroup = $('#txtproductgroup').val(); 
  var txtprojectmanager = $('#txtprojectmanager').val(); 
  var txtremark = $('#txtremark').val(); 
  var txtstatusid = $('#txtstatusid').val(); 
  

  if(txtworkorderno == '') {infoStatus('Work Order No. must be filled', 0); exit();}
  if(txtworkorderdate == '') {infoStatus('Work Order Date must be filled', 0); exit();}
  if(txtvendorid == '') {infoStatus('Vendor must be filled', 0); exit();}
  if(txtvendorcontactid == '') {infoStatus('Contact must be filled', 0); exit();}
  if(txtprojectmanager == '') {infoStatus('Project Manager must be filled', 0); exit();}
  if(txtstatusid == '') {infoStatus('Status must be filled', 0); exit();}

  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: base_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: base_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}