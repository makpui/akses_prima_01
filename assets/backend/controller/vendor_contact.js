if($(location).attr('origin').split('/').pop() == 'localhost')
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' + $(location).attr('pathname').split('/')[2] + '/';
}
else
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' ;
}

$(document).ready(function() {
  generateTable();

  $("#btnSave").click(function(){
    doSave();
  });
    
});

function generateTable()
{   
	var where = [];
		
	if($('#txtfilter_vendor_id').val() != '')
	{
		where.push({key : 'vendor_id', value : $('#txtfilter_vendor_id').val()});
	}
      
  var table = $('#tablelist').DataTable( {
    ajax: {
	  "url": basename_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 0, "desc" ]],
    dom: 'Bfrtip',
    buttons: [
        {
            text: '<i class="fa fa-plus"></i> Add New Contact(s)',
			className: 'dt-button-add',					
            action: function ( e, dt, node, config ) {
                generateModalForm('add', '');
            }
        }
    ]
  });
  
  $('.dt-button-add').hide(); 
  if($('#user_role_id').val() == 1 || $('#user_role_id').val() == 5)
  {  
	$('.dt-button-add').show(); 
  }    
}



function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}

function generateModalForm(state, row_id) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  var txtfilter_vendor_id = $('#txtfilter_vendor_id').val();
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id, 'txtfilter_vendor_id': txtfilter_vendor_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });
  
  $("#txtstatusid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusid').html('');
				//$('#txtstatusid').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
}

function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          //$('#txtproductid').select2();
      }
  });
}

function doSave(){

	var formData = new FormData($('.formInput')[0]);

	var txtvendorid = $('#txtvendorid').val(); 
	var txtname = $('#txtname').val(); 
	var txtposition = $('#txtposition').val(); 
	var txtorganization = $('#txtorganization').val(); 
	var txtemail = $('#txtemail').val(); 
	var txtmobileno = $('#txtmobileno').val(); 
	var txtstatusid = $('#txtstatusid').val(); 

  if(txtvendorid == '') {infoStatus('Vendor must be filled', 0); exit();}
  if(txtname == '') {infoStatus('Name must be filled', 0); exit();}
  if(txtposition == '') {infoStatus('Position must be filled', 0); exit();}
  if(txtorganization == '') {infoStatus('Oganization must be filled', 0); exit();}
  if(txtemail == '') {infoStatus('Email must be filled', 0); exit();}
  if(txtmobileno == '') {infoStatus('Mobile Number must be filled', 0); exit();}
  if(txtstatusid == '') {infoStatus('Status must be filled', 0); exit();}

  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: basename_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: basename_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}