$(document).ready(function() {
  generateDataTable();
    
  $("#btnSave").click(function(){
    doSave();
  });

});

function generateDataTable()
{                 
  var table = $('#tablelist').DataTable( {
    ajax: {
      "url": base_url +  "gridview",
      "type": "POST",
    },		
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 0, "asc" ]],
    dom: 'Bfrtip',
    buttons: [
    ]
  });
}


