$(document).ready(function() {
  generateTable();

  $("#btnSave").click(function(){
    doSave();
  });

  $("#txtstatusidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusidfilter').html('');
				$('#txtstatusidfilter').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);

        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusidfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtworkflowmilestoneidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getWorkFlowMilestoneList/',
          success: function(ret)
          {
				$('#txtworkflowmilestoneidfilter').html('');
				$('#txtworkflowmilestoneidfilter').append($('<option>').text('View All Work Flow Milestone').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtworkflowmilestoneidfilter').append($('<option>').text(value['name']).attr('value', value['id']));
        		  });
          }
    	});
  });

  $("#txttenantidfilter").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getTenantList/',
          success: function(ret)
          {
				$('#txttenantidfilter').html('');
				$('#txttenantidfilter').append($('<option>').text('View All Tenant').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txttenantidfilter').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

});

function generateTable()
{
	var where = [];

	if($('#txtstatusidfilter').val() != '')
	{
		where.push({key : 'status_id', value : $('#txtstatusidfilter').val()});
	}

	if($('#txtworkflowmilestoneidfilter').val() != '')
	{
		where.push({key : 'work_flow_milestone_id', value : $('#txtworkflowmilestoneidfilter').val()});
	}

	if($('#txttenantidfilter').val() != '')
	{
		where.push({key : 'tenant_id', value : $('#txttenantidfilter').val()});
	}


  var table = $('#tablelist').DataTable( {
    ajax: {
      "url": base_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    },
	scrollX: true,
    processing: true,
    serverSide: true,
    scrollCollapse: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 0, "desc" ]],
    dom: 'Bfrtip',
    buttons: [
        {
            text: '<i class="fa fa-plus"></i> Add New',
			className: 'dt-button-add',
            action: function ( e, dt, node, config ) {
                generateModalForm('add', '');
            }
        },
        {
            extend: 'copy',
            text: '<i class="fa fa-copy"></i> Copy',
            exportOptions: {
                columns: []
            }
        },
        {
            extend: 'excel',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-excel-o"></i> Excel',
            exportOptions: {
                columns: []
            }
        },
        {
            extend: 'pdf',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-file-pdf-o"></i> PDF',
            exportOptions: {
                columns: []
            }
        },
        {
            extend: 'print',
            title: '<?= ucwords($lbl_controller) ?>',
            text: '<i class="fa fa-print"></i> Print',
            exportOptions: {
                columns: []
            }
        }
    ]
  });

  /**
   * Controlling visibility add button
   */

  $('.dt-button-add').hide();
  if($('#is_so_maker').val() == 1)
  {
	$('.dt-button-add').show();
  }
}

function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}


function generateModalForm(state, row_id) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  $("#modalForm").on('shown.bs.modal', function () {
	$("#txtsalesorderdate").datepicker({dateFormat: 'dd-mm-yy'});
	$("#txtdrmdate").datepicker({dateFormat: 'dd-mm-yy'});
	$("#txtpotenantdate").datepicker({dateFormat: 'dd-mm-yy'});
  });

  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret);
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });

  $("#txttenantid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getTenantList/',
          success: function(ret)
          {
				$('#txttenantid').html('');
				$('#txttenantid').append($('<option>').text('--Select a Tenant--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txttenantid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $("#txtstatusid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: base_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusid').html('');
				//$('#txtstatusid').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

}

function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: base_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret);
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          $('#txttenantid').select2();
          $('#txtstatusid').select2();
      }
  });
}

function doSave(){

  var formData = new FormData($('.formInput')[0]);

  var txtsalesorderno = $('#txtsalesorderno').val();
  var txtsalesorderdate = $('#txtsalesorderdate').val();
  var txtdrmdate = $('#txtdrmdate').val();
  var txtbatch = $('#txtbatch').val();
  var txtpotenantno = $('#txtpotenantno').val();
  var txtpotenantdate = $('#txtpotenantdate').val();
  var txttenantid = $('#txttenantid').val();
  var txtstatusid = $('#txtstatusid').val();


  if(txtsalesorderno == '') {infoStatus('Sales Order No. must be filled', 0); exit();}
  if(txtsalesorderdate == '') {infoStatus('Sales Order Date must be filled', 0); exit();}
  //if(txtdrmdate == '') {infoStatus('DRM Date must be filled', 0); exit();}
  //if(txtpotenantno == '') {infoStatus('PO No. Tenant must be filled', 0); exit();}
  if(txtpotenantno != '')
  {
    if(txtpotenantdate == '') {infoStatus('PO Date Tenant must be filled', 0); exit();}
    
  }
  if(txttenantid == '') {infoStatus('Tenant must be filled', 0); exit();}
  if(txtstatusid == '') {infoStatus('Status must be filled', 0); exit();}

  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
    async: false,
    url: base_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret);
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: base_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret);
              reloadDatatable();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}
