if($(location).attr('origin').split('/').pop() == 'localhost')
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' + $(location).attr('pathname').split('/')[2] + '/';
}
else
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' ;
}

$(document).ready(function() {  
  generateTable();
  
  $("#btnSave").click(function(){ 
    doSave();
  });
  
  var doc_ref = document.referrer.split('/').pop();
  
  if(doc_ref != 'sales_order')
  {
	var approve_status_hash = window.location.hash.split('#').pop();
	if(approve_status_hash == '1')
	{
		$('.dt-button-add-approve').click();
	}
	
	if(approve_status_hash == '2')
	{
		$('.dt-button-add-reject').click();	
	}	 
  }
  
});

function generateTable()
{   
  	var where = [];

	if($('#txtfilter_salesordertable_salesorder_no').val() != '')
	{
		where.push({key : 'salesorder_no', value : $('#txtfilter_salesordertable_salesorder_no').val()});
	}
  
  var table = $('#tablelist').DataTable( { 
	
    ajax: {
	  "url": basename_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    }, 
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
	scrollX: true,
    destroy: true,
    iDisplayLength: 10,
    order: [[ 0, "desc" ]],
    dom: 'Bfrtip',
	buttons: 
			[ 
				{
					text: '<i class="fa fa-check"></i> Approval',
					className: 'dt-button-add-approve',					
					action: function ( e, dt, node, config ) {	
						generateModalForm('add', '', 1);
					}
				}
				,
				{
					text: '<i class="fa fa-close"></i> Reject',
					className: 'dt-button-add-reject',					
					action: function ( e, dt, node, config ) {	
						generateModalForm('add', '', 0);
					}
				}
				
			]
  });
  
  $('.dt-button-add-approve').hide();   
  $('.dt-button-add-reject').hide();   
  
  if($('#show_button_add').val())
  {
	  $('.dt-button-add-approve').show();   
	  $('.dt-button-add-reject').show();   
  }
  
 }


function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}

function generateModalForm(state, row_id, approved_status) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  	  
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id, 'txtapprovedstatus': approved_status, 'txtfilter_salesordertable_salesorder_no': $('#txtfilter_salesordertable_salesorder_no').val()},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });
    
}


function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          $('#txtproductid').select2();
      }
  });
}

function doSave(){
	var formData = new FormData($('.formInput')[0]);
	
	var txtfilter_salesordertable_row_id = $('#txtfilter_salesordertable_row_id').val();
	var txtapproveddate = $('#txtapproveddate').val(); 
	var txtremark = $('#txtremark').val(); 
	
	
	
	if(txtapproveddate == '') {infoStatus('Approved Date must be filled', 0); exit();}
	//if(txtremark == '') {infoStatus('Remark must be filled' , 0); exit();}
		
  $("#btnSave").html("Loading...");
  $.ajax({
    type: 'post',
	async: false,
    url: basename_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      reloadDatatable();
      infoStatus(data['msg'], data['type']);
    }
  });
  
  window.location.replace($(location).attr('origin') + '/' + $(location).attr('pathname').split('/')[1] + '/' + 'index'  + '/' + txtfilter_salesordertable_row_id);

}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: basename_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}



