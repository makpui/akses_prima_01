if($(location).attr('origin').split('/').pop() == 'localhost')
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' + $(location).attr('pathname').split('/')[2] + '/';
}
else
{
	var basename_url = $(location).attr('origin')+ '/' + $(location).attr('pathname').split('/')[1] + '/' ;
}

$(document).ready(function() {

  generateTable();
  
  $("#btnSave").click(function(){
    doSave();
  });
    
});

function generateTable()
{
	var where = [];

	if($('#txtfilter_salesordertable_salesorder_no').val() != '')
	{
		where.push({key : 'salesorder_no', value : $('#txtfilter_salesordertable_salesorder_no').val()});
	}
 
  var table = $('#tablelist').DataTable({
    ajax: {
      "url": basename_url + "gridview",
      "type": "POST",
      "data" : {'where': where}
    },
    processing: true, 
    serverSide: true,
    scrollCollapse: true,
	scrollX: true,
    destroy: true,
    iDisplayLength: 10,	
    order: [[ 0, "desc" ]],
    dom: 'Bfrtip',
	buttons: 
			[ 
				{
					text: '<i class="fa fa-plus"></i> Add New Line',
					className: 'dt-button-add',					
					action: function ( e, dt, node, config ) {	
						generateModalForm('add', '', $('#txtfilter_salesordertable_salesorder_no').val());
					}
				},
				{
					text: '<i class="fa fa-stop"></i> Complete',
					className: 'dt-button-stop',					
					action: function ( e, dt, node, config ) {	
						window.location = basename_url + 'completeCreation/' + $('#txtfilter_salesordertable_row_id').val();
					}
				},
				{
					text: '<i class="fa fa-play"></i> Un Complete' ,
					className: 'dt-button-play',					
					action: function ( e, dt, node, config ) {	
						window.location = basename_url + 'unCompleteCreation/' + $('#txtfilter_salesordertable_row_id').val();
					}
				}
				
			]
  });
  
  /**
   * Controlling visibility button
   * 1. Add New Line is visible at creation state only  && not approval
   * 2. Complete is visible at creation state && salesorder_no is already exist in salesorderlinetable
   * 3. Un-complete is available in approval state && still no one allready approve 
   */
   
  $('.dt-button-add').hide(); 
  $('.dt-button-stop').hide(); 
  $('.dt-button-play').hide();
  
  
  if($('#is_so_maker').val() == 1)
  {  
	  if($('#salesordertable_work_flow_milestone_id').val() ==  $('#workflowmilestonetable_creation_state').val())
	  {
		$('.dt-button-add').show(); 
		
		if($('#salesorderlinetable_sales_order_isexist').val())
		{
			$('.dt-button-stop').show(); 	
		}
	  }
	  else if($('#salesordertable_work_flow_milestone_id').val() !=  $('#workflowmilestonetable_ending_state').val() && !$('#salesorderapprovaltable_sales_order_isexist').val())
	  {
		$('.dt-button-play').show();
	  }
  }
  
  if($('#salesordertable_work_flow_milestone_id').val() > 1 && $('#roletable_row_id').val() == 1)
  {
	$('.dt-button-add').show(); 
  }
}

function reloadPage()
{
  location.reload(true);
}

function reloadDatatable()
{
  $('#tablelist').DataTable().ajax.reload();
}

function generateModalForm(state, row_id, salesorder_no) {
  //$('#modalForm').modal('show');
  
  $('#modalForm').modal({
	backdrop: 'static',
	keyboard: true, 
	show: true  
  });
  
  $("#modalForm").on('shown.bs.modal', function () {  
    $("#txtbaks").datepicker({dateFormat: 'dd-mm-yy'});
	$("#txtrfi").datepicker({dateFormat: 'dd-mm-yy'});
	$("#txtbaps").datepicker({dateFormat: 'dd-mm-yy'});
  });
  
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'form',
      data: {'txtstate': state, 'txtrowid': row_id, 'txtsalesorderno' : salesorder_no},
      success: function(ret) {
          var data = JSON.parse(ret);
          $('#btnSave').show();
          $('#tableModal').html(data['table']);
      }
  });
  
  
  /** DEFAULT ELEMENT VISIBILITY CONTROL
   */
    $(".txtsiteidapi_input").show();
	$(".txtsiteidapi_select").hide();
		
	$(".txtsitetypeid_input").hide();
	$(".txtsitetypeid_select").prop('disabled', false);
	
	$(".txttowerheightid_input").hide();
	$(".txttowerheightid_select").prop('disabled', false);
		
	$(".txtareaid_input").hide();
	$(".txtareaid_select").prop('disabled', false);
	
	$(".txtregionid_input").hide();
	$(".txtregionid_select").prop('disabled', false);
	
	$(".txtprovinceid_input").hide();
	$(".txtprovinceid_select").prop('disabled', false);
	
	$(".txtcityid_input").hide();
	$(".txtcityid_select").prop('disabled', false);	  
  
  
  /** FILL AUTOMATICALLY 
   *  A. Mini Macro / Macro
   *     - Target RFI  : 127 hari kalender
   *     - Target BAKS :  14 hari kalender dari target RFI
   *     - Target BAPS :  14 hari kalender dari target BAKS   
   *  B. MCP
   *     - Target RFI  :  92 hari kalender
   *     - Target BAKS :  14 hari kalender dari target RFI
   *     - Target BAPS :  14 hari kalender dari target BAKS   
   *  C. Colocation
   *     - Target RFI  :  40 hari kalender
   *     - Target BAKS :  14 hari kalender dari target RFI
   *     - Target BAPS :  14 hari kalender dari target BAKS   
   */
    
  var salesorderdate =  $("#txtheader_sodate").val().split("-");
  
   /** PRODUCT DROP DOWN LIST
	*/	
	$("#txtproductid").focus(function()
	{
		$.ajax({
			type: 'post',
			async: false,
			url: basename_url + 'getProductList/',
			success: function(ret)
			{
				$('#txtproductid').html('');
				$('#txtproductid').append($('<option>').text('--Select a Product--').attr('value', ''));
					var data = JSON.parse(ret);
					$.each(data, function(key, value)
					{
						$('#txtproductid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
					});
			}
		});
	});
	
  /** ELEMENT VISIBILITY BASED ON CHOOSED PRODUCT ID
   * if product = b2s 
   *    element following is made normal
   * if product = collo
   *    site it tenant is dropdown list made it from site table
   *    following element fill out from site table
   */
   	$("#txtproductid").change(function()
	{
		if($("#txtproductid").val() != 2 || !$("#txtproductid").val())
		{
			$("#txttowertypeid").html('');
			$("#txttowertypeid").show().append($('<option>').text('--Select a Tower Type--').attr('value', ''));

			$("#txtsiteidapi").prop('readonly', true).val('Generated by System');			
			$('#txtsitenameapi').prop('readonly', true).val('');
			
			$(".txtsitetypeid_input").hide();
			$(".txtsitetypeid_select").html('');
			$(".txtsitetypeid_select").prop('disabled', false).append($('<option>').text('--Select a Site Type--').attr('value', ''));
						
			$(".txttowerheightid_input").hide();
			$(".txttowerheightid_select").html('');
			$(".txttowerheightid_select").prop('disabled', false).append($('<option>').text('--Select a Tower Height--').attr('value', ''));
			
			$("#txtlatitude").prop('readonly', false).val('');
			$("#txtlongitude").prop('readonly', false).val('');
			
			$(".txtareaid_input").hide();
			$(".txtareaid_select").html('');
			$(".txtareaid_select").prop('disabled', false).append($('<option>').text('--Select an Area--').attr('value', ''));
			
			$(".txtregionid_input").hide();
			$(".txtregionid_select").html('');
			$(".txtregionid_select").prop('disabled', false).append($('<option>').text('--Select a Region--').attr('value', ''));
			
			$(".txtprovinceid_input").hide();
			$(".txtprovinceid_select").html('');
			$(".txtprovinceid_select").prop('disabled', false).append($('<option>').text('--Select a Province--').attr('value', ''));
			
			$(".txtcityid_input").hide();
			$(".txtcityid_select").html('');
			$(".txtcityid_select").prop('disabled', false).append($('<option>').text('--Select a City--').attr('value', ''));
			
			// clear date
			$("#txtrfi").val('');
			$("#txtbaks").val('');
			$("#txtbaps").val('');
			
			$("#txtremarks").val('');
			$("#txtrentperiod").val('');
			
		}
		else
		{
			/** SITE ID AUTO COMPLETE */
			$("#txtsiteidapi").prop('readonly', false).val('');		
			$("#txtsiteidapi").autocomplete({
				minLength: 0,
				source: function(request, response)
				{
					$.ajax({
						type: 'POST',
						url: basename_url + 'getSiteList',
						data : {searched_siteidapi: request.term, tower_type_id: $("#txttowertypeid").val()},
						dataType: 'json',
						
						success : function(ret) {
							if(ret.length < 1)
							{
								infoStatus('Data not found');
								$("#txtsiteidapi").focus().val('');
							}
							else
							{
								var siteidapis = [];
								$.each(ret, function(key, value)
								{
									siteidapis.push({label : value['site_id_api']});
								})
								response(siteidapis);
							}
						}		 
					})
				},
						
				select: function(event, ui) { 
					$('#txtsiteidapi').val(ui.item.label);

					/** auto filler*/
					$.ajax({
						type: 'post',
						async: false,
						url: basename_url + 'getSiteData/',
						data: {'txt_site_id_api': $("#txtsiteidapi").val()},
						success: function(ret)
						{
							var data = JSON.parse(ret);
							if(Array.isArray(data))
							{
								$.each(data, function(key, value)
								{						
									$('#txtsiteidapi').val(value['site_id_api']);
									
									$('#txtsitenameapi').val(value['site_name_api']);
									
									$('#txtsitetypeid').val(value['sitetype_id']);
									$('.txtsitetypeid_select').html('');
									$('.txtsitetypeid_select').append($('<option>').text(value['sitetypetable_txt']).attr('value', value['sitetype_id']));
									
									$('#txttowerheightid').val(value['towerheight_id']);
									$('.txttowerheightid_select').html('');
									$('.txttowerheightid_select').append($('<option>').text(value['towerheighttable_txt']).attr('value', value['towerheight_id']));
									
									$("#txtlatitude").val(value['latitude_ori']);
									$("#txtlongitude").val(value['longitude_ori']);

									$('#txtareaid').val(value['area_id']);
									$('.txtareaid_select').html('');
									$('.txtareaid_select').append($('<option>').text(value['areatable_txt']).attr('value', value['area_id']));

									$('#txtregionid').val(value['region_id']);
									$('.txtregionid_select').html('');
									$('.txtregionid_select').append($('<option>').text(value['regiontable_txt']).attr('value', value['region_id']));
									
									$('#txtprovinceid').val(value['province_id']);
									$('.txtprovinceid_select').html('');
									$('.txtprovinceid_select').append($('<option>').text(value['provincetable_txt']).attr('value', value['province_id']));
									
									$('#txtcityid').val(value['city_id']);
									$('.txtcityid_select').html('');
									$('.txtcityid_select').append($('<option>').text(value['citytable_txt']).attr('value', value['city_id']));
									
								});				
							}
						}
					});							
					/** end of auto filler*/
					
					return false;	
				}				
			}).focus(function() {
				$(this).autocomplete("search", "");
			});
			
			/** END - SITE ID AUTO COMPLETE */
			
			  $("#txtsiteidapi").blur(function(){
				$.ajax({
				  type: 'post',
				  async: false,
				  url: basename_url + 'getSiteIdApiFromSiteTable',
				  data: {site_id_api: $("#txtsiteidapi").val()},
				  success: function(ret)
				  {	  
					if(ret == 0)
					{
						infoStatus('Site Not Exist');
						$("#txtsiteidapi").focus();
					}
				  }
				});
			  
			  });			
			
						
			$(".txtsitetypeid_input").prop('type', 'hidden');
			$(".txtsitetypeid_select").prop('disabled', true);
			
			$(".txttowerheightid_input").hide();
			$(".txttowerheightid_select").prop('disabled', true);
			
			$("#txtlatitude").prop('readonly', true);
			$("#txtlongitude").prop('readonly', true);
			
			$(".txtareaid_input").hide();
			$(".txtareaid_select").prop('disabled', true);
			
			$(".txtregionid_input").hide();
			$(".txtregionid_select").prop('disabled', true);
			
			$(".txtprovinceid_input").hide();
			$(".txtprovinceid_select").prop('disabled', true);
			
			$(".txtcityid_input").hide();
			$(".txtcityid_select").prop('disabled', true);	
			
			// auto fill date
			var rfidate  = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 40);
			var baksdate = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 40 + 14);
			var bapsdate = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 40 + 14 + 14);

			$("#txtrfi").val((rfidate.getDate().toString().length == 2 ? rfidate.getDate().toString() : 0+rfidate.getDate().toString()) +'-'+ ((parseInt(rfidate.getMonth())+1).toString().legnth == 2 ? (parseInt(rfidate.getMonth())+1).toString() : 0+(parseInt(rfidate.getMonth())+1).toString()) +'-'+ rfidate.getFullYear());
			$("#txtbaks").val((baksdate.getDate().toString().length == 2 ? baksdate.getDate().toString() : 0+baksdate.getDate().toString()) +'-'+ ((parseInt(baksdate.getMonth())+1).toString().legnth == 2 ? (parseInt(baksdate.getMonth())+1).toString() : 0+(parseInt(baksdate.getMonth())+1).toString()) +'-'+ baksdate.getFullYear());
			$("#txtbaps").val((bapsdate.getDate().toString().length == 2 ? bapsdate.getDate().toString() : 0+bapsdate.getDate().toString()) +'-'+ ((parseInt(bapsdate.getMonth())+1).toString().legnth == 2 ? (parseInt(bapsdate.getMonth())+1).toString() : 0+(parseInt(bapsdate.getMonth())+1).toString()) +'-'+ bapsdate.getFullYear());			  			

			$("#txtremarks").val('');
			$("#txtrentperiod").val('');			
		}	
	});
	
   	$("#txtproductid").blur(function()
	{
		if(!$("#txtproductid").val())
		{
			infoStatus('Choose a Product');
			$("#txtproductid").focus();
			return false;
		}
	});
	
	$("#txttowertypeid").focus(function()
	{
		$.ajax({
			type: 'post',
			async: false,
			url: basename_url + 'getTowerTypeList/',
			success: function(ret)
			{
				$('#txttowertypeid').html('');
				$('#txttowertypeid').append($('<option>').text('--Select a Tower Type--').attr('value', ''));
				var data = JSON.parse(ret);
				$.each(data, function(key, value)
				{
					$('#txttowertypeid').append($('<option>').text(value['txt']).attr('value', value['row_id']));
				});
			}
		});
	});
		
	$("#txttowertypeid").blur(function()
	{	
		if(!$("#txttowertypeid").val())
		{
			infoStatus('Choose a Tower Type');
			$("#txttowertypeid").focus();
			return false;
		}
		else
		{
			if($("#txtproductid").val() != 2)
			{
				if($("#txttowertypeid").val() == 2 || $("#txttowertypeid").val() == 3)
				{
					var rfidate  = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 127);
					var baksdate = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 127 + 14);
					var bapsdate = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 127 + 14 + 14);
					
					$("#txtrfi").val((rfidate.getDate().toString().length == 2 ? rfidate.getDate().toString() : 0+rfidate.getDate().toString()) +'-'+ ((parseInt(rfidate.getMonth())+1).toString().legnth == 2 ? (parseInt(rfidate.getMonth())+1).toString() : 0+(parseInt(rfidate.getMonth())+1).toString()) +'-'+ rfidate.getFullYear());
					$("#txtbaks").val((baksdate.getDate().toString().length == 2 ? baksdate.getDate().toString() : 0+baksdate.getDate().toString()) +'-'+ ((parseInt(baksdate.getMonth())+1).toString().legnth == 2 ? (parseInt(baksdate.getMonth())+1).toString() : 0+(parseInt(baksdate.getMonth())+1).toString()) +'-'+ baksdate.getFullYear());
					$("#txtbaps").val((bapsdate.getDate().toString().length == 2 ? bapsdate.getDate().toString() : 0+bapsdate.getDate().toString()) +'-'+ ((parseInt(bapsdate.getMonth())+1).toString().legnth == 2 ? (parseInt(bapsdate.getMonth())+1).toString() : 0+(parseInt(bapsdate.getMonth())+1).toString()) +'-'+ bapsdate.getFullYear());			
				}

				if($("#txttowertypeid").val() == 1)
				{
					var rfidate  = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 92);
					var baksdate = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 92 + 14);
					var bapsdate = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]) + 92 + 14 + 14);
					
					$("#txtrfi").val((rfidate.getDate().toString().length == 2 ? rfidate.getDate().toString() : 0+rfidate.getDate().toString()) +'-'+ ((parseInt(rfidate.getMonth())+1).toString().legnth == 2 ? (parseInt(rfidate.getMonth())+1).toString() : 0+(parseInt(rfidate.getMonth())+1).toString()) +'-'+ rfidate.getFullYear());
					$("#txtbaks").val((baksdate.getDate().toString().length == 2 ? baksdate.getDate().toString() : 0+baksdate.getDate().toString()) +'-'+ ((parseInt(baksdate.getMonth())+1).toString().legnth == 2 ? (parseInt(baksdate.getMonth())+1).toString() : 0+(parseInt(baksdate.getMonth())+1).toString()) +'-'+ baksdate.getFullYear());
					$("#txtbaps").val((bapsdate.getDate().toString().length == 2 ? bapsdate.getDate().toString() : 0+bapsdate.getDate().toString()) +'-'+ ((parseInt(bapsdate.getMonth())+1).toString().legnth == 2 ? (parseInt(bapsdate.getMonth())+1).toString() : 0+(parseInt(bapsdate.getMonth())+1).toString()) +'-'+ bapsdate.getFullYear());			
				}
				
				if($("#txttowertypeid").val() == 4)
				{
					var rfidate  = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]));
					var baksdate = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]));
					var bapsdate = new Date(parseInt(salesorderdate[2]), parseInt(salesorderdate[1]) - 1, parseInt(salesorderdate[0]));

					$("#txtrfi").val((rfidate.getDate().toString().length == 2 ? rfidate.getDate().toString() : 0+rfidate.getDate().toString()) +'-'+ ((parseInt(rfidate.getMonth())+1).toString().legnth == 2 ? (parseInt(rfidate.getMonth())+1).toString() : 0+(parseInt(rfidate.getMonth())+1).toString()) +'-'+ rfidate.getFullYear());
					$("#txtbaks").val((baksdate.getDate().toString().length == 2 ? baksdate.getDate().toString() : 0+baksdate.getDate().toString()) +'-'+ ((parseInt(baksdate.getMonth())+1).toString().legnth == 2 ? (parseInt(baksdate.getMonth())+1).toString() : 0+(parseInt(baksdate.getMonth())+1).toString()) +'-'+ baksdate.getFullYear());
					$("#txtbaps").val((bapsdate.getDate().toString().length == 2 ? bapsdate.getDate().toString() : 0+bapsdate.getDate().toString()) +'-'+ ((parseInt(bapsdate.getMonth())+1).toString().legnth == 2 ? (parseInt(bapsdate.getMonth())+1).toString() : 0+(parseInt(bapsdate.getMonth())+1).toString()) +'-'+ bapsdate.getFullYear());			
				}
			}
		}
	});
  
    	
	/** TO AVOID
	 * 1. duplication of site id tenant at site table 
	 * 2. duplication of site id tenant at sales order line table when product is B2S 
     */
	$("#txtsiteidtenant").blur(function()
	{

		if(!$("#txtsiteidtenant").val())
		{
			infoStatus('Site Id Tenant empty is NOT ALLOWED');
			$("#txtsiteidtenant").focus();
			return false;
		}
		else
		{		
			$.ajax({
				type: 'post',
				async: false,
				url: basename_url + 'getSiteList/',
				success: function(ret)
				{
					var data = JSON.parse(ret);
					$.each(data, function(key, value)
					{
						if(value['site_id_tenant'] == $("#txtsiteidtenant").val())
						{
							infoStatus($("#txtsiteidtenant").val() + ' is ALREADY EXIST at Site Table');
							$('#txtsiteidtenant').focus();
							return false;
						}
					});
				}
			});
			
			if($("#txtproductid").val() != 2)
			{
				$.ajax({
					type: 'post',
					async: false,
					url: basename_url + 'getSalesOrderLineList/',
					success: function(ret)
					{
						var data = JSON.parse(ret);
						$.each(data, function(key, value)
						{
							if(value['site_id_tenant'] == $("#txtsiteidtenant").val() && value['towertype_id'] == $("#txttowertypeid").val() )
							{
								infoStatus('Choose ' + $("#txtsiteidtenant").val() + ' for above Tower Type is NOT ALLOWED' );
								$('#txtsiteidtenant').focus();
								return false;
							}
						});
					}
				});
			}	
		}
						
	});
  
  
   /** SITE ID API
	*  dropdwon list version (obsolete)
	
	$(".txtsiteidapi_select").focus(function()
	{
		$.ajax({
			type: 'post',
			async: false,
			url: basename_url + 'getSiteList/',
			data: {'tower_type_id': $("#txttowertypeid").val()},
			success: function(ret)
			{
				$('.txtsiteidapi_select').html('');
				$('.txtsiteidapi_select').append($('<option>').text('--Select a Site Id API--').attr('value', ''));
				var data = JSON.parse(ret);
				if(Array.isArray(data))
				{
					$.each(data, function(key, value)
					{
						$('.txtsiteidapi_select').append($('<option>').text(value['site_id_api'] + ' - ' + value['site_name_api'] ).attr('value', value['site_id_api']));
						
					});
				}
			}
		});
	});
  	*/	
	
    	
   	$("#txtsitenametenant").blur(function()
	{
		if(!$("#txtsitenametenant").val())
		{
			infoStatus('Site Name Tenant empty is NOT ALLOWED');
			$("#txtsitenametenant").focus();
			return false;
		}
	});
	
  
  $(".txtsitetypeid_select").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getSiteTypeList/',
          success: function(ret)
          {
				$('.txtsitetypeid_select').html('');
				$('.txtsitetypeid_select').append($('<option>').text('--Select an Site Type--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('.txtsitetypeid_select').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });  

  $(".txtsitetypeid_select").change(function()
  {
	$('#txtsitetypeid').val($(".txtsitetypeid_select").val())
  });
  
  
  $(".txttowerheightid_select").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          //url: basename_url + 'getTowerHeightList/'+$(".txtsitetypeid_select").val(),
		  url: basename_url + 'getTowerHeightList/',
          data: {'towertype_id': $("#txttowertypeid").val(), 'sitetype_id': $("#txtsitetypeid_select").val() },
		  
          success: function(ret)
          {
				$('.txttowerheightid_select').html('');
				$('.txttowerheightid_select').append($('<option>').text('--Select an Site Type--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('.txttowerheightid_select').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  }); 
  
  $(".txttowerheightid_select").change(function()
  {
	$('#txttowerheightid').val($(".txttowerheightid_select").val())
  });


  $(".txtareaid_select").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getAreaList/',
          success: function(ret)
          {
				$('.txtareaid_select').html('');
				$('.txtareaid_select').append($('<option>').text('--Select an Area--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('.txtareaid_select').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
  
  $(".txtareaid_select").change(function()
  {
	$('#txtareaid').val($(".txtareaid_select").val())
  });
  

  $(".txtregionid_select").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getRegionList/'+$(".txtareaid_select").val(),
          success: function(ret)
          {
				$('.txtregionid_select').html('');
				$('.txtregionid_select').append($('<option>').text('--Select a Region--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('.txtregionid_select').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $(".txtregionid_select").change(function()
  {
	$('#txtregionid').val($(".txtregionid_select").val())
  });
  
  
  $(".txtprovinceid_select").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getProvinceList/'+$(".txtregionid_select").val(),
          success: function(ret)
          {
				$('.txtprovinceid_select').html('');
				$('.txtprovinceid_select').append($('<option>').text('--Select a Province--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('.txtprovinceid_select').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  }); 
  
  $(".txtprovinceid_select").change(function()
  {
	$('#txtprovinceid').val($(".txtprovinceid_select").val())
  });
  
  
  $(".txtcityid_select").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getCityList/'+$(".txtprovinceid_select").val(),
          success: function(ret)
          {
				$('.txtcityid_select').html('');
				$('.txtcityid_select').append($('<option>').text('--Select a City--').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('.txtcityid_select').append($('<option>').text(value['txt']).attr('value', value['row_id']));
        		  });
          }
    	});
  });

  $(".txtcityid_select").change(function()
  {
	$('#txtcityid').val($(".txtcityid_select").val())
  });  
  
   	$("#txtrentperiod").blur(function()
	{
		if(!$("#txtrentperiod").val() || $("#txtrentperiod").val() == 0)
		{
			infoStatus('Rent Period must be greather than 0');
			$("#txtrentperiod").focus();
			return false;
		}
	});
	
   	$("#txtrentprice").number(true, 2);

	
  $("#txtstatusid").focus(function()
  {
    	$.ajax({
          type: 'post',
          async: false,
          url: basename_url + 'getStatusList/',
          success: function(ret)
          {
				$('#txtstatusid').html('');
				//$('#txtstatusid').append($('<option>').text('View All Status').attr('value', ''));
        		  var data = JSON.parse(ret);
        		  $.each(data, function(key, value)
              {
        				    $('#txtstatusid').append($('<option>').text(value['name']).attr('value', value['row_id']));
        		  });
          }
    	});
  });
    
}

function generateModalView(row_id) {
  $.ajax({
      type: 'post',
      async: false,
      url: basename_url + 'view',
      data: {'txtrowid': row_id},
      success: function(ret) {
          var data = JSON.parse(ret); 
          $('#btnSave').hide();
          $('#tableModal').html(data['table']);
          $('#txtproductid').select2();
      }
  });
}

function doSave(){

	var formData = new FormData($('.formInput')[0]);

	var txtsalesorderno = $('#txtsalesorderno').val(); 
	var txtproductid = $('#txttowertypeid').val(); 	
	
	var txtproductid = $('#txtproductid').val(); 	
	var txtsiteidtenant = $('#txtsiteidtenant').val();
		
	var txtsitenametenant = $('#txtsitenametenant').val();	
	var txtsiteidapi = $('#txtsiteidapi').val();
	
	var txtsitenameapi = $('#txtsitenameapi').val();	
	var txtsitetypeid = $('#txtsitetypeid').val();	
	var txttowerheightid = $('#txttowerheightid').val();	
	var txtlatitude = $('#txtlatitude').val();
	var txtlongitude = $('#txtlongitude').val();	
	var txtareaid = $('#txtareaid').val();	
	var txtregionid = $('#txtregionid').val();	
	var txtprovinceid = $('#txtprovinceid').val();	
	var txtcityid = $('#txtcityid').val();
	
	var txtbaks = new Date(parseInt($('#txtbaks').val().substring(7)), parseInt($('#txtbaks').val().substring(3,6)) -1, parseInt($('#txtbaks').val().substring(0,2)));	
	var txtrfi = new Date(parseInt($('#txtrfi').val().substring(7)), parseInt($('#txtrfi').val().substring(3,6)) - 1, parseInt($('#txtrfi').val().substring(0,2)));	
	var txtbaps = new Date(parseInt($('#txtbaps').val().substring(7)), parseInt($('#txtbaps').val().substring(3,6)) - 1, parseInt($('#txtbaps').val().substring(0,2)));
	
	var txtrentperiod = $('#txtrentperiod').val(); 	
	var txtremarks = $('#txtremarks').val();
		
	if(txtsalesorderno == '') {infoStatus('Sales Order No. must be filled', 0); exit();}
	if(txtproductid == '') {infoStatus('Product must be filled', 0); exit();}
	if(txttowertypeid == '') {infoStatus('Tower Type must be filled', 0); exit();}
	if(txtsiteidtenant == '') {infoStatus('Site Id Tenant must be filled' , 0); exit();}	
	if(txtsitenametenant == '') {infoStatus('Site Name Tenant must be filled', 0); exit();}
	if(txtsiteidapi == '') {infoStatus('Site Id API must be filled', 0); exit();}
	if(txtsitenameapi == '') {infoStatus('Site Name API must be filled', 0); exit();}
	if(txtsitetypeid == '') {infoStatus('Site Type must be filled', 0); exit();}
	if(txttowerheightid == '') {infoStatus('Tower Height  must be filled', 0); exit();}
	if(txtlatitude == '') {infoStatus('Latitude must be filled', 0); exit();}
	if(txtlongitude == '') {infoStatus('Longitude must be filled', 0); exit();}

	if($('#txtrfi').val() == '') {infoStatus('RFI must be filled', 0); exit();}
	if($('#txtbaks').val() == '') {infoStatus('BAKS must be filled', 0); exit();}
	if($('#txtbaps').val() == '') {infoStatus('BAPS must be filled', 0); exit();}
	
	if(txtbaks < txtrfi) {infoStatus('RFI later than BAKS not allowed', 0); exit();}
	if(txtbaps < txtbaks) {infoStatus('BAPS later than RFI not allowed', 0); exit();}
		
  $("#btnSave").html("Loading...");

  $.ajax({
    type: 'post',
	async: false,
    url: basename_url + 'save',
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(ret) {
      var data = JSON.parse(ret); 
      $('#modalForm').modal('hide');
      $("#btnSave").html("SAVE");
      //reloadDatatable();
	  reloadPage();
      infoStatus(data['msg'], data['type']);
    }
  });
  
}

function doDelete(row_id) {
  bootbox.confirm({
    message: "<span class='alert-txt'><i class='fa fa-question-circle'></i>&nbsp;&nbsp;  Are you sure?<span>",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
      if(result == true) {
        $.ajax({
            type: 'post',
            async: false,
            url: basename_url + 'delete',
            data: {'txtrowid': row_id},
            success: function(ret) {
              var data = JSON.parse(ret); 
              reloadDatatable();
			  reloadPage();
              infoStatus(data['msg'], data['type']);
            }
        });
      }
    }
  });
}

function genSiteNameApi(){
	var nameTenant = $("#txtsitenametenant").val();
	$("#txtsitenameapi").val("API_" + nameTenant);

}
