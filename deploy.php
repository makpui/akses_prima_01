<?php
/* (c) Anton Medvedev <anton@medv.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Deployer;

require 'recipe/codeigniter.php';

set('shared_files', ['application/config/database.php', 'assets/backend/controller/config.js']);
set('shared_dirs', ['application/cache', 'application/logs']);
set('writable_dirs', ['application/cache', 'application/logs']);

set('repository', 'git@bitbucket.org:makpui/akses_prima_01.git');

set('git_tty', true);
// set('ssh_multiplexing', false);
host('production')
    ->hostname('43.231.128.47')
    ->port(12320)
    ->user('deploy')
    ->set('branch', 'master')
    ->set('deploy_path', '/home/deploy/php/akses_prima');
